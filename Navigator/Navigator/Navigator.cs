﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace Navigator
{
    /// <summary>
    /// Descriptions about possible return value error
    /// </summary>
    public enum ErrorDescriptionEnum
    {
        None = 0,
        TimeOutElapsed = 9,
        UrlNotValid = 10,
        WebSiteError = 11,
        FieldOrAttributeNotFound = 12
    }

    public enum DisplayModeEnum
    {
        Normal = 0,
        Minimized = 1,
        Maximized = 2,
        Hidden = -1
    }

    internal static class Environ
    {
        //Valori di default
        internal static ErrorDescriptionEnum _ErrorRaised = ErrorDescriptionEnum.None;

        public static int TimeOut { get; set; }
        public static ErrorDescriptionEnum ErrorDescription { get { return _ErrorRaised; } }
        public static Uri CurrentUrl { get; set; }
        public static bool DisableFileDownloadRequest { get; set; }
        public static DisplayModeEnum DisplayMode { get; set; }
        public static bool AllowExceptionOnTimeOut = false;
        public static int TimeWaitForRedirection = 10;   //Millisecondi
        public static bool ClosePopupWindows = false;
    }

    /// <summary>
    /// Initialize class Navigator 
    /// </summary>
    public class Navigator : IDisposable
    {
        protected const string XMLFieldsVersion = "0.1";  //Versione dei campi Field

        protected bool bOpenedTag = false;
        protected bool OpenedTag
        {
            set { bOpenedTag = value; }
            get { return bOpenedTag; }
        }

        XmlDocument FileScript;
        XmlNode XmlTag;
        XmlNode XmlRootElement;

        Form1 FormCrisis;

        public int Timeout { get { return Environ.TimeOut; } set { Environ.TimeOut = value; } }

        /// <summary>
        /// Force close Popup Window dialog
        /// </summary>
        public bool ForceClosePopupWindows { get { return Environ.ClosePopupWindows; } set { Environ.ClosePopupWindows = value; } }
        public ErrorDescriptionEnum ErrorRaised { get { return Environ.ErrorDescription; } }
        public Uri CurrentURL
        {
            get
            {
                Uri RetVal = null;
                try
                {
                    if (Environ.CurrentUrl != null)
                    {
                        RetVal = Environ.CurrentUrl;
                    }
                }
                catch (Exception)
                { }
                return RetVal;
            }
        }

        /// <summary>
        /// Get or Set how many seconds wait for a redirection if occurs before response
        /// </summary>
        public int TimeWaitForRedirection
        {
            get { return Environ.TimeWaitForRedirection; }
            set { Environ.TimeWaitForRedirection = value; }
        }


        //public Crisis(string ProxyUrl, int ProxyPort)
        //{
        //    //Non funziona la modifica del registro
        //    FormCrisis = new Crif.CrifLab.Web.CrisisWeb.Form1(ProxyUrl, ProxyPort);

        //    FormCrisis.TimeOut = 100;

        //    FormCrisis.Show();

        //}
        /// <summary>
        /// Crisis Initialize
        /// </summary>
        public Navigator()
        {
            FormCrisis = new Form1();

            Environ.TimeOut = 180;
            Environ.DisableFileDownloadRequest = true;

            //if (CrisisEnviron.DisplayMode != DisplayModeEnum.Hidden)
            //{
            //    FormCrisis.WindowState = (FormWindowState)CrisisEnviron.DisplayMode;
            //    FormCrisis.Show();
            //}
            //else
            //{                
            //    FormCrisis.Hide();
            //}

            //DisableCanonicalizeAsFilePathRecognize();           
        }

        /// <summary>
        /// Close connection with CrisisWeb
        /// </summary>
        public void Close()
        {
            if (FormCrisis != null)
            {
                FormCrisis.FormClosing_Invoked = true;

                FormCrisis.Close();
                Thread.Sleep(100);
                Application.DoEvents();
                FormCrisis.Dispose();
                FormCrisis = null;
            }
        }

        /// <summary>
        /// Dispose resources with CrisisWeb
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose resources with CrisisWeb
        /// </summary>
        protected virtual void Dispose(bool disposable)
        {
            Close();
            //GC.SuppressFinalize(this);        
        }


        /*For future implementations*/
        //protected void DisableCanonicalizeAsFilePathRecognize()
        //{
        //    System.Reflection.MethodInfo getSyntax = typeof(UriParser).GetMethod("GetSyntax", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
        //    System.Reflection.FieldInfo flagsField = typeof(UriParser).GetField("m_Flags", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

        //    if (getSyntax != null && flagsField != null)
        //    {
        //        foreach (string scheme in new[] { "http", "https" })
        //        {
        //            UriParser parser = (UriParser)getSyntax.Invoke(null, new object[] { scheme });
        //            if (parser != null)
        //            {
        //                int flagsValue = (int)flagsField.GetValue(parser);
        //                // Clear the CanonicalizeAsFilePath attribute
        //                if ((flagsValue & 0x1000000) != 0)
        //                    flagsField.SetValue(parser, flagsValue & ~0x1000000);
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Control the Form Windows State
        /// </summary>
        public DisplayModeEnum WindowsState
        {
            get
            {
                return Environ.DisplayMode;
                //if (FormCrisis != null)                    
                //    return (int)FormCrisis.WindowState;
                //else
                //    return FormWindowState.Normal;
            }
            set
            {
                if (value != Environ.DisplayMode)
                {
                    if (FormCrisis != null)
                    {
                        if (value != DisplayModeEnum.Hidden)
                        {
                            FormCrisis.WindowState = (FormWindowState)value;
                            FormCrisis.Show();
                        }
                        else
                            FormCrisis.Hide();
                    }
                    Environ.DisplayMode = value;
                }
            }
        }

        /// <summary>
        /// Start a new Script and dialog with the web
        /// </summary>
        public void NewScript()
        {
            if (FileScript != null)
            {
                FileScript.RemoveAll();
                FileScript = null;
            }

            FileScript = new XmlDocument();

            XmlDeclaration xDecl = FileScript.CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.WebName, null);
            FileScript.AppendChild(xDecl);

            XmlRootElement = FileScript.CreateElement("", "CebisWeb", "");
            XmlAttribute XmlAttribute = FileScript.CreateAttribute("Version");
            XmlAttribute.Value = XMLFieldsVersion;
            XmlRootElement.Attributes.Append(XmlAttribute);
        }

        /// <summary>
        /// Script for URL and GET Navigation
        /// </summary>
        /// <param name="Url"></param>
        public void NewGET(string Url)
        {
            try
            {
                NewGET(new Uri(Url));
            }
            catch (UriFormatException)
            {
                OpenedTag = false;
            }
        }
        /// <summary>
        /// Script for URL and GET Navigation
        /// </summary>
        /// <param name="Url"></param>
        public void NewGET(Uri Url)
        {
            XmlTag = FileScript.CreateElement("", "Get", "");
            XmlAttribute XmlAttribute = FileScript.CreateAttribute("url");
            XmlAttribute.Value = Url.AbsoluteUri;
            XmlTag.Attributes.Append(XmlAttribute);

            OpenedTag = true;
        }
        /// <summary>
        /// Script for POST Code and Form navigation
        /// </summary>
        /// <param name="Name"></param>
        public void NewFORM(string Name)
        {
            XmlTag = FileScript.CreateElement("", "Form", "");
            XmlAttribute XmlAttribute = FileScript.CreateAttribute("name");
            XmlAttribute.Value = Name;
            XmlTag.Attributes.Append(XmlAttribute);

            OpenedTag = true;
        }
        /// <summary>
        /// Script for POST Code and Form navigation
        /// </summary>
        /// <param name="Name"></param>
        public void NewFORM(string Name, string Frame)
        {
            XmlTag = FileScript.CreateElement("", "Form", "");
            XmlAttribute XmlAttribute = FileScript.CreateAttribute("name");
            XmlAttribute.Value = Name;
            XmlTag.Attributes.Append(XmlAttribute);
            XmlAttribute = FileScript.CreateAttribute("frame");
            XmlAttribute.Value = Frame;
            XmlTag.Attributes.Append(XmlAttribute);

            OpenedTag = true;
        }
        /// <summary>
        /// Script for expected downloading Data in a Form navigation
        /// </summary>
        /// <param name="ActionUrl"></param>
        public void NewFORMDownloadData(string ActionUrl)
        {
            try
            {
                NewFORMDownloadData(new Uri(ActionUrl));
            }
            catch (UriFormatException)
            {
                OpenedTag = false;
            }
        }
        /// <summary>
        /// Script for expected downloading Data in a Form navigation
        /// </summary>
        /// <param name="ActionUrl"></param>
        public void NewFORMDownloadData(Uri ActionUrl)
        {
            XmlTag = FileScript.CreateElement("", "FormDownloadData", "");
            XmlAttribute XmlAttribute = FileScript.CreateAttribute("actionURL");
            XmlAttribute.Value = ActionUrl.AbsoluteUri;
            XmlTag.Attributes.Append(XmlAttribute);

            OpenedTag = true;
        }
        /// <summary>
        /// Create a memory dictionary with values
        /// </summary>
        /// <param name="Name">Name of the resource</param>
        /// <param name="Value">Value of the resource</param>
        /// <param name="Type">Type of the resource</param>
        public void NewDICTIONARY(string Name, string Value, string Type)
        {
            XmlAttribute XmlAttribute;

            XmlTag = FileScript.CreateElement("", "Dictionary", "");
            XmlAttribute = FileScript.CreateAttribute("name");
            XmlAttribute.Value = Value;
            XmlTag.Attributes.Append(XmlAttribute);

            XmlAttribute = FileScript.CreateAttribute("value");
            XmlAttribute.Value = Value;
            XmlTag.Attributes.Append(XmlAttribute);

            XmlAttribute = FileScript.CreateAttribute("type");
            XmlAttribute.Value = Type;
            XmlTag.Attributes.Append(XmlAttribute);

            OpenedTag = true;
        }
        /// <summary>
        /// Script for POST Code and Form navigation
        /// </summary>
        /// <param name="Tag">Accept: Field, Select, SelectText, Submit, Radio, CheckBox</param>
        /// <param name="Value">Value to search and select</param>
        public void NewCommand(string Tag, string Value)
        {
            if (!string.IsNullOrEmpty(Tag) && OpenedTag == true)
            {
                XmlAttribute XmlAttribute;
                XmlElement XmlElement = FileScript.CreateElement("", Tag, "");

                XmlAttribute = FileScript.CreateAttribute("value");
                XmlAttribute.Value = Value;
                XmlElement.Attributes.Append(XmlAttribute);

                XmlTag.AppendChild(XmlElement);
            }
        }
        /// <summary>
        /// Script for POST Code and Form navigation
        /// </summary>
        /// <param name="Tag">Accept: Field, Select, SelectText, Submit, Radio, CheckBox</param>
        /// <param name="Name">Name or Id of the control</param>
        /// <param name="Value">Value to search and select</param>
        public void NewCommand(string Tag, string Name, string Value)
        {
            if (!string.IsNullOrEmpty(Tag) && !string.IsNullOrEmpty(Name) && OpenedTag == true)
            {
                XmlAttribute XmlAttribute;
                XmlElement XmlElement = FileScript.CreateElement("", Tag, "");
                XmlAttribute = FileScript.CreateAttribute("name");
                XmlAttribute.Value = Name;
                XmlElement.Attributes.Append(XmlAttribute);

                XmlAttribute = FileScript.CreateAttribute("value");
                XmlAttribute.Value = Value;
                XmlElement.Attributes.Append(XmlAttribute);

                XmlTag.AppendChild(XmlElement);
            }
        }
        /// <summary>
        /// Clean web cache only without cookies
        /// </summary>
        public void RefreshPage()
        {
            NewScript();
            XmlTag = FileScript.CreateElement("", "Refresh", "");

            OpenedTag = true;
            Execute();
        }
        private void CloseTag()
        {
            //Appendo l'xml al documento
            if (OpenedTag == true)
            {
                XmlRootElement.AppendChild(XmlTag);

                FileScript.AppendChild(XmlRootElement);
                OpenedTag = false;
            }

            ////Eseguo l'aggiornamento dell refresh della maschera                        
            //if ( (int)CrisisEnviron.DisplayMode != (int)FormCrisis.WindowState)
            //{
            //    if (CrisisEnviron.DisplayMode != DisplayModeEnum.Hidden)
            //    {
            //        FormCrisis.WindowState = (FormWindowState)CrisisEnviron.DisplayMode;

            //        FormCrisis.Show();
            //    }
            //    else
            //    {
            //        FormCrisis.Hide();
            //    }
            //}
        }



        public string GetValue(string TagName, string Type, string Name, string GetAttributeName)
        {
            string Result = "";
            XmlDocument XmlFile = null;

            NewScript();
            XmlTag = FileScript.CreateElement("", "extractvalue", "");
            XmlAttribute XmlAttribute;

            XmlAttribute = FileScript.CreateAttribute("tagname");
            XmlAttribute.Value = TagName;
            XmlTag.Attributes.Append(XmlAttribute);
            XmlAttribute = FileScript.CreateAttribute("name");
            XmlAttribute.Value = Name;
            XmlTag.Attributes.Append(XmlAttribute);
            XmlAttribute = FileScript.CreateAttribute("type");
            XmlAttribute.Value = Type;
            XmlTag.Attributes.Append(XmlAttribute);
            XmlAttribute = FileScript.CreateAttribute("attributename");
            XmlAttribute.Value = GetAttributeName;
            XmlTag.Attributes.Append(XmlAttribute);

            OpenedTag = true;
            if (ExecuteWithResponse(out XmlFile) == true)
            {
                //Converto l'XML in ArrayList
                Result = XmlFile.SelectSingleNode("Value").InnerText;
            }
            else
            {
                return "";
            }

            return Result;

        }


        /// <summary>
        /// Extract the innerHtml of the page
        /// </summary>
        /// <returns></returns>
        public string SaveHtml()
        {
            NewScript();
            XmlTag = FileScript.CreateElement("", "SaveHTML", "");

            OpenedTag = true;

            XmlDocument xmlResponse;
            if (ExecuteWithResponse(out xmlResponse))
            {
                XmlCDataSection CData = (XmlCDataSection)xmlResponse.SelectSingleNode("/CebisWeb/Result").FirstChild;

                return CData.Data;
            }
            else
                return "";
        }
        /// <summary>
        /// Extract the innerHtml of the frame page
        /// </summary>
        /// <param name="FrameNameOrUrl">Name or Url attribute of the page to extract</param>
        /// <returns></returns>
        public string SaveHtml(string FrameName)
        {
            NewScript();
            XmlTag = FileScript.CreateElement("", "SaveHTML", "");
            XmlAttribute XmlAttribute;
            XmlAttribute = FileScript.CreateAttribute("name");
            XmlAttribute.Value = FrameName;
            XmlTag.Attributes.Append(XmlAttribute);

            OpenedTag = true;

            XmlDocument xmlResponse;
            if (ExecuteWithResponse(out xmlResponse))
            {
                XmlCDataSection CData = (XmlCDataSection)xmlResponse.SelectSingleNode("/CebisWeb/Result").FirstChild;

                return CData.Data;
            }
            else
                return "";
        }
        /// <summary>
        /// Navigate and Download result as a string
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        public string NavigateAndDownloadString(string Url)
        {
            string Result = string.Empty;
            try
            {
                Result = NavigateAndDownloadString(new Uri(Url), true);
            }
            catch (UriFormatException)
            {
                Result = string.Empty;
            }
            return Result;
        }
        /// <summary>
        /// Navigate and Download result as a string
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        public string NavigateAndDownloadString(Uri Url)
        {
            return NavigateAndDownloadString(Url, true);
        }
        /// <summary>
        /// Navigate and Download result as a string
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="AllowRedirection">if allow redirections to other sites</param>
        /// <returns></returns>
        public string NavigateAndDownloadString(string Url, bool AllowRedirection)
        {
            string Result = string.Empty;
            try
            {
                Result = NavigateAndDownloadString(new Uri(Url), AllowRedirection);
            }
            catch (UriFormatException)
            {
                Result = string.Empty;
            }
            return Result;
        }
        /// <summary>
        /// Navigate and Download result as a string
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="AllowRedirection">if allow redirections to other sites</param>
        /// <returns></returns>
        public string NavigateAndDownloadString(Uri Url, bool AllowRedirection)
        {
            NewScript();
            XmlTag = FileScript.CreateElement("", "NavigateAndDownload", "");
            XmlAttribute XmlAttribute;
            XmlAttribute = FileScript.CreateAttribute("name");
            XmlAttribute.Value = Url.AbsoluteUri;
            XmlTag.Attributes.Append(XmlAttribute);

            OpenedTag = true;

            XmlDocument xmlResponse;
            if (ExecuteWithResponse(out xmlResponse))
            {
                //Verifico se esiste un redirect                
                string ReturnData = string.Empty;

                if (AllowRedirection && xmlResponse.SelectSingleNode("/CebisWeb/Result").Attributes["Redirection"].Value == "1")
                {
                    //Eseguo una pulizia della CACHE DI SISTEMA
                    NewScript();
                    NewGET("about:blank");
                    Execute();
                    RefreshPage();


                    NewScript();
                    NewGET(Url);
                    if (Execute())
                    {
                        ReturnData = SaveHtml();
                    }
                }
                else
                {
                    XmlCDataSection CData = (XmlCDataSection)xmlResponse.SelectSingleNode("/CebisWeb/Result").FirstChild;
                    ReturnData = CData.Data;
                }
                return ReturnData;
            }
            else
                return "";
        }

        /// <summary>
        /// Execute and Close a GET, POST or Form navigation
        /// </summary>
        /// <returns></returns>
        public bool Execute()
        {
            if (Environ.DisplayMode != DisplayModeEnum.Hidden)
            {
                if (Environ.DisplayMode != (DisplayModeEnum)FormCrisis.WindowState)
                {
                    FormCrisis.WindowState = (FormWindowState)Environ.DisplayMode;
                    FormCrisis.Show();
                }
            }
            else
            {
                if (FormCrisis.Visible == true)
                    FormCrisis.Hide();
            }


            XmlDocument xmlResponse;
            bool result = false;
            xmlResponse = null;

            if (OpenedTag == true) { CloseTag(); }

            if (FileScript != null)
            {
                result = FormCrisis.Execute(FileScript, out xmlResponse);
            }
            else
            {
                return false;
            }

            FileScript = null;

            if (result == false && Environ.AllowExceptionOnTimeOut) throw new Exception("Pagina Bloccata");

            return result;
        }
        /// <summary>
        /// Execute and Close a GET, POST or Form navigation, when some data is expected
        /// </summary>
        /// <param name="xmlResponse"></param>
        /// <returns></returns>
        public bool ExecuteWithResponse(out XmlDocument xmlResponse)
        {
            if (Environ.DisplayMode != DisplayModeEnum.Hidden)
            {
                if (Environ.DisplayMode != (DisplayModeEnum)FormCrisis.WindowState)
                {
                    FormCrisis.WindowState = (FormWindowState)Environ.DisplayMode;
                    FormCrisis.Show();
                }
            }
            else
            {
                if (FormCrisis.Visible == true)
                    FormCrisis.Hide();
            }

            bool result = false;
            xmlResponse = null;

            if (OpenedTag == true) { CloseTag(); }

            if (FileScript != null)
            {
                result = FormCrisis.Execute(FileScript, out xmlResponse);
            }
            else
            {
                return false;
            }

            FileScript = null;

            if (result == false && Environ.AllowExceptionOnTimeOut) throw new Exception("Pagina Bloccata");

            return result;
        }

        /// <summary>
        /// Find a string in Html
        /// </summary>
        /// <param name="StringToFind"></param>
        /// <returns></returns>
        public bool FindString(string StringToFind)
        {
            NewScript();
            XmlAttribute XmlAttribute;

            XmlTag = FileScript.CreateElement("", "FindString", "");
            XmlAttribute = FileScript.CreateAttribute("string");
            XmlAttribute.Value = StringToFind;
            XmlTag.Attributes.Append(XmlAttribute);

            OpenedTag = true;
            try
            {

                XmlDocument xmlResponse;
                if (ExecuteWithResponse(out xmlResponse))
                {
                    if (xmlResponse.SelectSingleNode("/CebisWeb/Result").Attributes["returnvalue"].Value.ToUpper() == "YES")
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Get the First occurrance in html
        /// </summary>
        /// <param name="TagName">Tag name for filter</param>
        /// <param name="AttributeFilter">Attribute name for filter</param>
        /// <param name="AttributeValueFilter">Attribute value for filter</param>        
        /// <param name="htmlNode">List of all results node</param>
        /// <returns>if no errors occours</returns>
        public bool GetElements(string TagName, string AttributeFilter, string AttributeValueFilter, out List<HtmlAgilityPack.HtmlNode> htmlNodeList)
        {
            bool RetVal = false;
            htmlNodeList = null;

            if (!string.IsNullOrEmpty(TagName) && !string.IsNullOrEmpty(AttributeFilter) && !string.IsNullOrEmpty(AttributeValueFilter))
            {
                NewScript();
                XmlAttribute XmlAttribute;

                XmlTag = FileScript.CreateElement("", "Extractelement", "");

                XmlAttribute = FileScript.CreateAttribute("type");
                XmlAttribute.Value = TagName;
                XmlTag.Attributes.Append(XmlAttribute);

                XmlAttribute = FileScript.CreateAttribute("name");
                XmlAttribute.Value = AttributeFilter;
                XmlTag.Attributes.Append(XmlAttribute);

                XmlAttribute = FileScript.CreateAttribute("value");
                XmlAttribute.Value = AttributeValueFilter;
                XmlTag.Attributes.Append(XmlAttribute);

                XmlAttribute = FileScript.CreateAttribute("all");
                XmlAttribute.Value = "True";
                XmlTag.Attributes.Append(XmlAttribute);


                try
                {
                    OpenedTag = true;
                    XmlDocument xmlResponse;
                    if (ExecuteWithResponse(out xmlResponse))
                    {
                        if (xmlResponse != null)
                        {
                            if (xmlResponse.SelectNodes("//item").Count > 0)
                            {
                                foreach (XmlElement _item in xmlResponse.SelectNodes("//item"))
                                {
                                    XmlCDataSection xmlCData = (XmlCDataSection)_item.FirstChild;

                                    HtmlAgilityPack.HtmlNode _htmlResult = HtmlAgilityPack.HtmlNode.CreateNode(xmlCData.Data.Trim());
                                    if (htmlNodeList == null)
                                        htmlNodeList = new List<HtmlAgilityPack.HtmlNode>();

                                    htmlNodeList.Add(_htmlResult);
                                }
                            }
                            //XmlCDataSection xmlCData = (XmlCDataSection)xmlResponse.SelectSingleNode("CebisWeb/Result").FirstChild;                            

                            //HtmlAgilityPack.HtmlNode _htmlResult = HtmlAgilityPack.HtmlNode.CreateNode(xmlCData.Data.Trim());

                            //if (_htmlResult.SelectNodes("root").Count > 0)
                            //    htmlNode = _htmlResult.SelectNodes("root");                               
                        }
                        RetVal = true;
                    }
                    else
                        RetVal = false;
                }
                catch (Exception)
                {
                    RetVal = false;
                }
            }

            return RetVal;
        }
        /// <summary>
        /// Get the all occurrance in html
        /// </summary>
        /// <param name="TagName">Tag name for filter</param>
        /// <param name="InnerTextValue">InnerText contains for filter</param>       
        /// <param name="htmlNode">List of all results node</param>
        /// <returns>if no errors occours</returns>
        public bool GetElements(string TagName, string InnerTextValue, out List<HtmlAgilityPack.HtmlNode> htmlNodeList)
        {
            bool RetVal = false;
            htmlNodeList = null;

            if (!string.IsNullOrEmpty(TagName) && !string.IsNullOrEmpty(InnerTextValue))
            {
                NewScript();
                XmlAttribute XmlAttribute;

                XmlTag = FileScript.CreateElement("", "Extractelement", "");

                XmlAttribute = FileScript.CreateAttribute("type");
                XmlAttribute.Value = TagName;
                XmlTag.Attributes.Append(XmlAttribute);

                XmlAttribute = FileScript.CreateAttribute("value");
                XmlAttribute.Value = InnerTextValue;
                XmlTag.Attributes.Append(XmlAttribute);

                XmlAttribute = FileScript.CreateAttribute("all");
                XmlAttribute.Value = "1";
                XmlTag.Attributes.Append(XmlAttribute);

                try
                {
                    OpenedTag = true;
                    XmlDocument xmlResponse;
                    if (ExecuteWithResponse(out xmlResponse))
                    {
                        if (xmlResponse != null)
                        {
                            if (xmlResponse.SelectNodes("//item").Count > 0)
                            {
                                foreach (XmlElement _item in xmlResponse.SelectNodes("//item"))
                                {
                                    XmlCDataSection xmlCData = (XmlCDataSection)_item.FirstChild;

                                    HtmlAgilityPack.HtmlNode _htmlResult = HtmlAgilityPack.HtmlNode.CreateNode(xmlCData.Data.Trim());
                                    if (htmlNodeList == null)
                                        htmlNodeList = new List<HtmlAgilityPack.HtmlNode>();

                                    htmlNodeList.Add(_htmlResult);
                                }
                            }
                        }
                        RetVal = true;
                    }
                    else
                        RetVal = false;
                }
                catch (Exception)
                {
                    RetVal = false;
                }
            }

            return RetVal;
        }
        /// <summary>
        /// Get the First occurrance in html
        /// </summary>
        /// <param name="TagName">Tag name for filter</param>
        /// <param name="AttributeFilter">Attribute name for filter</param>
        /// <param name="AttributeValueFilter">Attribute value for filter</param>
        /// <param name="htmlNode">Result node</param>
        /// <returns>if no errors occours</returns>
        public bool GetFirstElement(string TagName, string AttributeFilter, string AttributeValueFilter, out string htmlNode)
        {
            HtmlAgilityPack.HtmlNode _htmlNode;
            bool RetValue = false;
            RetValue = GetFirstElement(TagName, AttributeFilter, AttributeValueFilter, out _htmlNode);
            htmlNode = _htmlNode.InnerHtml;

            return RetValue;
        }
        /// <summary>
        /// Get the First occurrance in html
        /// </summary>
        /// <param name="TagName">Tag name for filter</param>
        /// <param name="AttributeFilter">Attribute name for filter</param>
        /// <param name="AttributeValueFilter">Attribute value for filter</param>
        /// <param name="htmlNode">Result node</param>
        /// <returns>if no errors occours</returns>
        public bool GetFirstElement(string TagName, string AttributeFilter, string AttributeValueFilter, out HtmlAgilityPack.HtmlNode htmlNode)
        {
            bool RetVal = false;
            htmlNode = null;

            if (!string.IsNullOrEmpty(TagName) && !string.IsNullOrEmpty(AttributeFilter) && !string.IsNullOrEmpty(AttributeValueFilter))
            {
                NewScript();
                XmlAttribute XmlAttribute;

                XmlTag = FileScript.CreateElement("", "Extractelement", "");

                XmlAttribute = FileScript.CreateAttribute("type");
                XmlAttribute.Value = TagName;
                XmlTag.Attributes.Append(XmlAttribute);

                XmlAttribute = FileScript.CreateAttribute("name");
                XmlAttribute.Value = AttributeFilter;
                XmlTag.Attributes.Append(XmlAttribute);

                XmlAttribute = FileScript.CreateAttribute("value");
                XmlAttribute.Value = AttributeValueFilter;
                XmlTag.Attributes.Append(XmlAttribute);


                try
                {
                    OpenedTag = true;
                    XmlDocument xmlResponse;
                    if (ExecuteWithResponse(out xmlResponse))
                    {
                        if (xmlResponse != null)
                        {
                            XmlCDataSection xmlCData = (XmlCDataSection)xmlResponse.SelectSingleNode("CebisWeb/Result").FirstChild;
                            htmlNode = HtmlAgilityPack.HtmlNode.CreateNode(xmlCData.Data.Trim());
                        }
                        RetVal = true;
                    }
                    else
                        RetVal = false;
                }
                catch (Exception)
                {
                    RetVal = false;
                }
            }

            return RetVal;
        }
        /// <summary>
        /// Get the First occurrance in html
        /// </summary>
        /// <param name="TagName">Tag name for filter</param>
        /// <param name="InnerTextValue">InnerText contains for filter</param>       
        /// <param name="htmlNode">Result node</param>
        /// <returns>if no errors occours</returns>
        public bool GetFirstElement(string TagName, string InnerTextValue, out string htmlNode)
        {
            HtmlAgilityPack.HtmlNode _htmlNode;
            bool RetValue = false;
            RetValue = GetFirstElement(TagName, InnerTextValue, out _htmlNode);
            htmlNode = _htmlNode.InnerHtml;

            return RetValue;
        }
        /// <summary>
        /// Get the First occurrance in html
        /// </summary>
        /// <param name="TagName">Tag name for filter</param>
        /// <param name="InnerTextValue">InnerText contains for filter</param>       
        /// <param name="htmlNode">Result node</param>
        /// <returns>if no errors occours</returns>
        public bool GetFirstElement(string TagName, string InnerTextValue, out HtmlAgilityPack.HtmlNode htmlNode)
        {
            bool RetVal = false;
            htmlNode = null;

            if (!string.IsNullOrEmpty(TagName) && !string.IsNullOrEmpty(InnerTextValue))
            {
                NewScript();
                XmlAttribute XmlAttribute;

                XmlTag = FileScript.CreateElement("", "Extractelement", "");

                XmlAttribute = FileScript.CreateAttribute("type");
                XmlAttribute.Value = TagName;
                XmlTag.Attributes.Append(XmlAttribute);

                XmlAttribute = FileScript.CreateAttribute("value");
                XmlAttribute.Value = InnerTextValue;
                XmlTag.Attributes.Append(XmlAttribute);


                try
                {
                    OpenedTag = true;
                    XmlDocument xmlResponse;
                    if (ExecuteWithResponse(out xmlResponse))
                    {
                        if (xmlResponse != null)
                        {
                            XmlCDataSection xmlCData = (XmlCDataSection)xmlResponse.SelectSingleNode("CebisWeb/Result").FirstChild;

                            htmlNode = HtmlAgilityPack.HtmlNode.CreateNode(xmlCData.Data.Trim());
                        }
                        RetVal = true;
                    }
                    else
                        RetVal = false;
                }
                catch (Exception)
                {
                    RetVal = false;
                }
            }

            return RetVal;
        }
        /// <summary>
        /// Get the First occurrance in html
        /// </summary>
        /// <param name="TagName">Tag name for filter</param>
        /// <param name="AttributeFilter">Attribute name for filter</param>
        /// <param name="AttributeValueFilter">Attribute value for filter</param>
        /// <param name="htmlNode">Result node</param>
        /// <returns>if no errors occours</returns>
        public bool ClickFirstElement(string TagName, string AttributeFilter, string AttributeValueFilter)
        {
            bool RetVal = false;

            if (!string.IsNullOrEmpty(TagName) && !string.IsNullOrEmpty(AttributeFilter))
            {
                NewScript();
                XmlAttribute XmlAttribute;

                XmlTag = FileScript.CreateElement("", "Clickelement", "");

                XmlAttribute = FileScript.CreateAttribute("type");
                XmlAttribute.Value = TagName;
                XmlTag.Attributes.Append(XmlAttribute);

                XmlAttribute = FileScript.CreateAttribute("name");
                XmlAttribute.Value = AttributeFilter;
                XmlTag.Attributes.Append(XmlAttribute);

                XmlAttribute = FileScript.CreateAttribute("value");
                XmlAttribute.Value = AttributeValueFilter;
                XmlTag.Attributes.Append(XmlAttribute);


                try
                {
                    OpenedTag = true;
                    XmlDocument xmlResponse;
                    if (Execute())
                    {
                        RetVal = true;
                    }
                    else
                        RetVal = false;
                }
                catch (Exception)
                {
                    RetVal = false;
                }
            }

            return RetVal;
        }
        /// <summary>
        /// Extract Text about all the tables in the page
        /// </summary>
        /// <returns>Table's list</returns>
        public List<string[,]> ExtractTables()
        {
            List<string[,]> TablesArray = new List<string[,]>();

            NewScript();
            XmlTag = FileScript.CreateElement("", "ExtractTables", "");

            OpenedTag = true;
            XmlDocument xmlResponse;
            if (ExecuteWithResponse(out xmlResponse) == true)
            {
                TablesArray = fnReadXmlTables(xmlResponse);
            }
            else
            {
                return null;
            }

            return TablesArray;
        }
        /// <summary>
        /// Extract Html about all the tables in the page
        /// </summary>
        /// <returns>Table's list</returns>
        public List<string[,]> ExtractTablesHTML()
        {
            List<string[,]> TablesArray = new List<string[,]>();

            NewScript();
            XmlTag = FileScript.CreateElement("", "ExtractTablesHTML", "");

            OpenedTag = true;
            XmlDocument xmlResponse;
            if (ExecuteWithResponse(out xmlResponse) == true)
            {
                TablesArray = fnReadXmlTables(xmlResponse);
            }
            else
            {
                return null;
            }

            return TablesArray;
        }

        private static List<string[,]> fnReadXmlTables(XmlDocument xmlResponse)
        {
            List<string[,]> TablesArray = new List<string[,]>();

            XmlDocument XmlFile = new XmlDocument();
            XmlFile = xmlResponse;
            //XmlFile.Load(sPathOutputCrisis + "\\tables.xml");

            //Converto l'XML in ArrayList
            XmlNode XmlTables = XmlFile.SelectSingleNode("Tables");

            int iCurRow = 0; int iCurCol = 0;

            foreach (XmlNode TableElem in XmlTables)
            {
                try
                {
                    string[,] Table = new string[int.Parse(TableElem.Attributes.GetNamedItem("rows").Value), int.Parse(TableElem.Attributes.GetNamedItem("cols").Value)];
                    iCurRow = 0;

                    foreach (XmlNode RowElem in TableElem)
                    {
                        iCurCol = 0;
                        foreach (XmlNode ColElem in RowElem)
                        {
                            Table[iCurRow, iCurCol] = ColElem.InnerText;
                            iCurCol++;
                        }
                        iCurRow++;
                    }
                    TablesArray.Add(Table);
                }
                catch (Exception e)
                { }
            }
            return TablesArray;
        }


        /*
        [DllImport("user32.dll", CharSet=CharSet.Unicode)]        
        protected static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
        protected static IntPtr findMessageBox()
        {
            IntPtr hwndMB = FindWindowEx(IntPtr.Zero, IntPtr.Zero, "#32770", "Microsoft Internet Explorer");

            if (hwndMB == IntPtr.Zero)
            {
                //Secondo tentativo di recupero per un'altra versione di internet explorer (7 o 8)
                hwndMB = FindWindowEx(IntPtr.Zero, IntPtr.Zero, "#32770", "Windows Internet Explorer");
            }

            return hwndMB;
        }
        [DllImport("user32.dll", CharSet=CharSet.Unicode)]
        protected static extern bool PostMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);
        protected static void SendNoMessage(IntPtr hwnd)
        {
            IntPtr button = FindWindowEx(hwnd, IntPtr.Zero, "Button", "&No");

            PostMessage(button, 256, (uint)'N', 0);
            PostMessage(button, 257, (uint)'N', 0);
        }
        */


        /// <summary>
        /// Sleep Web Page for seconds
        /// </summary>
        /// <param name="Seconds">Seconds to wait. Zero need user press button at top of the form</param>
        public void WaitForDebug(int Seconds)
        {
            if (Seconds > 0)
            {

                DateTime Date_Start = DateTime.Now;
                double SecondsNow = 0;
                do
                {
                    Application.DoEvents();

                    SecondsNow = DateTime.Now.Subtract(Date_Start).TotalSeconds;
                    if (SecondsNow >= Seconds)
                        break;
                    //Devo controllare se esiste un messaggio di chiusura pagina molto fastidioso.
                    //Controllo se c'è una fastidiosa messagebox che dice di premere Si o No
                    //Questo è un vestito per il progetto BPer che automatizza sul NO la risposta di chiudere la finestra del browser
                    //IntPtr Messaggio = findMessageBox();
                    //if (Messaggio != IntPtr.Zero)
                    //{
                    //    //MessageBox.Show("Test: Trovato il messaggio di tentativo chiusura pagina");
                    //    //MessageBox.Show("Test: Tentativo di sbloccaggio della procedura in automatico");

                    //    //Invio la pressione del pulsante NO
                    //    SendNoMessage(Messaggio);

                    //    //MessageBox.Show("Test: Il messaggio dovrebbe essere sparito. Alla pressione di OK la procedura dovrebbe continuare automaticamente");
                    //}       

                    //Attesa dei file
                    Thread.Sleep(10);


                } while (SecondsNow <= Seconds);
            }
            else if (Seconds == 0)
            {
                FormCrisis.WebPause();
            }
        }

    }
}
