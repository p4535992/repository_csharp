﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Q326201CS;

namespace Navigator
{
    partial class Form1 : Form
    {
        //private WebBrowser webBrowser1;
        //private Label lblStatus = null;
        protected const string XMLFieldsVersion = "0.1";  //Versione dei campi Field        
        Thread Thread_DisableFileDownload;
        Thread Thread_WaitForExecutions;

        public bool FormClosing_Invoked = false;

        public Form1()
        {
            InitializeComponent();
            //DictionaryRomis.Clear();
        }

        /// <summary>
        /// Imposta il proxy: testato fino a Windows 8
        /// </summary>
        /// <param name="ProxyUrl">Url del proxy</param>
        /// <param name="ProxyPort">Porta del proxy</param>
        //public Form1(string ProxyUrl, int ProxyPort)
        //{
        //    string key = "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings";            

        //    Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(key, true);

        //    List<string> KeyNames = new List<string>(RegKey.GetValueNames());
        //    List<object> KeyValues = new List<object>(KeyNames.Count);
        //    for (int iKey = 0; iKey < KeyNames.Count; iKey++)
        //        KeyValues.Add(RegKey.GetValue(KeyNames[iKey]));
        //    //http://pacsrv.crif.com/proxy.pac

        //    Dictionary<string, object> NewConfig = new Dictionary<string, object>();
        //    NewConfig.Add("ProxyServer", ProxyUrl + ":" + ProxyPort);
        //    NewConfig.Add("ProxyEnable", 1);
        //    NewConfig.Add("AutoConfigURL", null);

        //    foreach (KeyValuePair<string, object> Key in NewConfig)
        //    {                
        //        if (RegKey.GetValue(Key.Key) != null)
        //        {                    
        //            RegKey.DeleteValue(Key.Key);                    
        //        }
        //        if (Key.Value != null)                    
        //        {
        //            //Microsoft.Win32.RegistryKey newkey = RegKey.CreateSubKey(Key.Key);
        //            RegKey.SetValue(Key.Key, Key.Value);
        //        }

        //    } 
        //    RegKey.Flush();

        //    InitializeComponent();

        //    //Ripristino la situazione iniziale
        //    foreach (KeyValuePair<string, object> Key in NewConfig)
        //    {
        //        if (RegKey.GetValue(Key.Key) != null)
        //            RegKey.DeleteValue(Key.Key);
        //        if (KeyNames.Contains(Key.Key) == true)                                    
        //        {
        //            //Microsoft.Win32.RegistryKey newkey = RegKey.CreateSubKey(Key.Key);
        //            int index = KeyNames.IndexOf(Key.Key);
        //            RegKey.SetValue(KeyNames[index], KeyValues[index]);
        //        }                
        //    }

        //}



        #region Variabili Globali

        protected volatile bool Executed;
        /*
        //Old
        protected bool bContinueScripting;
        protected bool Executed
        {
            get { return bContinueScripting; }
            set { bContinueScripting = value; }
        }
         * */

        protected int CountExecuting = 0;

        protected Dictionary<string, string> DictionaryRomis = new Dictionary<string, string>();

        #endregion        

        private static XmlDocument CreaXmlOuput(string Result)
        {
            XmlDocument XmlOutput = new XmlDocument();

            XmlElement XmlElement;
            XmlElement XmlElement2;
            XmlAttribute XmlAttribute;


            XmlDeclaration xDecl = XmlOutput.CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.WebName, null);
            XmlOutput.AppendChild(xDecl);
            //Root
            XmlElement = XmlOutput.CreateElement("", "CebisWeb", "");

            XmlElement2 = XmlOutput.CreateElement("", "Result", "");
            XmlAttribute = XmlOutput.CreateAttribute("returnvalue");
            XmlAttribute.Value = Result;
            XmlElement2.Attributes.Append(XmlAttribute);

            XmlElement.AppendChild(XmlElement2);
            XmlOutput.AppendChild(XmlElement);

            //XmlOutput.Save(FileXml);

            return XmlOutput;
        }


        #region XMLLoad

        protected XmlDocument XmlFile = new XmlDocument();

        public bool LoadFileXml(string UriXml)
        {
            //Caricamento del file XML dei Fields
            bool RetVal = false;
            for (int iTry = 1; iTry <= 3; iTry++)
            {
                try
                {
                    XmlFile.Load(UriXml);
                    RetVal = true;
                    break;
                }
                catch (Exception e)
                {
                }
            }
            if (RetVal == false)
                return RetVal;
            string VersionFields = "";

            XmlNode XMLNode = XmlFile.SelectSingleNode("//CebisWeb");
            VersionFields = XMLNode.Attributes.GetNamedItem("Version").Value;

            //Controllo della Versione            
            if (VersionFields != XMLFieldsVersion)
            {
                XmlFile = null;
                return false;
            }

            return true;
        }

        public bool ExecuteFile(out XmlDocument xmlResponse)
        {
            bool Result = true;

            xmlResponse = null;

            if (XmlFile == null) return false;
            //XmlNode XmlWork = XmlFile.SelectSingleNode("//CebisWeb");

            foreach (XmlElement Nodo in XmlFile.SelectSingleNode("//CebisWeb"))
            {
                switch (Nodo.Name.ToUpper())
                {
                    case "FORM":
                        if (SendRequest(Nodo, "POST", Nodo.Attributes["name"].Value) != 1) Result = false;
                        break;
                    case "GET":
                        if (SendRequest(Nodo, "GET", "") != 1) Result = false;
                        break;
                    case "NAVIGATEANDDOWNLOAD":
                        Uri _uri = new Uri(Nodo.Attributes["name"].Value);
                        if (NavigateAndDownloadString(_uri, out xmlResponse) != 1) Result = false;
                        break;
                    case "DICTIONARY":
                        if (GetDictionary(Nodo) != 1) Result = false;
                        break;
                    case "FINDSTRING":
                        xmlResponse = CreaXmlOuput((FindString(Nodo)) ? "YES" : "NO");
                        Result = true;
                        break;
                    case "EXTRACTTABLES":
                        if (ExtractTablesIntoArray(Nodo, false, out xmlResponse) != 1) Result = false;
                        break;
                    case "EXTRACTLISTS":
                        if (ExtractListsIntoArray(Nodo, false, out xmlResponse) != 1) Result = false;
                        break;
                    case "EXTRACTLISTSHTML":
                        if (ExtractListsIntoArray(Nodo, true, out xmlResponse) != 1) Result = false;
                        break;
                    case "EXTRACTVALUE":
                        if (ExtractValue(Nodo, out xmlResponse) != 1) Result = false;
                        break;
                    case "EXTRACTTABLESHTML":
                        if (ExtractTablesIntoArray(Nodo, true, out xmlResponse) != 1) Result = false;
                        break;
                    case "EXTRACTELEMENT":
                        if (ExtractElement(Nodo, out xmlResponse) != 1) Result = false;
                        break;
                    case "CLICKELEMENT":
                        if (SendRequest(Nodo, "INVOKEMEMBER", "CLICK") != 1) Result = false;
                        break;
                    case "SAVEHTML":
                        if (SalvaHTML(Nodo, out xmlResponse) != 1) Result = false;
                        break;
                    case "REFRESH":
                        RefreshPage();
                        break;
                    case "CONFIGURE":
                        Configure(Nodo);
                        break;
                    case "FORMDOWNLOADDATA":

                        /*
                         * 
                         * <?xml version="1.0" encoding="utf-8"?><CebisWeb Version="0.1"><Form name="logonForm"><Field name="j_username" value="LVRCLD72R49E289C" /><Field name="j_password" value="Maggio2013?!" /><Submit value="Accedi" /></Form></CebisWeb>
                         * <?xml version="1.0" encoding="utf-8"?><CebisWeb Version="0.1"><FormDownloadData action="https://sister2.agenziaterritorio.it/Ispezioni/ConsultazioneRichieste.do"><Field name="idRichiesta" value="749334881" /><Field name="metodo" value="Apri" /></FormDownloadData></CebisWeb>
                         */

                        byte[] _responseData;
                        if (SendRequest(Nodo, "FORMDOWNLOADDATA", Nodo.Attributes["actionURL"].Value, out _responseData) != 1)
                            Result = false;
                        else
                        {
                            xmlResponse = new XmlDocument();

                            XmlElement XmlElement;
                            XmlElement XmlElement2;
                            XmlAttribute XmlAttribute;

                            XmlDeclaration xDecl = xmlResponse.CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.WebName, null);
                            xmlResponse.AppendChild(xDecl);
                            //Root
                            XmlElement = xmlResponse.CreateElement("", "CebisWeb", "");

                            XmlElement2 = xmlResponse.CreateElement("", "Result", "");
                            XmlAttribute = xmlResponse.CreateAttribute("returnvalue");
                            XmlAttribute.Value = "OK";
                            XmlElement2.Attributes.Append(XmlAttribute);
                            XmlCDataSection CData = xmlResponse.CreateCDataSection(Convert.ToBase64String(_responseData));
                            XmlElement2.AppendChild(CData);

                            XmlElement.AppendChild(XmlElement2);
                            xmlResponse.AppendChild(XmlElement);

                            //XmlOutput.Save(FileXml);

                            //xmlResponse = XmlOutput;


                        };
                        break;
                }

                if (Result == false)
                    break;
            }
            return Result;
        }

        #endregion

        public bool Execute(XmlDocument XmlRequest, out XmlDocument xmlResponse)
        {
            bool RetVal = false;
            xmlResponse = null;

            XmlNode XMLNode = XmlRequest.SelectSingleNode("//CebisWeb");
            string VersionFields = XMLNode.Attributes.GetNamedItem("Version").Value;

            //Controllo della Versione            
            if (VersionFields != XMLFieldsVersion)
            {
                XmlFile = null;
            }
            else
            {
                XmlFile = XmlRequest;
                RetVal = ExecuteFile(out xmlResponse);
            }

            return RetVal;
        }

        private bool WaitForFile(string PathFile)
        {
            bool boolResult = false;

            try
            {
                while (boolResult == false)
                {
                    FileInfo Test = new FileInfo(PathFile);
                    if (Test.IsReadOnly == false)
                        boolResult = true;
                    else
                        boolResult = false;
                }
            }
            catch (Exception )
            {
                boolResult = true;
            }
            return boolResult;

        }

        protected int SendRequest(XmlNode XMLWork, string method, string FormName)
        {
            byte[] _responseData = null;
            return SendRequest(XMLWork, method, FormName, out _responseData);
        }

        protected int SendRequest(XmlNode XMLWork, string method, string FormName, out byte[] _responseData)
        {
            int result = -10;
            _responseData = null;

            Dictionary<string, string> postVariables = new Dictionary<string, string>();

            bool bOk = false;           
            while (bOk == false)
            {
                try
                {
                    ASCIIEncoding encoding = new ASCIIEncoding();

                    this.Executed = false;

                    if (method == "GET")
                    {
                        string UriRequest = XMLWork.Attributes.GetNamedItem("url").Value;

                        string VariableName = "";
                        string VariableValue = "";
                        StringBuilder postData = new StringBuilder();

                        for (int iNodiField = 0; iNodiField < XMLWork.ChildNodes.Count; iNodiField++)
                        {
                            switch (XMLWork.ChildNodes.Item(iNodiField).Name)
                            {
                                case "Field":
                                    VariableName = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("name").Value;
                                    VariableValue = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("value").Value;

                                    if (VariableValue.StartsWith("%") && VariableValue.EndsWith("%"))
                                    {
                                        if (DictionaryRomis.ContainsKey(VariableName))
                                        {
                                            VariableValue = DictionaryRomis[VariableName];
                                        }
                                    }

                                    //postVariables.Add(VariableName, VariableValue);

                                    postData.Append(VariableName);
                                    if (VariableValue != "")
                                    {
                                        postData.Append("=");
                                        postData.Append(VariableValue);
                                    }
                                    postData.Append("&");

                                    break;
                            }
                        }
                        if (postData.Length > 0)
                        {
                            //postData =  postData.Substring(0, postData.Length - 1);
                            UriRequest += "?" + postData.ToString().Substring(0, postData.Length - 1);
                        }

                        //Tento la cancellazione in cache
                        //DeleteUrlCache(UriRequest.ToString());

                        webBrowser1.ScriptErrorsSuppressed = true;
                        webBrowser1.Url = new Uri(UriRequest);

                    }
                    else if (method == "POST")
                    {
                        string VariableName = "";
                        string VariableValue = "";

                        string Frame = "";
                        if (XMLWork.Attributes.GetNamedItem("frame") != null)
                            Frame = XMLWork.Attributes.GetNamedItem("frame").Value;

                        HtmlElementCollection els = null;
                        //Faccio questo per ovviare al problema del form senza nome                        
                        if (FormName == "")
                        {
                            for (int ii = 0; ii <= webBrowser1.Document.Forms.Count; ii++)
                            {
                                els = webBrowser1.Document.Forms.GetElementsByName("");
                                //if (els.Count > ii)
                                //{
                                //    if (els[ii].Name == "")
                                //    {
                                //        el = els[ii];
                                //        break;
                                //    }
                                //}
                            }
                        }
                        else
                        {
                            try
                            {
                                if (Frame != "")
                                    els = webBrowser1.Document.Window.Frames[Frame].Document.Forms.GetElementsByName(FormName);
                                else
                                    els = webBrowser1.Document.Forms.GetElementsByName(FormName);

                                //els = webBrowser1.Document.Window.Frames["ricercaFrame"].Document.Forms.GetElementsByName(FormName);

                                //el = webBrowser1.Document.Forms[FormName];
                            }
                            catch (NullReferenceException)
                            {
                            }
                        }

                        for (int iNodiField = 0; iNodiField < XMLWork.ChildNodes.Count; iNodiField++)
                        {
                            switch (XMLWork.ChildNodes.Item(iNodiField).Name.ToUpper())
                            {
                                case "FIELD":
                                    VariableName = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("name").Value;
                                    VariableValue = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("value").Value;

                                    if (VariableValue.StartsWith("%") && VariableValue.EndsWith("%"))
                                    {
                                        if (DictionaryRomis.ContainsKey(VariableName))
                                        {
                                            VariableValue = DictionaryRomis[VariableName];
                                        }
                                    }

                                    if (VariableValue.StartsWith("++"))
                                    {
                                        string ValorePrec = "";
                                        try
                                        {
                                            if (Frame != "")
                                                //els = webBrowser1.Document.Window.Frames["ricercaFrame"].Document.Forms.GetElementsByName(FormName);
                                                ValorePrec = webBrowser1.Document.All[VariableName].GetAttribute("value");
                                            else
                                                ValorePrec = webBrowser1.Document.All[VariableName].GetAttribute("value");
                                        }
                                        catch (NullReferenceException e)
                                        {
                                            //                                            ErrDescription = "Impossibile trovare il campo " + VariableName;
                                        }

                                        int ValoreNum = int.Parse(VariableValue.Replace("+", ""));

                                        if (ValorePrec != "")
                                        {
                                            ;
                                            ValoreNum += int.Parse(ValorePrec);
                                        }
                                        VariableValue = ValoreNum.ToString();
                                    }

                                    try
                                    {
                                        if (Frame != "")
                                            webBrowser1.Document.Window.Frames[Frame].Document.All[VariableName].SetAttribute("value", VariableValue);
                                        else
                                            webBrowser1.Document.All[VariableName].SetAttribute("value", VariableValue);
                                    }
                                    catch (NullReferenceException e)
                                    {
                                        throw new WebException("Campo non trovato");
                                    }

                                    break;

                                case "CHECKBOX":

                                    VariableName = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("name").Value;
                                    VariableValue = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("value").Value;

                                    try
                                    {
                                        foreach (HtmlElement el in els)
                                        {
                                            foreach (HtmlElement formEl in el.GetElementsByTagName("input"))
                                            {
                                                if (formEl.Name.ToLower() == VariableName.ToLower() && formEl.GetAttribute("type").ToLower() == "checkbox")
                                                {
                                                    if (VariableValue.ToLower() == "true" || VariableValue.ToLower() == "1")
                                                    {
                                                        formEl.SetAttribute("checked", "True");
                                                        break;
                                                    }
                                                    else if (VariableValue.ToLower() == "false" || VariableValue.ToLower() == "0")
                                                    {
                                                        formEl.SetAttribute("checked", null);
                                                        break;
                                                    }
                                                    else if (formEl.GetAttribute("value").ToLower().Trim() == VariableValue.ToLower().Trim())
                                                    {
                                                        formEl.SetAttribute("checked", "True");
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        throw new WebException("Campo non trovato");
                                    }

                                    break;

                                case "RADIO": //Passo gli altri campi che non sono di tipo Field validi solo per metodi POST

                                    VariableName = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("name").Value;
                                    VariableValue = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("value").Value;

                                    try
                                    {
                                        foreach (HtmlElement el in els)
                                        {
                                            foreach (HtmlElement formEl in el.GetElementsByTagName("input"))
                                            {
                                                if (formEl.Name.ToLower() == VariableName.ToLower() && formEl.GetAttribute("value") == VariableValue)
                                                {
                                                    try
                                                    {
                                                        formEl.Focus();
                                                    }
                                                    catch (NotSupportedException e)
                                                    {
                                                    }
                                                    formEl.InvokeMember("Click");
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        throw new WebException("Campo non trovato");
                                    }

                                    break;

                                case "SELECT":
                                case "SELECTTEXT":
                                    VariableName = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("name").Value;
                                    VariableValue = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("value").Value;

                                    try
                                    {
                                        foreach (HtmlElement el in els)
                                        {
                                            foreach (HtmlElement formEl in el.GetElementsByTagName("select"))
                                            {
                                                if (formEl.Name == VariableName)
                                                {
                                                    formEl.Focus();
                                                    if (XMLWork.ChildNodes.Item(iNodiField).Name == "SelectText")
                                                    {
                                                        foreach (HtmlElement ChildEl in formEl.Children)
                                                        {
                                                            if ((ChildEl.InnerText == null && VariableValue == "") || (ChildEl.InnerText != null && ChildEl.InnerText.Trim().ToLower() == VariableValue.Trim().ToLower()))
                                                            {
                                                                formEl.SetAttribute("value", ChildEl.GetAttribute("value"));
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        formEl.SetAttribute("value", VariableValue);
                                                    }
                                                    //formEl.InvokeMember("Click");                                                    

                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    catch (NullReferenceException) { }
                                    break;

                                case "SUBMIT":
                                    bool Found = false;

                                    VariableValue = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("value").Value;
                                    try
                                    {
                                        VariableName = XMLWork.ChildNodes.Item(iNodiField).Attributes.GetNamedItem("name").Value;
                                    }
                                    catch (NullReferenceException e)
                                    {
                                        VariableName = "";
                                    }
                                    //try
                                    //{
                                    //    foreach (HtmlElement el in els)
                                    //    {
                                    //        if (el.GetElementsByTagName("input").Count == 0)
                                    //        {
                                    //            throw new WebException("Campo non trovato");
                                    //        }
                                    //    }
                                    //}
                                    //catch (Exception)
                                    //{
                                    //    throw new WebException("Campo non trovato");
                                    //}

                                    if (VariableName != "")
                                    {
                                        foreach (HtmlElement el in els)
                                        {
                                            foreach (HtmlElement Contenitore in el.Children)
                                            {
                                                if (Contenitore.Id == VariableName)
                                                {
                                                    foreach (HtmlElement formEl in Contenitore.GetElementsByTagName("input"))
                                                    {
                                                        if (formEl.GetAttribute("value") == VariableValue)
                                                        {
                                                            try
                                                            {
                                                                formEl.Focus();
                                                            }
                                                            catch (NotSupportedException e)
                                                            {
                                                            }
                                                            Found = true;
                                                            formEl.InvokeMember("Click");
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //Tento di cercare il submit con la scansione di tutto il documento
                                    if (Found == false)
                                    {
                                        foreach (HtmlElement el in els)
                                        {
                                            foreach (HtmlElement formEl in el.GetElementsByTagName("input"))
                                            {
                                                if (formEl.GetAttribute("value").ToLower().Trim() == VariableValue.ToLower().Trim() &&
                                                    (formEl.GetAttribute("type").ToLower().Trim() == "submit" ||
                                                        formEl.GetAttribute("type").ToLower().Trim() == "button")
                                                    )
                                                {
                                                    try
                                                    {
                                                        formEl.Focus();
                                                    }
                                                    catch (NotSupportedException e)
                                                    {
                                                    }
                                                    formEl.InvokeMember("Click");
                                                    Found = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (Found == false)
                                    {
                                        foreach (HtmlElement el in els)
                                        {
                                            el.InvokeMember("submit");
                                        }
                                    }
                                    break;
                                case "DONOTSUBMIT":
                                    //CrisisEnviron._ErrorRaised = ErrorDescriptionEnum.None;
                                    result = 1;
                                    return result;

                                    break;
                            }
                        }
                    }
                    else if (method == "FORMDOWNLOADDATA")
                    {
                        //Prelevo l'azione del form
                        string ActionValue = FormName;

                        System.Collections.Specialized.NameValueCollection reqPostData = new System.Collections.Specialized.NameValueCollection();
                        for (int iNodiField = 0; iNodiField < XMLWork.ChildNodes.Count; iNodiField++)
                            reqPostData.Add(XMLWork.ChildNodes.Item(iNodiField).Attributes["name"].Value, XMLWork.ChildNodes.Item(iNodiField).Attributes["value"].Value);

                        //DownloadUrlFileByPostData(XMLWork.Attributes.GetNamedItem("url").Value, reqPostData, "POST", "TestFilePDF.pdf", out ds);
                        DownloadUrlFileByPostData(ActionValue, reqPostData, "POST", "TestFilePDF.pdf", out _responseData);

                        //Simulo la corretta esecuzione dell'attività
                        this.Executed = true;
                    }
                    else if (method == "INVOKEMEMBER")
                    {
                        string ActionValue = FormName;

                        string VariableName = "";
                        string VariableValue = "";
                        string VariableType = "";
                        bool SearchInnerText = true;

                        bool Found = false;

                        try
                        {
                            VariableType = XMLWork.Attributes.GetNamedItem("type").Value;
                            if (XMLWork.Attributes.GetNamedItem("name") != null)
                            {
                                VariableName = XMLWork.Attributes.GetNamedItem("name").Value;
                                SearchInnerText = true;
                            }
                            VariableValue = XMLWork.Attributes.GetNamedItem("value").Value;

                            foreach (HtmlElement Element in webBrowser1.Document.All)
                            {
                                if (Element.TagName.ToLower() == VariableType.ToLower())// && Element.GetAttribute(VariableName).ToLower() == VariableValue.ToLower())
                                {
                                    string GetAttrib = "";
                                    if (SearchInnerText)
                                    {
                                        if (VariableName.ToLower() == "class")
                                            GetAttrib = Element.GetAttribute("className");
                                        else
                                            GetAttrib = Element.GetAttribute(VariableName);

                                        if (GetAttrib.ToLower().Trim() == VariableValue.ToLower().Trim())
                                        {
                                            Found = true;
                                            //htmlElement = Element;
                                            if (ActionValue.ToUpper() == "CLICK")
                                            {
                                                Element.Focus();
                                                Element.InvokeMember("Click");
                                            }
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (Element.InnerText != null && Element.InnerText.ToLower().Trim().Contains(VariableValue.ToLower().Trim()))
                                        {
                                            Found = true;
                                            if (ActionValue.ToUpper() == "CLICK")
                                            {
                                                Element.Focus();
                                                Element.InvokeMember("Click");
                                            }

                                            break;
                                        }
                                    }
                                }
                            }



                            //Faccio un tentativo di ricerca nei frames se ci sono
                            if (Found == false && webBrowser1.Document.Window.Frames.Count > 0)
                            {
                                foreach (HtmlWindow Window in webBrowser1.Document.Window.Frames)
                                {
                                    foreach (HtmlElement Element in Window.Document.All)
                                    {
                                        if (Element.TagName.ToLower() == VariableType.ToLower())// && Element.GetAttribute(VariableName).ToLower() == VariableValue.ToLower())
                                        {
                                            string GetAttrib = "";
                                            if (SearchInnerText)
                                            {
                                                if (VariableName.ToLower() == "class")
                                                    GetAttrib = Element.GetAttribute("className");
                                                else
                                                    GetAttrib = Element.GetAttribute(VariableName);

                                                if (GetAttrib.ToLower().Trim() == VariableValue.ToLower().Trim())
                                                {
                                                    Found = true;
                                                    //htmlElement = Element;
                                                    if (ActionValue.ToUpper() == "CLICK")
                                                    {
                                                        Element.Focus();
                                                        Element.InvokeMember("Click");
                                                    }
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (Element.InnerText != null && Element.InnerText.ToLower().Trim().Contains(VariableValue.ToLower().Trim()))
                                                {
                                                    Found = true;
                                                    if (ActionValue.ToUpper() == "CLICK")
                                                    {
                                                        Element.Focus();
                                                        Element.InvokeMember("Click");
                                                    }

                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        catch (NullReferenceException e)
                        {
                            VariableName = "";
                        }
                    }
                    bOk = true;
                }
                catch (WebException e)
                {
                    bOk = false;
                    //CrisisEnviron._ErrorRaised = ErrorDescriptionEnum.WebSiteError;
                    break;
                }
                catch (Exception err)
                {
                    bOk = false;
                    //CrisisEnviron._ErrorRaised = ErrorDescriptionEnum.FieldOrAttributeNotFound;
                    break;
                }
            }

            if (bOk == false)
            {
                result = -10;
                return result;
            }
            else
            {
                //Attesa della risposta
                Thread_DisableFileDownload = new Thread(new ParameterizedThreadStart(DiscardWindowsHandles));

                Thread_DisableFileDownload.Start(webBrowser1.Handle);

                //Thread_WaitForExecutions = new Thread(WaitForExecute);
                //Thread_WaitForExecutions.Start();

                //Application.DoEvents();
                //Thread_WaitForExecutions.Join();

                //if (CrisisEnviron._ErrorRaised != ErrorDescriptionEnum.None)
                //    result = -10;

                double bTimerStart = DateTime.Now.Ticks;
                double mseconds = 0;
                do
                {

                    Application.DoEvents();
                    mseconds = (DateTime.Now.Ticks - bTimerStart) / 10000;
                    //double iSecondi = mseconds / 10000000;
                    if (mseconds / 1000 > Environ.TimeOut)
                    {
                        //Probabile blocco della procedura mando l'errore
                        result = -10;
                        Environ._ErrorRaised = ErrorDescriptionEnum.TimeOutElapsed;

                        break;
                    }
                    Thread.Sleep(4);
                } while (this.Executed == false || mseconds < Environ.TimeWaitForRedirection);
                //} while (this.CountExecuting <= 0);
                //MessageBox.Show(this.CountExecuting.ToString());

                if (Thread_DisableFileDownload != null && Thread_DisableFileDownload.ThreadState != ThreadState.Stopped)
                {
                    do
                    {
                        Thread_DisableFileDownload.Abort();
                        //Thread_DisableFileDownloadClosing = true;
                        Thread.Sleep(10);

                    } while (Thread_DisableFileDownload.ThreadState != ThreadState.Aborted);
                    Thread_DisableFileDownload = null;
                }

                if (this.Executed == true)
                {
                    Environ._ErrorRaised = ErrorDescriptionEnum.None;
                    result = 1;
                }

                try
                {
                    Environ.CurrentUrl = webBrowser1.Url;
                }
                catch (Exception)
                {
                    Environ.CurrentUrl = null;
                }
            }

            return result;
        }

        protected int NavigateAndDownloadString(Uri Url, out XmlDocument xmlResponse)
        {
            int _retvalue = 0;
            const int _buffer = 1024;
            const int MaxLenght = 4 * 1024 * 1024; //Max page lenght 4 MB
            bool Redirection = false;

            /*
             * 1) TEST:
             * Pattern: \<script[^>]*.* *location\.[replace|assign|href]*
             * Testo: 
               <html>
                        <head>
                        <script language="javascript"><!-- aa es  / location.href("http://www.autolineelorenzini.com");
                        -->
                        </script></head></html>
            */


            XmlDocument XmlOutput = new XmlDocument();

            XmlElement XmlElement;
            XmlElement XmlElement2;
            XmlAttribute XmlAttribute;

            XmlDeclaration xDecl = XmlOutput.CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.WebName, null);
            XmlOutput.AppendChild(xDecl);
            //Root
            XmlElement = XmlOutput.CreateElement("", "CebisWeb", "");

            XmlElement2 = XmlOutput.CreateElement("", "Result", "");
            XmlAttribute = XmlOutput.CreateAttribute("returnvalue");


            Environ._ErrorRaised = ErrorDescriptionEnum.None;
            Environ.CurrentUrl = Url;

            try
            {
                ////REDIRECT?
                //HttpWebRequest httpWebRequest = WebRequest.Create(Url) as HttpWebRequest;
                //httpWebRequest.Method = "HEAD";
                //httpWebRequest.AllowAutoRedirect = false;
                //System.Net.Cache.HttpRequestCachePolicy policyred = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.BypassCache);
                //HttpWebRequest.DefaultCachePolicy = policyred;

                //// Set the Credentials in two locations to get past the 407 error:
                //httpWebRequest.Proxy = WebRequest.DefaultWebProxy;                //WebRequest.GetSystemWebProxy();

                //httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
                //httpWebRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                //HttpWebResponse httpWebResponse = httpWebRequest.GetResponse() as HttpWebResponse;
                //if (httpWebResponse.StatusCode == HttpStatusCode.Redirect)
                //{
                //    Console.WriteLine("redirected to: " + httpWebResponse.GetResponseHeader("Location"));
                //}
                //else
                //{
                //    Console.WriteLine("no redirect");
                //}              

                // Creates an HttpWebRequest with the specified URL. 
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(Url);


                //Configurazione
                myHttpWebRequest.AllowAutoRedirect = true;
                myHttpWebRequest.Timeout = Environ.TimeOut * 1000;
                myHttpWebRequest.KeepAlive = true;
                myHttpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2"; //Emulo il browser                
                //myHttpWebRequest.UserAgent = "Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0"; //Emulo il browser

                //No Cache
                System.Net.Cache.HttpRequestCachePolicy policy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.BypassCache);
                HttpWebRequest.DefaultCachePolicy = policy;

                // Set the Credentials in two locations to get past the 407 error:
                myHttpWebRequest.Proxy = WebRequest.DefaultWebProxy;                //WebRequest.GetSystemWebProxy();

                myHttpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
                myHttpWebRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;


                // Sends the HttpWebRequest and waits for the response. 
                HttpWebResponse myHttpWebResponse;
                StringBuilder strWebResponse = new StringBuilder();
                //string strWebResponse2 = "";

                try
                {
                    myHttpWebRequest.Accept = "text/html, application/xhtml+xml, */*";
                    myHttpWebRequest.Headers.Add("Accept-Language", "it-IT,it;q=0.8,en-US;q=0.5,en;q=0.3");
                    //myHttpWebRequest.Headers.Add("Accept-Encoding", "gzip, deflate");                    

                    myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                    // Gets the stream associated with the response.                    
                    Stream receiveStream = myHttpWebResponse.GetResponseStream();

                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");

                    // Pipes the stream to a higher level stream reader with the required encoding format. 
                    StreamReader readStream = new StreamReader(receiveStream, encode);

                    //Console.WriteLine("\r\nResponse stream received.");
                    Char[] read = new Char[_buffer];

                    // Reads 1024 (_buffer) characters at a time.    
                    int count = readStream.Read(read, 0, _buffer);
                    //string _str2 = "";
                    //Console.WriteLine("HTML...\r\n");                                        
                    while (count > 0)
                    {
                        // Dumps the 1024 characters on a string and displays the string to the console.                        
                        string _str = new String(read, 0, count);
                        //_str2 = new String(read, 0, count);
                        strWebResponse.Append(_str);
                        //strWebResponse2 += _str2;

                        //Console.Write(_str);
                        count = readStream.Read(read, 0, _buffer);

                        if (strWebResponse.Length > MaxLenght)
                            throw new Exception("Site lenght overflow: > " + MaxLenght);
                    }
                    //Console.WriteLine("");
                    // Releases the resources of the response.
                    myHttpWebResponse.Close();
                    // Releases the resources of the Stream.
                    readStream.Close();

                    Environ.CurrentUrl = myHttpWebResponse.ResponseUri;
                }
                catch (WebException e)
                {
                    if (e.Response == null)
                        throw new Exception(e.Message);

                    using (WebResponse response = e.Response)
                    {
                        //HttpWebResponse httpResponse = (HttpWebResponse)response;
                        //Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                        //using (Stream data = response.GetResponseStream())
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            strWebResponse = new StringBuilder(reader.ReadToEnd());
                            //if (strWebResponse.Contains("Unknown Host"))
                            //    throw new Exception(e.Message);

                            //Console.WriteLine(text);
                        }

                        Environ.CurrentUrl = e.Response.ResponseUri;
                    }

                    if (myHttpWebRequest.HaveResponse == false)
                        throw new Exception(e.Message);
                }




                _retvalue = 1;


                //Test Redirection
                HtmlAgilityPack.HtmlDocument _testRedirect = new HtmlAgilityPack.HtmlDocument();
                _testRedirect.LoadHtml(strWebResponse.ToString().ToLower());

                //if (strWebResponse.ToString() != strWebResponse2)
                //    _retvalue = -1;

                HtmlAgilityPack.HtmlNodeCollection _co = _testRedirect.DocumentNode.SelectNodes("//script");
                if (_co != null)
                {
                    foreach (HtmlAgilityPack.HtmlNode _node in _co)
                    {
                        if (_node.InnerText.Replace(" ", string.Empty).Contains("location.replace(") ||
                            _node.InnerText.Replace(" ", string.Empty).Contains("location.href=") ||
                            _node.InnerText.Replace(" ", string.Empty).Contains("window.location=") ||
                            _node.InnerText.Replace(" ", string.Empty).Contains("location.assign("))
                        {
                            Redirection = true; //Probale Redirect      
                            break;
                        }
                    }
                }
                //Redirection with <META HTTP-EQUIV=Refresh CONTENT="0; URL=http://shop.leselle.it"> EX: http://www.leselle.it
                HtmlAgilityPack.HtmlNodeCollection _meta = _testRedirect.DocumentNode.SelectNodes("//meta[@http-equiv='refresh']");
                if (_meta != null)
                {
                    foreach (HtmlAgilityPack.HtmlNode _node in _meta)
                    {
                        if (_node.Attributes["content"].Value.Replace(" ", string.Empty).Contains("url=") ||
                            _node.Attributes["content"].Value.Replace(" ", string.Empty).Contains(";http"))
                        {
                            Redirection = true; //Probable Redirect      
                            break;
                        }
                    }
                }


                XmlElement2.Attributes.Append(XmlAttribute);
                XmlCDataSection CData = XmlOutput.CreateCDataSection(strWebResponse.ToString());
                XmlElement2.AppendChild(CData);

                //Console.WriteLine("IsFromCache? {0}", myHttpWebResponse.IsFromCache);

                XmlAttribute.Value = "OK";

                XmlElement2.SetAttribute("Redirection", Redirection ? "1" : "0");

                strWebResponse.Clear();
            }
            catch (TimeoutException)
            {
                Environ._ErrorRaised = ErrorDescriptionEnum.TimeOutElapsed;
                XmlAttribute.Value = "ERR";

                _retvalue = -1;
            }
            catch (Exception)
            {
                Environ._ErrorRaised = ErrorDescriptionEnum.WebSiteError;
                XmlAttribute.Value = "ERR";

                _retvalue = -1;
            }


            XmlElement.AppendChild(XmlElement2);
            XmlOutput.AppendChild(XmlElement);

            //XmlOutput.Save(FileXml);

            xmlResponse = XmlOutput;

            return _retvalue;
        }



        protected void WaitForExecute()
        {
            Environ._ErrorRaised = ErrorDescriptionEnum.None;

            double bTimerStart = DateTime.Now.Ticks;
            do
            {
                Thread.Sleep(10);
                Application.DoEvents();
                double iSecondi = (DateTime.Now.Ticks - bTimerStart) / 10000000;
                if (iSecondi > Environ.TimeOut)
                {
                    //Probabile blocco della procedura mando l'errore

                    Environ._ErrorRaised = ErrorDescriptionEnum.TimeOutElapsed;

                    break;
                }
            } while (this.Executed == false);
            ////} while (this.CountExecuting <= 0);
            ////MessageBox.Show(this.CountExecuting.ToString());            
        }


        protected int GetDictionary(XmlNode XMLWork)
        {
            int result = -10;

            string VariableType = "";
            string VariableName = "";
            string VariableValue = "";
            string RetVal = "";

            try
            {
                VariableType = XMLWork.Attributes.GetNamedItem("type").Value;
                VariableName = XMLWork.Attributes.GetNamedItem("name").Value;
                VariableValue = XMLWork.Attributes.GetNamedItem("value").Value;
                RetVal = webBrowser1.Document.All[VariableName].GetAttribute(VariableValue);
            }
            catch (NullReferenceException e)
            {
                result = -10;
                return result;
            }
            if (DictionaryRomis.ContainsKey(VariableName) == false)
            {
                DictionaryRomis.Add(VariableName, RetVal);
            }
            else
            {
                DictionaryRomis[VariableName] = RetVal;
            }
            return 1;
        }
        protected void Configure(XmlNode XMLWork)
        {
            string VariableName = "";
            string VariableValue = "";

            try
            {
                VariableName = XMLWork.Attributes.GetNamedItem("name").Value.ToUpper();
                VariableValue = XMLWork.InnerText;

                switch (VariableName)
                {
                    case "TIMEOUT":
                        int Time = Environ.TimeOut;
                        if (int.TryParse(VariableValue, out Time))
                            Environ.TimeOut = Time;
                        break;
                }
            }
            catch (Exception e)
            {
            }

        }

        protected void RefreshPage()
        {
            try
            {
                webBrowser1.Refresh(WebBrowserRefreshOption.Normal);
                DeleteCache deleteCache = new DeleteCache();
                DeleteCache.DeleteAllCache();
            }
            catch (Exception) { }
        }

        //protected void DeleteUrlCache(string Url)
        //{            
        //    DeleteCache DeleteCache = new DeleteCache();
        //    DeleteCache.DeleteAllCache();
        //    long r = DeleteUrlCacheEntry(Url);
        //}

        protected bool FindString(XmlNode XMLWork)
        {
            string VariableName = "";

            try
            {
                VariableName = XMLWork.Attributes.GetNamedItem("string").Value;
            }
            catch (NullReferenceException e)
            {
                return false;
            }

            bool Found = false;
            if (webBrowser1.Document != null && webBrowser1.Document.Window.Frames.Count > 0)
            {
                try
                {
                    foreach (HtmlWindow Frame in webBrowser1.Document.Window.Frames)
                    {
                        if (Frame != null && Frame.Document.Body != null && Frame.Document.Body.InnerText != null && Frame.Document.Body.InnerText.ToLower().Contains(VariableName.ToLower()))
                        {
                            Found = true;
                            break;
                        }
                    }
                }
                catch (Exception)
                {
                }

                if (Found == false)
                    Found = webBrowser1.DocumentText.ToLower().Contains(VariableName.ToLower());

                return Found;
            }
            else
                return webBrowser1.DocumentText.ToLower().Contains(VariableName.ToLower());
        }

        protected int ExtractElement(XmlNode XMLWork, out XmlDocument xmlResponse)
        {
            int result = -10;

            string VariableType = "";
            string VariableName = "";
            string VariableValue = "";
            bool SearchInnerText = false;
            bool All = false;


            xmlResponse = null;
            HtmlElement htmlElement = null;
            List<HtmlElement> htmlElementCollection = new List<HtmlElement>();
            try
            {
                VariableType = XMLWork.Attributes.GetNamedItem("type").Value;
                if (XMLWork.Attributes.GetNamedItem("name") != null)
                {
                    VariableName = XMLWork.Attributes.GetNamedItem("name").Value;
                    SearchInnerText = true;
                }
                VariableValue = XMLWork.Attributes.GetNamedItem("value").Value;

                if (XMLWork.Attributes.GetNamedItem("all") != null)
                {
                    All = (XMLWork.Attributes.GetNamedItem("all").Value.ToUpper() == "TRUE") ? true : false;
                }

                foreach (HtmlElement Element in webBrowser1.Document.All)
                {
                    if (Element.TagName.ToLower() == VariableType.ToLower())// && Element.GetAttribute(VariableName).ToLower() == VariableValue.ToLower())
                    {
                        string GetAttrib = "";
                        if (SearchInnerText)
                        {
                            if (VariableName.ToLower() == "class")
                                GetAttrib = Element.GetAttribute("className");
                            else
                                GetAttrib = Element.GetAttribute(VariableName);

                            if (GetAttrib.ToLower().Trim() == VariableValue.ToLower().Trim())
                            {
                                if (All == false)
                                {
                                    htmlElement = Element;
                                    break;
                                }
                                else
                                    htmlElementCollection.Add(Element);
                            }
                        }
                        else
                        {
                            if (Element.InnerText != null && Element.InnerText.ToLower().Trim().Contains(VariableValue.ToLower().Trim()))
                            {
                                if (All == false)
                                {
                                    htmlElement = Element;
                                    break;
                                }
                                else
                                    htmlElementCollection.Add(Element);
                            }
                        }
                    }
                }

                //Faccio un tentativo di ricerca nei frames se ci sono
                if (htmlElement == null && webBrowser1.Document.Window.Frames.Count > 0)
                {
                    foreach (HtmlWindow Window in webBrowser1.Document.Window.Frames)
                    {
                        foreach (HtmlElement Element in Window.Document.All)
                        {
                            if (Element.TagName.ToLower() == VariableType.ToLower())// && Element.GetAttribute(VariableName).ToLower() == VariableValue.ToLower())
                            {
                                string GetAttrib = "";
                                if (SearchInnerText)
                                {
                                    if (VariableName.ToLower() == "class")
                                        GetAttrib = Element.GetAttribute("className");
                                    else
                                        GetAttrib = Element.GetAttribute(VariableName);

                                    if (GetAttrib.ToLower().Trim() == VariableValue.ToLower().Trim())
                                    {
                                        if (All == false)
                                        {
                                            htmlElement = Element;
                                            break;
                                        }
                                        else
                                            htmlElementCollection.Add(Element);
                                    }
                                }
                                else
                                {
                                    if (Element.InnerText != null && Element.InnerText.ToLower().Trim().Contains(VariableValue.ToLower().Trim()))
                                    {
                                        if (All == false)
                                        {
                                            htmlElement = Element;
                                            break;
                                        }
                                        else
                                            htmlElementCollection.Add(Element);
                                    }
                                }
                            }
                        }
                    }

                }

            }
            catch (NullReferenceException e)
            {
                result = -10;
                xmlResponse = null;
                return result;
            }

            if (htmlElement != null || htmlElementCollection.Count > 0)
            {
                XmlDocument XmlOutput = new XmlDocument();

                XmlElement XmlElement;
                XmlElement XmlElement2;
                XmlAttribute XmlAttribute;

                XmlDeclaration xDecl = XmlOutput.CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.WebName, null);
                XmlOutput.AppendChild(xDecl);
                //Root
                XmlElement = XmlOutput.CreateElement("", "CebisWeb", "");

                XmlElement2 = XmlOutput.CreateElement("", "Result", "");
                XmlAttribute = XmlOutput.CreateAttribute("returnvalue");
                XmlAttribute.Value = "OK";
                XmlElement2.Attributes.Append(XmlAttribute);
                if (All == true)
                {
                    //Create root group
                    XmlElement rootgroup = XmlOutput.CreateElement("", "root", "");
                    foreach (HtmlElement _node in htmlElementCollection)
                    {
                        XmlElement elemgroup = XmlOutput.CreateElement("", "item", "");
                        XmlCDataSection CData = XmlOutput.CreateCDataSection(_node.OuterHtml);
                        elemgroup.AppendChild(CData);
                        rootgroup.AppendChild(elemgroup);
                    }
                    XmlElement2.AppendChild(rootgroup);
                }
                else
                {
                    XmlCDataSection CData = XmlOutput.CreateCDataSection(htmlElement.OuterHtml);
                    XmlElement2.AppendChild(CData);
                }

                XmlElement.AppendChild(XmlElement2);
                XmlOutput.AppendChild(XmlElement);

                //XmlOutput.Save(FileXml);

                xmlResponse = XmlOutput;

                result = 1;
            }


            return result;

        }

        #region ESTRAZIONE DI UN VALORE DA UN CAMPO FORM        
        protected int ExtractValue(XmlNode XMLWork, out XmlDocument xmlResponse)
        {
            string TagName = "";
            int result = -10;

            string VariableType = "";
            string VariableName = "";
            string VariableValue = "";
            string RetVal = "";

            try
            {
                TagName = XMLWork.Attributes.GetNamedItem("tagname").Value;
                VariableType = XMLWork.Attributes.GetNamedItem("type").Value;
                VariableName = XMLWork.Attributes.GetNamedItem("name").Value;
                VariableValue = XMLWork.Attributes.GetNamedItem("attributename").Value;
                foreach (HtmlElement _element in webBrowser1.Document.GetElementsByTagName(TagName))
                {
                    if (
                        //_element.GetAttribute("type").ToLower() == VariableType.ToLower() &&
                        _element.GetAttribute("name").ToLower() == VariableName.ToLower())
                    {
                        if (
                            VariableType.ToLower() == "radio" ||
                            VariableType.ToLower() == "checkbox"
                            )
                        {
                            if (
                            _element.GetAttribute("checked").ToLower() == "true" ||
                            _element.GetAttribute("selected").ToLower() == "true"
                            )
                                RetVal = _element.GetAttribute(VariableValue);
                        }
                        else
                            RetVal = _element.GetAttribute(VariableValue);
                    }
                }

            }
            catch (NullReferenceException e)
            {
                result = -10;
                xmlResponse = null;
                return result;
            }

            //HtmlElementCollection RootTag = webBrowser1.Document.GetElementsByTagName(TagName);

            //foreach (HtmlElement Elemento in RootTag)
            //{
            //    if (Elemento.GetAttribute(VariableValue) == VariableName)
            //    {
            //        RetVal = Elemento.GetAttribute(VariableValue);
            //        break;
            //    }
            //}

            SaveXmlValueExtract(RetVal, out xmlResponse);

            return 1;

        }

        protected void SaveXmlValueExtract(string Valore, out XmlDocument xmlResponse)
        {
            XmlDocument XmlOutput = new XmlDocument();

            XmlElement XmlElement;

            XmlDeclaration xDecl = XmlOutput.CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.WebName, null);
            XmlOutput.AppendChild(xDecl);

            //Root
            XmlElement = XmlOutput.CreateElement("", "Value", "");

            //XmlAttribute Attributo = XmlOutput.CreateAttribute("value");
            //Attributo.Value = Valore;
            //XmlElement.Attributes.Append(Attributo);
            XmlElement.InnerText = Valore;

            XmlOutput.AppendChild(XmlElement);

            xmlResponse = XmlOutput;
        }
        #endregion

        #region ESTRAZIONE DELLE TABELLE IN ARRAY

        //int iCols = 0;
        //int iRows = 0;        

        //List<string[,]> TabCollection = new List<string[,]>();


        protected int ExtractListsIntoArray(XmlNode XMLWork, bool HTML, out XmlDocument xmlResponse)
        {
            HtmlElementCollection RootTag = webBrowser1.Document.GetElementsByTagName("DL");

            //TabCollection.Clear();

            List<List<List<string>>> TableResultCollection;
            subTable(RootTag, HTML, out TableResultCollection);

            SaveXmlTableColl(out xmlResponse, TableResultCollection);

            return 1;
        }

        protected int ExtractTablesIntoArray(XmlNode XMLWork, bool HTML, out XmlDocument xmlResponse)
        {
            HtmlElementCollection RootTag = webBrowser1.Document.GetElementsByTagName("TABLE");

            //TabCollection.Clear();

            List<List<List<string>>> TableResultCollection;
            subTable(RootTag, HTML, out TableResultCollection);

            SaveXmlTableColl(out xmlResponse, TableResultCollection);

            return 1;
        }

        protected void SaveXmlTableColl(out XmlDocument xmlResponse, List<List<List<string>>> TableResultCollection)
        {
            int iNumber = 1;

            XmlDocument XmlOutput = new XmlDocument();

            XmlElement XmlElement;

            XmlDeclaration xDecl = XmlOutput.CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.WebName, null);
            XmlOutput.AppendChild(xDecl);

            //Root
            XmlElement = XmlOutput.CreateElement("", "Tables", "");

            foreach (List<List<string>> Tabella in TableResultCollection)
            {
                if (Tabella.Count > 0)
                {
                    XmlElement XmlElementTable = XmlOutput.CreateElement("", "Table", "");
                    XmlAttribute Attributo = XmlOutput.CreateAttribute("count");
                    Attributo.Value = iNumber.ToString();
                    XmlElementTable.Attributes.Append(Attributo);

                    Attributo = XmlOutput.CreateAttribute("rows");
                    Attributo.Value = Tabella.Count.ToString();
                    XmlElementTable.Attributes.Append(Attributo);

                    Attributo = XmlOutput.CreateAttribute("cols");
                    Attributo.Value = Tabella[0].Count.ToString();
                    XmlElementTable.Attributes.Append(Attributo);

                    foreach (List<string> Rows in Tabella)
                    {
                        if (Rows.Count > Tabella[0].Count)
                        {
                            XmlElementTable.Attributes["cols"].Value = Rows.Count.ToString();
                        }

                        XmlElement XmlRow = XmlOutput.CreateElement("", "Row", "");

                        foreach (string Cols in Rows)
                        {
                            XmlElement XmlCol = XmlOutput.CreateElement("Col");
                            XmlCol.InnerText = Cols;

                            XmlRow.AppendChild(XmlCol);
                        }

                        XmlElementTable.AppendChild(XmlRow);
                    }

                    XmlElement.AppendChild(XmlElementTable);

                    iNumber++;
                }
            }

            XmlOutput.AppendChild(XmlElement);

            xmlResponse = XmlOutput;
        }

        protected void subTable(HtmlElementCollection TableColl, bool HTML, out List<List<List<string>>> TableResultCollection)
        {
            //Inizio tabella
            //string[,] CurTabella = new string[0,0];
            //int iCurCol = 0;
            //int iCurRow = 0;

            TableResultCollection = new List<List<List<string>>>();

            //Nuova tabella
            foreach (HtmlElement TableElem in TableColl)
            {
                switch (TableElem.TagName.ToUpper())
                {
                    case "TABLE":
                        {
                            //WebPause();
                            //iCurCol = 0; iCurRow = 0;
                            int iMaxCol = 0;

                            List<List<string>> dtTable = new List<List<string>>();

                            foreach (HtmlElement _CurrentElement in TableElem.Children)
                            {
                                switch (_CurrentElement.TagName.ToUpper())
                                {
                                    case "TR":
                                        {
                                            List<string> Cols = new List<string>();

                                            for (int iElem = 0; iElem < _CurrentElement.Children.Count; iElem++)
                                            {
                                                int ColSpan = 0;

                                                if (_CurrentElement.Children[iElem].TagName == "TD" ||
                                                    _CurrentElement.Children[iElem].TagName == "TH")
                                                {
                                                    if (_CurrentElement.Children[iElem].GetAttribute("colspan") != null)
                                                        ColSpan = int.Parse(_CurrentElement.Children[iElem].GetAttribute("colspan"));
                                                    else
                                                        ColSpan = 1;  //Almeno una colonna devo inserirla

                                                    string Testo = "";
                                                    if (HTML == false)
                                                        Testo = _CurrentElement.Children[iElem].InnerText;
                                                    else
                                                        Testo = _CurrentElement.Children[iElem].InnerHtml;


                                                    for (int iColsSpan = 0; iColsSpan < ColSpan; iColsSpan++)
                                                        Cols.Add((Testo == null) ? "" : Testo);
                                                    //iCurCol++;

                                                }
                                            }
                                            if (Cols.Count > iMaxCol) iMaxCol = Cols.Count;
                                            dtTable.Add(Cols);
                                        }

                                        break;
                                    case "TBODY":
                                    case "THEAD":
                                    case "TFOOT":
                                        {
                                            //HtmlElement _CurrentElement2 = _CurrentElement.FirstChild;

                                            foreach (HtmlElement _rows in _CurrentElement.Children)
                                            {
                                                if (_rows.TagName == "TR")
                                                {
                                                    List<string> Cols = new List<string>();

                                                    for (int iElem = 0; iElem < _rows.Children.Count; iElem++)
                                                    {
                                                        int ColSpan = 0;

                                                        if (_rows.Children[iElem].TagName == "TD" ||
                                                            _rows.Children[iElem].TagName == "TH")
                                                        {
                                                            if (_rows.Children[iElem].GetAttribute("colspan") != null)
                                                                ColSpan = int.Parse(_rows.Children[iElem].GetAttribute("colspan"));
                                                            else
                                                                ColSpan = 1;  //Almeno una colonna devo inserirla

                                                            string Testo = "";
                                                            if (HTML == false)
                                                                Testo = _rows.Children[iElem].InnerText;
                                                            else
                                                                Testo = _rows.Children[iElem].InnerHtml;

                                                            for (int iColsSpan = 0; iColsSpan < ColSpan; iColsSpan++)
                                                                Cols.Add((Testo == null) ? "" : Testo);


                                                            //iCurCol++;
                                                        }
                                                    }
                                                    if (Cols.Count > iMaxCol) iMaxCol = Cols.Count;
                                                    dtTable.Add(Cols);
                                                }
                                            }
                                        }

                                        break;

                                }

                            }






                            //HtmlElement _CurElement = TableElem;
                            //while(_CurElement.FirstChild != null && _CurElement.FirstChild.TagName != "TR")  
                            //{
                            //    _CurElement = _CurElement.FirstChild;
                            //} ;                      

                            ////Devo Calcolare la massima colonna che posso trovare                        
                            //for (int iRow = 0; iRow < _CurElement.Children.Count; iRow++)
                            //{
                            //    if (_CurElement.Children[iRow].TagName == "TR")
                            //    {
                            //        for (int iElem = 0; iElem < _CurElement.Children[iRow].Children.Count; iElem++)
                            //        {                                    
                            //            if (_CurElement.Children[iRow].Children[iElem].TagName == "TD")
                            //                iCurCol++;
                            //            if (_CurElement.Children[iRow].Children[iElem].TagName == "TH")
                            //                iCurCol++;
                            //        }
                            //        if (iCurCol > iMaxCol) iMaxCol = iCurCol;

                            //        iCurRow++;
                            //        iCurCol = 0;
                            //    }
                            //}

                            //iRows = iCurRow;
                            //iCols = iMaxCol;  //Prendo un massimo di colonna possibile
                            //iCurCol = 0; iCurRow = 0;

                            //CurTabella = new string[iRows, iCols];

                            ////Inizio prelevamento del testo
                            //for (int iRow = 0; iRow < _CurElement.Children.Count; iRow++)
                            //{
                            //    if (_CurElement.Children[iRow].TagName == "TR")
                            //    {
                            //        for (int iElem = 0; iElem < _CurElement.Children[iRow].Children.Count; iElem++)
                            //        {
                            //            string Testo = "";
                            //            if (HTML == false)
                            //                Testo = _CurElement.Children[iRow].Children[iElem].InnerText;
                            //            else
                            //                Testo = _CurElement.Children[iRow].Children[iElem].InnerHtml;

                            //            if (_CurElement.Children[iRow].Children[iElem].TagName == "TD")
                            //                CurTabella[iCurRow, iCurCol] = (Testo == null) ? "" : Testo;
                            //            if (_CurElement.Children[iRow].Children[iElem].TagName == "TH")
                            //                CurTabella[iCurRow, iCurCol] = (Testo == null) ? "" : Testo;
                            //            iCurCol++;
                            //        }                                
                            //    }
                            //    iCurRow++;
                            //    iCurCol = 0;                               
                            //}

                            //TabCollection.Add(CurTabella);


                            //HtmlElementCollection TR = TableElem.GetElementsByTagName("TR");
                            //HtmlElementCollection TD = TableElem.GetElementsByTagName("TD");
                            //HtmlElementCollection TH = TableElem.GetElementsByTagName("TH");

                            //foreach (HtmlElement RowElem in TR)
                            //{                            
                            //    foreach (HtmlElement ColElem in RowElem.GetElementsByTagName("TD"))
                            //    {                   

                            //        iCurCol++;
                            //    }

                            //    if (iCurCol > iMaxCol) iMaxCol = iCurCol;

                            //    iCurRow++;
                            //    iCurCol = 0;
                            //}

                            //iRows = TR.Count;
                            //iCols = iMaxCol;  //Prendo un massimo di colonna possibile
                            //iCurCol = 0; iCurRow = 0;

                            //CurTabella = new string[iRows, iCols];

                            ////Inizio prelevamento del testo
                            //foreach (HtmlElement RowElem in TR)
                            //{
                            //    foreach (HtmlElement ColElem in RowElem.GetElementsByTagName("TD"))
                            //    {
                            //        if (HTML == false)
                            //        {
                            //            CurTabella[iCurRow, iCurCol] = ColElem.InnerText;
                            //        }
                            //        else
                            //        {
                            //            CurTabella[iCurRow, iCurCol] = ColElem.InnerHtml;
                            //        }
                            //        iCurCol++;
                            //    }
                            //    iCurRow++;
                            //    iCurCol = 0;
                            //}

                            //TabCollection.Add(CurTabella);


                            TableResultCollection.Add(dtTable);
                        }
                        break;
                    case "DL":
                        {
                            //Inizio prelevamento del testo
                            HtmlElement _CurrentElement = TableElem;

                            //iCurCol = 0; iCurRow = 0;
                            int iMaxCol = 0;
                            int iCurCol = 0;

                            List<List<string>> dtTable = new List<List<string>>();

                            List<string> Cols = new List<string>();
                            for (int iRow = 0; iRow < _CurrentElement.Children.Count; iRow++)
                            {


                                if (_CurrentElement.Children[iRow].TagName == "DT")
                                {
                                    if (Cols.Count > iMaxCol) iMaxCol = Cols.Count;
                                    if (Cols.Count > 0) dtTable.Add(Cols);

                                    Cols = new List<string>();

                                    iCurCol = 0;
                                    //iCurRow++;
                                    string Testo = "";
                                    if (HTML == false)
                                        Testo = _CurrentElement.Children[iRow].InnerText;
                                    else
                                        Testo = _CurrentElement.Children[iRow].InnerHtml;

                                    Cols.Add((Testo == null) ? "" : Testo);

                                    //CurTabella[iCurRow - 1, iCurCol] = (Testo == null) ? "" : Testo;
                                    iCurCol = 1;
                                }
                                if (_CurrentElement.Children[iRow].TagName == "DD" && iCurCol == 1)
                                {
                                    string Testo = "";
                                    if (HTML == false)
                                        Testo = _CurrentElement.Children[iRow].InnerText;
                                    else
                                        Testo = _CurrentElement.Children[iRow].InnerHtml;

                                    Cols.Add((Testo == null) ? "" : Testo);

                                    //CurTabella[iCurRow - 1, iCurCol] = (Testo == null) ? "" : Testo;

                                    iCurCol++;
                                }
                            }

                            TableResultCollection.Add(dtTable);
                        }

                        break;
                        //case "DL":

                        //    //WebPause();
                        //    HtmlElement _CurElement = TableElem;
                        //    iCurCol = 0; iCurRow = 0;
                        //    iMaxCol = 0;

                        //    _CurElement = TableElem;
                        //    while (_CurElement.FirstChild != null && _CurElement.FirstChild.TagName != "DT")
                        //    {
                        //        _CurElement = _CurElement.FirstChild;
                        //    };

                        //    //Devo Calcolare la massima colonna che posso trovare                        
                        //    for (int iRow = 0; iRow < _CurElement.Children.Count; iRow++)
                        //    {
                        //        if (_CurElement.Children[iRow].TagName == "DT")
                        //            iCurRow++;
                        //    }

                        //    iMaxCol = 2; //Fisso a 2 perchè i tag sono DT e DD

                        //    iRows = iCurRow;
                        //    iCols = iMaxCol;  //Prendo un massimo di colonna possibile
                        //    iCurCol = 0; iCurRow = 0;

                        //    CurTabella = new string[iRows, iCols];

                        //    //Inizio prelevamento del testo
                        //    for (int iRow = 0; iRow < _CurElement.Children.Count; iRow++)
                        //    {
                        //        if (_CurElement.Children[iRow].TagName == "DT")
                        //        {
                        //            iCurCol = 0;
                        //            iCurRow++;
                        //            string Testo = "";
                        //            if (HTML == false)
                        //                Testo = _CurElement.Children[iRow].InnerText;
                        //            else
                        //                Testo = _CurElement.Children[iRow].InnerHtml;

                        //            CurTabella[iCurRow - 1, iCurCol] = (Testo == null) ? "" : Testo;
                        //            iCurCol = 1;
                        //        }
                        //        if (_CurElement.Children[iRow].TagName == "DD" && iCurCol == 1)
                        //        {
                        //            string Testo = "";
                        //            if (HTML == false)
                        //                Testo = _CurElement.Children[iRow].InnerText;
                        //            else
                        //                Testo = _CurElement.Children[iRow].InnerHtml;

                        //            CurTabella[iCurRow - 1, iCurCol] = (Testo == null) ? "" : Testo;

                        //            iCurCol++;
                        //        }
                        //    }

                        //    TabCollection.Add(CurTabella);

                        //    break;         
                }
            }
        }

        #endregion

        private int SalvaHTML(XmlNode XMLWork, out XmlDocument xmlResponse)
        {
            XmlDocument XmlOutput = new XmlDocument();

            XmlElement XmlElement;
            XmlElement XmlElement2;
            XmlAttribute XmlAttribute;

            string NameFrame = string.Empty;
            if (XMLWork.Attributes.Count > 0)
            {
                NameFrame = XMLWork.Attributes["name"].Value;
            }

            XmlDeclaration xDecl = XmlOutput.CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.WebName, null);
            XmlOutput.AppendChild(xDecl);
            //Root
            XmlElement = XmlOutput.CreateElement("", "CebisWeb", "");

            XmlElement2 = XmlOutput.CreateElement("", "Result", "");
            XmlAttribute = XmlOutput.CreateAttribute("returnvalue");
            XmlAttribute.Value = "OK";
            XmlElement2.Attributes.Append(XmlAttribute);

            XmlCDataSection CData;
            if (NameFrame != string.Empty)
            {
                if (webBrowser1.Document.Window.Frames[NameFrame] != null)
                    CData = XmlOutput.CreateCDataSection(webBrowser1.Document.Window.Frames[NameFrame].Document.Body.OuterHtml);
                else
                    CData = XmlOutput.CreateCDataSection(string.Empty);
            }
            else
                CData = XmlOutput.CreateCDataSection(webBrowser1.DocumentText);

            XmlElement2.AppendChild(CData);

            XmlElement.AppendChild(XmlElement2);
            XmlOutput.AppendChild(XmlElement);

            //XmlOutput.Save(FileXml);

            xmlResponse = XmlOutput;

            return 1;
        }


        /// <summary>
        /// Questa funzione permette di scaricare da un url passando dei Post Data
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="PostData"></param>
        /// <param name="FullNameSaveFile"></param>
        /// <returns></returns>
        private bool DownloadUrlFileByPostData(string Url, System.Collections.Specialized.NameValueCollection UploadData, string Method, string FullNameSaveFile, out byte[] ResponseData)
        {
            bool RetVal = false;
            ResponseData = null;

            try
            {
                WebClient webCl1;
                //string FileName = @"c:\\test.pdf";

                //webCl1.Headers.Add(HttpRequestHeader.ProxyAuthorization, "");
                //webCl1.Proxy = System.Net.GlobalProxySelection.GetEmptyWebProxy();


                IWebProxy defaultWebProxy = WebRequest.DefaultWebProxy;
                defaultWebProxy.Credentials = CredentialCache.DefaultCredentials;
                webCl1 = new WebClient
                {
                    Proxy = defaultWebProxy
                };
                webCl1.Headers.Add(HttpRequestHeader.Cookie, webBrowser1.Document.Cookie);

                Method = (Method.ToLower() == "post") ? "POST" : "GET";

                byte[] responsebytes = webCl1.UploadValues(Url, Method, UploadData);

                string _TestData = System.Text.Encoding.UTF8.GetString(responsebytes);
                //string responsebody = Encoding.UTF8.GetString(responsebytes);
                if (responsebytes.Length > 0)
                {
                    //using (FileStream =
                    //        new System.IO.FileStream(FullNameSaveFile, System.IO.FileMode.Create,
                    //                      System.IO.FileAccess.Write))
                    //using (FileStream =
                    //        new System.IO.MemoryStream())
                    //{
                    //    // Writes a block of bytes to this stream using data from
                    //    // a byte array.
                    //    FileStream.Write(responsebytes, 0, responsebytes.Length);

                    //    // close file stream
                    //    FileStream.Close();
                    //}
                    ResponseData = responsebytes;
                    RetVal = true;
                }
            }
            catch (Exception err)
            { }


            return RetVal;
        }

        private void WebBrowserNavigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            this.CountExecuting++;
            this.Executed = false;
        }
        private void WebBrowserDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        { //Esecuzione terminata     
            //this.CountExecuting--;
            Thread.Sleep(30);

            if (sender.Equals(webBrowser1))
                this.Executed = true;
            else
                this.Executed = false;

            this.Executed = true;
        }
        private void webBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            this.CountExecuting--;
        }
        internal void WebPause()
        {
            chkDebug.Checked = false;
            chkDebug.Visible = true;

            
            lblStatus.Text = "In attesa del Continua ------------------------------------------->";

            do
            {
                Application.DoEvents();
                //Continue = true;            
            } while (chkDebug.Checked == false);

            chkDebug.Checked = false;
            chkDebug.Visible = false;
        }

        private void webBrowser1_NewWindow(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FormClosing_Invoked == false)
                e.Cancel = true;
            else
            {
                //Rilascio correttamente le risorse altrimenti Crash di sistema                
                webBrowser1.Dispose();
                Application.DoEvents();    //Qui apre le pagine su Browser (da risolvere!!)
                Thread.Sleep(100);
                Application.DoEvents();

                if (Thread_DisableFileDownload != null && Thread_DisableFileDownload.ThreadState != ThreadState.Stopped && Thread_DisableFileDownload.ThreadState != ThreadState.Aborted)
                    Thread_DisableFileDownload.Abort();
                if (Thread_WaitForExecutions != null && Thread_WaitForExecutions.ThreadState != ThreadState.Stopped && Thread_WaitForExecutions.ThreadState != ThreadState.Aborted)
                    Thread_DisableFileDownload.Abort();
            }
        }


        private void DiscardWindowsHandles(object ThreadIdCaller)
        {
            int ExitLoop = 0;
            //Thread.Sleep(2000);
            //MessageBox.Show("test1");

            do
            {
                try
                {
                    Thread.Sleep(10);

                    IntPtr WindowsHdl = NativeMethods.findMessageBox();
                    //MessageBox.Show("test2");
                    if (WindowsHdl != IntPtr.Zero)
                    {
                        // MessageBox.Show("test3");
                        //Premo il tasto ESC
                        NativeMethods.PostMessage(WindowsHdl, 256, (IntPtr)27, (IntPtr)0);
                        NativeMethods.PostMessage(WindowsHdl, 257, (IntPtr)27, (IntPtr)0);
                        //MessageBox.Show("test4");
                    }


                    //if (Thread_DisableFileDownloadClosing == true)                    
                    //    break;
                }
                catch { }

            } while (ExitLoop == 0);

        }

        internal static class NativeMethods
        {
            [DllImport("user32.dll", CharSet = CharSet.Unicode)]
            public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
            public static IntPtr findMessageBox()
            {
                IntPtr hwndMB = IntPtr.Zero;

                hwndMB = FindWindowEx(IntPtr.Zero, IntPtr.Zero, "#32770", "Message from webpage");
                if (hwndMB != IntPtr.Zero) return hwndMB;

                if (Environ.DisableFileDownloadRequest)
                {
                    hwndMB = FindWindowEx(IntPtr.Zero, IntPtr.Zero, "#32770", "File Download");
                    if (hwndMB != IntPtr.Zero) return hwndMB;
                }

                if (Environ.ClosePopupWindows)
                {
                    IntPtr hwndMBDestroy = FindWindowEx(IntPtr.Zero, IntPtr.Zero, "Internet Explorer_TridentDlgFrame", null);
                    if (hwndMBDestroy != IntPtr.Zero)
                    {
                        //Try to destroy the popup page
                        //PostMessage(hwndMBDestroy, 6, 0, 0);  //WM_ACTIVATE = 0x06
                        //PostMessage(hwndMBDestroy, 7, 0, 0);  //WM_SETFOCUS = 0x07
                        PostMessage(hwndMBDestroy, 16, (IntPtr)0, (IntPtr)0);   //WM_CLOSE = 0x10
                        //PostMessage(hwndMBDestroy, 2, 0, 0);    //WM_DESTROY = 0x02      

                        //Need Fire the Navigated Event Here!                    
                    }
                }

                //if (hwndMB == IntPtr.Zero)
                //{
                //    //Secondo tentativo di recupero per un'altra versione di internet explorer (7 o 8)
                //    hwndMB = FindWindowEx(IntPtr.Zero, IntPtr.Zero, "#32770", "Windows Internet Explorer");
                //}

                return hwndMB;
            }
            [DllImport("user32.dll", CharSet = CharSet.Unicode)]
            public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

            //[DllImport("kernel32.dll")]
            //public static extern uint GetCurrentThreadId();

            //public void SendNoMessage(IntPtr hwnd)
            //{
            //    IntPtr button = FindWindowEx(hwnd, IntPtr.Zero, "Button", "&Cancel");

            //    PostMessage(button, 256, (uint)'C', 0);  //tasto C
            //    PostMessage(button, 257, (uint)'C', 0);

            //    PostMessage(hwnd, 256, 27, 0);  //Tasto ESC
            //    PostMessage(hwnd, 257, 27, 0);
            //}
        }
    }
}
