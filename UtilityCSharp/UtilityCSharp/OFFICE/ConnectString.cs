﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace UtilityCSharp.OFFICE
{

    public enum ConnectionType
    {
        Server = 1,
        Database = 2,
        Sqlserver = 3,
        //FILE = "file",
        Localhost = 4,
        Mdb = 5,
        Excel = 6,
        Text = 6,
        Txt = 7,
        Csv = 8

    };


    /// <summary>
    /// 2015-04-13
    /// </summary>
    public class ConnectString
    {
        private static ConnectString _instance;
        public static string Extension { get; private set; }
        public static bool Local { get; set; }
        public static bool Server { get; set; }

        public static string Connectionstring { get; set; }

        public static string Filepath { get; set; }
        public static string Filename { get; set; }
        public static string Conntype { get; set; }
        public static string Fileextension
        {
            get
            {
                if (!Filename.Contains(".")) return Extension = "";
                var ex = Filename.Split('.');
                return Extension = ex[ex.Length - 1];
            }
            set { Extension = value; }
        }

        private static string Pathtofile { get; set; }

        //private static string CONNECTION { get; set; }
        private static string Database { get; set; }
        private static string Port { get; set; }
        private static string Host { get; set; } //OPPURE IP ADDDRESS

        //PUBLIC PARAMETER
        public static string Password { get; set; }
        public static string User { get; set; }


        //Connection String
        //private static string DEFAULT = ConsoleApplication1.Properties.Settings.Default.ConnectionString;
        //CONNECT TO SQL Mdb
        private static readonly string ConnMdbOleDb1 =
            "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + Pathtofile + ";Jet OLEDB:Database Password=" + Password;

        //CONNECT TO Excel FILE
        private static readonly string ConnXlsxOleDb1 =
            "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + Pathtofile + ";" + "Extended Properties=Excel 12.0 Xml";

        private static string ConnXlsxOleDb2 { get; } = "Provider=Microsoft.ACE.OLEDB.14.0;" + "Data Source=" +
                                                       Pathtofile + ";" + "Extended Properties=Excel 14.0 Xml";

        private static string ConnXlsxOleDb3 { get; } = "Provider=Microsoft.ACE.OLEDB.15.0;" + "Data Source=" +
                                                        Pathtofile + ";" + "Extended Properties=Excel 15.0 Xml";

        //CONNECT TO Csv OR Txt FILE
        private static string CONN_CSV1 = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + Pathtofile + ";Extensions=asc,csv,tab,txt;Persist Security Info=False";

        //CONNECT TO SQL Server
        //private static string CONN_SQL_SERVER1 = "server=("+ Host +");database=" + Database + ";Trusted_Connection=True";//TRUSTED_CONNECTION
        //private static string CONN_SQL_SERVER2 = "server=.;database=" + Database + ";Integrated Security=SSPI";

        //ODBC -- Standard Connection
        private static readonly string ConnSqlOdbc1 =
            "Driver={SQL Server};" + "Server=" + Host + ";" + "DataBase=" + Database + ";" + "Uid=" + User + ";" + "Pwd=" + Password + ";";

        //ODBC -- Trusted Connection

        private static string ConnSqlServerOdbc2 { get; } = "Driver={SQL Server};" + "Server=" + Host + ";" +
                                                            "DataBase=" + Database + ";" + "Trusted_Connection=Yes;";

        //OleDb -- Standard Connection

        private static string ConnSqlServerOleDb1 { get; } = "Driver=SQLOLEDB;" + "Data Source=" + Host + ";" +
                                                             "Initial Catalog=" + Database + ";" + "User Id=" + User +
                                                             ";" + "Password=" + Password + ";";

        //OleDb -- Trusted Connection
        private static readonly string ConnSqlServerOleDb2 =
            "Driver=SQLOLEDB;" + "Data Source=" + Host + ";" + "Initial Catalog=" + Database + ";" + "Integrated Security=SSPI;";

        //OleDb -- via IP Address
        private static readonly string ConnSqlServerOleDb3 =
            "Driver=SQLOLEDB;" + "Network Library=DBMSSOCN;" + "Data Source=" + Host + "," + Port + ";" + "Initial Catalog=" + Database + ";" +
            "User Id=" + User + ";" + "Password=" + Password + ";";

        //.NET DataProvider -- Standard Connection
        private static readonly string ConnSqlServerNet1 =
           "Data Source=" + Host + ";" + "Initial Catalog=" + Database + ";" + "User Id=" + User + ";" + "Password=" + Password + ";";

        //ECC.

        /// <summary>
        /// An ConnectionString _instance that exposes a single _instance
        /// </summary>
        public static ConnectString Instance
        {
            get
            {
                try
                {
                    Pathtofile = string.IsNullOrEmpty(Fileextension) ?
                        string.Concat(Filepath, Path.DirectorySeparatorChar, Filename) :
                        string.Concat(Filepath, Path.DirectorySeparatorChar, Filename, ".", Fileextension);
                    // If the _instance is null then create one and init the Queue
                    if (_instance == null)
                    {
                        //SE A Local FILE
                        if (Local && !(string.IsNullOrEmpty(Filepath)) && !(string.IsNullOrEmpty(Filename)))
                        {
                            if (string.IsNullOrEmpty(Fileextension))
                            {
                                if (File.Exists(Pathtofile))
                                {
                                    _instance = new ConnectString();
                                }
                            }
                            else
                            {
                                if (File.Exists(Pathtofile))
                                {
                                    _instance = new ConnectString();
                                }
                            }

                        }
                        //SE Server
                        else if (Server)
                        {
                            _instance = new ConnectString();
                        }
                        else
                        {
                            throw new Exception("[ERROR] The parameter Filepath or Filename are not initialized");
                        }

                        if (_instance == null)
                        {
                            throw new Exception("[ERROR] File not found the parameter Filepath or Filename are wrong");
                        }

                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.StackTrace);
                }
                return _instance;
            }
        }

        /// <summary>
        /// Method for get a connection string for specific parameter
        /// </summary>  
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <param name="connType"></param>
        /// <returns></returns>
        public static string GetConnectionString(string filePath, string fileName, string connType)
        {
            Filepath = filePath;
            Filename = fileName;
            Conntype = connType;
            SubGetConnectionString();
            return Connectionstring;
        }

        /// <summary>
        /// Method for get a connection string for specific parameter
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <param name="connType"></param>
        /// <returns></returns>
        public static string GetConnectionString(string filePath, string fileName, ConnectionType connType)
        {
            Filepath = filePath;
            Filename = fileName;
            Conntype = connType.GetType().Name;
            Connectionstring = "";
            SubGetConnectionString(connType);
            return Connectionstring;

        }

        /// <summary>
        /// Method for get a connection string for specific parameter
        /// </summary>
        /// <param name="hostName"></param>
        /// <param name="databaseName"></param>
        /// <param name="portNumber"></param>
        /// <param name="typeDatabase"></param>
        /// <param name="connType"></param>
        /// <returns></returns>
        public static string GetConnectionString(string hostName, string databaseName, string portNumber, string typeDatabase, string connType)
        {
            Host = hostName;
            Database = databaseName;
            Port = portNumber;
            Conntype = connType;
            Connectionstring = "";
            SubGetConnectionString();
            return Connectionstring;

        }

        /// <summary>
        /// Method for get a connection string for specific parameter
        /// </summary>
        /// <param name="hostName"></param>
        /// <param name="databaseName"></param>
        /// <param name="portNumber"></param>
        /// <param name="typeDatabase"></param>
        /// <param name="connType"></param>
        /// <returns></returns>
        public static string GetConnectionString(string hostName, string databaseName, string portNumber, string typeDatabase, ConnectionType connType)
        {
            Host = hostName;
            Database = databaseName;
            Port = portNumber;
            Conntype = connType.GetType().Name;
            Connectionstring = "";
            SubGetConnectionString(connType);
            return Connectionstring;

        }

        private static void SubGetConnectionString()
        {
            Connectionstring = "";
            ConnectionType connectionType;
            if (Enum.TryParse(Conntype, out connectionType))
            {
                SetParameter(connectionType);
                if (string.IsNullOrEmpty(Connectionstring))
                {
                    throw new Exception(@"[ERROR] The parameter host,database,port or CONNECTIONTYPE are not valid value 
                                        CONNECTIONTYPE =['LOCALFILE','Server','FILE']");
                }
            }
            else
            {
                throw new Exception(@"[ERROR] The parameter host,database,port or CONNECTIONTYPE are not valid value 
                                        CONNECTIONTYPE =['LOCALFILE','Server','FILE']");
            }
        }

        private static void SubGetConnectionString(ConnectionType connectionType)
        {
            Connectionstring = "";
            SetParameter(connectionType);
            if (string.IsNullOrEmpty(Connectionstring))
            {
                throw new Exception(@"[ERROR] The parameter host,database,port or CONNECTIONTYPE are not valid value 
                                    CONNECTIONTYPE =['LOCALFILE','Server','FILE']");
            }
        }


        /// <summary>
        /// Method of support different combination of parameters
        /// </summary>   
        private static void SetParameter(ConnectionType connectionType)
        {
            var connType = connectionType;
            //if (Enum.TryParse(Conntype, out connType))
            //{                       
            var values = EnumUtil.GetValues<ConnectionType>();
            foreach (var t in values)
            {
                if (connType == t)
                {
                    var val = (string)Convert.ChangeType(connType, connType.GetTypeCode());

                    if (val != null)
                    {
                        switch (val.ToLower())
                        {
                            //case "local": 
                            //    connString = ConnSqlOdbc1;
                            //    break;
                            case "mdb":
                                Connectionstring = ConnMdbOleDb1;
                                break;
                            case "csv":
                                Connectionstring = CONN_CSV1;
                                break;
                            case "text":
                            case "txt":
                                Connectionstring = ConnSqlOdbc1;
                                break;
                            case "excel":
                                Connectionstring = ConnXlsxOleDb1;
                                break;
                            case "sqlserver":
                            case "server":
                            case "database":
                                {
                                    if (string.IsNullOrEmpty(Host) || string.IsNullOrEmpty(Database))
                                    {
                                        if (string.IsNullOrEmpty(User) || string.IsNullOrEmpty(Password))
                                        {
                                            Connectionstring = string.IsNullOrEmpty(Port)
                                                ? ConnSqlServerNet1
                                                : ConnSqlServerOleDb3;
                                        }
                                        else
                                        {
                                            Connectionstring = ConnSqlServerOleDb2;
                                            //CONN_SQL_SERVER_ODBC2,ConnSqlServerOleDb2
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception(
                                            "ERROR: Can't create the connection witht these parameter , ensure you have set the Host and Database variable");
                                    }
                                    break;
                                }
                            default:
                                throw new Exception(
                                    "ERROR: Can't create the connection witht these parameter , ensure you have set the Host and Database variable");
                        } //switch    
                    }
                    else
                    {
                        throw new Exception(
                            "ERROR: Can't create the connection witht the parameter NULL");
                    }
                } //if
                else
                {
                    throw new Exception("Error to parse the enum  object from a string");
                }
            }//for each
            //}                    
        }

        /// <summary>
        /// Method for get all values on a enum object
        /// Usage: var values = EnumUtil.GetValues();
        /// </summary>
        private static class EnumUtil
        {
            public static IEnumerable<T> GetValues<T>()
            {
                return Enum.GetValues(typeof(T)).Cast<T>();
            }
        }

    }


}
