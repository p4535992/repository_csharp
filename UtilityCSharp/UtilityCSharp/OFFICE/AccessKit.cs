﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UtilityCSharp.logging;

namespace com.testLab.util.support
{
    /// <summary>
    /// 2015-04-13
    /// </summary>
    public class AccessKit
    {
        private static SystemLog writer = SystemLog.Instance;
        private static OleDbConnection DBconn = new OleDbConnection();
      

       
        public AccessKit() {
            DBconn = new OleDbConnection();
        }

        #region METODI DI MARCO 1 -> WRITE/READ SU FILE ACCESS (.mdb)

        /// <summary>
        /// METODO CHE VERIFICA L'ESISTENZA DELLA TABELLA E CHE NON SIA VUOTA
        /// </summary>
        /// <param name="nome_tabella"></param>
        /// <returns></returns>
        public static Boolean checkIfTheAcceesTableAlreadyExists(string nome_tabella)
        {
            Boolean b2 = false;
            //SE ESISTE LA TABELLA ALLORA EXISTENCEQUERY E' DIVERSA DA NULL
            string existenceQuery = "SELECT TOP 1 * FROM " + nome_tabella + ";";
            try
            {
                OleDbCommand cmd2 = new OleDbCommand(existenceQuery, DBconn);
                cmd2.ExecuteNonQuery();
                //RESTITUISCE IL PRIMO RECORD DELLA TABELLA
            }
            catch (Exception e) { b2 = true; /*NON ESISTE IL RECORD*/}
            return b2;
        }

     
        /// <summary>
        /// Metodo che apre la connessione con il file ACCESSS
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileNameAccess"></param>
        public static OleDbConnection OpenConnection(string filePath, string fileNameAccess)
        {
            string StringConn = "";
            if (filePath == "")
            {
                StringConn = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + fileNameAccess + ";Jet OLEDB:Database Password=";
            }
            else
            {
                StringConn = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + filePath + "\\" + fileNameAccess + ";Jet OLEDB:Database Password=";
            }
            //Apri la connessione alla database access
            DBconn = new OleDbConnection(StringConn);
            DBconn.Open();
            return DBconn;
        }

        
        /// <summary>
        /// Metodo che apre la connessione con il file ACCESSS
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileNameAccess"></param>
        public static OleDbConnection OpenConnection(string filePath, string fileNameAccess,string encoding)
        {
            string StringConn = "";
            if (encoding != null)
            {
                if(encoding.ToLower().Contains("utf-8")){
                    StringConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+ fileNameAccess +";Extended Properties=\"text;HDR=Yes;FMT=Delimited;CharacterSet=65001;\"";
                }
              
            }else{
                if (filePath == "")
                {
                    StringConn = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + fileNameAccess + ";Jet OLEDB:Database Password=";
                }
                else
                {
                    StringConn = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + filePath + "\\" + fileNameAccess + ";Jet OLEDB:Database Password=";
                }
            }
            //Apri la connessione alla database access
            DBconn = new OleDbConnection(StringConn);
            DBconn.Open();
            return DBconn;
        }
        /// <summary>
        /// Metodo che chiude la connessione con il file ACCESSS
        /// </summary>
        public static void CloseConnection()
        {
            DBconn.Close();
        }

        /// <summary>
        /// Metodo che ALTERA il contenuto di una tabella ACCESS
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="nome_tabella"></param>
        /// <param name="nome_colonna_result"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static DataTable AlterTableOnAccess(DataTable dt, string nome_tabella, string nome_colonna_result, string defaultValue) //here Path is root of file and IsFirstRowHeader is header is there or not
        {
            string query = string.Empty;
            DataTable dataTable = dt;
            OleDbCommand cmd = new OleDbCommand();
            //set cmd settings
            cmd.Connection = DBconn;
            cmd.CommandType = CommandType.Text;
            string alterTable = "ALTER TABLE " + nome_tabella.Replace("'", "") + " ADD ";
            if (dt != null)
            {
                System.Text.StringBuilder sc2 = new System.Text.StringBuilder();
                sc2.Append(alterTable);
                //for (int j = 0; j < dataTable.Columns.Count; j++)
                //{
                //    string record_colonne = dt.Columns[j].ToString();
                //    if (j == dataTable.Columns.Count - 1) { sc2.Append(record_colonne + " VARCHAR(200);"); }
                //    else { sc2.Append(record_colonne + " VARCHAR(200),"); }
                //}
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    string record_colonne = dt.Columns[j].ToString();
                    record_colonne = Regex.Replace(record_colonne, "[^a-zA-Z0-9]", "_");
                    if (j == dataTable.Columns.Count - 1)
                    {
                        if (nome_colonna_result != null) { sc2.Append(record_colonne + " VARCHAR(200)," + nome_colonna_result + " VARCHAR(200) DEFAULT " + defaultValue + ""); }
                        else { sc2.Append(record_colonne + " VARCHAR(200);"); }
                    }
                    else { sc2.Append(record_colonne + " VARCHAR(200),"); }
                }
                cmd.CommandText = sc2.ToString();
            }
            else
            {
                cmd.CommandText = "ALTER TABLE " + nome_tabella.Replace("'", "") + " ADD " + nome_colonna_result + " VARCHAR(200) DEFAULT " + defaultValue + "";
            }
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            return dataTable;
        }


        /// <summary>
        /// Metodo che trasferisce una DataTable in una tabella ACcess
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static void TransferTableToAccess(DataTable dataTable, string nome_tabella) //here Path is root of file and IsFirstRowHeader is header is there or not
        {
            string query = string.Empty;
            //DataTable dataTable = dt;
            //CODICE OPZIONALE UTILE SE SI DEVE CARICARE FILE Csv MOLTO GRANDI DELL?ORDINE DEI GIGA
            //*********************************************************************************************************************************
            //if (IsFirstRowHeader) header 
            //query = @"SELECT * FROM [" + fileNameXML + "]";
            //using (OleDbConnection connection = new OleDbConnection((@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePathCSV + ";Extended Properties=\"Text;HDR=" + header + "\"")))                
            //{                     
            //    using (OleDbCommand command = new OleDbCommand(query, connection))
            //    {
            //        using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            //        {
            //            //connection.Open();
            //            dataTable = new DataTable();                                                                      
            //            //dataTable = dt;
            //            adapter.Fill(dataTable);
            //            //connection.Close();                         
            //           
            //**********************************************************************************************************************************                                                                                                               
            OleDbCommand cmd = new OleDbCommand();
            //set cmd settings
            cmd.Connection = DBconn;
            cmd.CommandType = CommandType.Text;

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                string pieceOfRecord = "";
                string record_colonne;
                string record;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.Text.StringBuilder sc = new System.Text.StringBuilder();
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    record_colonne = dataTable.Columns[j].ColumnName.ToString();
                    record_colonne = Regex.Replace(record_colonne, "[^a-zA-Z0-9]", "_");
                    record_colonne += ",";
                    string piece = (dataTable.Rows[i].ItemArray.GetValue(j) != DBNull.Value) ? "'" + dataTable.Rows[i].ItemArray.GetValue(j).ToString().Replace("'", "''") + "'" : "NULL";
                    //piece = piece.Replace("\'", "");                                                
                    //pieceOfRecord = "'" + dataTable.Rows[i].ItemArray.GetValue(j) + "',";
                    pieceOfRecord = piece + ",";
                    if (j == dataTable.Columns.Count - 1)
                    {
                        pieceOfRecord = pieceOfRecord.Remove(pieceOfRecord.Length - 1);
                        record_colonne = record_colonne.Remove(record_colonne.Length - 1);
                    }//if
                    sb.Append(pieceOfRecord);
                    sc.Append(record_colonne);
                }//foreach columns
                record = sb.ToString();
                //System.Console.WriteLine(record);
                record_colonne = sc.ToString();
                //cmd.CommandText = "INSERT INTO tb_amm (cod_amm,nomeCompletoResponsabile,titoloResponsabile,idAmm,labelAmm,acronimoAmm,categoriaAmm,labelCatAmm)" +
                //                 " VALUES ('" + dataTable.Rows[i].ItemArray.GetValue(0) + "','" + dataTable.Rows[i].ItemArray.GetValue(1) + "','" + dataTable.Rows[i].ItemArray.GetValue(2) +
                //                 "','" + dataTable.Rows[i].ItemArray.GetValue(3) + "','" + dataTable.Rows[i].ItemArray.GetValue(4) + "','" + dataTable.Rows[i].ItemArray.GetValue(5) +
                //                 "','" + dataTable.Rows[i].ItemArray.GetValue(6) + "','" + dataTable.Rows[i].ItemArray.GetValue(7) + "')";                       
                cmd.CommandText = "INSERT INTO " + nome_tabella + "(" + record_colonne + ")" +
                                " VALUES (" + record + ")";
                //System.Console.WriteLine("INSERT INTO " + nome_tabella + "(" + record_colonne + ")" +
                //                " VALUES (" + record + ")");
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e) { }

            }//foreach row                                                       
            cmd.Dispose();
            //        }
            //    }
            //}
            //checks folder exists
            //if (Directory.Exists(filePath))
            //{
            //deletes all folder contents and recreates an empty folder
            //Directory.Delete(filePath, true);
            //Directory.CreateDirectory(filePath);
            //}       

        }//TransferTableToAccess

        /// <summary>
        /// Metodo che scrive il contenuto di una lista di stringhe in un file di default se la filepath è null lo legge dalla cartella di debug
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="filePath"></param>
        /// <param name="listOfRecord"></param>
        public static void TransferListOfStringToAccess(string nome_tabella, List<string> listOfValue, List<string> listOfColumn)
        {
            string query = string.Empty;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = DBconn;
            cmd.CommandType = CommandType.Text;
            try
            {
                string record_colonne = "";
                string record = "";
                //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                //System.Text.StringBuilder sc = new System.Text.StringBuilder();

                for (int i = 0; i < listOfValue.Count; i++)
                {
                    if (listOfValue[i] != null)
                    {
                        listOfValue[i].Trim();
                        if (i < listOfValue.Count - 1)
                        {
                            record += "'" + listOfValue[i].Replace("'", "''") + "',";
                        }
                        else
                        {
                            record += "'" + listOfValue[i].Replace("'", "''") + "'";
                        }
                    }
                    else
                    {
                        if (i < listOfValue.Count - 1)
                        {
                            record += "'" + listOfValue[i] + "',";
                        }
                        else
                        {
                            record += "'" + listOfValue[i] + "'";
                        }

                    }

                }
                //if (record.Contains("'")) 
                //{
                //    record.Replace("'", "''");
                //}
                for (int i = 0; i < listOfColumn.Count; i++)
                {
                    listOfColumn[i] = Regex.Replace(listOfColumn[i], "[^a-zA-Z0-9]", "_");
                    listOfColumn[i] += ",";
                    if (i < listOfColumn.Count - 1)
                    {
                        record_colonne += listOfColumn[i].Replace("'", "") + ",";
                    }
                    else
                    {
                        record_colonne += listOfColumn[i].Replace("'", "");
                    }

                }
                cmd.CommandText = "INSERT INTO " + nome_tabella + "(" + record_colonne + ")" + " VALUES (" + record + ")";
                try
                {
                    cmd.ExecuteNonQuery();
                    //writer.WriteToLog("...INSERIMENTO RIUSCITO", "OUT");
                    writer.WriteToLog("...INSERIMENTO RIUSCITO", "OUT");
                }
                catch (Exception e)
                {
                    writer.WriteToLog("...INSERIMENTO NON RIUSCITO", "ERR");
                }
            }
            catch (Exception e)
            {
                writer.WriteToLog("Error in fase di creazione della query INSERT INTO", "ERR");
            }
            cmd.Dispose();
        }

        /// <summary>
        /// Setta un nuovo valore per l'elemento del record in esame in una determinata colonna
        /// </summary>
        /// <param name="nome_tabella"></param>
        /// <param name="nome_colonna_set"></param>
        /// <param name="nome_colonna_where"></param>
        /// <param name="nome_record"></param>
        /// <param name="newValue"></param>
        public static void UpdateTheValueOfSpecificColumnOfSpecificTableOnAccess(string nome_tabella, string nome_colonna_set, string nome_colonna_where, string nome_record, string newValue)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = DBconn;
            cmd.CommandType = CommandType.Text;
            string query = "";
            if (nome_colonna_where == null)
            {
                query = "UPDATE " + nome_tabella + " " +
                                "SET " + nome_tabella + "." + nome_colonna_set + " = '" + newValue + "';";
            }
            else
            {
                query = "UPDATE " + nome_tabella + " " +
                                "SET " + nome_tabella + "." + nome_colonna_set + " = '" + newValue + "' " +
                                "WHERE " + nome_colonna_where + " = '" + nome_record + "';";
            }
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
        }//UpdateTheValueOfSpecificColumnOfSpecificTableOnAccess

        /// <summary>
        /// Metodo che crea una tabella ACCESS
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static void CreateTableOnToAccessFile(string nameTableAccess, Boolean erase) //here Path is root of file and IsFirstRowHeader is header is there or not
        {
            //set cmd settings
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = DBconn;
            cmd.CommandType = CommandType.Text;
            try
            {
                cmd.CommandText = "CREATE TABLE " + nameTableAccess.Replace("'", "") + ";";
                cmd.ExecuteNonQuery();
            }
            catch (System.Data.OleDb.OleDbException oe)
            {
                //E' necessario per evitare di cancellare la tabella
                //per riprendere una precedente ricerca
                if (erase == true)
                {
                    //Tabella 'tb_amm' già esistente. ex.Message()
                    if (oe.Message.Contains("Tabella " + nameTableAccess.Replace("'", "") + " già esistente"))
                    {
                        cmd.CommandText = "DROP TABLE " + nameTableAccess.Replace("'", "") + ";";
                        cmd.ExecuteNonQuery();
                        erase = false;
                        CreateTableOnToAccessFile(nameTableAccess, erase);

                    }//tabella già esistente
                    if (erase == true)
                    {
                        cmd.CommandText = "DROP TABLE " + nameTableAccess.Replace("'", "") + ";";
                        cmd.ExecuteNonQuery();
                        erase = false;
                        CreateTableOnToAccessFile(nameTableAccess, erase);
                    }
                } //erase true                 
            } //catch                         
        }//CreateTableOnToAccessFile

        /// <summary>
        /// Method for create a access table froma object datatable with the columns
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static void CreateTableOnToAccessFile(string nameTableAccess,DataTable dt, Boolean erase) //here Path is root of file and IsFirstRowHeader is header is there or not
        {
            //set cmd settings
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = DBconn;
            cmd.CommandType = CommandType.Text;
            try
            {
                cmd.CommandText = "CREATE TABLE " + nameTableAccess.Replace("'", "") + ";";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (System.Data.OleDb.OleDbException oe)
            {
                //E' necessario per evitare di cancellare la tabella
                //per riprendere una precedente ricerca
                if (erase == true)
                {
                    //Tabella 'tb_amm' già esistente. ex.Message()
                    if (oe.Message.Contains("Tabella " + nameTableAccess.Replace("'", "") + " già esistente"))
                    {
                        cmd.CommandText = "DROP TABLE " + nameTableAccess.Replace("'", "") + ";";
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        erase = false;
                        CreateTableOnToAccessFile(nameTableAccess, erase);

                    }//tabella già esistente
                    if (erase == true)
                    {
                        cmd.CommandText = "DROP TABLE " + nameTableAccess.Replace("'", "") + ";";
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        erase = false;
                        CreateTableOnToAccessFile(nameTableAccess, erase);
                    }
                } //erase true                 
            } //catch  
            
            //ADD COLUMN
            try {
                cmd = new OleDbCommand();
                cmd.Connection = DBconn;
                cmd.CommandType = CommandType.Text;
                string cStr = "ALTER TABLE "+nameTableAccess.Replace("'", "")+" ADD COLUMN \r\n";
                for(int i = 0; i < dt.Columns.Count; i++)
                {
                    cStr += dt.Columns[i].ColumnName + " Text ";
                    if (i < dt.Columns.Count - 1) {
                        cStr += ", \r\n";
                    }
                }
                cmd.CommandText = cStr;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception e) 
            { 
            }
        }//CreateTableOnToAccessFile

        /// <summary>
        /// Method for create a access table froma object datatable with the columns
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static void CreateTableOnToAccessFile(string nameTableAccess, string[] columns, Boolean erase) //here Path is root of file and IsFirstRowHeader is header is there or not
        {
            //set cmd settings
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = DBconn;
            cmd.CommandType = CommandType.Text;
            try
            {
                cmd.CommandText = "CREATE TABLE " + nameTableAccess.Replace("'", "") + ";";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (System.Data.OleDb.OleDbException oe)
            {
                //E' necessario per evitare di cancellare la tabella
                //per riprendere una precedente ricerca
                if (erase == true)
                {
                    //Tabella 'tb_amm' già esistente. ex.Message()
                    if (oe.Message.Contains("Tabella " + nameTableAccess.Replace("'", "") + " già esistente"))
                    {
                        cmd.CommandText = "DROP TABLE " + nameTableAccess.Replace("'", "") + ";";
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        erase = false;
                        CreateTableOnToAccessFile(nameTableAccess, erase);

                    }//tabella già esistente
                    if (erase == true)
                    {
                        cmd.CommandText = "DROP TABLE " + nameTableAccess.Replace("'", "") + ";";
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        erase = false;
                        CreateTableOnToAccessFile(nameTableAccess, erase);
                    }
                } //erase true                 
            } //catch  

            //ADD COLUMN
            try
            {
                cmd = new OleDbCommand();
                cmd.Connection = DBconn;
                cmd.CommandType = CommandType.Text;
                string cStr = "ALTER TABLE " + nameTableAccess.Replace("'", "") + " ADD COLUMN \r\n";
                for (int i = 0; i < columns.Length; i++)
                {
                    cStr += columns[i] + " Text ";
                    if (i < columns.Length - 1)
                    {
                        cStr += ", \r\n";
                    }
                }
                cmd.CommandText = cStr;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception e)
            {
            }
        }//CreateTableOnToAccessFile

        /// <summary>
        /// Method for create a access table froma object datatable with the columns
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static void CreateTableOnToAccessFile(string nameTableAccess, Dictionary<string[], Dictionary<OleDbType[], int[]>> dictionary, Boolean erase) //here Path is root of file and IsFirstRowHeader is header is there or not
        {
            //set cmd settings
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = DBconn;
            cmd.CommandType = CommandType.Text;
            string sql="";
            try
            {
                cmd.CommandText = "CREATE TABLE " + nameTableAccess.Replace("'", "") + ";";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (System.Data.OleDb.OleDbException oe)
            {
                //E' necessario per evitare di cancellare la tabella
                //per riprendere una precedente ricerca
                if (erase == true)
                {
                    //Tabella 'tb_amm' già esistente. ex.Message()
                    if (oe.Message.Contains("Tabella " + nameTableAccess.Replace("'", "") + " già esistente"))
                    {
                        cmd.CommandText = "DROP TABLE " + nameTableAccess.Replace("'", "") + ";";
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        erase = false;
                        CreateTableOnToAccessFile(nameTableAccess, erase);

                    }//tabella già esistente
                    if (erase == true)
                    {
                        cmd.CommandText = "DROP TABLE " + nameTableAccess.Replace("'", "") + ";";
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        erase = false;
                        CreateTableOnToAccessFile(nameTableAccess, erase);
                    }
                } //erase true                 
            } //catch  

            //ADD COLUMN
            try
            {
                //cmd = new OleDbCommand();
                //cmd.Connection = DBconn;
                //cmd.CommandType = CommandType.Text;
                sql = "ALTER TABLE [" + nameTableAccess.Replace("'", "") + "] ADD COLUMN \r\n ";
                                  
                List<KeyValuePair<string[],Dictionary<OleDbType[],int[]>>> list1 = dictionary.ToList();         
                List<KeyValuePair<OleDbType[],int[]>> list2 = list1[0].Value.ToList();

                string[] ss = list1[0].Key[0].ToString().Split('\t');
                if(ss.Length == list2[0].Key.Length+1)
                {                   
                        ss = list1[0].Key[0].ToString().Split('\t');
                        for (int g = 0; g < ss.Length; g++)
                        {
                            if (g < ss.Length - 1)
                            {
                                sql += "[" + ss[g] + "] " + list2[0].Key[g];
                                    //" (" + list2[0].Value[g] + ") "; //Not work ????
                                if (g < ss.Length - 2)
                                {
                                    sql += ",\r\n ";
                                }
                            }
                        }                                         
                }      
      
                sql += "\r\n";                                     
                //INSERT INTO
                // Stores the strSQL insert statement into a command object
                //cmd.CommandText = sql;
                cmd =new OleDbCommand(sql, DBconn);
                // Executes the insert statement
                cmd.ExecuteNonQuery();
               // cmd.Parameters.Clear();
                cmd.Dispose();         
            }
            catch (Exception e)
            {
            }
        }//CreateTableOnToAccessFile

        /// <summary>
        /// Method for create a access table froma object datatable with the columns
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static void CreateTableOnToAccessFile(string nameTableAccess, string[] column, OleDbType[] dbTypes,bool erase) //here Path is root of file and IsFirstRowHeader is header is there or not
        {
            //set cmd settings
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = DBconn;
            cmd.CommandType = CommandType.Text;
            string sql = "";
            try
            {
                cmd.CommandText = "CREATE TABLE " + nameTableAccess.Replace("'", "") + ";";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (System.Data.OleDb.OleDbException oe)
            {
                //E' necessario per evitare di cancellare la tabella
                //per riprendere una precedente ricerca
                if (erase == true)
                {
                    //Tabella 'tb_amm' già esistente. ex.Message()
                    if (oe.Message.Contains("Tabella " + nameTableAccess.Replace("'", "") + " già esistente"))
                    {
                        cmd.CommandText = "DROP TABLE " + nameTableAccess.Replace("'", "") + ";";
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        erase = false;
                        CreateTableOnToAccessFile(nameTableAccess, erase);

                    }//tabella già esistente
                    if (erase == true)
                    {
                        cmd.CommandText = "DROP TABLE " + nameTableAccess.Replace("'", "") + ";";
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        erase = false;
                        CreateTableOnToAccessFile(nameTableAccess, erase);
                    }
                } //erase true                 
            } //catch  


            //ADD COLUMN
            try
            {
                //cmd = new OleDbCommand();
                //cmd.Connection = DBconn;
                //cmd.CommandType = CommandType.Text;
                sql = "ALTER TABLE [" + nameTableAccess.Replace("'", "") + "] ADD COLUMN \r\n ";
                if (string.IsNullOrEmpty(column[column.Length - 1]))
                {
                    column = column.Take(column.Count() - 1).ToArray();
                }
                if (column.Length == dbTypes.Length)
                {
                    for (int i = 0; i < column.Length; i++ )
                    {                   
                        sql += "[" + column[i] + "] " + dbTypes[i];
                        //" (" + list2[0].Value[g] + ") "; //Not work ????
                        if (i < column.Length - 1)
                        {
                            sql += ",\r\n ";
                        }                        
                    }
                }
                else {
                    throw new Exception("ERROR: Make sure the array of the column and array of the dbTypes have the smae number of argument");
                }

                sql += "\r\n";
                //INSERT INTO
                // Stores the strSQL insert statement into a command object
                //cmd.CommandText = sql;
                cmd = new OleDbCommand(sql, DBconn);
                // Executes the insert statement
                cmd.ExecuteNonQuery();
                // cmd.Parameters.Clear();
                cmd.Dispose();
            }
            catch (Exception e)
            {
                throw new Exception("ERROR: check the input arrays");
            }
        }//CreateTableOnToAccessFile

        /// <summary>
        /// Metodo che importa il contenuto di una tabella access a un DataTable
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileNameAccess"></param>
        /// <param name="nameTableAccess"></param>
        /// <param name="nome_colonna"></param>
        /// <returns></returns>
        public static DataTable importFromAccessToDatatableWithDataAdapter(string nameTableAccess, List<String> columns, string nome_colonna_result, string valueColumn)
        {
            DataTable dt = new DataTable();
            string  sql  = "";
            //static List<ObjectNamespace> namespaceList = new System.Collections.Generic.List<ObjectNamespace>();
            if (columns == null) { 
                sql = "SELECT * FROM " + nameTableAccess + " "; 
            }
            else
            {

                string queryStringColonne = "";
                for (int i = 0; i < columns.Count; i++)
                {
                    if (i == columns.Count - 1) { queryStringColonne = queryStringColonne + columns[i]; }
                    else { queryStringColonne = queryStringColonne + columns[i] + ","; }
                }
                
                //string StringConn = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + filePath + "\\" + fileNameAccess + ";Jet OLEDB:Database Password=";
                //OleDbConnection DBconn = new OleDbConnection(StringConn);                      
                //OleDbDataAdapter dataAdap= new OleDbDataAdapter("SELECT * FROM tb_amm",DBconn);
                //OleDbDataAdapter dataAdap = new OleDbDataAdapter("SELECT "+nome_colonna+" FROM "+nome_tabella+"", DBconn);
                sql = "SELECT " + queryStringColonne + " FROM " + nameTableAccess + " ";

                if ((valueColumn != "" && valueColumn != null) || nome_colonna_result != null)
                {
                    sql = sql + " WHERE " + nome_colonna_result + "='" + valueColumn + "';";
                }

            }

            OleDbDataAdapter dataAdap = new OleDbDataAdapter(sql, DBconn);
            //La seguente line di codice riempie un dataTable con il contenuto di una tabella ACCESS
            dataAdap.Fill(dt);
            return dt;
        }

        #endregion

       

       

        #region Database METHOD
        /*
		 * Generates the create table command using the schema table, and
		 * runs it in the sql database.
		 */
        public static bool CreateTableInSQLSevrerDatabase(DataTable dtSchemaTable,string filePath, string fileNameAccess, string tableName)
        {
            try
            {
               OleDbCommand cmd ;
               string ctStr="" ;
               bool error = false;
                // Generates the create table command.
                // The first column of schema table contains the column names.
                // The data type is nvarcher(4000) in all columns.
                try
                {
                    ctStr = "CREATE TABLE [" + fileNameAccess + "].[" + tableName + "]\r\n ";

                    //ctStr += "(\r\n";
                    //for (int i = 0; i < dtSchemaTable.Rows.Count; i++)
                    //{
                    //    ctStr += "  [" + dtSchemaTable.Rows[i][j].ToString().Replace("'", "") + "] [nvarchar](200) NULL";
                    //    if (i < dtSchemaTable.Rows.Count)
                    //    {
                    //        ctStr += ",";
                    //    }
                    //    ctStr += "\r\n";
                    //}
                    //ctStr += ")";
                    cmd = new OleDbCommand();
                    cmd.Connection = DBconn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = ctStr;
                    cmd.ExecuteNonQuery();
                }catch(Exception e){
                    if(!e.Message.ToLower().Contains("already exists")){
                       error = true;
                    }
                }

                if(error == false){
                    ctStr = "";

                    ctStr += @"ALTER TABLE "+tableName+" ";
                    for(int i=0; i < dtSchemaTable.Columns.Count; i++){
                        ctStr += " ADD " + dtSchemaTable.Columns[i].ColumnName + " Text(50) ";
                           // ctStr += "[nvarchar](200) NULL";
                     if ( i < dtSchemaTable.Columns.Count-1)
                        {
                            ctStr += ",";
                        }
                        ctStr += "\r\n";
                    }
                }


                // You can check the sql statement if you want:
                //MessageBox.Show(ctStr);


                // Runs the sql command to make the destination table.
                //string connectionString;
                //if (filePath == "")
                //{
                //    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + fileNameAccess + ";Jet OLEDB:Database Password=";
                //}
                //else
                //{
                //    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + filePath + "\\" + fileNameAccess + ";Jet OLEDB:Database Password=";
                //}

                //SqlConnection conn = new SqlConnection(connectionString);
                //SqlCommand command = conn.CreateCommand();
                //command.CommandText = ctStr;
                //conn.Open();
                //command.ExecuteNonQuery();
                //conn.Close();


                cmd = new OleDbCommand();
                cmd.Connection = DBconn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = ctStr;
                cmd.ExecuteNonQuery();

                return true;

            }
            catch (Exception e)
            {               
                return false;
            }
        }
        #endregion

        //NUOVI METODI
        public static void insertIntoAccessTable(string nameTableAccess, string[] column, OleDbType[] dbTypes, int[] sizes,List<string> lineToInsert,char separator,bool firstRowColumn) 
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = DBconn;
            string sql2 = "";
            string sql = "";
                     
                if (firstRowColumn == true) { lineToInsert.RemoveAt(0); } //...jumpt the column row
                sql += "INSERT INTO " + nameTableAccess + " ( \r\n";
                
                //Clean up if array ends with the separator
                if(string.IsNullOrEmpty(column[column.Length-1])){
                    column = column.Take(column.Count() - 1).ToArray();
                }               

                if (column.Length == dbTypes.Length && column.Length == sizes.Length)
                { 
                    for (int i = 0; i < column.Length; i++)
                    {
                        sql += column[i] + "";
                        if (i < column.Length - 1)
                        {
                            sql += ",\r\n";
                        }
                    }
                }
                else
                {
                    throw new Exception("Wrong input !!!!!");
                }

                sql += "\r\n ) VALUES (\r\n";
                //SET VALUE sql query
                for (int j = 0; j < column.Length; j++)
                {
                    string param = "@" + j.ToString();
                    sql += param;
                    if (j < column.Length - 1)
                    {
                        sql += ",\r\n";
                    }
                    else { break; }
                }
                sql += "\r\n)\r\n";
              
                foreach (string s in lineToInsert)
                {                
                    string[] value = s.Split(separator);

                    if (string.IsNullOrEmpty(value[value.Length - 1]) && value.Length  > column.Length)
                    {
                        value = value.Take(value.Count() - 1).ToArray();
                    }
                    if(column.Length == value.Length){
                        sql2 = "";
                                                                                        
                        //SET parameter                   
                        for (int f = 0; f < column.Length; f++)
                        {                           
                            OleDbParameter param = new OleDbParameter("@" + f.ToString(), dbTypes[f], sizes[f]);
                            param.Value = value[f];
                            if (string.IsNullOrEmpty(param.Value.ToString()))
                            {
                                param.Value = DBNull.Value;
                            }
                            else
                            {
                                //..add control for decimal
                                if (dbTypes[f].ToString().ToLower().Contains("decimal"))
                                {
                                    param.Scale = 2;
                                    param.Precision = 0;
                                }
                            }
                            cmd.Parameters.Add(param);
                            //cmd.Parameters.Add(new OleDbParameter("@"+k.ToString(), pair.Value.ToList()[0].Key[k], pair.Value.ToList()[0].Value[k])).Value = pair.Key[k];
                            //test.Add("@"+k.ToString()+" -> "+pair.Value.ToList()[0].Key[k] + "(" +pair.Value.ToList()[0].Value[k]+") = "+pair.Key[k]+""); 
                            if (f == column.Length - 1) { break; }
                        }
                        sql2 = sql + sql2 +";";

                        //....Just for test
                        //string msg = "";
                        //showOledbParameters(cmd, out msg);

                        //INSERT INTO
                        // Stores the strSQL insert statement into a command object
                        cmd.CommandText = sql2;
                        // Executes the insert statement
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }else
                    {
                        throw new Exception("Wrong input !!!!!");
                    }
                                          
                }  //foreach string in the fileToInsert   
                     
            cmd.Dispose();

        }//insertIntoAccessTable

        public static void insertIntoAccessTable(string nameTableAccess, string[] column, OleDbType[] dbTypes, int[] sizes,string[] rows)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = DBconn;
            string sql2 = "";
            string sql = "";

            sql += "INSERT INTO " + nameTableAccess + " ( \r\n";

            //Clean up if array ends with the separator
            if (string.IsNullOrEmpty(column[column.Length - 1]))
            {
                column = column.Take(column.Count() - 1).ToArray();
            }

            if (column.Length == dbTypes.Length && column.Length == sizes.Length)
            {
                for (int i = 0; i < column.Length; i++)
                {
                    sql += column[i] + "";
                    if (i < column.Length - 1)
                    {
                        sql += ",\r\n";
                    }
                }
            }
            else
            {
                throw new Exception("Wrong input !!!!!");
            }

            sql += "\r\n ) VALUES (\r\n";
            //SET VALUE sql query
            for (int j = 0; j < column.Length; j++)
            {
                string param = "@" + j.ToString();
                sql += param;
                if (j < column.Length - 1)
                {
                    sql += ",\r\n";
                }
                else { break; }
            }
            sql += "\r\n)\r\n";

            foreach (string s in rows)
            {                             
                if (column.Length == rows.Length)
                {
                    sql2 = "";

                    //SET parameter                   
                    for (int f = 0; f < column.Length; f++)
                    {
                        OleDbParameter param = new OleDbParameter("@" + f.ToString(), dbTypes[f], sizes[f]);
                        param.Value = rows[f];
                        if (string.IsNullOrEmpty(param.Value.ToString()))
                        {
                            param.Value = DBNull.Value;
                        }
                        else
                        {
                            //..add control for decimal
                            if (dbTypes[f].ToString().ToLower().Contains("decimal"))
                            {
                                param.Scale = 2;
                                param.Precision = 0;
                            }
                        }
                        cmd.Parameters.Add(param);
                        //cmd.Parameters.Add(new OleDbParameter("@"+k.ToString(), pair.Value.ToList()[0].Key[k], pair.Value.ToList()[0].Value[k])).Value = pair.Key[k];
                        //test.Add("@"+k.ToString()+" -> "+pair.Value.ToList()[0].Key[k] + "(" +pair.Value.ToList()[0].Value[k]+") = "+pair.Key[k]+""); 
                        if (f == column.Length - 1) { break; }
                    }
                    sql2 = sql + sql2 + ";";

                    //....Just for test
                    //string msg = "";
                    //showOledbParameters(cmd, out msg);

                    //INSERT INTO
                    // Stores the strSQL insert statement into a command object
                    cmd.CommandText = sql2;
                    // Executes the insert statement
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
                else
                {
                    throw new Exception("Wrong input !!!!!");
                }

            }  //foreach string in the fileToInsert   

            cmd.Dispose();

        }//insertIntoAccessTable


        public static void showOledbParameters(OleDbCommand cmd,out string msg) 
        {
            string message ="";
            for (int i = 0; i < cmd.Parameters.Count; i++)
            {
                message += cmd.Parameters[i].ToString() + " -> " + cmd.Parameters[i].OleDbType + "(" + cmd.Parameters[i].Size + ") = " + cmd.Parameters[i].Value + "" + "";
                message += "\r\n SCALE:" + cmd.Parameters[i].Scale + ", PRECISION:" + cmd.Parameters[i].Precision + "\r\n";
            }
            //return null;
            msg = message;
        }

        public static string setTableName(string tableName){
            tableName = Regex.Replace(tableName, @"(\s+)|[(]|[)]", "_");
            tableName = Regex.Replace(tableName, @"([_]+)|[(]|[)]", "_");
            return tableName;
        }

        /// <summary>
        /// Metodo per l'UPDATE dei valori della tabella access (NOT WORK)
        /// </summary>
        /// <param name="nome_tabella"></param>
        /// <param name="nome_colonna"></param>
        /// <param name="oldString"></param>
        /// <param name="newString"></param>
        private void ReplaceTheValueOfSpecificColumnOfSpecificTableOnAccess(string nome_tabella, string nome_colonna, string oldString, string newString)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = DBconn;
            cmd.CommandType = CommandType.Text;
            string query = "UPDATE " + nome_tabella + " " +
                            "SET " + nome_tabella + "." + nome_colonna + " = REPLACE (" + nome_colonna + ",\"" + oldString + "\",\"" + newString + "\");";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
        }//ReplaceTheValueOfSpecificColumnOfSpecificTableOnAccess
    }
}
