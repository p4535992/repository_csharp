﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.testLab.util.support
{
    /// <summary>
    /// 2015-04-13
    /// </summary>
    class ExcelKit
    {
        private static OleDbConnection ExcelConn = new OleDbConnection();
        private static int tentativi = 0;
        
        //WRITE/READ SU FILE Excel (.xlsx)

        public static void OpenConnectionToExcelFile(string fileName, string filePath)
        {
            // connect to xls file
            // NOTE: it will be created if not exists
            try
            {
                //conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" +
                //    "Data Source=" + Application.StartupPath + "\\test.xlsx;" +
                //    "Extended Properties=Excel 12.0 Xml");
                //conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" +
                //    "Data Source="+ filePath+fileName + ".xlsx;" +
                //    "Extended Properties=Excel 12.0 Xml");
                ExcelConn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" +
                    "Data Source=" + filePath + fileName + ";" +
                    "Extended Properties=Excel 12.0 Xml");
                ExcelConn.Open();
            }
            catch
            {
                try
                {
                    //conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.14.0;" +
                    //    "Data Source=" + Application.StartupPath + "\\test.xlsx;" +
                    //    "Extended Properties=Excel 14.0 Xml");
                    ExcelConn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.14.0;" +
                        "Data Source=" + filePath + fileName + ";" +
                        "Extended Properties=Excel 14.0 Xml");
                    ExcelConn.Open();
                }
                catch
                {
                    try
                    {
                        //conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.15.0;" +
                        //    "Data Source=" + Application.StartupPath + "\\test.xlsx;" +
                        //    "Extended Properties=Excel 15.0 Xml");
                        ExcelConn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.15.0;" +
                           "Data Source=" + filePath + fileName + ";" +
                           "Extended Properties=Excel 15.0 Xml");
                        ExcelConn.Open();
                    }
                    catch
                    {
                    }
                }
                //"provider=Microsoft.Jet.OLEDB.4.0;Data Source='informations.xls';Extended Properties=Excel 8.0;"
            }
        }

        public static void CloseConnectionToExcelFile()
        {
            ExcelConn.Close();
        }

        public static List<string> ReadFromExcel(string fileName, string filePath, string columnName, string tableName)
        {
            List<string> listWebsite = new List<string>();
            OleDbDataAdapter adapter;
            DataTable dt = new DataTable();

            OpenConnectionToExcelFile(fileName, filePath);

            // create a sheet "Sheet1" if not exists
            // NOTE: no "Id" field needed
            // WARNING: spaces in sheet's name are supported if names are in [] (automatically replace with _)
            // spaces in column names NOT supported with OleDbCommandBuilder!
            try
            {
                //string cmdText = "CREATE TABLE [Sheet 1] (text_col MEMO, int_col INT)";
                //string cmdText = "SELECT website FROM [MOBILE$] ";
                //using (OleDbCommand cmd = new OleDbCommand(cmdText, conn))
                //{
                //    cmd.ExecuteNonQuery();
                //}
                //OPPURE USI ADAPTER
                adapter = new OleDbDataAdapter("SELECT " + columnName + " FROM [" + tableName + "$]", ExcelConn);
                new OleDbCommandBuilder(adapter);
                //dt = new DataTable();
                adapter.Fill(dt);
            }
            catch { }

            // MOSTRA TUTTE LE TABELLA /FOGLI DEL FILE Excel
            //dt = conn.GetSchema("TABLES");

            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                //    if (dt.Rows[i].ItemArray[dt.Columns.IndexOf("TABLE_TYPE")].ToString() == "TABLE" &&
                //        !dt.Rows[i].ItemArray[dt.Columns.IndexOf("TABLE_NAME")].ToString().Contains("$"))
                //    {
                //        comboBox1.Items.Add(dt.Rows[i].ItemArray[dt.Columns.IndexOf("TABLE_NAME")]);
                //    }
                string website = dt.Rows[i].ItemArray[dt.Columns.IndexOf(columnName)].ToString();
                listWebsite.Add(website);
            }
            CloseConnectionToExcelFile();
            return listWebsite;
        }

        public static void WriteToExcel(string fileName, string filePath, List<string> columnsName, string columnName, string tableName, List<string> multipleValues, string singleValue, Boolean uniqueCommand)
        {
            OpenConnectionToExcelFile(fileName, filePath);
            try
            {
                string column = null;
                string value = null;
                if (columnsName == null)
                {
                    column = columnName;
                }
                else
                {
                    for (int i = 0; i < columnsName.Count(); i++)
                    {
                        if (i < columnsName.Count() - 1)
                        {
                            column += columnsName[i] + ",";
                        }
                        else
                        {
                            column += columnsName[i];
                        }
                    }
                }

                if (multipleValues == null)
                {
                    //"provider=Microsoft.Jet.OLEDB.4.0;Data Source='informations.xls';Extended Properties=Excel 8.0;"
                    //sql = "Update [Sheet1$A1:A15] SET A15 = 'DesiredNumber'"; 
                    //Select * from [Sheet1$A1:B10] 
                    //sql = "Insert into [Sheet1$] (Id,name) values('5','e')";
                    //string cmdText = "UPDATE [" + tableName + "$A1:B1] SET B1 = '" + singleValue + "';";  
                    //WORK
                    string cmdText = "INSERT INTO [" + tableName + "$](" + column + ") VALUES (" + singleValue.Replace("'", "''").ToString() + ")";
                    //string cmdText = "INSERT INTO [" + tableName + "$](" + column + ") VALUES (?var1,?var2)";              
                    using (OleDbCommand cmd = new OleDbCommand(cmdText, ExcelConn))
                    {
                        cmd.CommandText = cmdText;
                        //cmd.Parameters.Add("?var1", OleDbType.VarChar).Value = singleValue.Replace("'", "''").ToString();
                        //cmd.Parameters.Add("?var2", OleDbType.VarChar).Value = singleValue.Replace("'", "''").ToString();
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
                else
                {
                    string cmdText = "";
                    if (uniqueCommand == true)
                    {
                        for (int i = 0; i < multipleValues.Count(); i++)
                        {
                            if (i < multipleValues.Count() - 1)
                            {
                                value += multipleValues[i].Replace("'", "''") + "\',\'";
                            }
                            else
                            {
                                value += multipleValues[i].Replace("'", "''");
                            }
                        }

                        //string cmdText = "INSERT INTO [" + tableName + "$](" + column + ") VALUES (\'" + multipleValues[0] + "\',\'" + multipleValues[1] + "\')";
                        cmdText = "INSERT INTO [" + tableName + "$](" + column + ") VALUES (\'" + value + "\')";
                        using (OleDbCommand cmd = new OleDbCommand(cmdText, ExcelConn))
                        {
                            //cmd.Parameters.Add("?var1", OleDbType.VarChar).Value = var1.ToString();
                            //cmd.Parameters.Add("?var2", OleDbType.VarChar).Value = var2.ToString();
                            cmd.CommandText = cmdText;
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                        }
                    }
                    else
                    {
                        cmdText = "INSERT INTO [" + tableName + "$](" + column + ") VALUES (";
                        //List<string> listvar = new List<string>();
                        for (int i = 0; i < columnsName.Count(); i++)
                        {
                            if (i < columnsName.Count() - 1)
                            {
                                cmdText += "?,";
                            }
                            else
                            {
                                cmdText += "?)";
                            }
                            //listvar.Add(multipleValues[i].Replace("'", "''"));
                        }
                        //cmdText = "INSERT INTO [" + tableName + "$](" + column + ") VALUES (?,?)";

                        //string var1 = multipleValues[0].Replace("'", "''");
                        //string var2 = multipleValues[1].Replace("'", "''");
                        //cmd.CommandText = cmdText;
                        using (OleDbCommand cmd = new OleDbCommand(cmdText, ExcelConn))
                        {
                            for (int i = 0; i < columnsName.Count(); i++)
                            {
                                if (multipleValues[i].Length > 100)
                                {
                                    multipleValues[i] = multipleValues[i].Substring(0, 50) + "...";
                                }
                                cmd.Parameters.Add(columnsName[i], OleDbType.VarChar).Value = multipleValues[i].Substring(0);
                            }
                            //cmd.Parameters.Add("website", OleDbType.VarChar).Value = var1.ToString().Substring(1,var1.Length);
                            //cmd.Parameters.Add("result", OleDbType.VarChar).Value = var2.ToString().Substring(1, var2.Length);
                            cmd.CommandText = cmdText;
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                        }


                    }

                    ////cmd.CommandText = cmdText;
                    //using (OleDbCommand cmd = new OleDbCommand(cmdText, ExcelConn))
                    //{
                    //    cmd.Parameters.Add("?var1", OleDbType.VarChar).Value = var1.ToString();
                    //    cmd.Parameters.Add("?var2", OleDbType.VarChar).Value = var2.ToString();
                    //    cmd.CommandText = cmdText;
                    //    cmd.ExecuteNonQuery();
                    //    cmd.Dispose();
                    //}

                }
            }
            catch (Exception e)
            {
                if (e.Message.ToUpper().Contains("CONNECT"))
                {
                    if (tentativi == 2)
                    {
                        WriteToExcel(fileName, filePath, columnsName, columnName, tableName, multipleValues, singleValue, uniqueCommand);
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            finally
            {
                tentativi = 0;
                CloseConnectionToExcelFile();
            }
        }


        


    }
}
