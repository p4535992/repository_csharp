﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Common.Logging;

namespace UtilityCSharp.db
{
    public class SqlUtilities
    {
        private static readonly ILog Log = LogManager.GetLogger<SqlUtilities>();
        public static DataTable SelectSqlserver(string tableName, string strConnDb)
        {
            // Create a String to hold the database connection string.
            // NOTE: Put in a real database connection string here or runtime won't work
            //string sdwConnectionString = @"Data Source = ServerName; user Id=UserName; password=P@sswd!; Initial Catalog = DatabaseName;";
 
            // Create a connection
            SqlConnection sdwDbConnection = new SqlConnection(strConnDb);
 
            // Open the connection
            sdwDbConnection.Open();
 
            // Create a String to hold the query.
            string query = string.Concat("SELECT * FROM ",tableName);
 
            // Create a SqlCommand object and pass the constructor the connection string and the query string.
            SqlCommand queryCommand = new SqlCommand(query, sdwDbConnection);
 
            // Use the above SqlCommand object to create a SqlDataReader object.
            SqlDataReader queryCommandReader = queryCommand.ExecuteReader();
 
            // Create a DataTable object to hold all the data returned by the query.
            DataTable dataTable = new DataTable();
 
            // Use the DataTable.Load(SqlDataReader) function to put the results of the query into a DataTable.
            dataTable.Load(queryCommandReader);
 
            // Example 1 - Print your  Column Headers
            String columns = string.Empty;
            foreach (DataColumn column in dataTable.Columns)
            {
                columns += column.ColumnName + " | ";
            }
            Console.WriteLine(columns);
 
            // Example 2 - Print the first 10 row of data
            int topRows = 10;
            for (int i = 0; i < topRows; i++)
            {
                String rowText = string.Empty;
                foreach (DataColumn column in dataTable.Columns)
                {
                    rowText += dataTable.Rows[i][column.ColumnName] + " | ";
                }
                Console.WriteLine(rowText);
            }
 
            // Close the connection
            sdwDbConnection.Close();
            return dataTable;
        }//SelectSqlserver

        public static Object SelectSpecificColumnSqlserver(string column, string strConnDb) 
        {
            object result;
            //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringNameInWebConfig"].ConnectionString);
            SqlConnection conn = new SqlConnection(strConnDb);
            SqlCommand cmd = new SqlCommand("Stored_Proc_Name", conn) {CommandType = CommandType.StoredProcedure};
            cmd.Parameters.Add(new SqlParameter("@ParameterNameIfNeeded", column));
            conn.Open();
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //result = rdr.Cast()
            result = rdr.Cast<IDataRecord>()
            .Select(dr => new { Col_3_index_Value = dr.GetString(3), Col_7_Index_Value = dr.GetString(7) }).ToList();
            // The above statement selects the desired columns by index the assigned name can be whatever you like.
            return result;
        }//SelectSpecificColumnSqlserver


        /// <summary>
        /// Metodo che legge un file csv di bilancio di SIOPE e ne carica le informazioni su SQL Server
        /// </summary>
        /// <param name="pathFileName"></param>
        /// <param name="separator"></param>
        /// <param name="strConnDb"></param>
        public void readCSVAndLoadToSQLServer_1(string pathFileName, string separator,string strConnDb)
         {
             //GUID DI RIFERIMENTO DEI BILANCI
             Guid g;
             int counter = 0;
             string row;
             string[] columns;
             //List<string> values = new List<string>();
             // Read the file and display it line by line.
             using (SqlConnection connection = new SqlConnection(strConnDb))
             {
                 connection.Open();
                 using (var file = new StreamReader(pathFileName))
                 {

                     bool firstRow = true;
                     bool setRecordBilanci = true;
                     SqlCommand command;
                     string ctStr;
                     while ((row = file.ReadLine()) != null)
                     {
                         //JUMP THE FIRST LINE
                         if (firstRow)
                         {
                             //row = file.ReadLine();
                             firstRow = false;
                         }
                         else
                         {
                             string[] line = row.Split(separator.ToCharArray()[0]);
                             //INSERIAMO I BILANCI (NO IMPORTI) JUST ONE TIME FOR FILE
                             if (setRecordBilanci)
                             {
                                 g = Guid.NewGuid();
                                 //InsertDataTableBilanciInDatabase(values, "PA", "SIOPE_ListaAmministrazioni_Bilanci", g, columns);
                                 columns = new[] { "uid", "codamm", "databil", "comparto" };
                                 setRecordBilanci = false;
                                 //SET SOME VALUE                                                     
                                 string sanno = Regex.Replace(line[6], "[^\\d]", "").Trim();
                                 int anno = Convert.ToInt32(sanno.Trim());
                                 //string comparto = Siope.getBilancio().PERIODICITA.CODICE_PERIODO;
                                 string comparto = Regex.Replace(line[6], "[\\d]", "").Trim();
                                 //comparto = SetCodiceProspetto(comparto);
                                 DateTime datetime = new DateTime(anno, 1, 1);
                                 //******************************************************************************************
                                 //BEFORE INSERT DELETE THE RECORD IF ALREADY EXISTS
                                 ctStr = "DELETE FROM [PA].dbo.[SIOPE_ListaAmministrazioni_Bilanci] WHERE " +
                                                     "codamm = '" + line[0].Replace("'", "''") + "'" +
                                                     " AND " +
                                                     "databil = '" + anno.ToString().Replace("'", "''") + "'" +
                                                     " AND " +
                                                     "comparto = '" + comparto.Replace("'", "''") + "'"
                                                     ;
                                 //*******************************************************************************************                                                     
                                 //INSERT INTO THE Database THE RECORD

                                 command = new SqlCommand(ctStr, connection);
                                 command.ExecuteNonQuery();
                                 command.Dispose();

                                 command = new SqlCommand(null, connection);
                                 ctStr = "INSERT INTO [PA].[dbo].[SIOPE_ListaAmministrazioni_Bilanci](\r\n";
                                 for (int i = 0; i < columns.Length; i++)
                                 {
                                     ctStr += " " + columns[i] + " ";
                                     if (i < columns.Length - 1)
                                     {
                                         ctStr += ",";
                                     }
                                     ctStr += "\r\n";
                                 }
                                 ctStr += ")";
                                 ctStr += " VALUES (\r\n";
                                 for (int i = 0; i < columns.Length; i++)
                                 {
                                     ctStr += "  @" + columns[i];
                                     //values.Add("@" + dtB.Columns[i].ColumnName.ToString());
                                     if (i < columns.Length - 1)
                                     {
                                         ctStr += ",";
                                     }
                                     ctStr += "\r\n";
                                 }
                                 ctStr += "\r\n";
                                 ctStr += ")";

                                 //Set the type of the element you want to insert
                                 SqlParameter uidParam = new SqlParameter("@" + columns[0], SqlDbType.UniqueIdentifier);
                                 SqlParameter codammParam = new SqlParameter("@" + columns[1], SqlDbType.VarChar, 20);
                                 SqlParameter databilParam = new SqlParameter("@" + columns[2], SqlDbType.DateTime);
                                 SqlParameter compartoParam = new SqlParameter("@" + columns[3], SqlDbType.VarChar, 2);

                                 //Add your param to thwe command
                                 command.Parameters.Add(uidParam);
                                 command.Parameters.Add(codammParam);
                                 command.Parameters.Add(databilParam);
                                 command.Parameters.Add(compartoParam);

                                 // Call Prepare after setting the Commandtext and Parameters.
                                 command.CommandText = ctStr;
                                 command.Prepare();

                                 // Change parameter values and call ExecuteNonQuery.
                                 command.Parameters[0].Value = g;
                                 command.Parameters[1].Value = line[0].Replace("'", "''");
                                 command.Parameters[2].Value = datetime;
                                 command.Parameters[3].Value = comparto;
                                 command.ExecuteNonQuery();
                                 command.Dispose();
                                 //values.Clear();
                             }//if -> fine record bilanci                          
                         }//else

                         counter++;
                     }//while                   
                     file.Close();
                 }//using streamreader 
                 connection.Close();
             }//using sql connection
             // Writes the number of imported rows to the form
             Log.Info("Imported: " + counter.ToString() + "/" + counter.ToString() + " row(s)");
         }//readCSVAndLoadToSQLServer_1
    
         public void SimpleRead()
		 {
			// declare the SqlDataReader, which is used in
			// both the try block and the finally block
			SqlDataReader rdr = null;
			// create a connection object
			SqlConnection conn = new SqlConnection("Data Source=(local);Initial Catalog=Northwind;Integrated Security=SSPI");
			// create a command object
			SqlCommand cmd  = new SqlCommand("select * from Customers", conn);
			try
			{
				// open the connection
				conn.Open();

				// 1. get an instance of the SqlDataReader
				rdr = cmd.ExecuteReader();

				// print a set of column headers
				Console.WriteLine(@"Contact Name             City                Company Name");
			    Console.WriteLine( @"------------             ------------        ------------");

				// 2. print necessary columns of each record                
				while (rdr.Read())
				{
					// get the results of each column
					string contact = (string)rdr["ContactName"];
					string company = (string)rdr["CompanyName"];
					string city    = (string)rdr["City"];

					// print out the results
					Console.Write(@"{0,-25}", contact);
					Console.Write(@"{0,-20}", city);
					Console.Write(@"{0,-25}", company);
					Console.WriteLine();
				}
			}
			finally
			{
				// 3. close the reader
				if (rdr != null)
				{
					rdr.Close();
				}

				// close the connection
			    conn.Close();
			}		
	    }//simpleRead

        public static void CleanSqlScript(string absolutePathToSQlScript)
        {
            var symbol = "'";
            IEnumerable<string> lines = new List<string>();         
            try
            {
                lines = File.ReadLines(absolutePathToSQlScript);
            }
            catch (Exception e)
            {
                Log.Error("Can't read the File:"+absolutePathToSQlScript,e);
            }
            var newLines = new ArrayList();
            foreach (var line in lines)
            {
                //Regex r = new Regex(symbol+"(.+?)"+symbol, RegexOptions.Compiled);
                
                var newLine =Regex.Replace(line, symbol + "(.+?)" + symbol, delegate (Match match)
                {
                    string v = match.ToString();
                    v = v.Replace("'", "");
                    v = v.Replace(";", ""); 
                    v = Regex.Replace(v, @"\s+", " "); //collapse whitespace
                    return v.Trim();
                });
                newLines.Add(newLine);
            }
            // Create a file to write to.
            string[] createText = (string[]) newLines.ToArray();
            try
            {
                File.WriteAllLines(absolutePathToSQlScript + "_" + DateTime.Now, createText, Encoding.UTF8);
            }
            catch (Exception e)
            {
                Log.Error("Can't write the File:" + absolutePathToSQlScript + "_" + DateTime.Now, e);
            }
        }

       

    }//end of class SqlCmdKit
    
}
