using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Common.Logging;

namespace UtilityCSharp.@string
{

	#region Class StringEnum

	/// <summary>
	/// Helper class for working with 'extended' enums using <see cref="StringValueAttribute"/> attributes.
    /// Usage: 
    /// //Values intentionally unordered...
    ///public enum CarType
    ///{
    ///    [StringValue("Saloon / Sedan")] Saloon = 5,
    ///    [StringValue("Coupe")] Coupe = 4,
    ///    [StringValue("Estate / Wagon")] Estate = 6,
    ///    [StringValue("Hatchback")] Hatchback = 8,
    ///    [StringValue("Utility")] Ute = 1,
    ///}
    ///public void TestMethod()
    ///{
    ///    MessageBox.Show(StringEnum.GetStringValue(CarType.Estate));
    ///    //The line above shows 'Estate / Wagon'

    ///    MessageBox.Show(StringEnum.Parse(typeof(CarType)"estate / wagon", true).ToString());
    ///    //The line above converts back to an enum 
    ///    //value from String Value (case insensitive)
    ///    //and shows 'Estate'

    ///    int enumValue = (int)StringEnum.Parse(typeof(CarType),"estate / wagon", true);
    ///    MessageBox.Show(enumValue.ToString());
    ///    //The line above does the same again but this time shows 
    ///    //the numeric value of the enum (6)
    ///}
	/// </summary>
	public class StringEnum
	{
        private static readonly ILog Log = LogManager.GetLogger<StringEnum>();
        #region Instance implementation

        private readonly Type _enumType;
		private static readonly Hashtable StringValues = new Hashtable();

		/// <summary>
		/// Creates a new <see cref="StringEnum"/> instance.
		/// </summary>
		/// <param name="enumType">Enum type.</param>
		public StringEnum(Type enumType)
		{
			if (!enumType.IsEnum)
				throw new ArgumentException($"Supplied type must be an Enum.  Type was {enumType}");
			_enumType = enumType;
		}

		/// <summary>
		/// Gets the string value associated with the given enum value.
		/// </summary>
		/// <param name="valueName">Name of the enum value.</param>
		/// <returns>String Value</returns>
		public string GetStringValue(string valueName)
		{
		    string stringValue = null;
			try
			{
			    var enumType = (Enum) Enum.Parse(_enumType, valueName);
			    stringValue = GetStringValue(enumType);
			}
			catch (Exception)
			{
			    // ignored
			} //Swallow!

			return stringValue;
		}

		/// <summary>
		/// Gets the string values associated with the enum.
		/// </summary>
		/// <returns>String value array</returns>
		public Array GetStringValues()
		{
			var values = new ArrayList();
            //Look for our string value associated with fields in this enum
            //foreach (var fi in _enumType.GetFields())
            //{
            //	//Check for our custom attribute
            //	var attrs = fi.GetCustomAttributes(typeof (StringValueAttribute), false) as StringValueAttribute[];
            //	if (attrs != null && attrs.Length > 0)
            //		values.Add(attrs[0].Value);

            //}
            foreach (var attrs in _enumType.GetFields()
                .Select(fi => fi.GetCustomAttributes(typeof(StringValueAttribute), false) 
                as StringValueAttribute[]).Where(attrs => attrs != null && attrs.Length > 0))
            {
                values.Add(attrs[0].Value);
            }
            return values.ToArray();
		}

		/// <summary>
		/// Gets the values as a 'bindable' list datasource.
		/// </summary>
		/// <returns>IList for data binding</returns>
		public IList GetListValues()
		{
			Type underlyingType = Enum.GetUnderlyingType(_enumType);
			ArrayList values = new ArrayList();
			//Look for our string value associated with fields in this enum
			foreach (FieldInfo fi in _enumType.GetFields())
			{
				//Check for our custom attribute
				StringValueAttribute[] attrs = fi.GetCustomAttributes(typeof (StringValueAttribute), false) as StringValueAttribute[];
				if (attrs != null && attrs.Length > 0)
					values.Add(new DictionaryEntry(Convert.ChangeType(Enum.Parse(_enumType, fi.Name), underlyingType), attrs[0].Value));

			}
			return values;
		}

		/// <summary>
		/// Return the existence of the given string value within the enum.
		/// </summary>
		/// <param name="stringValue">String value.</param>
		/// <returns>Existence of the string value</returns>
		public bool IsStringDefined(string stringValue)
		{
			return Parse(_enumType, stringValue) != null;
		}

		/// <summary>
		/// Return the existence of the given string value within the enum.
		/// </summary>
		/// <param name="stringValue">String value.</param>
		/// <param name="ignoreCase">Denotes whether to conduct a case-insensitive match on the supplied string value</param>
		/// <returns>Existence of the string value</returns>
		public bool IsStringDefined(string stringValue, bool ignoreCase)
		{
			return Parse(_enumType, stringValue, ignoreCase) != null;
		}

		/// <summary>
		/// Gets the underlying enum type for this instance.
		/// </summary>
		/// <value></value>
		public Type EnumType
		{
			get { return _enumType; }
		}

        //OHTER METHOD 
        public bool IsStringEqualEnum(string svalue, Enum eValue)
        {
            //Expect to retrieve a string value
            //Assert.AreEqual("Third Value", StringEnum.GetStringValue(EnumWithStrings.Fox));
            ////No string value to retrieve
            //Assert.IsNull(StringEnum.GetStringValue(EnumWithoutStrings.Seashells));
            ////String values exist but not for this enum value
            //Assert.IsNull(StringEnum.GetStringValue(EnumPartialStrings.Frost))
            if (svalue.Equals(GetStringValue(eValue)))
            {
                return true;
            }
            return false;
        }

        public bool IsStringEqualType(string svalue, Type eValue)
        {
            //Case Sensitive (not found)
            //Assert.IsNull(StringEnum.Parse(typeof(EnumPartialStrings), "jacK be nImbLe"));
            ////Case Sensitive (found)
            //Assert.AreEqual(EnumPartialStrings.Jack, StringEnum.Parse(typeof(EnumPartialStrings), "Jack be nimble"));
            ////Case insensitive (found)
            //Assert.AreEqual(EnumPartialStrings.Jack, StringEnum.Parse(typeof(EnumPartialStrings), "jacK be nImbLe", true));
            var  eevalue = (string)Parse(eValue, svalue);
            return eevalue.Equals(svalue);
        }



		#endregion

		#region Static implementation

		/// <summary>
		/// Gets a string value for a particular enum value.
		/// </summary>
		/// <param name="value">Value.</param>
		/// <returns>String Value associated via a <see cref="StringValueAttribute"/> attribute, or null if not found.</returns>
		public static string GetStringValue(Enum value)
		{
			string output = null;
			Type type = value.GetType();

			if (StringValues.ContainsKey(value))
			{
			    var stringValueAttribute = StringValues[value] as StringValueAttribute;
			    if (stringValueAttribute != null)
			        output = stringValueAttribute.Value;
			}
			else 
			{
				//Look for our 'StringValueAttribute' in the field's custom attributes
				var fi = type.GetField(value.ToString());
				var attrs = fi.GetCustomAttributes(typeof (StringValueAttribute), false) as StringValueAttribute[];
			    if (attrs == null || attrs.Length <= 0) return null;
			    StringValues.Add(value, attrs[0]);
			    output = attrs[0].Value;
			}
			return output;

		}

		/// <summary>
		/// Parses the supplied enum and string value to find an associated enum value (case sensitive).
		/// </summary>
		/// <param name="type">Type.</param>
		/// <param name="stringValue">String value.</param>
		/// <returns>Enum value associated with the string value, or null if not found.</returns>
		public static object Parse(Type type, string stringValue)
		{
			return Parse(type, stringValue, false);
		}

		/// <summary>
		/// Parses the supplied enum and string value to find an associated enum value.
		/// </summary>
		/// <param name="type">Type.</param>
		/// <param name="stringValue">String value.</param>
		/// <param name="ignoreCase">Denotes whether to conduct a case-insensitive match on the supplied string value</param>
		/// <returns>Enum value associated with the string value, or null if not found.</returns>
		public static object Parse(Type type, string stringValue, bool ignoreCase)
		{
			object output = null;
			string enumStringValue = null;

			if (!type.IsEnum)
				throw new ArgumentException($"Supplied type must be an Enum.  Type was {type}");

			//Look for our string value associated with fields in this enum
			foreach (var fi in type.GetFields())
			{
				//Check for our custom attribute
				var attrs = fi.GetCustomAttributes(typeof (StringValueAttribute), false) as StringValueAttribute[];
				if (attrs != null && attrs.Length > 0)
					enumStringValue = attrs[0].Value;

				//Check for equality then select actual enum value.
			    if (string.Compare(enumStringValue, stringValue, ignoreCase) != 0) continue;
			    output = Enum.Parse(type, fi.Name);
			    break;
			}

			return output;
		}

		/// <summary>
		/// Return the existence of the given string value within the enum.
		/// </summary>
		/// <param name="stringValue">String value.</param>
		/// <param name="enumType">Type of enum</param>
		/// <returns>Existence of the string value</returns>
		public static bool IsStringDefined(Type enumType, string stringValue)
		{
			return Parse(enumType, stringValue) != null;
		}

		/// <summary>
		/// Return the existence of the given string value within the enum.
		/// </summary>
		/// <param name="stringValue">String value.</param>
		/// <param name="enumType">Type of enum</param>
		/// <param name="ignoreCase">Denotes whether to conduct a case-insensitive match on the supplied string value</param>
		/// <returns>Existence of the string value</returns>
		public static bool IsStringDefined(Type enumType, string stringValue, bool ignoreCase)
		{
			return Parse(enumType, stringValue, ignoreCase) != null;
		}


       
		#endregion
	}

	#endregion

	#region Class StringValueAttribute

	/// <summary>
	/// Simple attribute class for storing String Values
	/// </summary>
	public class StringValueAttribute : Attribute
	{
		private string _value;

		/// <summary>
		/// Creates a new <see cref="StringValueAttribute"/> instance.
		/// </summary>
		/// <param name="value">Value.</param>
		public StringValueAttribute(string value)
		{
			_value = value;
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <value></value>
		public string Value
		{
			get { return _value; }
		}
	}

	#endregion

    /// <summary>
    /// Usage:
    ///  public enum Fruit
    ///{
    ///    [Description("Apple")]A,
    ///    [Description("Banana")]B,
    ///    [Description("Cherry")]C
    ///    //oppure
    ///    [DescriptionAttribute("Chocolate Chip")]ChocolateChip, 
    ///    [DescriptionAttribute("Rocky Road")]RockyRoad, 
    ///    Vanilla 
    ///}
    /// </summary>
    class UtilEnum 
    {
        public static T String2Enum<T>(string name)
        {
            return (T)Enum.Parse(typeof(T), name);
        }

        public static string Enum2DescriptionString(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =(DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute),false);
               
            if (attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static object EnumValueOf(string value, Type enumType)
        {
            string[] names = Enum.GetNames(enumType);
            foreach (string name in names)
            {
                if (Enum2DescriptionString((Enum)Enum.Parse(enumType, name)).Equals(value))
                {
                    return Enum.Parse(enumType, name);
                }
            }
            throw new ArgumentException("The string is not a description or value of the specified enum.");
        }    
    }//end of the class enumutil

    /// <summary>
    /// Class you can use like alternative to enum object,Try type-safe-enum pattern.
    /// http://stackoverflow.com/questions/424366/c-sharp-string-enums
    /// </summary>
    public sealed class AuthenticationMethod
    {

        private readonly string _name;
        private readonly int _value;

        //Elements of the enum
        public static readonly AuthenticationMethod Forms = new AuthenticationMethod(1, "Forms");
        public static readonly AuthenticationMethod Windowsauthentication = new AuthenticationMethod(2, "WINDOWS");
        public static readonly AuthenticationMethod Singlesignon = new AuthenticationMethod(3, "SSN");

        //you can  use a map object
        //n.b. In order that the initialisation of the the "enum member" fields doesn't throw a NullReferenceException when calling the 
        //Instance constructor, be sure to put the Dictionary field before the "enum member" fields in your class. This is because static
        //field initialisers are called in declaration order, and before the static constructor, creating the weird and necessary
        //but confusing situation that the Instance constructor can be called before all static fields have been initialised, and before
        //the static constructor is called.
        private static readonly Dictionary<string, AuthenticationMethod> Instance = new Dictionary<string, AuthenticationMethod>();

        private AuthenticationMethod(int value, string name)
        {
            //filling the map 
            Instance[name] = this;
            //filling standard input
            _name = name;
            _value = value;
        }
        
        public int GetValue()
        {
            return _value;
        }

        public string GetName()
        {
            return _name;
        }

        public override string ToString()
        {
            return _name;
        }

        /// <summary>
        /// Method user-defined type conversion operator
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static explicit operator AuthenticationMethod(string str)
        {
            AuthenticationMethod result;
            if (Instance.TryGetValue(str, out result))
                return result;
            else
                throw new InvalidCastException();
        }
    }//end of the class AuthenticationMethod
}