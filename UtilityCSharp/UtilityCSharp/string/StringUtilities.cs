﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Common.Logging;

namespace UtilityCSharp.@string
{
    /// <summary>
    /// 2015-04-13
    /// </summary>
    public class StringUtilities
    {
        private static readonly ILog Log = LogManager.GetLogger<StringUtilities>();

        public static string AddSpacesToSentence(string text, bool preserveAcronyms)
        {
            if (string.IsNullOrWhiteSpace(text))
                return string.Empty;
            var newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (var i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]))
                {
                    if (char.IsUpper(text[i - 1]))
                    {
                        if (preserveAcronyms && i < text.Length - 1 && !char.IsUpper(text[i + 1]))
                        {
                            newText.Append(' ');
                        }
                    }
                    else if (text[i - 1] != ' ')
                    {
                        newText.Append(' ');
                    }
                }
                newText.Append(text[i]);
            }
            return newText.ToString();
        }

        public static string FormatPrice(decimal price, string cultureName)
        {
            string formatted;
            var info = new CultureInfo(cultureName);
            switch (info.Name)
            {
                case "en-US":
                    formatted = string.Format(CultureInfo.CreateSpecificCulture("en-US"),
                        "{0:C}", price);
                    break;
                case "en-GB":
                    formatted = string.Format(CultureInfo.CreateSpecificCulture("en-GB"),
                        "{0:C}", price);
                    break;
                case "sq-AL":
                    formatted = string.Format(CultureInfo.CreateSpecificCulture("sq-AL"),
                        "{0:C}", price);
                    break;
                case "se-SE":
                    formatted = string.Format(CultureInfo.CreateSpecificCulture("se-SE"),
                        "{0:C}", price);
                    break;
                case "it-IT":
                    formatted = string.Format(CultureInfo.CreateSpecificCulture("it-IT"),
                        "{0:C}", price);
                    break;
                default:
                    formatted = string.Format(CultureInfo.CreateSpecificCulture("en-US"),
                       "{0:C}", price);
                    break;
            }

            return formatted;
        }


        private static readonly char[] WhitespaceAndZero = new[] {' ','\t','\r','\n',
            '\u000b', // vertical tab
            '\u000c', // form feed
            '0'
        };

        public static string TrimEndWhitespaceAndZeros(string s)
        {
            return s.Contains('.') ? s.TrimEnd(WhitespaceAndZero) : s;
        }

        public static bool TryParseAfterTrim(string s, out decimal d)
        {
            return Decimal.TryParse(TrimEndWhitespaceAndZeros(s), out d);
        }


        public static string ToUtf8(string source) { 
            //To encode a string to UTF8 encoding
            //string source = "hello world";
            var utf8Encodes = Encoding.UTF8.GetBytes(source); 
            //get the string from UTF8 encoding
            var output = Encoding.UTF8.GetString(utf8Encodes);
            return output;
        }

        public static string ToIso(string src) {
          var iso = Encoding.GetEncoding("iso8859-1");
          var unicode = Encoding.UTF8;
          var unicodeBytes = unicode.GetBytes(src);
          return iso.GetString(unicodeBytes);
        }

        public static string IsotoUtf8(string src) {
          var iso = Encoding.GetEncoding("iso8859-1");
          var unicode = Encoding.UTF8;
          var isoBytes = iso.GetBytes(src);
          return unicode.GetString(isoBytes);
        }

        public static DateTime ToDatetime(string date){
            return DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }

        public static string FormattaDecimali(string value)
        {
            var retValue = value;
            double dvalue;

            //"." come decimale -> lo sostituisco con la ","
            if (retValue.Length > 3 && retValue.Substring(retValue.Length - 3, 1) == ".")
                retValue = retValue.Substring(0, retValue.Length - 3) + "," + retValue.Substring(retValue.Length - 2);

            if (retValue.Length > 2 && retValue.Substring(retValue.Length - 2, 1) == ".")
                retValue = retValue.Substring(0, retValue.Length - 2) + "," + retValue.Substring(retValue.Length - 1);

            //formatto il numero (no separatore migliaia e separatore decimale "," con 2 decimali)
            if (double.TryParse(retValue, NumberStyles.Float | NumberStyles.AllowThousands, new CultureInfo("it-IT"), out dvalue))
            {
                retValue = dvalue.ToString("0.00", CultureInfo.InvariantCulture);
                retValue = retValue.Replace(".", ",");
            }
            else
                retValue = value;

            return retValue;
        }

        public static bool FindString(string strVal, char cVal)
        {
            //foreach (char c in strVal)
            //{
            //    if (c == cVal)
            //        return true;
            //}
            //return false;
            return strVal.Any(c => c == cVal);
        }

        public static string FormattaData(string data)
        {
            //routine che ricevuta una data nel formato dd/mm/yyyy la formatta come yyyymmdd (se esistente)
            var retValue = "";

            if (string.IsNullOrEmpty(data)) return retValue;
            try
            {
                var dtN = Convert.ToDateTime(data, CultureInfo.GetCultureInfo("it-IT"));
                retValue = dtN.Year.ToString("0000") + dtN.Month.ToString("00") + dtN.Day.ToString("00");
            }
            catch (Exception e)
            {
                Log.Error(e.Message,e);
            }

            return retValue;
        }


        private static bool TimeBetween(DateTime datetime, TimeSpan start, TimeSpan end)
        {
            // convert datetime to a TimeSpan
            var now = datetime.TimeOfDay;
            // see if start comes before end
            if (start < end)return start <= now && now <= end;
            // start is after end, so do the inverse comparison
            return !(end < now && now < start);
        }//TimeBetween

        private static string ParseXpathString(string input)
        {
            var ret = "";
            if (input.Contains("'"))
            {
                var inputstrs = input.Split('\'');
                foreach (string inputstr in inputstrs)
                {
                    if (ret != "")
                        ret += ",\"'\",";
                    ret += "\"" + inputstr + "\"";
                }
                ret = "concat(" + ret + ")";
            }
            else
            {
                ret = "'" + input + "'";
            }
            return ret;
        }//ParseXpathString  

        #region Archiver Utility
        internal class Archiver
        {
            public static byte[] CreateGZip(string input)
            {
                var bytes = Encoding.UTF8.GetBytes(input);

                using (var msi = new MemoryStream(bytes))
                using (var mso = new MemoryStream())
                {
                    using (var gs = new System.IO.Compression.GZipStream(mso, System.IO.Compression.CompressionMode.Compress))
                    {
                        msi.CopyTo(gs);
                    }

                    return mso.ToArray();
                }
            }

            public static string DecompressGZip(byte[] bytes)
            {
                using (var msi = new MemoryStream(bytes))
                using (var mso = new MemoryStream())
                {
                    using (var gs = new System.IO.Compression.GZipStream(msi, System.IO.Compression.CompressionMode.Decompress))
                    {
                        gs.CopyTo(mso);
                    }

                    return Encoding.UTF8.GetString(mso.ToArray());
                }
            }
        }//end class archiver
        #endregion
	

        #region UTILITY PER I FILE
        private static string ReturnExtension(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".htm":
                case ".html":
                case ".log":
                    return "text/HTML";
                case ".txt":
                    return "text/plain";
                case ".doc":
                    return "application/ms-word";
                case ".tiff":
                case ".tif":
                    return "image/tiff";
                case ".asf":
                    return "video/x-ms-asf";
                case ".avi":
                    return "video/avi";
                case ".zip":
                    return "application/zip";
                case ".xls":
                case ".csv":
                    return "application/vnd.ms-excel";
                case ".gif":
                    return "image/gif";
                case ".jpg":
                case "jpeg":
                    return "image/jpeg";
                case ".bmp":
                    return "image/bmp";
                case ".wav":
                    return "audio/wav";
                case ".mp3":
                    return "audio/mpeg3";
                case ".mpg":
                case "mpeg":
                    return "video/mpeg";
                case ".rtf":
                    return "application/rtf";
                case ".asp":
                    return "text/asp";
                case ".pdf":
                    return "application/pdf";
                case ".fdf":
                    return "application/vnd.fdf";
                case ".ppt":
                    return "application/mspowerpoint";
                case ".dwg":
                    return "image/vnd.dwg";
                case ".msg":
                    return "application/msoutlook";
                case ".xml":
                case ".sdxl":
                    return "application/xml";
                case ".xdp":
                    return "application/vnd.adobe.xdp+xml";
                default:
                    return "application/octet-stream";
            }
        }
        #endregion


        //public static T[,] convertListsToMultidimensional<T>(IList<IList<T>> listOfLists)
        //{
        //    // List<List<string>> lst = new List<List<string>>();
        //    T[,] str = new T[listOfLists.Count, listOfLists[0].Count];
        //    for (int j = 0; j < listOfLists.Count; j++)
        //    {
        //        for (int i = 0; i < listOfLists[j].Count; i++)
        //        {
        //            str[j, i] = listOfLists[j][i];
        //        }
        //    }
        //    return str;
        //}


        public static T[,] CreateRectangularArray<T>(IList<T[]> arrays)
        {
            // TODO: Validation and special-casing for arrays.Count == 0
            int minorLength = arrays[0].Length;
            T[,] ret = new T[arrays.Count, minorLength];
            for (int i = 0; i < arrays.Count; i++)
            {
                var array = arrays[i];
                if (array.Length != minorLength)
                {
                    throw new ArgumentException
                        ("All arrays must be the same length");
                }
                for (int j = 0; j < minorLength; j++)
                {
                    ret[i, j] = array[j];
                }
            }
            return ret;
        }

        /// <summary>
        /// Convert a List of List of objjects to 2d array of objects.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>  
        public static T[,] To2DArray<T>(List<List<T>> list)
        {
            if (list.Count == 0 || list[0].Count == 0)
                throw new ArgumentException("The list must have non-zero dimensions.");

            var result = new T[list.Count, list[0].Count];
            for (var i = 0; i < list.Count; i++)
            {
                for (var j = 0; j < list[i].Count; j++)
                {
                    if (list[i].Count != list[0].Count)
                        throw new InvalidOperationException("The list cannot contain elements (lists) of different sizes.");
                    result[i, j] = list[i][j];
                }
            }
            return result;
        }

        /// <summary>
        /// Method for get all values on a enum object
        /// Usage: var values = EnumUtil.GetValues();
        /// </summary>
        public static class EnumUtil
        {
            public static IEnumerable<T> GetValues<T>()
            {
                return Enum.GetValues(typeof(T)).Cast<T>();
            }
        }

    }
}
