﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using Common.Logging;
using UtilityCSharp.http;

namespace UtilityCSharp.http
{
        /// <summary>
        ///This base class provides implementation of request 
        ///and response methods during Http Calls.
        /// 2015-04-13
        /// </summary>
        public class HttpBaseUtil
        {
            private static readonly ILog Log = LogManager.GetLogger<HttpBaseUtil>();
            private readonly string _userName;
            private readonly string _userPwd;
            private readonly string _proxyServer;
            private readonly int _proxyPort;
            private readonly string _request;

            public HttpBaseUtil(string httpUserName, string httpUserPwd, string httpProxyServer, int httpProxyPort, string httpRequest)
            {
                _userName = httpUserName;
                _userPwd = httpUserPwd;
                _proxyServer = httpProxyServer;
                _proxyPort = httpProxyPort;
                _request = httpRequest;
            }

            private readonly bool _allowAutoRedirect;
            public int Timeout { get; }
            private readonly bool _keepAlive = true;
            public string UserAgent { get; }


            public HttpBaseUtil(bool allowAutoRedirect, int timeout, bool keepAlive, string userAgent, string httpRequest)
            {
                _allowAutoRedirect = allowAutoRedirect;
                Timeout = timeout;
                _keepAlive = keepAlive;
                UserAgent = userAgent;
                _request = httpRequest;
            }

            /// <summary>
            /// This method creates secure/non secure web
            /// request based on the parameters passed.
            /// </summary>
            /// <param name="uri"></param>
            /// <param name="collHeader">This parameter of type
            ///    NameValueCollection may contain any extra header
            ///    elements to be included in this request      </param>
            /// <param name="requestMethod">Value can POST OR GET</param>
            /// <param name="nwCred">In case of secure request this would be true</param>
            /// <returns></returns>
            public virtual HttpWebRequest CreateWebRequest(string uri, NameValueCollection collHeader, string requestMethod, bool nwCred)
            {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uri);
                webrequest.KeepAlive = _keepAlive;

                webrequest.Method = requestMethod;

                string key;
                string keyvalue;

                //SETTIAMO HEADERS
                if (collHeader != null)
                {
                    var iCount = collHeader.Count;
                    if (iCount != 0)
                    {
                        for (var i = 0; i < iCount; i++)
                        {
                            key = collHeader.Keys[i];
                            keyvalue = collHeader[i];
                            webrequest.Headers.Add(key, keyvalue);
                        }
                    }
                }
                else
                {
                    webrequest.Accept = "text/html, application/xhtml+xml, */*";
                    webrequest.Headers.Add("Accept-Language", "it-IT,it;q=0.8,en-US;q=0.5,en;q=0.3");
                    webrequest.Headers.Add("Accept-Encoding", "gzip, deflate");
                }


                webrequest.ContentType = "text/html";
                //"application/x-www-form-urlencoded";

                if (!string.IsNullOrEmpty(_proxyServer))
                {
                    webrequest.Proxy = new WebProxy(_proxyServer, _proxyPort);
                }
                //Usa impostazioni proxy del sistema
                else
                {
                    //No Cache
                    System.Net.Cache.HttpRequestCachePolicy policy = 
                        new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.BypassCache);
                    HttpWebRequest.DefaultCachePolicy = policy;
                    // Set the Credentials in two locations to get past the 407 error:
                    webrequest.Proxy = WebRequest.DefaultWebProxy;                //WebRequest.GetSystemWebProxy();
                    //webrequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    webrequest.Proxy.Credentials = CredentialCache.DefaultCredentials;
                }


                webrequest.AllowAutoRedirect = _allowAutoRedirect;


                if (nwCred)
                {
                    var wrCache = new CredentialCache
                    {
                        {new Uri(uri), "Basic", new NetworkCredential(_userName, _userPwd)}
                    };
                    webrequest.Credentials = wrCache;
                }
                else
                {
                    webrequest.Credentials = CredentialCache.DefaultCredentials;
                }
                //Remove collection elements
                collHeader?.Clear();
                return webrequest;
            }//End of secure CreateWebRequest

            /// <summary>
            /// This method retreives redirected URL from response header and also passes back any cookie (if there is any)
            /// </summary>
            /// <param name="webresponse"></param>
            /// <param name="cookie"></param>
            /// <returns></returns>
            public virtual string GetRedirectUrl(HttpWebResponse webresponse, ref string cookie)
            {
                string uri;

                WebHeaderCollection headers = webresponse.Headers;

                if ((webresponse.StatusCode == HttpStatusCode.Found) ||
                (webresponse.StatusCode == HttpStatusCode.Redirect) ||
                (webresponse.StatusCode == HttpStatusCode.Moved) ||
                (webresponse.StatusCode == HttpStatusCode.MovedPermanently)
                    //(webresponse.StatusCode == HttpStatusCode.
                    )
                {
                    // Get redirected uri
                    uri = headers["Location"];
                    uri = uri.Trim();

                }
                else
                {
                    uri = webresponse.ResponseUri.ToString();
                }

                //Check for any cookies
                if (headers["Set-cookie"] != null)
                {
                    cookie = headers["Set-cookie"];
                }
                //                string StartURI = "http:/";
                //                if (uri.Length > 0 && uri.StartsWith(StartURI)==false)
                //                {
                //                      uri = StartURI + uri;
                //                }
                return uri;
            }//End of GetRedirectUrl method


            public virtual string GetFinalResponse(string reUri, string cookie, string requestMethod, bool nwCred)
            {
                NameValueCollection collHeader = new NameValueCollection();

                if (cookie.Length > 0)
                {
                    collHeader.Add("cookie", cookie);
                }

                HttpWebRequest webrequest = CreateWebRequest(reUri, collHeader, requestMethod, nwCred);

                BuildReqStream(ref webrequest);

                var webresponse = (HttpWebResponse)webrequest.GetResponse();
                
                var enc = Encoding.GetEncoding(1252);
                // ReSharper disable once AssignNullToNotNullAttribute
                var loResponseStream = new StreamReader(stream: webresponse.GetResponseStream(), encoding: enc);

                var response = loResponseStream.ReadToEnd();

                loResponseStream.Close();
                webresponse.Close();

                return response;
                
            }

            private void BuildReqStream(ref HttpWebRequest webrequest)
            //This method build the request stream for WebRequest
            {
                var bytes = Encoding.ASCII.GetBytes(_request);
                webrequest.ContentLength = bytes.Length;
                //Get the request stream
                var oStreamOut = webrequest.GetRequestStream();
                oStreamOut.Write(bytes, 0, bytes.Length);
                oStreamOut.Close();
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            #region METODI CHE SIMULANO I  MOBILE
            #region ORIGINALI SIMONE
            /// <summary>
            /// Descriptions about possible return value error
            /// </summary>
            public enum ErrorDescriptionEnum
            {
                None = 0,
                TimeOutElapsed = 9,
                UrlNotValid = 10,
                WebSiteError = 11,
                FieldOrAttributeNotFound = 12
            }

            public enum DisplayModeEnum
            {
                Normal = 0,
                Minimized = 1,
                Maximized = 2,
                Hidden = -1
            }
            internal static class Environ
            {
                //private static readonly ILog Log = LogManager.GetLogger<Environ>();
                //Valori di default
                internal static ErrorDescriptionEnum ErrorRaised = ErrorDescriptionEnum.None;

                public static int TimeOut { get; set; }
                //public static ErrorDescriptionEnum ErrorDescription { get { return ErrorRaised; } }
                public static ErrorDescriptionEnum ErrorDescription => ErrorRaised;
                public static Uri CurrentUrl { get; set; }
                public static bool DisableFileDownloadRequest { get; set; }
                public static DisplayModeEnum DisplayMode { get; set; }
                public static bool AllowExceptionOnTimeOut = false;
                public static int TimeWaitForRedirection = 10;   //Millisecondi
            }

            public static int NavigateAndDownloadString(Uri url, out XmlDocument xmlResponse)
            {
                int retvalue;
                const int buffer = 1024;
                const int maxLenght = 4 * 1024 * 1024; //Max page lenght 4 MB
                bool redirection = false;


                /*
                 * 1) TEST:
                 * Pattern: \<script[^>]*.* *location\.[replace|assign|href]*
                 * Testo: 
                   <html>
                            <head>
                            <script language="javascript"><!-- aa es  / location.href("http://www.autolineelorenzini.com");
                            -->
                            </script></head></html>
                */


                var xmlOutput = new XmlDocument();

                var xDecl = xmlOutput.CreateXmlDeclaration("1.0", Encoding.UTF8.WebName, null);
                xmlOutput.AppendChild(xDecl);
                //Root
                var xmlElement = xmlOutput.CreateElement("", "CebisWeb", "");

                var xmlElement2 = xmlOutput.CreateElement("", "Result", "");
                var xmlAttribute = xmlOutput.CreateAttribute("returnvalue");


                Environ.ErrorRaised = ErrorDescriptionEnum.None;
                Environ.CurrentUrl = url;

                try
                {
                    ////REDIRECT?
                    //HttpWebRequest httpWebRequest = WebRequest.Create(url) as HttpWebRequest;
                    //httpWebRequest.method = "HEAD";
                    //httpWebRequest._allowAutoRedirect = false;
                    //System.Net.Cache.HttpRequestCachePolicy policyred = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.BypassCache);
                    //HttpWebRequest.DefaultCachePolicy = policyred;

                    //// Set the Credentials in two locations to get past the 407 error:
                    //httpWebRequest.Proxy = WebRequest.DefaultWebProxy;                //WebRequest.GetSystemWebProxy();

                    //httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    //httpWebRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                    //HttpWebResponse httpWebResponse = httpWebRequest.GetResponse() as HttpWebResponse;
                    //if (httpWebResponse.StatusCode == HttpStatusCode.Redirect)
                    //{
                    //    Console.WriteLine("redirected to: " + httpWebResponse.GetResponseHeader("Location"));
                    //}
                    //else
                    //{
                    //    Console.WriteLine("no redirect");
                    //}              

                    // Creates an HttpWebRequest with the specified URL. 
                    HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(url);


                    //Configurazione
                    myHttpWebRequest.AllowAutoRedirect = true;
                    myHttpWebRequest.Timeout = Environ.TimeOut * 1000;
                    myHttpWebRequest.KeepAlive = true;
                    myHttpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2"; //Emulo il browser                
                    //myHttpWebRequest.userAgent = "Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0"; //Emulo il browser

                    //No Cache
                    System.Net.Cache.HttpRequestCachePolicy policy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.BypassCache);
                    HttpWebRequest.DefaultCachePolicy = policy;

                    // Set the Credentials in two locations to get past the 407 error:
                    myHttpWebRequest.Proxy = WebRequest.DefaultWebProxy;                //WebRequest.GetSystemWebProxy();

                    myHttpWebRequest.Credentials = CredentialCache.DefaultCredentials;
                    myHttpWebRequest.Proxy.Credentials = CredentialCache.DefaultCredentials;


                    // Sends the HttpWebRequest and waits for the response. 
                    var strWebResponse = "";

                    try
                    {
                        myHttpWebRequest.Accept = "text/html, application/xhtml+xml, */*";
                        myHttpWebRequest.Headers.Add("Accept-Language", "it-IT,it;q=0.8,en-US;q=0.5,en;q=0.3");
                        //myHttpWebRequest.Headers.Add("Accept-Encoding", "gzip, deflate");                    

                        var myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                        // Gets the stream associated with the response.                    
                        var receiveStream = myHttpWebResponse.GetResponseStream();

                        var encode = Encoding.GetEncoding("utf-8");

                        // Pipes the stream to a higher level stream reader with the required encoding format. 
                        // ReSharper disable once AssignNullToNotNullAttribute
                        var readStream = new StreamReader(receiveStream, encoding: encode);

                        //Console.WriteLine("\r\nResponse stream received.");
                        var read = new char[buffer];

                        // Reads 1024 (_buffer) characters at a time.    
                        var count = readStream.Read(read, 0, buffer);
                        //Console.WriteLine("HTML...\r\n");                    
                        while (count > 0)
                        {
                            // Dumps the 1024 characters on a string and displays the string to the console.                        
                            var str = new string(read, 0, count);
                            strWebResponse += str;
                            //Console.Write(_str);
                            count = readStream.Read(read, 0, buffer);

                            if (strWebResponse.Length > maxLenght)
                                throw new Exception("Site lenght overflow: > " + maxLenght);
                        }
                        //Console.WriteLine("");
                        // Releases the resources of the response.
                        myHttpWebResponse.Close();
                        // Releases the resources of the Stream.
                        readStream.Close();

                        Environ.CurrentUrl = myHttpWebResponse.ResponseUri;
                    }
                    catch (WebException e)
                    {
                        if (e.Response == null)
                            throw new Exception(e.Message);

                        using (var response = e.Response)
                        {
                            //Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                            using (var data = response.GetResponseStream())
                                // ReSharper disable once AssignNullToNotNullAttribute
                            using (var reader = new StreamReader(data))
                            {
                                strWebResponse = reader.ReadToEnd();
                                //if (strWebResponse.Contains("Unknown Host"))
                                //    throw new Exception(e.Message);

                                //Console.WriteLine(text);
                            }

                            Environ.CurrentUrl = e.Response.ResponseUri;
                        }

                        if (myHttpWebRequest.HaveResponse == false)
                            throw new Exception(e.Message);
                    }
                    retvalue = 1;


                    //Test Redirection
                    var testRedirect = new HtmlAgilityPack.HtmlDocument();
                    testRedirect.LoadHtml(strWebResponse.ToLower());

                    var co = testRedirect.DocumentNode.SelectNodes("//script");
                    if (co != null)
                    {
                    // foreach (HtmlAgilityPack.HtmlNode _node in _co)
                    if (co.Any(node => node.InnerText.Replace(" ", string.Empty).Contains("location.replace(") ||
                                         node.InnerText.Replace(" ", string.Empty).Contains("location.href=") ||
                                         node.InnerText.Replace(" ", string.Empty).Contains("window.location=") ||
                                         node.InnerText.Replace(" ", string.Empty).Contains("location.assign(")))
                    {
                        redirection = true; //Probale Redirect      
                    }
                    }
                    //Redirection with <META HTTP-EQUIV=Refresh CONTENT="0; URL=http://shop.leselle.it"> EX: http://www.leselle.it
                    var meta = testRedirect.DocumentNode.SelectNodes("//meta[@http-equiv='refresh']");
                    if (meta != null)
                    {
                        if (meta.Any(node => node.Attributes["content"].Value.Replace(" ", string.Empty).Contains("url=") ||
                                               node.Attributes["content"].Value.Replace(" ", string.Empty).Contains(";http")))
                        {
                            redirection = true; //Probable Redirect      
                        }
                    }


                    xmlElement2.Attributes.Append(xmlAttribute);
                    var cData = xmlOutput.CreateCDataSection(strWebResponse);
                    xmlElement2.AppendChild(cData);

                    //Console.WriteLine("IsFromCache? {0}", myHttpWebResponse.IsFromCache);

                    xmlAttribute.Value = "OK";

                    xmlElement2.SetAttribute("Redirection", redirection ? "1" : "0");


                }
                catch (TimeoutException)
                {
                    Environ.ErrorRaised = ErrorDescriptionEnum.TimeOutElapsed;
                    xmlAttribute.Value = "ERR";

                    retvalue = -1;
                }
                catch (Exception)
                {
                    Environ.ErrorRaised = ErrorDescriptionEnum.WebSiteError;
                    xmlAttribute.Value = "ERR";

                    retvalue = -1;
                }


                xmlElement.AppendChild(xmlElement2);
                xmlOutput.AppendChild(xmlElement);

                //XmlOutput.Save(FileXml);

                xmlResponse = xmlOutput;

                return retvalue;
            }
            #endregion
            public static int NavigateSimulateMobile(Uri url, out XmlDocument xmlResponse, string useragent)
            {
                int retvalue;
                const int buffer = 1024;
                const int maxLenght = 4 * 1024 * 1024; //Max page lenght 4 MB
                var redirection = false;

                var xmlOutput = new XmlDocument();

                var xDecl = xmlOutput.CreateXmlDeclaration("1.0", Encoding.UTF8.WebName, null);
                xmlOutput.AppendChild(xDecl);
                //Root
                var xmlElement = xmlOutput.CreateElement("", "CebisWeb", "");

                var xmlElement2 = xmlOutput.CreateElement("", "Result", "");
                var xmlAttribute = xmlOutput.CreateAttribute("returnvalue");


                Environ.ErrorRaised = ErrorDescriptionEnum.None;
                Environ.CurrentUrl = url;

                try
                {
                    // Creates an HttpWebRequest with the specified URL. 
                    HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    /*
                    httpRequest req = new httpRequest("", url.ToString(), "");
                    TextWriter writer = null;
                    HttpResponse resp = new HttpResponse((TextWriter) writer);
                    HttpContext cont = new HttpContext(req, resp);
                    String userAgent = HttpContext.Current._request.userAgent.ToLower();
                    if (userAgent.Contains("iphone;"))
                    {
                        // iPhone
                    }
                    else if (userAgent.Contains("ipad;"))
                    {
                        // iPad
                    }
                    else
                    {
                        // Think Different ;)
                    }
                     * */
                    /*
                    CustomWebClient cw = new CustomWebClient();
                    WebRequest WebRequest = cw.GetWebRequest(new Uri(url.ToString()),String user_agent);
                    HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest;
                     * */
                    //Configurazione
                    myHttpWebRequest.AllowAutoRedirect = true;
                    myHttpWebRequest.Timeout = Environ.TimeOut * 1000;
                    myHttpWebRequest.KeepAlive = true;
                    //myHttpWebRequest.userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2"; //Emulo il browser                
                    //myHttpWebRequest.userAgent = "Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0"; //Emulo il browser
                    myHttpWebRequest.UserAgent = useragent;
                    //No Cache
                    var policy = new 
                    System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.BypassCache);
                    HttpWebRequest.DefaultCachePolicy = policy;

                    // Set the Credentials in two locations to get past the 407 error:
                    myHttpWebRequest.Proxy = WebRequest.DefaultWebProxy;                //WebRequest.GetSystemWebProxy();

                    myHttpWebRequest.Credentials = CredentialCache.DefaultCredentials;
                    myHttpWebRequest.Proxy.Credentials = CredentialCache.DefaultCredentials;


                    // Sends the HttpWebRequest and waits for the response. 
                    var strWebResponse = "";

                    try
                    {
                        myHttpWebRequest.Accept = "text/html, application/xhtml+xml, */*";
                        myHttpWebRequest.Headers.Add("Accept-Language", "it-IT,it;q=0.8,en-US;q=0.5,en;q=0.3");
                        //myHttpWebRequest.Headers.Add("Accept-Encoding", "gzip, deflate");                    
                        myHttpWebRequest.Timeout = 10000;
                        var myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                        // Gets the stream associated with the response.                    
                        var receiveStream = myHttpWebResponse.GetResponseStream();

                        var encode = Encoding.GetEncoding("utf-8");

                        // Pipes the stream to a higher level stream reader with the required encoding format. 
                        // ReSharper disable once AssignNullToNotNullAttribute
                        var readStream = new StreamReader(receiveStream, encode);

                        //Console.WriteLine("\r\nResponse stream received.");
                        var read = new char[buffer];

                        // Reads 1024 (_buffer) characters at a time.    
                        var count = readStream.Read(read, 0, buffer);
                        //Console.WriteLine("HTML...\r\n");                    
                        while (count > 0)
                        {
                            // Dumps the 1024 characters on a string and displays the string to the console.                        
                            var str = new string(read, 0, count);
                            strWebResponse += str;
                            //Console.Write(_str);
                            count = readStream.Read(read, 0, buffer);

                            if (strWebResponse.Length > maxLenght)
                                throw new Exception("Site lenght overflow: > " + maxLenght);
                        }
                        //Console.WriteLine("");
                        // Releases the resources of the response.
                        myHttpWebResponse.Close();
                        // Releases the resources of the Stream.
                        readStream.Close();

                        Environ.CurrentUrl = myHttpWebResponse.ResponseUri;
                    }
                    catch (WebException e)
                    {
                        if (e.Response == null)
                            throw new Exception(e.Message);

                        using (var response = e.Response)
                        {
                            //Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                            using (var data = response.GetResponseStream())
                                // ReSharper disable once AssignNullToNotNullAttribute
                            using (var reader = new StreamReader(data))
                            {
                                strWebResponse = reader.ReadToEnd();
                                //if (strWebResponse.Contains("Unknown Host"))
                                //    throw new Exception(e.Message);

                                //Console.WriteLine(text);
                            }

                            Environ.CurrentUrl = e.Response.ResponseUri;
                        }

                        if (myHttpWebRequest.HaveResponse == false)
                            throw new Exception(e.Message);
                    }
                    retvalue = 1;


                    //Test Redirection
                    var testRedirect = new HtmlAgilityPack.HtmlDocument();
                    testRedirect.LoadHtml(strWebResponse.ToLower());

                    var co = testRedirect.DocumentNode.SelectNodes("//script");
                    if (co != null)
                    {
                        if (co.Any(node => node.InnerText.Replace(" ", string.Empty).Contains("location.replace(") ||
                                             node.InnerText.Replace(" ", string.Empty).Contains("location.href=") ||
                                             node.InnerText.Replace(" ", string.Empty).Contains("window.location=") ||
                                             node.InnerText.Replace(" ", string.Empty).Contains("location.assign(")))
                        {
                            redirection = true; //Probale Redirect      
                        }
                    }
                    //Redirection with <META HTTP-EQUIV=Refresh CONTENT="0; URL=http://shop.leselle.it"> EX: http://www.leselle.it
                    HtmlAgilityPack.HtmlNodeCollection meta = testRedirect.DocumentNode.SelectNodes("//meta[@http-equiv='refresh']");
                    if (meta != null)
                    {
                        if (meta.Any(node => node.Attributes["content"].Value.Replace(" ", string.Empty).Contains("url=") ||
                                               node.Attributes["content"].Value.Replace(" ", string.Empty).Contains(";http")))
                        {
                            redirection = true; //Probable Redirect      
                        }
                    }


                    xmlElement2.Attributes.Append(xmlAttribute);
                    var cData = xmlOutput.CreateCDataSection(strWebResponse);
                    xmlElement2.AppendChild(cData);

                    //Console.WriteLine("IsFromCache? {0}", myHttpWebResponse.IsFromCache);

                    xmlAttribute.Value = "OK";

                    xmlElement2.SetAttribute("Redirection", redirection ? "1" : "0");


                }
                catch (TimeoutException)
                {
                    Environ.ErrorRaised = ErrorDescriptionEnum.TimeOutElapsed;
                    xmlAttribute.Value = "ERR";

                    retvalue = -1;
                }
                catch (Exception)
                {
                    Environ.ErrorRaised = ErrorDescriptionEnum.WebSiteError;
                    xmlAttribute.Value = "ERR";

                    retvalue = -1;
                }


                xmlElement.AppendChild(xmlElement2);
                xmlOutput.AppendChild(xmlElement);

                //XmlOutput.Save(FileXml);

                xmlResponse = xmlOutput;

                return retvalue;
            }

            /*
            public class CustomWebClient : WebClient
            {
                public CustomWebClient() { }

                public override WebRequest GetWebRequest(Uri address)
                {
                    var request = base.GetWebRequest(address) as HttpWebRequest;
                    request.userAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0;)";

                    //... your other custom code...

                    return request;
                }
            }
             * */
            #endregion

            /// <summary>
            /// Questa funzione permette di scaricare da un url passando dei Post Data
            /// USe: DownloadUrlFileByPostData(Formname\url, reqPostData, "POST", "TestFilePDF.pdf", out _responseData);
            /// </summary>
            /// <param name="url"></param>
            /// <param name="method"></param>
            /// <param name="fullNameSaveFile"></param>
            /// <param name="responseData"></param>
            /// <param name="uploadData"></param>
            /// <returns></returns>
            private bool DownloadUrlFileByPostData(string url, NameValueCollection uploadData, string method, string fullNameSaveFile, out byte[] responseData)
            {
                var retVal = false;
                responseData = null;

                try
                {
                    WebClient webCl1;
                    //string FileName = @"c:\\test.pdf";

                    //webCl1.Headers.Add(HttpRequestHeader.ProxyAuthorization, "");
                    //webCl1.Proxy = System.Net.GlobalProxySelection.GetEmptyWebProxy();


                    var defaultWebProxy = WebRequest.DefaultWebProxy;
                    defaultWebProxy.Credentials = CredentialCache.DefaultCredentials;
                    webCl1 = new WebClient
                    {
                        Proxy = defaultWebProxy
                    };
                    //webCl1.Headers.Add(HttpRequestHeader.cookie, webBrowser1.Document.cookie);

                    method = (method.ToLower() == "post") ? "POST" : "GET";

                    var responsebytes = webCl1.UploadValues(url, method, uploadData);

                    //string responsebody = Encoding.UTF8.GetString(responsebytes);
                    if (responsebytes.Length > 0)
                    {
                        // Open file for reading
                        FileStream fileStream =
                           new FileStream(fullNameSaveFile, FileMode.Create,FileAccess.Write);
                        // Writes a block of bytes to this stream using data from
                        // a byte array.
                        fileStream.Write(responsebytes, 0, responsebytes.Length);

                        // close file stream
                        fileStream.Close();
                        responseData = responsebytes;

                        retVal = true;
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e.Message,e);
                }
                return retVal;
            }

           

            /// <summary>
            /// METHOD OF SUPPORT FOR OTHER TIPOLOGY OF COMMUNICATION WITH THE WEB
            /// </summary>
            /// <param name="mittente"></param>
            /// <param name="dest"></param>
            /// <param name="incc"></param>
            /// <param name="subject"></param>
            /// <param name="body"></param>
            /// <param name="attachmentName"></param>
            /// <param name="smtpHost"></param>
            /// <param name="smtpPort"></param>
            /// <param name="smtpUser"></param>
            /// <param name="smtpPassword"></param>
            /// <returns></returns>
            private static bool SendMail(string mittente, IReadOnlyList<string> dest, IReadOnlyList<string> incc, string subject, string body, string[] attachmentName, string smtpHost, int smtpPort, string smtpUser, string smtpPassword)
            {
                //routine che crea ed invia una mail con i dati ricevuti in input

                if (dest.Count == 0 || mittente == "") return false;

                try
                {
                    //imposto l'smtp client
                    string mailServer;
                    mailServer = string.IsNullOrEmpty(smtpHost) ? "smtp.crifnet.com" : smtpHost;

                    SmtpClient myClient;
                    myClient = smtpPort == 0 ? new SmtpClient(mailServer) : new SmtpClient(mailServer, smtpPort);

                    //gestisco la password sicura
                    if (!string.IsNullOrEmpty(smtpUser) && !string.IsNullOrEmpty(smtpPassword))
                        myClient.Credentials = new NetworkCredential(smtpUser, smtpPassword);
                
                    //creo il messaggio
                    var myMessage = new MailMessage();

                    var from = new MailAddress(mittente);
                    myMessage.From = from;
                    foreach (var to in from t in dest where t.Length > 5 select new MailAddress(t))
                    {
                        myMessage.To.Add(to);
                    }
                    if (incc != null)
                    {
                        foreach (var cc in from t in incc where t.Length > 5 select new MailAddress(t))
                        {
                            myMessage.CC.Add(cc);
                        }
                    }

                    myMessage.Subject = subject;
                    myMessage.Body = body;

                    if (attachmentName != null)
                    {
                        foreach (var myAttachment in from attachfile in attachmentName where attachfile != "" select new Attachment(attachfile))
                        {
                            myMessage.Attachments.Add(myAttachment);
                        }
                    }
                    myClient.Send(myMessage);
                    myMessage.Attachments.Dispose();
                    myMessage.Dispose();
                }
                catch (Exception)
                {
                    //outMessage = e.Message + "\r\n" + e.InnerException;

                    return false;
                }
                return true;
            }

            //METHOD TO TEST
            public void test_1(string sUrl)
            {
                var url = new Uri(sUrl);

                ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
                var cookieJar = new CookieContainer();

                var request = (HttpWebRequest)WebRequest.Create(url);
                request.CookieContainer = cookieJar;
                request.Method = "GET";
                HttpStatusCode responseStatus;

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    responseStatus = response.StatusCode;
                    url = request.Address;
                }

                if (responseStatus == HttpStatusCode.OK)
                {
                    var urlBuilder = new UriBuilder(url);
                    urlBuilder.Path = urlBuilder.Path.Remove(urlBuilder.Path.LastIndexOf('/')) + "/j_security_check";

                    request = (HttpWebRequest)WebRequest.Create(urlBuilder.ToString());
                    request.Referer = url.ToString();
                    request.CookieContainer = cookieJar;
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";

                    using (var requestStream = request.GetRequestStream())
                    using (var requestWriter = new StreamWriter(requestStream, Encoding.ASCII))
                    {
                    requestWriter.Write("j_username=user&j_password=user&submit=Send");
                    }

                    string responseContent;

                    using (var response = (HttpWebResponse)request.GetResponse())
                    using (var responseStream = response.GetResponseStream())
                        // ReSharper disable once AssignNullToNotNullAttribute
                    using (var responseReader = new StreamReader(responseStream))
                    {
                        responseContent = responseReader.ReadToEnd();
                    }

                    Console.WriteLine(responseContent);
                }
                else
                {
                    Console.WriteLine(@"Client was unable to connect!");
                }       
            }

            //public void test_2(string url) 
            //{
            //    string data = _request.Form["data"];
            //    data = HttpUtility.UrlDecode(data);
            //    Response.Clear();
            //    //Response.AddHeader("content-disposition", "attachment;filename=report.xls");
            //    Response.AddHeader("content-disposition", "attachment;filename=report.pdf");
            //    Response.Charset = "";
            //    Response.ContentType = "application/pdf";
            //    HttpContext.Current.Response.Write( data );
            //    HttpContext.Current.Response.Flush();
            //    HttpContext.Current.Response.End();
            //    //- See more at: http://growingtech.blogspot.in/2012/10/export-html-to-excel-using-jquery-and.html#   .23cnbVme.dpuf
            //}
        }

                         
}

         /// <summary>
         ///  HttpRequestResponse s = new HttpRequestResponse("GET", WEB);
         ///  string sResponse = s.SendRequest2(); //sendrequest2 modificata per rispecchiare i parametrii del crif lab
         ///  HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
         ///  doc.LoadHtml(sResponse);
         /// </summary>
        public class HttpRequestResponse
        {
             private static readonly ILog Log = LogManager.GetLogger<HttpRequestResponse>();
             private readonly string _uri;
             private readonly string _request;
             private string _requestMethod = "GET";


             public HttpRequestResponse(string pRequest, string pUri)//Constructor
            {
                _request = pRequest;
                _uri = pUri;
            }

    //public string HttpUserName { get { return _userName; } set { _userName = value; } }
    //public string HttpUserPassword { get { return _userPwd; } set { _userPwd = value; } }
    //public string ProxyServer { get { return _proxyServer; } set { _proxyServer = value; } }
    //public int ProxyPort { get { return _proxyPort; } set { _proxyPort = value; } }
    //public string Status { get { return _status; } set { _status = value; } }
    //public bool Allowautoredirect { get { return _allowAutoRedirect; } set { _allowAutoRedirect = value; } }
    //private int Timeout { get { return _timeout; } set { _timeout = value; } }
    //private bool Keepalive { get { return _keepAlive; } set { _keepAlive = value; } }
    //private string Useragent { get { return _userAgent; } set { _userAgent = value; } } //Emulo il browser   

    public string HttpUserName { get; set; }

    public string HttpUserPassword { get; set; }

    public string ProxyServer { get; set; }

    public int ProxyPort { get; set; }

    public string Status { get; set; }

    public bool Allowautoredirect { get; set; }

    private bool Keepalive { get; set; }

    private string Useragent { get; set; } //Emulo il browser   

    /*This public interface receives the request and send the response of type string. */
    public string SendRequest()
            {
                string finalResponse;
                var cookie = "";

                var collHeader = new NameValueCollection();

                var baseHttp = new HttpBaseUtil(HttpUserName, HttpUserPassword, ProxyServer, ProxyPort, _request);
                try
                {
                    var webrequest = baseHttp.CreateWebRequest(_uri, collHeader, _requestMethod, true);
                    var webresponse = (HttpWebResponse)webrequest.GetResponse();

                    var redirectUrl = baseHttp.GetRedirectUrl(webresponse, ref cookie);
                    //Check if there is any redirected _uri.
                    webresponse.Close();
                    redirectUrl = redirectUrl.Trim();
                    if (redirectUrl.Length == 0) //No redirection _uri
                    {
                        redirectUrl = _uri;
                    }
                    _requestMethod = "POST";
                    finalResponse = baseHttp.GetFinalResponse(redirectUrl, cookie, _requestMethod, true);

                }//End of Try Block

                catch (WebException e)
                {
                    throw CatchHttpExceptions(e.Message);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                return finalResponse;
            } //End of SendRequestTo method

            public string GetCurrentUri()
            {
                return _reUri;
            }

            public string GetCurrentStatus()
            {
                return Status;
            }

            private string _reUri;
            /*This public interface receives the request and send the response of type string. */
            public string SendRequest2()
            {
                string finalResponse;
                var cookie = "";

                //new NameValueCollection();

                //HttpBaseClass BaseHttp = new HttpBaseClass(_userName, _userPwd, _proxyServer, _proxyPort, _request);
                var baseHttp = new HttpBaseUtil(true, 1 * 10000, true, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2", _request);
                //HttpBaseClass BaseHttp = new HttpBaseClass(allowAutoRedirect, timeout, keepAlive, userAgent, _request);
                try
                {
                    //HttpWebRequest webrequest = BaseHttp.CreateWebRequest(_uri, collHeader, requestMethod, true);
                    var webrequest = baseHttp.CreateWebRequest(_uri, null, _requestMethod, false);
                    var webresponse = (HttpWebResponse)webrequest.GetResponse();
                    _reUri = baseHttp.GetRedirectUrl(webresponse, ref cookie);

                    Status = webresponse.StatusDescription;

                    //string reUri = BaseHttp.GetRedirectUrl(webresponse, ref cookie);
                    //Check if there is any redirected _uri.
                    webresponse.Close();
                    _reUri = _reUri.Trim();
                    if (_reUri.Length == 0) //No redirection _uri
                    {
                        _reUri = _uri;
                    }
                    _requestMethod = "POST";
                    finalResponse = baseHttp.GetFinalResponse(_reUri, cookie, _requestMethod, true);

                }//End of Try Block
                catch (WebException e)
                {
                    throw CatchHttpExceptions(e.Message);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }                
                return finalResponse;
            } //End of SendRequestTo method

            private WebException CatchHttpExceptions(string errMsg)
            {
                errMsg = "Error During Web Interface. Error is: " + errMsg;
                return new WebException(errMsg);
            }


           
        }//End of RequestResponse Class

        public class HttpCrawler 
        {
            private static readonly ILog Log = LogManager.GetLogger<HttpCrawler>();
            //First crawl the entire web content, data on the byte [] (transmitted on the network in the form byte) further converted to a String, so that its operation, the following are examples:
            private static string GetPageData(string url)
            {
                if (url == null || url.Trim () == "")
                    return null;
                var wc = new WebClient {Credentials = CredentialCache.DefaultCredentials}; // defined
                var pageData = wc.DownloadData (url);
                return Encoding.Default.GetString (pageData) ;//. ASCII.GetString
            }

            //Above simply access to the website data, but the page still exist in the coding problem, this time in order to avoid the problem of garbled,
            //you need to be dealt with the following code:
            // To access the Web site address, charset is the encoding of the target page, if the incoming encoding is null or "", 
            //it automatically analyzes web pages
            private string GetPageData (string url, string charSet)
            {
                var strWebData = string.Empty;
                if (string.IsNullOrEmpty(url)) return strWebData;
                var myWebClient = new WebClient {Credentials = CredentialCache.DefaultCredentials};
                // Create a WebClient instance myWebClient
                // Need to pay attention to:
                // Some pages may be the next down, there are a variety of reasons such as the need cookies, coding problems, etc.
                // This is necessary to analyze specific issues, such as the head added to the cookie
                // Webclient.Headers.Add ("cookie", cookie);
                // This may take some overloaded methods. Need to write on it
                // Gets or sets the network credentials used to authenticate the request to the Internet resource.
                // If the server to verify the user name, password
                // NetworkCredential mycred = new NetworkCredential (struser, strpassword);
                // MyWebClient.Credentials = mycred;
                // Download data from the resource and returns a byte array. (Add @ since the middle of the URL "/" symbol)
                byte [] myDataBuffer = myWebClient.DownloadData (url);
                strWebData = Encoding.Default.GetString (myDataBuffer);
                // Get the page character encoding description
                var charSetMatch = Regex.Match (strWebData, "<meta ([^<]*) charset = ([^<]*) /\" ", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                var webCharSet = charSetMatch.Groups [2].Value;
                if (string.IsNullOrEmpty(charSet))
                {
                    // If you do not get to coding, set the default encoding
                    charSet = webCharSet == "" ? "UTF-8" : webCharSet;
                }
                if (charSet!= "" && !Equals(Encoding.GetEncoding (charSet), Encoding.Default))
                {
                    strWebData = Encoding.GetEncoding (charSet).GetString(myDataBuffer);
                }
                return strWebData;
             }

            //Data string, and then parse Web pages (in fact, the various operations on strings and regular expressions):
            // Parse the page, find the link
            // Here need to be extended, there is some form of link is not recognized
            public string ParseThePageFindTheLink (string strResponse){
                var strStatus = "";//?????
                // string strRef = @ "(href | HREF | src | SRC | action | ACTION | Action) [] * = [] * [" "'] [^" "' #>] + [" "']"; 
                const string strRef = "(href|HREF|src|SRC|action|ACTION|Action)[] * = [] * [\" \"'] [^\" \"' #>]+ [\" \"']";
                var matches = new Regex(strRef).Matches(strResponse);
                strStatus += "found:" + matches.Count + "a link /r/n";

                //The above example will be a link in a web page parsing out, StrRef variables means that the regular expression pattern, 
                //variable matches said the collection of items which matches were found, behind Regex (StrRef). Matches (strResponse) is to create
                //regular rules in line makes strResponse The mode string strRef return. Variable and then calls the matches can get all kinds of information.
                //Of course, there can only identify some basic form of links, like links in the script and some without "link is not supported, this extension
                //is quite simple.

                //Commonly used analytical as well as the following:
                // Get the Title
                var titleMatch = Regex.Match (strResponse, "<Title> ([^<]*) </ Title>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                var title = titleMatch.Groups [1].Value;

                // Get the description of
                var desc = Regex.Match (strResponse, "<Meta name = /\" DESCRIPTION /\"content = /\" ([^<]*) / \">", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                var strdesc = desc.Groups[1].Value;

                // Get the size of the page
                var size = strResponse.Length;

                // Removal HTML tags
                strResponse = StripHtml(strResponse);

                //GET WORDS ONLY
                strResponse = allSpaceInASpace(strResponse);
                return "Status:" + strStatus + ",Title:" + title + ",Description:" + strdesc + ",Response:" +
                       strResponse+",Size:"+size;
            }

            private static string StripHtml (string strHtml)
            {
                var objRegExp = new Regex ("<(.|/N)+?>");
                var strOutput = objRegExp.Replace (strHtml, "");
                strOutput = strOutput.Replace ("<", "<");
                strOutput = strOutput.Replace (">", ">");
                return strOutput;
            }

            //Some exceptions will make removal is not clean, it is recommended that two consecutive transformation. Html tags such conversion in 
            //order to spaces. Too many consecutive spaces will affect the operation of the string. So then add this statement:
            // All spaces into a space
            private string allSpaceInASpace(string strResponse){
                var r = new Regex (@"/s+");
                var wordsOnly = r.Replace (strResponse, "");
                wordsOnly = wordsOnly.Trim ();
                return wordsOnly;
            }

            //I:Write simple, but I can see and understand, pay attention to using System.Text;
            //using System.Text.RegularExpressions; write on
            //Practical application: I do the test, to obtain a district meteorological department weather forecast
            private static string GetPageData2 (string url) //Get the url string
            {
                if (url == null || url.Trim () == "")
                    return null;
                var wc = new WebClient {Credentials = CredentialCache.DefaultCredentials};

                var pageData = wc. DownloadData (url);
                return Encoding.Default.GetString (pageData) ;//. ASCII.GetString
            }
            /*
            protected void Button1_Click (object sender, EventArgs e)
            {
                string strResponse = GetPageData (TextBox1.Text);
                string strRef = "(href|HREF|src|SRC|action|ACTION|Action) [] * = [] * [\" \"'] [^\" \"' #>] + [\" \"']";
                MatchCollection matches = new Regex (strRef). Matches (strResponse) ;// strResponse matching string
                TextBox2.Text = "Found:" + matches.Count + "a link /r/n";
                //Above modeled on the example of learning, the test was successful, really successful, the following application
                string strRef2 = "MARQUEE" ;// define the key string, I had to get keywords that are seen in the pages of the data, see the html source
                TextBox3.Text = strResponse.Substring (strResponse.IndexOf (strRef2, 2800) +120, 135).ToString();
                //StrResponse string interception from strResponse.IndexOf (strRef2, 2800) +120 135 characters.
            }
            */

            //Test: I entered in TextBox1 Text: , then immediately get weather reports text, which is the most basic c # crawl class.
            //Finally, you need a little modification oh. In character uncertainty because sometimes you need to do to adjust, such as the weather, and sometimes not so much character display, you insist, an error occurs Oh. My source code segment
            // Grab weather conditions
            private string GrabWeatherConditions(string strResponse){
                //string strResponse = GetPageData("");
                string getWeathe;
                const string strRef2 = "MARQUEE";
                var strLastIndex = strResponse.Substring (strResponse.IndexOf (strRef2, 2800, StringComparison.Ordinal) + 113, 70).Trim();
                //when the the 
                if (strLastIndex.IndexOf('0')> 1) // 091110 found that it has been unable to resolve the two '' problems, modify genuine line 10
                {
                    getWeathe = strResponse.Substring (strResponse.IndexOf(strRef2, 2800, StringComparison.Ordinal) + 113, strLastIndex.IndexOf('0')+2).Trim();
                    // String IndexOf (strRef2, 2800) + 113 '' marked by the end of
                }
                else
                {
                    getWeathe = strResponse.Substring (strResponse.IndexOf (strRef2, 2800, StringComparison.Ordinal) + 113, 60).Trim();
                }
                //The main advantage of the indexof judgment '' character position to address vulnerability sometimes can not be displayed
                //a global variable, get_weathe that time can be in the foreground with the javascript call. ok
                //091110 update

                if (getWeathe == "" && strLastIndex.IndexOf('0')> 1)
                {
                    if (strLastIndex.IndexOf('0') <50) // judge '' in the string is greater than an index, but more than one ''
                    {
                        getWeathe = strResponse.Substring (strResponse.IndexOf (strRef2, 2800, StringComparison.Ordinal) + 113, strLastIndex.LastIndexOf('0') + 10).Trim();
                        // from IndexOf (strRef2 2800) + 113 '' marked by the end of
                    }
                    else
                    {
                        getWeathe = strResponse.Substring (strResponse.IndexOf (strRef2, 2800, StringComparison.Ordinal) + 113, strLastIndex.IndexOf('0') + 2).Trim();
                        // String IndexOf (strRef2, 2800) + 113 '' marked by the end of
                    }
                }
                else
                {
                    getWeathe = strResponse.Substring (strResponse.IndexOf (strRef2, 2800, StringComparison.Ordinal) + 113, 60).Trim();
                }
                return getWeathe;
            }
            //Of course, we can crawl a site data changes updated to use the OUTPUT && input database.
            //The key to this example is: various operations on strings and regular expressions application


            //Briefly explain WebClient:
            //The WebClient class provides public methods the URI identifies any local, intranet or Internet resources to send data and receive data from these resources.
            //WebClient class uses the WebRequest class to provide access to resources. The WebClient instance by has WebRequest.RegisterPrefix method of WebRequest descendants registered access the data.
            //Note
            //By default NET Framework support to http:, https:, ftp:, and file: scheme identifiers beginning of the URI.

            //The following describes WebClient methods used to upload data to the resources:
            //OpenWrite retrieves one used to send data to the resource Stream.
            //OpenWriteAsync retrieval Stream, without blocking the calling thread to send data to the resource.
            //UploadData array of bytes sent to the resource and returns a Byte array containing any response.
            //The case does not block the calling thread, UploadDataAsync Byte array to send to the resource.
            //The UploadFile local file is sent to the resource and returns a Byte array containing any response.
            //UploadFileAsync case does not block the calling thread, the local file is sent to the resource.
            //The UploadValues ??the NameValueCollection sent to the resource and returns a Byte array containing any response.
            //The case does not block the calling thread, UploadValuesAsync NameValueCollection sent to the resource and returns a 
            //Byte array containing any response.
            //UploadString case does not block the calling thread, String sent to the resources.
            //UploadStringAsync case does not block the calling thread, String sent to the resources.

            //The following describes the the WebClient methods of data downloaded from the resource:
            //OpenRead return data from the resource as a Stream.
            //OpenReadAsync without blocking the calling thread returns from the resource data.
            //DownloadData download data from a resource and returns a Byte array.
            //DownloadDataAsync without blocking the calling thread, download the data from the resource and returns a Byte array.
            //DownloadFile to download data from the resource to a local file.
            //DownloadFileAsync case does not block the calling thread, the data downloaded from the resource to a local file.
            //DownloadString from the Download String and a String.
            //DownloadStringAsync without blocking the calling thread, downloaded from the resource String.

            //You can use the CancelAsync method to cancel asynchronous operations that have not yet been completed.
            //By default, the WebClient instance does not send optional HTTP headers. If your request requires an optional header, 
            //the header must be added to the Headers collection. For example, to be retained in the response to query, it is necessary 
            //to add the User-Agent header. In addition, if the user agent header is missing, the server may return 500 (Internal Server Error).
            //In the the WebClient example, The allowAutoRedirect set to true.
            //To Inheritors Derived classes should call the base class implementation of WebClient to ensure that the derived class work as expected.
            //For example:
            //C # WebClient class to steal site (the website ) phone number information

            //First of all, we want to check each other's cell phone location and what type of card that we use C # package class WebClient, 
            //NameValueCollection, Regex, respectively, belonging to a namespace using System.Net using System.Text using System.Collections . 
            //Specialized, using System.Text.RegularExpressions. Tested in VS2005 environment, the following code tests are as follows:
            private void InitWeaOne (string url)
            {
                 WebClient wb = new WebClient ();
                 NameValueCollection myNameValueCollection = new NameValueCollection ();          
                 myNameValueCollection.Add ("mobile", "13777483912");
                 myNameValueCollection.Add ("action", "mobile");
                 byte[] pageData = wb.UploadValues(url,myNameValueCollection);
                     //(myNameValueCollection);
                 string result = Encoding.Default.GetString (pageData);
                 string pat = "tdc2> ([^ <] *) </ TD>";
                 Regex r = new Regex (pat, RegexOptions.IgnoreCase);
                 Match m = r.Match (result);
                 string [] strInfo = new string [] {"", "", ""};
                 int i = 0;
                 while (m.Success)
                 {
                     if (i <strInfo.Length)
                     {
                         strInfo [i] = m.ToString (). Substring (5);
                     }
                     m = m.NextMatch ();
                     i++;
                }
                //string a = strInfo [0].ToString ();
                //string g = strInfo [1].ToString ();
                //string f = strInfo [2].ToString ();         
            }

            
        }
