﻿using System;
using System.Collections.Generic;
using System.IO;

namespace UtilityCSharp.logging
{
    //LogWriter writer = LogWriter.Instance;
    //writer.WriteToLog(message);


    //APPUNTI

    //Cartella su dove si trova il file
    //Variabile che identifica la Path della cartella debug all'interno del programma
    //string PathDDL = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;

    //string filePath5 = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;    

    //string filePath = Path.GetDirectoryName(ConfigurationManager.AppSettings[PathDLL]);
    //string filePath2 = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
    //string filePath3 = Environment.CurrentDirectory; ;
    //string filepath4 = System.IO.Directory.GetCurrentDirectory();
    //Metti un parent per ogni volta che vuoi salire con la cartella
    //string filePath5 = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName; 
    //string filePath5 = "";

    /// <summary>
    /// A Logging class implementing the Singleton pattern and an internal Queue to be flushed perdiodically
    /// USE:  
    ///       //Il mio file di log
    ///        LogWriter writer = LogWriter.Instance;
    /// </summary>
    public class SystemLog
    {
        private static SystemLog _instance;
        private static Queue<Log> _logQueue;
        //private static string LogDir = <Path to your Log Dir or Config Setting>;
        //private static string _logFile = <Your Log File Name or Config Setting>;
        //private static int MaxLogAge = int.Parse(<Max Age in seconds or Config Setting>);
        //private static int QueueSize = int.Parse(<Max Queue Size or Config Setting);
        //e.g. new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;
        private static readonly string LogDir =
            new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase))
                .LocalPath;

        private static readonly string _logFile = GetDate() + "_LOG.txt";
        private static readonly string LogPath = LogDir + "\\" + _logFile;
        private static readonly int MaxLogAge = int.Parse("100");
        private static readonly int QueueSize = int.Parse("1");
        private static DateTime _lastFlushed = DateTime.Now;

        /// <summary>
        /// An LogWriter _instance that exposes a single _instance
        /// </summary>
        public static SystemLog Instance
        {
            get
            {
                // If the _instance is null then create one and init the Queue
                if (_instance != null) return _instance;
                if (File.Exists(LogPath))
                {
                    File.Delete(LogPath);
                }

                _instance = new SystemLog();
                lock (_logQueue)
                {
                    _logQueue = new Queue<Log>();
                }
                return _instance;
            }
        }


        /// <summary>
        /// The single _instance method that writes to the log file
        /// </summary>
        /// <param name="message">The message to write to the log</param>
        public void WriteToLog(string message)
        {
            // Lock the queue while writing to prevent contention for the log file
            lock (_logQueue)
            {
                // Create the entry and push to the Queue
                var logEntry = new Log(message);
                _logQueue.Enqueue(logEntry);

                // If we have reached the Queue Size then flush the Queue
                if (_logQueue.Count >= QueueSize || DoPeriodicFlush())
                {
                    FlushLog();
                }
            }
        }

        /// <summary>
        /// The single _instance method that writes to the log file
        /// </summary>
        /// <param name="message">The message to write to the log</param>
        /// /// <param name="type">The Level type of the log</param>
        public void WriteToLog(string message, string type)
        {
            // Lock the queue while writing to prevent contention for the log file
            lock (_logQueue)
            {
                // Create the entry and push to the Queue
                var logEntry = new Log(message, type);
                _logQueue.Enqueue(logEntry);

                // If we have reached the Queue Size then flush the Queue
                if (_logQueue.Count >= QueueSize || DoPeriodicFlush())
                {
                    //FlushLog();
                    //Modificato
                    Write(_logFile, LogDir, logEntry.ToString());
                }
            }
        }

        private static bool DoPeriodicFlush()
        {
            var logAge = DateTime.Now - _lastFlushed;
            if (logAge.TotalSeconds >= MaxLogAge)
            {
                _lastFlushed = DateTime.Now;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Flushes the Queue to the physical log file
        /// </summary>
        private void FlushLog()
        {
            while (_logQueue.Count > 0)
            {
                var entry = _logQueue.Dequeue();
                //string LogPath = LogDir +"\\"+ entry.LogDate + "_" + _logFile;
                // This could be optimised to prevent opening and closing the file for each write                 
                using (var fs = File.Open(LogPath, FileMode.Append, FileAccess.Write))
                {
                    using (var log = new StreamWriter(fs))
                    {
                        log.WriteLine("{0}\t{1}", entry.LogTime, entry.Message);
                    }
                }
                /*
                        using (FileStream fs = File.Open(LogPath, FileMode.Create, FileAccess.Write))
                        {
                            using (StreamWriter log = new StreamWriter(fs))
                            {
                                log.WriteLine(string.Format("{0}\t{1}", entry.LogTime, entry.message));
                            }
                        }
                        */
            }
        }

        /// <summary>
        /// Metodo che legge il contenuto di un file di default se la filepath è null lo legge dalla cartella di debug
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private List<string> Read(string fileName, string filePath)
        {
            var listOfRecord = new List<string>();
            if (!File.Exists(fileName)) return listOfRecord;
            using (var sr = new StreamReader(filePath + fileName, true))
            {
                while (sr.EndOfStream == false)
                {
                    listOfRecord.Add(sr.ReadLine());
                    if (sr.EndOfStream) break;
                }
            }
            return listOfRecord;
        }

        /// <summary>
        /// Metodo che scrive il contenuto di una lista di stringhe in un file di default se la filepath è null lo legge dalla cartella di debug
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="filePath"></param>
        /// <param name="listOfRecord"></param>
        private static void Write(string fileName, string filePath, List<string> listOfRecord)
        {
            if (filePath == null)
            {
                filePath = "";
            }
            foreach (var s in listOfRecord)
            {
                using (var sw = new StreamWriter(filePath + fileName, true))
                {
                    sw.WriteLine(s);
                    sw.Flush();
                }
            }
        }

        /// <summary>
        /// Metodo che scrive il contenuto di una lista di stringhe in un file di default se la filepath è null lo legge dalla cartella di debug
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="filePath"></param>
        /// <param name="message"></param>
        private static void Write(string fileName, string filePath, string message)
        {
            if (filePath == null)
            {
                filePath = "";
            }
            using (var sw = new StreamWriter(filePath + "\\" + fileName, true))
            {
                sw.WriteLine(message);
                sw.Flush();
            }
        }

        //METODI DI MARCO
        private static string GetDateAndTime()
        {
            //DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz") //for get the timezone
            return DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
        }

        private static string GetDate()
        {
            //DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz") //for get the timezone
            //LogDate = DateTime.Now.ToString("yyyy-MM-dd");
            return DateTime.Now.ToString("yyyy-MM-dd");
        }

        private static string GetTime()
        {
            return DateTime.Now.ToString("hh:mm:ss.fff tt");
        }

        /// <summary>
        /// The single _instance method that writes to the log file
        /// </summary>
        /// <param name="msg">The message to write to the log</param>
        public void Log(string msg)
        {
            // Lock the queue while writing to prevent contention for the log file
            lock (_logQueue)
            {
                // Create the entry and push to the Queue
                var logEntry = new Log(msg);
                _logQueue.Enqueue(logEntry);

                // If we have reached the Queue Size then flush the Queue
                if (_logQueue.Count >= QueueSize || DoPeriodicFlush())
                {
                    FlushLog();
                }
            }
        }

        /// <summary>
        /// The single _instance method that writes to the log file
        /// </summary>
        /// <param name="msg">The message to write to the log</param>
        /// <param name="type">The Level type of the log</param>
        public void Log(string msg, string type)
        {
            // Lock the queue while writing to prevent contention for the log file
            lock (_logQueue)
            {
                // Create the entry and push to the Queue
                var logEntry = new Log(msg, type);
                _logQueue.Enqueue(logEntry);

                // If we have reached the Queue Size then flush the Queue
                if (_logQueue.Count >= QueueSize || DoPeriodicFlush())
                {
                    //FlushLog();
                    //Modificato
                    Write(_logFile, LogDir, logEntry.ToString());
                }
            }
        }

        /// <summary>
        /// The single _instance method that writes to the log file
        /// </summary>
        /// <param name="msg">The message to write to the log</param>
        public static void Out(string msg)
        {
            // Lock the queue while writing to prevent contention for the log file
            lock (_logQueue)
            {
                // Create the entry and push to the Queue
                var logEntry = new Log();
                _logQueue.Enqueue(logEntry);

                // If we have reached the Queue Size then flush the Queue
                if (_logQueue.Count < QueueSize) return;
                //FlushLog();
                //Modificato
                logEntry.Out(msg);

                Write(_logFile, LogDir, logEntry.ToString());
            }
        }

        /// <summary>
        /// The single _instance method that writes to the log file
        /// </summary>
        /// <param name="msg">The message to write to the log</param>
        public static void Warn(string msg)
        {
            // Lock the queue while writing to prevent contention for the log file
            lock (_logQueue)
            {
                // Create the entry and push to the Queue
                var logEntry = new Log();
                _logQueue.Enqueue(logEntry);

                // If we have reached the Queue Size then flush the Queue
                if (_logQueue.Count < QueueSize) return;
                //FlushLog();
                //Modificato
                logEntry.Warn(msg);
                Write(_logFile, LogDir, logEntry.ToString());
            }
        }

        /// <summary>
        /// The single _instance method that writes to the log file
        /// </summary>
        /// <param name="msg">The message to write to the log</param>
        public static void Err(string msg)
        {
            // Lock the queue while writing to prevent contention for the log file
            lock (_logQueue)
            {
                // Create the entry and push to the Queue
                Log logEntry = new Log();
                _logQueue.Enqueue(logEntry);

                // If we have reached the Queue Size then flush the Queue
                if (_logQueue.Count < QueueSize) return;
                //FlushLog();
                //Modificato
                logEntry.Err(msg);
                Write(_logFile, LogDir, logEntry.ToString());
            }
        }

        /// <summary>
        /// The single _instance method that writes to the log file
        /// </summary>
        /// <param name="msg">The message to write to the log</param>
        public static void Exit(string msg)
        {
            // Lock the queue while writing to prevent contention for the log file
            lock (_logQueue)
            {
                // Create the entry and push to the Queue
                var logEntry = new Log();
                _logQueue.Enqueue(logEntry);

                // If we have reached the Queue Size then flush the Queue
                if (_logQueue.Count < QueueSize) return;
                //FlushLog();
                //Modificato
                logEntry.Exit(msg);
                Write(_logFile, LogDir, logEntry.ToString());
                if (System.Windows.Forms.Application.MessageLoop)
                {
                    // WinForms app
                    //Informs all message pumps that they must terminate, and then closes all application windows after 
                    //the messages have been processed. This is the code to use if you are have called Application.Run (WinForms applications), 
                    //this method stops all running message loops on all threads and closes all windows of the application.
                    System.Windows.Forms.Application.Exit();
                }
                else
                {
                    // Console app
                    //Terminates this process and gives the underlying operating system the specified exit code. 
                    //This is the code to call when you are using console application. e.g. System.Environment.Exit(1);
                    Environment.Exit(1);
                }
            }
        }

        /// <summary>
        /// The single _instance method that writes to the log file
        /// </summary>
        /// <param name="ex">The message to write to the log</param>
        public static void Exc(Exception ex)
        {
            // Lock the queue while writing to prevent contention for the log file
            lock (_logQueue)
            {
                // Create the entry and push to the Queue
                var logEntry = new Log();
                _logQueue.Enqueue(logEntry);

                // If we have reached the Queue Size then flush the Queue
                if (_logQueue.Count < QueueSize) return;
                //FlushLog();
                //Modificato
                logEntry.Exc(ex);
                Write(_logFile, LogDir, logEntry.ToString());
            }
        }

        //Creazione Log di esecuzione
        protected void LogFile(string message, string stringFilePathLog)
        {
            if (Directory.Exists(stringFilePathLog) == false)
                Directory.CreateDirectory(stringFilePathLog);

            using (
                var fileLog =
                    new StreamWriter(stringFilePathLog + "\\Log.ERR.RomisWeb3." + DateTime.Now.Date.Year +
                                     DateTime.Now.Month.ToString().PadLeft(2, '0') +
                                     DateTime.Now.Day.ToString().PadLeft(2, '0') +
                                     DateTime.Now.Hour.ToString().PadLeft(2, '0') +
                                     DateTime.Now.Minute.ToString().PadLeft(2, '0') +
                                     DateTime.Now.Second.ToString().PadLeft(2, '0') + ".log"))
            {
                //StreamWriter FileLog = new StreamWriter(stringFilePathLog + "\\Log.ERR.RomisWeb3." + DateTime.Now.Date.Year + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0') + ".log");
                if (message.Length > 0) fileLog.WriteLine(message);
                fileLog.Close();
                fileLog.Dispose();
            }
        }

        protected string DateConversion(string data)
        {
            string result;

            switch (data.Length)
            {
                case 8:
                    result = data.Substring(6, 2) + "/" + data.Substring(4, 2) + "/" + data.Substring(0, 4);
                    break;
                case 10:
                    result = data.Substring(6, 4) + data.Substring(3, 2) + data.Substring(0, 2);
                    break;
                default:
                    result = data;
                    break;
            }

            return result;
        }
    } //end class SystemLog


    /// <summary>
    /// A Log class to store the message and the Date and Time the log entry was created
    /// </summary>
    public class Log
    {
        public string Message { get; set; }
        public string LogTime { get; set; }
        public string LogDate { get; set; }


        public Log()
        {
            LogDate = DateTime.Now.ToString("yyyy-MM-dd");
            LogTime = DateTime.Now.ToString("hh:mm:ss.fff tt");
        }


        public Log(string message)
        {
            Message = message;
            LogDate = DateTime.Now.ToString("yyyy-MM-dd");
            LogTime = DateTime.Now.ToString("hh:mm:ss.fff tt");
        }

        public Log(string msg, string type)
        {
            if (type.Contains("ERR"))
            {
                msg = "[ERROR]" + msg;
            }
            else if (type.Contains("WAR"))
            {
                msg = "[WARNING]" + msg;
            }
            else if (type.Contains("OUT"))
            {
                //
            }
            else
            {
                //
            }
            Message = msg;
            LogDate = DateTime.Now.ToString("yyyy-MM-dd");
            LogTime = DateTime.Now.ToString("hh:mm:ss.fff tt");
        }

        public void Out(string msg)
        {
            Message = msg;
        }

        public void Warn(string msg)
        {
            Message = "[WARNING]" + msg;
        }

        public void Err(string msg)
        {
            Message = "[ERROR]" + msg;
        }

        public void Exc(Exception ex)
        {
            Message = "[ERROR]" + ex.Message;
        }

        public void Exit(int msg)
        {
            Message = "[EXIT] Exit of programm with code " + msg + "";
        }

        public void Exit(string msg)
        {
            Message = "[EXIT] Exit of programm with code " + msg + "";
        }

        public int PrintStringToConsole(string msg)
        {
            const int tabSize = 4;
            //string usageText = "Usage: EXPANDTABSEX inputfile.txt outputfile.txt";
            StreamWriter writer;
            var standardOutput = Console.Out;
            var args = msg.Split('|');
            if (args.Length <= 1)
            {
                Console.WriteLine(msg);
                return 1;
            }

            try
            {
                writer = new StreamWriter(args[1]);
                Console.SetOut(writer);
                Console.SetIn(new StreamReader(args[0]));
            }
            catch (IOException e)
            {
                var errorWriter = Console.Error;
                errorWriter.WriteLine(e.Message);
                errorWriter.WriteLine(msg);
                return 1;
            }
            int i;
            while ((i = Console.Read()) != -1)
            {
                var c = (char) i;
                if (c == '\t')
                    Console.Write(("").PadRight(tabSize, ' '));
                else
                    Console.Write(c);
            }
            writer.Close();
            // Recover the standard output stream so that a 
            // completion message can be displayed.
            Console.SetOut(standardOutput);
            Console.WriteLine(@"EXPANDTABSEX has completed the processing of {0}.", args[0]);
            return 0;
        }

        public override string ToString()
        {
            return "[" + LogTime + "]" + Message;
        }

        ////IL NOSTRO PROCESSO LATO Server E' SOLO CONSOLE QUINDI INVECE DI APPLICATION.EXIT SI DEVE USARE ENVIRONMENT.EXIT
        //#region APPUNTI SULLA DIFFERENZA TRA  APPLICATION.EXIT E ENVIRONMENT.EXIT
        ////if (System.Windows.Forms.Application.MessageLoop)
        ////{
        ////    // WinForms app
        ////    //Informs all message pumps that they must terminate, and then closes all application windows after 
        ////    //the messages have been processed. This is the code to use if you are have called Application.Run (WinForms applications), 
        ////    //this method stops all running message loops on all threads and closes all windows of the application.
        ////    System.Windows.Forms.Application.Exit();
        ////}
        ////else
        ////{
        ////    // Console app
        ////    //Terminates this process and gives the underlying operating system the specified exit code. 
        ////    //This is the code to call when you are using console application.
        ////    System.Environment.Exit(1);
        ////}
        //#endregion
    }
}
