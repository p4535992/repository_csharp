﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Common.Logging;

namespace UtilityCSharp
{
    class Program
    {

        private static readonly ILog Log = LogManager.GetLogger<Program>();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string file = "C:\\Users\\Marco\\Desktop\\geodocument_2015_09_18.sql";
            CleanSqlScript(file);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        public static void CleanSqlScript(string absolutePathToSQlScript)
        {
            var symbol = "'";
            IEnumerable lines = new List<string>();
            try
            {
                lines = File.ReadLines(absolutePathToSQlScript);
            }
            catch (Exception e)
            {
                Log.Error("Can't read the File:" + absolutePathToSQlScript, e);
            }
            var newLines = new ArrayList();
            foreach (string line in lines)
            {
                //Regex r = new Regex(symbol+"(.+?)"+symbol, RegexOptions.Compiled);

                var newLine = Regex.Replace(line, symbol + "(.+?)" + symbol, delegate (Match match)
                {
                    string v = match.ToString();
                    v = v.Replace("'", "");
                    v = v.Replace(";", "");
                    v = Regex.Replace(v, @"\s+", " "); //collapse whitespace
                    return v.Trim();
                });            
                newLines.Add(newLine);
            }
            // Create a file to write to.
            string[] createText = (string[])newLines.ToArray();
            try
            {
                File.WriteAllLines(absolutePathToSQlScript + "_" + DateTime.Now, createText, Encoding.UTF8);
            }
            catch (Exception e)
            {
                Log.Error("Can't write the File:" + absolutePathToSQlScript + "_" + DateTime.Now, e);
            }
        }
    }
}
