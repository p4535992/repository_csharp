﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;

namespace UtilityCSharp.WebService
{
    /// <summary>
    /// 2015-04-13
    /// </summary>
    class GooglePageSpeed
    {
        public static int SUsability { get; set; }
        public static int SSpeed { get; set; }
        public static string Title { get; set; }
        public static int ResponseCode { get; set; }


        /*
        {
            "Id": "http://www.unifi.it/",
            "ResponseCode": 200,
            "Title": "UniFI - Università degli Studi di Firenze",
            "RuleGroups": {
                "Speed": {
                    "Score": 73
                },
                "Usability": {
                    "Score": 58
                }
            }
        }
         * */

        private const string ApiKey = "AIzaSyAavqCT9S4yPLKzsoOuTnLIST812w1srdc";

        public static string SetUrl(string url)
        {
            //GET https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=http%3A%2F%2Fwww.unifi.it&filter_third_party_resources=false&strategy=mobile&fields=Id%2CruleGroups%2Ctitle&key={YOUR_API_KEY}
            url = url.Replace(":", "%3A").Replace("/", "%2F");
            var jsonUrl = "https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url="
                            + url
                            + "&filter_third_party_resources=false&strategy=mobile&fields=Id%2CresponseCode%2CruleGroups%2Ctitle&key="
                            + ApiKey;
            return jsonUrl;

        }

        private static Stream GetStreamResponse(string url,int numTentativi)
        {        
            const int maxNumTentativi = 3;
            Stream receiveStream = null;
            try
            {
                url = SetUrl(url);
                // Creates an HttpWebRequest with the specified URL. 
                var myHttpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                // Set the Credentials in two locations to get past the 407 error:
                myHttpWebRequest.Proxy = WebRequest.DefaultWebProxy;
                myHttpWebRequest.Credentials = CredentialCache.DefaultCredentials; 
                myHttpWebRequest.Proxy.Credentials = CredentialCache.DefaultCredentials;

                // Sends the HttpWebRequest and waits for the response.                
                var myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                // Gets the stream associated with the response.            
                receiveStream = myHttpWebResponse.GetResponseStream();
            }
            catch (Exception)
            {
                System.Threading.Thread.Sleep(3000);
                if (numTentativi < maxNumTentativi)
                {
                    numTentativi++;
                    receiveStream =  GetStreamResponse(url, numTentativi);
                }
                else {
                    //dO NOTHING
                }
            }          
            /*
            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");

            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, encode);
            string strresponsexml = readStream.ReadToEnd();
            myHttpWebRequest = null;
            myHttpWebResponse.Close();
            */
            return receiveStream;
        }

        public static void MakeNewJsonRequest(string url)
        {
            SUsability = 0;
            SSpeed = 0;
            Title = null;

            var receiveStream = GetStreamResponse(url,0);
            //Convert stream to json
            var jsonSerializer = new DataContractJsonSerializer(typeof(GoogleMobileSpeed));
            //object objResponse = jsonSerializer.ReadObject(strresponsexml.GetResponseStream());
            var objResponse = jsonSerializer.ReadObject(receiveStream);
            try
            {

                //myHttpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                // Set the Credentials in two locations to get past the 407 error:
                //myHttpWebRequest.Proxy = WebRequest.DefaultWebProxy;
                //myHttpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials; ;
                //myHttpWebRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                // Sends the HttpWebRequest and waits for the response.                
                //myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                // Gets the stream associated with the response.
                //receiveStream = myHttpWebResponse.GetResponseStream();

                //jsonSerializer = new DataContractJsonSerializer(typeof(GoogleMobileSpeed));
                //object objResponse = jsonSerializer.ReadObject(strresponsexml.GetResponseStream());
                //objResponse = jsonSerializer.ReadObject(receiveStream);
                var jsonResponseDetail = objResponse as GoogleMobileSpeed;
                if (jsonResponseDetail == null) return;
                ResponseCode = jsonResponseDetail.ResponseCode;
                if (ResponseCode == 200)
                {
                    SUsability = jsonResponseDetail.RuleGroups.Usability.Score;
                    SSpeed = jsonResponseDetail.RuleGroups.Speed.Score;
                    Title = jsonResponseDetail.Title;
                }
                else
                {
                    SUsability = 0;
                    SSpeed = 0;
                    Title = null;
                }
            }
            catch (Exception)
            {
                SUsability = 0;
                SSpeed = 0;
                Title = null;
            }
        }


        //public static void SerializeJson(string json) { 
        //    Test myDeserializedObj = (Test)JavaScriptConvert.DeserializeObject(Request["jsonString"], typeof(Test));
        //    List<test> myDeserializedObjList = (List<test>)Newtonsoft.Json.JsonConvert.DeserializeObject(Request["jsonString"], typeof(List<test>));

        //}

        /*
        //Richiede JSON.Net
        public static Dicitionary<string,string[]> convertJSONResponseToDicitonary(string jsonSource){
            Dictionary<string, string>[] dic = JsonConvert.DeserializeObject<Dictionary<string, string>[]>(jsonSource);
            //System.Diagnostics.Debug.WriteLine(contacts[1]["name"])
            return dic;
        }
         */


    }

    //CLASSE COSTRUITA PER LA NOSTRA RISPOSTA JSON

    public class GoogleMobileSpeed
    {
    public string Id { get; set; }
    public int ResponseCode { get; set; }
    public string Title { get; set; }
    public Rulegroups RuleGroups { get; set; }
    }

    public class Rulegroups
    {
    public Speed Speed { get; set; }
    public Usability Usability { get; set; }
    }

    public class Speed
    {
    public int Score { get; set; }
    }

    public class Usability
    {
    public int Score { get; set; }
    }


    /*
    [DataContract]
    public class GoogleMobileSpeed
    {
        [DataMember(Name = "Id")]
        public string Id { get; set; }
        [DataMember(Name = "ResponseCode")]
        public int ResponseCode { get; set; }
        [DataMember(Name = "Title")]
        public string Title { get; set; }
        [DataMember(Name = "RuleGroups")]
        public Rulegroups RuleGroups { get; set; }
    }
    [DataContract]
    public class Rulegroups
    {
        [DataMember(Name = "Speed")]
        public Speed Speed { get; set; }
        [DataMember(Name = " Usability")]
        public Usability Usability { get; set; }
    }
    [DataContract]
    public class Speed
    {
        [DataMember(Name = "Score")]
        public int Score { get; set; }
    }
    [DataContract]
    public class Usability
    {
        [DataMember(Name = "Score")]
        public int Score { get; set; }
    }
    */
    /*
      public static Response MakeRequest(string requestUrl)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new Exception(String.Format("Server error (HTTP {0}: {1}).",response.StatusCode,response.StatusDescription));
                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Response));
                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
                    Response jsonRespons = objResponse as Response;
                    return jsonResponse;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

        }
     * */

   

}
