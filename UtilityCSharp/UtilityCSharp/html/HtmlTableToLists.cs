﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Common.Logging;
using HtmlAgilityPack;

/**
    //Label libreria checked TickStyle propongono e una versione modificata della Romis.ExtractTable elenco le differenze
    // - cattura dei <caption> nelle tabelle che contengono di solito i titoli
    // - che dovrebbere essere in grado di estrarre anche da <OL> <UL> <LI> (esitono liste di soli LI)
    // - numero di righe di codice ridotto perchè si usano metodi ricorsivi
    // - meno leggibile della romis ma nel complesso migliorata


    //Esmpio di un test effettuato
    string URI ="http://finanzalocale.interno.it/apps/floc.php/certificati/index/codice_ente/1030577010/cod/4/anno/2013/md/0/cod_modello/CCMU/tipo_modello/U/cod_quadro/04";
    RomisScript.NewScript();
    RomisScript.NewGET(URI);
    RomisScript.Execute();
    XmlDocument xmlRepsonse = new XmlDocument();

    string html = RomisScript.SaveHtml();

    //TEST NO html

    //Estrazione con attributi
    //TEST 1 
    HAXU3.FilterAttr = false;
    List<List<List<string>>> col1 = HAXU3.TablesExtractor(html, false); //with agility pack          
    HAXU3ROMIS.FilterAttr = false;
    List<List<List<string>>> romis1 = HAXU3ROMIS.ExtractTablesIntoArray(html, false, out xmlRepsonse); //with HtmlElement

    //TEST 2 senza attributi
    HAXU3.FilterAttr = true;
    List<List<List<string>>> col2 = HAXU3.TablesExtractor(html, false); //with agility pack
    HAXU3ROMIS.FilterAttr = true;
    List<List<List<string>>> romis2 = HAXU3ROMIS.ExtractTablesIntoArray(html, false, out xmlRepsonse); //with HtmlElement

    //TEST CONFRONTO FINALE NO html
    List<string[,]> romis = RomisScript.ExtractTables();   //...Estrazione della Romis classica
    List<string[,]> col11 = HAXU3Support.ConvertListsTo2Darray(col1); //...con gli attributi != Romis ma più utile della romis in certi ambienti
    List<string[,]> col22 = HAXU3Support.ConvertListsTo2Darray(col2); //...senza gli attributi == alla romis


    //TEST CONFRONTO FINALE html
    List<List<List<string>>> col4 = HAXU3.TablesExtractor(html, true); //with agility pack
    List<List<List<string>>> romis4 = HAXU3ROMIS.ExtractTablesIntoArray(html, true, out xmlRepsonse); //with HtmlElement

    List<string[,]> romisHtml = RomisScript.ExtractTablesHTML();
    List<string[,]> col44 = HAXU3Support.ConvertListsTo2Darray(col4); //...senza gli attributi == alla romis
*/
namespace UtilityCSharp.html
{
    /// <summary>
    /// Class with method ready for integration with the Agility Library, version 3 2015-04-16
    /// </summary>
    public class HtmlTableToLists
    {
        private static readonly ILog Log = LogManager.GetLogger<HtmlTableToLists>();
        //Variable to filter the attributes
        public static bool FilterAttr { get; set; } = false;

        /// <summary>
        /// Method for Clean the string o html of the HTMLO document
        /// </summary>
        /// <param name="testo"></param>
        /// <returns></returns>
        private static string Clean(string testo)
        {
            if (!string.IsNullOrEmpty(testo) && !string.IsNullOrWhiteSpace(testo))
            {
                //testo.Replace("&nbsp;", " ").Replace("\t", " ").Replace("\r", " ").Replace("\n", " ").Trim();
                testo = testo.Replace("&nbsp;", " ").Replace("\t", " ").Replace("\r", " ").Replace("\n", " ").Replace("�", "à").Trim();
                testo = Regex.Replace(testo, @"\s+", " ");
            }
            //else if (testo != null) { testo = ""; }
            else { testo = null; }
            return testo;        
        }

        /// <summary>
        /// Method for extract any type of element from a html document 
        /// </summary>     
        /// <param name="htmlDocument"></param>
        /// <param name="html"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public static List<List<List<string>>> UniversalExtractor(string htmlDocument, bool html, string tagName)
        {
             HtmlAgilityPack.HtmlDocument htmldoc = new HtmlAgilityPack.HtmlDocument();
             htmldoc.LoadHtml(htmlDocument);            
             HtmlNode htmlWork = htmldoc.DocumentNode; 
             return UniversalExtractor(htmlWork, html,tagName);
        }

        /// <summary>
        /// Method for the extraction of simple item list <li></li>"
        /// </summary>
        /// <param name="htmlDocument"></param>
        /// <param name="html"></param>
        /// <returns></returns>
        public static List<List<List<string>>> TablesExtractor(string htmlDocument, bool html)
        {
            HtmlAgilityPack.HtmlDocument htmldoc = new HtmlAgilityPack.HtmlDocument();
            htmldoc.LoadHtml(htmlDocument);
            HtmlNode htmlWork = htmldoc.DocumentNode;
            return UniversalExtractor(htmlWork, html, "table");
        }

        /// <summary>
        /// Method for the extraction of simple item list <li></li>"
        /// </summary>     
        /// <param name="htmlDocument"></param>
        /// <param name="html"></param>  
        /// <returns></returns>
        public static List<List<List<string>>> SimpleListItemExtractor(string htmlDocument, bool html)
        {
            HtmlAgilityPack.HtmlDocument htmldoc = new HtmlAgilityPack.HtmlDocument();
            htmldoc.LoadHtml(htmlDocument);
            HtmlNode htmlWork = htmldoc.DocumentNode;
            return UniversalExtractor(htmlWork, html, "li");
        }

        /// <summary>
        /// Method for the extraction of a order list <ol></ol>"
        /// </summary> 
        /// <param name="htmlDocument"></param>
        /// <param name="html"></param>         
        /// <returns></returns>
        public static List<List<List<string>>> OrderListItemExtractor(string htmlDocument, bool html)
        {
            HtmlAgilityPack.HtmlDocument htmldoc = new HtmlAgilityPack.HtmlDocument();
            htmldoc.LoadHtml(htmlDocument);
            HtmlNode htmlWork = htmldoc.DocumentNode;
            return UniversalExtractor(htmlWork, html, "ol");
        }

        /// <summary>
        /// Method for the extraction of a order list <ul></ul>"
        /// </summary>
        /// <param name="htmlDocument"></param>
        /// <param name="html"></param>
        /// <returns></returns>
        public static List<List<List<string>>> UnorderListItemExtractor(string htmlDocument, bool html)
        {
            HtmlAgilityPack.HtmlDocument htmldoc = new HtmlAgilityPack.HtmlDocument();
            htmldoc.LoadHtml(htmlDocument);
            HtmlNode htmlWork = htmldoc.DocumentNode;
            return UniversalExtractor(htmlWork, html, "ul");
        }

        /// <summary>
        /// Method for extract any type of element from a html document 
        /// </summary>
        /// <param name="htmlDocument"></param>
        /// <param name="html"></param>
        /// <param name="tagName"></param>
        /// <param name="attribute"></param>
        /// <param name="valueAttribute"></param>
        /// <returns></returns>
        public static List<List<List<string>>> UniversalExtractor(string htmlDocument, bool html, string tagName, string attribute, string valueAttribute)
        {
            HtmlAgilityPack.HtmlDocument htmldoc = new HtmlAgilityPack.HtmlDocument();
            htmldoc.LoadHtml(htmlDocument);
            HtmlNode htmlWork = htmldoc.DocumentNode;
            return UniversalExtractor(htmlWork, html, tagName,attribute,valueAttribute);
        }

        /// <summary>
        /// Method for extract any type of element from a html document 
        /// </summary>
        /// <param name="htmlWork"></param>
        /// <param name="html"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public static List<List<List<string>>> UniversalExtractor(HtmlNode htmlWork, bool html, string tagName)
        {
            string rootTag;
            if (tagName.ToLower().Equals("ul") || tagName.ToLower().Contains("//ul")) { rootTag = "ul"; }
            else if (tagName.ToLower().Equals("ol")|| tagName.ToLower().Contains("//ol")) { rootTag = "ol"; }
            else if (tagName.ToLower().Equals("li") || tagName.ToLower().Contains("//li")) { rootTag = "li"; }
            else if (tagName.ToLower().Equals("table") || tagName.ToLower().Contains("//table")) { rootTag = "table"; }
            else { throw new Exception("ERROR: the selected tagName in not supported, plese use : li,ul,ol or table"); }
            List<List<List<string>>> resultCollection;
            //HtmlNodeCollection RootTag2 = htmlWork.SelectNodes(".//tbody");
            HtmlNodeCollection rootTagCol = htmlWork.SelectNodes(".//" + rootTag + "");//e.g ul,ol,table
            SubExtractor6(rootTagCol, html, out resultCollection);       
            return resultCollection;
        }

        /// <summary>
        /// Method for extract any type of element from a html document
        /// </summary>
        /// <param name="htmlWork"></param>
        /// <param name="html"></param>       
        /// <param name="tagName"></param>
        /// <param name="attribute"></param>
        /// <param name="valueAttribute"></param>
        /// <returns></returns>
        public static List<List<List<string>>> UniversalExtractor(HtmlNode htmlWork, bool html,string tagName,string attribute, string valueAttribute)
        {          
            string newTagName  = "//" + tagName + "[@" + attribute + "='" + valueAttribute + "']";
            return UniversalExtractor(htmlWork, html, newTagName); 
        }       
                                 
        /// <summary>
        /// Method for extract all elements from a html document , version : 6 (2015-04-15)
        /// </summary>
        /// <param name="tableColl"></param>
        /// <param name="html"></param>
        /// <param name="resultCollection"></param>
        private static void SubExtractor6(HtmlNodeCollection tableColl, bool html, out List<List<List<string>>> resultCollection)
        {
            resultCollection = new List<List<List<string>>>();
            //Nuova tabella
            foreach (HtmlNode tableElem in tableColl)
            {
                switch (tableElem.Name.ToUpper())
                {
                    case "TABLE":
                    case "UL":
                    case "OL":
                        {
                            int iMaxCol = 0;
                            List<List<string>> dtTableList = new List<List<string>>();
                            foreach (HtmlNode currentElement in tableElem.ChildNodes)
                            {
                                switch (currentElement.Name.ToUpper())
                                {
                                    case "TR":                                         
                                        {
                                            List<string> cols = new List<string>();                                        
                                            foreach (HtmlNode t in currentElement.ChildNodes)
                                            {
                                                if (t.Name.ToUpper() == "TD"|| t.Name.ToUpper() == "TH")
                                                {
                                                    if (t.ChildNodes.Count == 0) cols = Extractor6(t, html, cols);                                                 
                                                    else
                                                        foreach (HtmlNode node in t.ChildNodes)
                                                        {
                                                            cols = Extractor6(node, html, cols);
                                                        }                                                  
                                                }
                                                //if td o th
                                                else
                                                {
                                                    cols = Extractor6(t, html, cols);
                                                }
                                            }
                                            if (cols.Count > iMaxCol) iMaxCol = cols.Count;
                                            if (cols.Count > 0) dtTableList.Add(cols);
                                        }
                                        break;
                                    case "TBODY":
                                    case "THEAD":
                                    case "TFOOT":
                                        {
                                            //cols = ExtractorRomis(_CurrentElement, html, cols);
                                            foreach (HtmlNode rows in currentElement.ChildNodes)
                                            {                                              
                                                if (rows.Name.ToUpper() == "TR")
                                                {
                                                    List<string> cols = new List<string>();                                                  
                                                    foreach (HtmlNode t in rows.ChildNodes)
                                                    {
                                                        if (t.Name.ToUpper() == "TD" || t.Name.ToUpper() == "TH")
                                                        {
                                                            if (t.ChildNodes.Count == 0) cols = Extractor6(t, html, cols);                                                          
                                                            else 
                                                                foreach (HtmlNode node in t.ChildNodes)
                                                                {
                                                                    cols = Extractor6(node, html, cols);
                                                                }                                                          
                                                        }
                                                    }
                                                    if (cols.Count > iMaxCol) iMaxCol = cols.Count;
                                                    if (cols.Count > 0) dtTableList.Add(cols);
                                                }//if _rows NamespaceHandling is TR
                                            }//foreach HtmlNode _rows in _CurrentElement.ChildNodes
                                        }
                                        break;
                                    case "CAPTION":
                                    case "H3":
                                    case "H2":
                                    case "LI":
                                        {
                                            List<string> cols = new List<string>();
                                            cols = Extractor6(currentElement, html, cols);
                                            if (cols.Count > iMaxCol) iMaxCol = cols.Count;
                                            if (cols.Count > 0) dtTableList.Add(cols);
                                            break;
                                        }
                                }//end of the switch TR,TBODY,CAPTION
                            }
                            resultCollection.Add(dtTableList);
                        }
                        break;
                    case "LI":
                        {
                            //int iMaxCol = 0;
                            List<List<string>> dtTableList = new List<List<string>>();
                            List<string> cols = new List<string>();
                            cols = Extractor6(tableElem, html, cols);
                            //if (cols.Count > iMaxCol) iMaxCol = cols.Count;
                            if (cols.Count > 0) dtTableList.Add(cols);
                            resultCollection.Add(dtTableList);
                        }
                        break;
                    case "DL":
                        {
                            HtmlNode currentElement = tableElem;
                            //iCurCol = 0; iCurRow = 0;
                            int iMaxCol = 0;
                            //int iCurCol = 0;
                            List<List<string>> dtTable = new List<List<string>>();
                            List<string> cols = new List<string>();
                            foreach (HtmlNode t in currentElement.ChildNodes)
                            {
                                if (t.Name.ToUpper() == "DT")
                                {
                                    cols = Extractor6(t, html, cols);
                                    if (cols.Count > iMaxCol) iMaxCol = cols.Count;
                                    if (cols.Count > 0) dtTable.Add(cols);
                                    cols = new List<string>();
                                    //iCurCol = 0;                                
                                    //CurTabella[iCurRow - 1, iCurCol] = (testo == null) ? "" : testo;
                                    //iCurCol = 1;
                                }

                                if (t.Name.ToUpper() == "DD" 
                                    //&& iCurCol == 1
                                    )
                                {
                                    cols = Extractor6(t, html, cols);
                                }
                                //iCurCol++;
                            }
                            resultCollection.Add(dtTable);
                        }
                        break;
                }
            }
        }//subTableWithHtmlNode6

        /// <summary>
        /// Method of support for UniversalExtractor(); , version : 6 (2015-04-15)
        /// </summary>
        /// <param name="node"></param>
        /// <param name="html"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        private static List<string> Extractor6(HtmlNode node, bool html, List<string> cols)
        {
            string[] goodTag = { "A", "IMG", "P", "SPAN", "CAPTION", "DIV", "DD", "DT", "DL", "LI", "DIV", "BR", "STRONG"};//...possible new entry -> STRONG,#Text
            string[] goodAttr = { "SRC", "HREF", "ID", "NAME", "VALUE", "Title", "ALT","ONCLICK" }; //... aggiungere via via gli attributi che si reputano interessanti
            string testo;                      
            HtmlNode child;                
            if (node.ChildNodes.Count == 1 && goodTag.Contains(node.FirstChild.Name.ToUpper())) //Se è una tag in cui non si deve estarre informazioni a questo giro
            { 
                return cols; //..do nothing
            }
            if (node.ChildNodes.Count == 1 && node.FirstChild.Name.ToUpper() != "#Text")
            {  //...o il figlio è unico e di tipo testo...               
                child = node.ChildNodes[0];
            }
            else
            {
                child = node; //...non ho a figli
            }

            if (html == false)
            {
                testo = Clean(child.InnerText);
                if (testo != null) cols.Add(testo); 
                if (!FilterAttr && goodTag.Contains(child.Name.ToUpper()))
                {
                    testo = "";
                    bool check = false;
                    foreach (string attr in goodAttr)
                    {
                        if (child.Attributes.Contains(attr) &&
                            !string.IsNullOrEmpty(child.Attributes[attr].Value) && !string.IsNullOrWhiteSpace(child.Attributes[attr].Value))
                        {
                            testo = testo + "[" + Clean(child.Attributes[attr].Name) + "=" + Clean(child.Attributes[attr].Value) + "] ";
                        }
                        else if (child.Attributes.Contains(attr) && testo == "" && goodAttr.Contains(attr))
                        {
                            check = true;
                        }
                    }
                    testo = Clean(testo);
                    if (check && string.IsNullOrEmpty(testo)) cols.Add((testo == null) ? "" : testo); //testo = ""; 
                    else if (testo != null)  cols.Add(testo); 
                }
                else if ("IMG".Contains(child.Name.ToUpper()))//.Equals("IMG")...Se il filtro degli attrbituti è true ma sappiamo che vi è un qualcosa di utile l'unico caso trovato sono le immagini
                {
                    testo = "";
                    cols.Add(testo); 
                }
            }
            else
            {
                testo = Clean(child.OuterHtml.Trim());
                if (testo != null) cols.Add(testo); 
            }          
            return cols;
        }
                       
    }
   
    /// <summary>
    /// Class with method ready for integration with the Romis Library, version 3 2015-04-16
    /// </summary>
    public class HtmlTableToListsRomiS
    {
        private static readonly ILog Log = LogManager.GetLogger<HtmlTableToListsRomiS>();

        //Variable to filter the attributes
        public static bool FilterAttr { get; set; } = false;

        /// <summary>
        /// Method for Clean the string o html of the HTMLO document
        /// </summary>
        /// <param name="testo"></param>
        /// <returns></returns>
        private static string Clean(string testo)
        {
            if (!string.IsNullOrEmpty(testo) && !string.IsNullOrWhiteSpace(testo))
            {
                //testo.Replace("&nbsp;", " ").Replace("\t", " ").Replace("\r", " ").Replace("\n", " ").Replace("�", "à").Trim();
                //Regex.Replace(value, "[\n\r\t]", " ")
                testo = testo.Replace("&nbsp;", " ").Replace("\t", " ").Replace("\r", " ").Replace("\n", " ").Replace("�", "à").Trim();
                testo = Regex.Replace(testo, @"\s+", " ");                
            }           
            else { testo = null; }
            return testo;
        }

        /// <summary>
        /// Method for extract all elements from a html document , version : 6 (2014-04-15)
        /// </summary>
        /// <param name="tableColl"></param>
        /// <param name="html"></param>
        /// <param name="resultCollection"></param>
        public static void SubTable6(HtmlElementCollection tableColl, bool html, out List<List<List<string>>> resultCollection)
        {
            resultCollection = new List<List<List<string>>>();
            //Nuova tabella
            foreach (HtmlElement tableElem in tableColl)
            {
                switch (tableElem.TagName.ToUpper())
                {
                    case "TABLE":
                    case "UL":
                    case "OL":
                        {
                            int iMaxCol = 0;
                            List<List<string>> dtTableList = new List<List<string>>();
                            foreach (HtmlElement currentElement in tableElem.Children)
                            {
                                switch (currentElement.TagName.ToUpper())
                                {
                                    case "TR":                                                                        
                                        {
                                            List<string> cols = new List<string>();                                         
                                            for(int iElem = 0; iElem < currentElement.Children.Count; iElem++)
                                            {
                                                if (currentElement.Children[iElem].TagName.ToUpper() == "TD"
                                                    || currentElement.Children[iElem].TagName.ToUpper() == "TH")
                                                {
                                                    if (currentElement.Children[iElem].Children.Count == 0) cols = ExtractorRomis6(currentElement, html, cols);                                                 
                                                    else
                                                        foreach (HtmlElement node in currentElement.Children[iElem].Children)
                                                        {
                                                            cols = ExtractorRomis6(node, html, cols);
                                                        }                                                                                                    
                                                }
                                                //if td o th
                                                else
                                                {
                                                    cols = ExtractorRomis6(currentElement.Children[iElem], html, cols);
                                                }
                                            }//for each node in tr
                                            if (cols.Count > iMaxCol) iMaxCol = cols.Count;
                                            if (cols.Count > 0) dtTableList.Add(cols);
                                        }
                                        break;
                                    case "TBODY":
                                    case "THEAD":
                                    case "TFOOT":
                                        {                                                                                    
                                            foreach (HtmlElement rows in currentElement.Children)
                                            {                                         
                                                if (rows.TagName.ToUpper() == "TR")
                                                {
                                                    List<string> cols = new List<string>();
                                                    for (int iElem = 0; iElem < rows.Children.Count; iElem++)
                                                    {
                                                        if (rows.Children[iElem].TagName.ToUpper() == "TD" || rows.Children[iElem].TagName.ToUpper() == "TH")
                                                        {
                                                            if (rows.Children[iElem].Children.Count == 0) cols = ExtractorRomis6(rows.Children[iElem], html, cols);         
                                                            else
                                                                foreach (HtmlElement node in rows.Children[iElem].Children)
                                                                {
                                                                    cols = ExtractorRomis6(node, html, cols);
                                                                }                                                          
                                                        }
                                                    }//for ielem _rows.ChildNodes
                                                    if (cols.Count > iMaxCol) iMaxCol = cols.Count;
                                                    if (cols.Count > 0) dtTableList.Add(cols);                                                  
                                                }//if _rows NamespaceHandling is TR
                                            }//foreach HtmlNode _rows in _CurrentElement.ChildNodes
                                        }
                                        break;
                                    case "CAPTION":
                                    case "H3":
                                    case "H2":
                                    case "LI":
                                        {
                                            List<string> cols = new List<string>();
                                            cols = ExtractorRomis6(currentElement, html, cols);
                                            if (cols.Count > iMaxCol) iMaxCol = cols.Count;
                                            if(cols.Count > 0) dtTableList.Add(cols);
                                            break;
                                        }
                                }//end of the switch TR,TBODY,CAPTION
                            }
                            resultCollection.Add(dtTableList);
                        }
                        break;
                    case "LI":
                        {
                            //int iMaxCol = 0;
                            List<List<string>> dtTableList = new List<List<string>>();
                            List<string> cols = new List<string>();
                            cols = ExtractorRomis6(tableElem, html, cols);                          
                            //if (cols.Count > iMaxCol) iMaxCol = cols.Count;
                            if (cols.Count > 0) dtTableList.Add(cols);
                            resultCollection.Add(dtTableList);
                        }
                        break;
                    case "DL":
                        {
                            HtmlElement currentElement = tableElem;
                            //iCurCol = 0; iCurRow = 0;
                            int iMaxCol = 0;
                            //int iCurCol = 0;
                            List<List<string>> dtTable = new List<List<string>>();
                            List<string> cols = new List<string>();
                            for (int iRow = 0; iRow < currentElement.Children.Count; iRow++)
                            {
                                if (currentElement.Children[iRow].TagName.ToUpper() == "DT")
                                {
                                    cols = ExtractorRomis6(currentElement.Children[iRow], html, cols);
                                    if (cols.Count > iMaxCol) iMaxCol = cols.Count;
                                    if (cols.Count > 0) dtTable.Add(cols);
                                    cols = new List<string>();
                                    //iCurCol = 0;                                                         

                                    //CurTabella[iCurRow - 1, iCurCol] = (testo == null) ? "" : testo;
                                    //iCurCol = 1;
                                }

                                if (currentElement.Children[iRow].TagName.ToUpper() == "DD" 
                                    //&& iCurCol == 1
                                    )
                                {
                                    cols = ExtractorRomis6(currentElement.Children[iRow], html, cols);
                                }
                                //iCurCol++;
                            }
                            resultCollection.Add(dtTable);
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Method for extract all tables from a html document , version : 6 (2014-04-15)
        /// </summary>
        /// <param name="contentHtml"></param>
        /// <param name="html"></param>
        /// <param name="xmlResponse"></param>
        /// <returns></returns>
        public static List<List<List<string>>> ExtractTablesIntoArray(string contentHtml, bool html, out XmlDocument xmlResponse)
        {
            WebBrowser webControl = new WebBrowser {ScriptErrorsSuppressed = true};
            webControl.Navigate("about:blank");
            webControl.Document?.Write(contentHtml);
            webControl.DocumentText = contentHtml;
            System.Windows.Forms.HtmlDocument doc = webControl.Document;
            //HtmlElementCollection RootTag = webBrowser1.Document.GetElementsByTagName("TABLE");          
            if (doc == null)
            {
                xmlResponse = null;
                return null;
            }
            HtmlElementCollection rootTagCol = doc.GetElementsByTagName("TABLE");
            List<List<List<string>>> tableResultCollection;
            SubTable6(rootTagCol, html, out tableResultCollection);
            HtmlTableToListsSupport.SaveXmlTableColl(out xmlResponse, tableResultCollection);
            return tableResultCollection;
        }
            
        /// <summary>
        /// Method of support for ExtractTablesIntoArray, version : 6 (2014-04-15)
        /// </summary>
        /// <param name="node"></param>
        /// <param name="html"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        private static List<string> ExtractorRomis6(HtmlElement node, bool html, List<string> cols)
        {
            string[] goodTag = { "A", "IMG", "P", "SPAN", "CAPTION", "DIV", "DD", "DT", "DL", "LI", "DIV","BR","STRONG"};//...possible new entry -> STRONG,#Text          
            string[] goodAttr = { "SRC", "HREF", "ID", "NAME", "VALUE", "Title", "ALT" }; //... aggiungere via via
            string testo;                    
            HtmlElement child;
            if (node?.FirstChild == null)
            {
                return null;
            }
            if (node.Children.Count == 1 && goodTag.Contains(node.FirstChild.Name.ToUpper())) //Se è una tag in cui non si deve estarre informazioni a questo giro
            {
                return cols; //..do nothing
            }
            if (node.Children.Count == 1 && node.FirstChild.Name.ToUpper() != "#Text")
            {  //...o il figlio è unico e di tipo testo...               
                child = node.Children[0];
            }
            else
            {
                child = node; //...ho a figli
            }

            if (html == false)
            {
                if (child.InnerHtml == "&nbsp;") { cols.Add(""); }
                else
                {
                    testo = Clean(child.InnerText);
                    if (testo != null) cols.Add(testo);
                }
                if (!FilterAttr && goodTag.Contains(child.TagName.ToUpper()))
                {
                    testo = "";
                    bool check = false;
                    foreach (string attr in goodAttr)
                    {
                        if (!string.IsNullOrEmpty(child.GetAttribute(attr)) || 
                            !string.IsNullOrWhiteSpace(child.GetAttribute(attr)))
                        {
                            testo = testo + "[" + Clean(attr) + "=" + Clean(child.GetAttribute(attr)) + "] ";
                        }
                        else if (child.GetAttribute(attr) != "" && testo == "" && goodAttr.Contains(attr))
                        {
                            check = true;
                        }
                    }
                    testo = Clean(testo);
                    if (check && string.IsNullOrEmpty(testo)) cols.Add(testo); //TESTO = ""
                    else if (testo != null) cols.Add((testo)); 
                }
                else if ("IMG".Contains(child.TagName.ToUpper()))//.Equals("IMG")...Se il filtro degli attrbituti è true ma sappiamo che vi è un qualcosa di utile l'unico caso trovato sono le immagini
                {
                    testo = "";
                    cols.Add(testo);
                }
            }
            else
            {
                testo = Clean(child.OuterHtml.Trim());
                if (testo != null) cols.Add(testo); 
            }                         
            return cols;
        }

         
    }

    /// <summary> 
    /// Class with methos for make many operations with html and XML Documents, versione 3 2015-04-15
    /// </summary>
    static class HtmlTableToListsSupport
    {
        //private static readonly ILog Log = LogManager.GetLogger<HtmlTableToListsSupport>();

        //////////////////////////////
        #region GETTER E LOADER
        //////////////////////////////

        /// <summary>
        /// Method to save the content html of a web page to a string object
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetWebPageHtmlFromUrl(string url)
        {
            WebClient webClient = new WebClient();
            return webClient.DownloadString(url);
        }   

        /// <summary>
        /// Method to get all attributes values of a tag in HTMl string
        /// </summary>
        /// <param name="htmlSource">html text</param>
        /// <param name="tag">tag name like IMG</param>
        /// <param name="attr">attribute name like href for IMG</param>
        /// <returns></returns>
        public static List<string> GetAttributeFromHtmlString(string htmlSource, string tag, string attr)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<" + tag + @"[^>]*?" + attr + @"\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links;
        }

        /// <summary>
        /// Method to get all attributes values of a tag in HTMl string
        /// </summary>
        /// <param name="htmlSource">html text</param>
        /// <param name="tag">tag name like IMG</param>
        /// <returns></returns>
        public static List<string> GetAttributeFromHtmlString(string htmlSource, string tag)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<" + tag + @"[^>]*?\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links;
        }

        /// <summary>
        /// Method for load XML Document from a uri string
        /// </summary>
        /// <param name="uriXml"></param>
        /// <returns></returns>
        public static bool LoadFileXml(string uriXml)
        {
            bool retVal;
            XmlDocument xmlFile = new XmlDocument();
            try
            {
                xmlFile.Load(uriXml);
                retVal = true;
            }
            catch (Exception)
            {
                retVal = false;
            }
            return retVal;
        }
     
        /// <summary>
        /// Method to get the value of a attribute from a html string 
        /// </summary>
        /// <param name="htmlString"></param>
        /// <param name="tagname"></param>
        /// <param name="tagattribute"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static string GetAttributeValueFromHtmlString(string htmlString, string tagname, string tagattribute, out HtmlNode n)
        {
            //CONVERT TO XMLNODE                                 
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlString);
            HtmlNode node = doc.DocumentNode.SelectSingleNode("//" + tagname + "");
            n = node;
            string value;
            if (tagattribute == null)
            {
                value = node.InnerText;
            }
            else
            {
                value = node.Attributes[tagattribute].Value;
            }
            return value;
        }

        /// <summary>
        /// Method to get the value of a attribute from a html string 
        /// </summary>
        /// <param name="htmlString"></param>
        /// <param name="tagname"></param>
        /// <param name="tagattribute"></param>      
        /// <returns></returns>
        public static string GetAttributeValueFromHtmlString(string htmlString, string tagname, string tagattribute)
        {
            //CONVERT TO XMLNODE                                 
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlString);
            HtmlNode node = doc.DocumentNode.SelectSingleNode("//" + tagname + "");
            string value;
            if (tagattribute == null)
            {
                value = node.InnerText;
            }
            else
            {
                value = node.Attributes[tagattribute].Value;
            }
            return value;
        }

        /// <summary>
        /// Method for remove the duplicate object
        /// </summary>
        /// <param name="tableResultCollection"></param>
        /// <returns></returns>
        public static List<List<List<string>>> RemoveDuplicate(List<List<List<string>>> tableResultCollection)
        {
            List<string> list2;
            List<List<string>> table2;
            List<List<List<string>>> tableResultCollection2 = new List<List<List<string>>>();
            foreach (List<List<string>> table in tableResultCollection)
            {
                table2 = new List<List<string>>();
                foreach (List<string> list in table)
                {
                    // Case sensitive comparison
                    list2 = list.Distinct().ToList();
                    //Console.WriteLine(String.Join(", ", list)); // prints a, b, A, c, d
                    table2.Add(list2);
                    // In case you want to use case insensitive, invariant culture comparison, use this one. Different comparers are available: http://msdn.microsoft.com/en-us/library/system.stringcomparer(v=vs.110).aspx
                    //list = list.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
                    //Console.WriteLine(String.Join(", ", list)); // prints a, b, c, d               
                }
                tableResultCollection2.Add(table2);
            }
            return tableResultCollection2;
        }

        /// <summary>
        /// Method to convert a List of Lists to a simple XML document
        /// </summary>
        /// <param name="xmlResponse"></param>
        /// <param name="tableResultCollection"></param>
        public static void SaveXmlTableColl(out XmlDocument xmlResponse, List<List<List<string>>> tableResultCollection)
        {
            int iNumber = 1;
            XmlDocument xmlOutput = new XmlDocument();
            XmlDeclaration xDecl = xmlOutput.CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.WebName, null);
            xmlOutput.AppendChild(xDecl);
            //Root
            var xmlElement = xmlOutput.CreateElement("", "Tables", "");
            foreach (List<List<string>> tabella in tableResultCollection)
            {
                if (tabella.Count > 0)
                {
                    XmlElement xmlElementTable = xmlOutput.CreateElement("", "Table", "");
                    XmlAttribute attributo = xmlOutput.CreateAttribute("count");
                    attributo.Value = iNumber.ToString();
                    xmlElementTable.Attributes.Append(attributo);

                    attributo = xmlOutput.CreateAttribute("rows");
                    attributo.Value = tabella.Count.ToString();
                    xmlElementTable.Attributes.Append(attributo);

                    attributo = xmlOutput.CreateAttribute("cols");
                    attributo.Value = tabella[0].Count.ToString();
                    xmlElementTable.Attributes.Append(attributo);

                    foreach (List<string> rows in tabella)
                    {
                        if (rows.Count > tabella[0].Count)
                        {
                            xmlElementTable.Attributes["cols"].Value = rows.Count.ToString();
                        }
                        XmlElement xmlRow = xmlOutput.CreateElement("", "Row", "");
                        foreach (string cols in rows)
                        {
                            XmlElement xmlCol = xmlOutput.CreateElement("Col");
                            xmlCol.InnerText = cols;
                            xmlRow.AppendChild(xmlCol);
                        }
                        xmlElementTable.AppendChild(xmlRow);
                    }
                    xmlElement.AppendChild(xmlElementTable);
                    iNumber++;
                }
            }
            xmlOutput.AppendChild(xmlElement);
            xmlResponse = xmlOutput;
        }

        

        #endregion
        //////////////////////////////
        #region CONVERTERS
        //////////////////////////////

        /// <summary>
        /// Method for convert with my style a List of Lists to a List of array.
        /// </summary>
        /// <param name="tableResultCollection"></param>
        /// <returns></returns>
        public static List<T[,]> ConvertListsTo2Darray<T>(List<List<List<T>>> tableResultCollection)
        {
            List<T[,]> tableResultArrays = new List<T[,]>();
            foreach (List<List<T>> ll in tableResultCollection)
            {
                T[,] result = To2DQuadraticArray(ll);
                result = ArrayRemoveNullRow(result);
                result = ArrayRemoveNullColumn(result);
                result = ArrayLoadEmptyValue(result);
                tableResultArrays.Add(result);
            }
            return tableResultArrays;
        }

        /// <summary>
        /// Method to convert a string to System.Windows.Forms.HtmlDocument
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static System.Windows.Forms.HtmlDocument ConvertString2HtmlDocument(string html)
        {
            WebBrowser webControl = new WebBrowser();
            webControl.ScriptErrorsSuppressed = true;
            webControl.Navigate("about:blank");
            webControl.Document?.Write(html);
            webControl.DocumentText = html;
            System.Windows.Forms.HtmlDocument doc = webControl.Document;
            return doc;
        }

        /// <summary>
        /// Method to convert a System.Windows.Forms.HtmlDocument to string
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static string ConvertHtmlDocument2String(System.Windows.Forms.HtmlDocument doc)
        {
            //WebBrowser webControl = new WebBrowser();
            //webControl.DocumentText = html;
            string html = doc.Body?.InnerHtml;
            return html;
        }

        /// <summary>
        /// Method to convert a System.Xml.XMLNode to a System.Xml.Linq.XMLElement
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static System.Xml.Linq.XElement ConvertXmlNode2LinqXmlElement(XmlNode node)
        {
            System.Xml.Linq.XDocument xDoc = new System.Xml.Linq.XDocument();
            using (XmlWriter xmlWriter = xDoc.CreateWriter())
                node.WriteTo(xmlWriter);
            return xDoc.Root;
        }

        /// <summary>
        /// Method to convert a System.Xml.Linq.XMLElement to a System.Xml.XMLNode
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static XmlNode ConvertLinqXElement2XmlNode(System.Xml.Linq.XElement element)
        {
            using (XmlReader xmlReader = element.CreateReader())
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlReader);
                return xmlDoc;
            }
        }

        /// <summary>
        /// Method to convert a System.Xml.XmlDocument to a array of System.Xml.XmlElement
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public static XmlElement[] ConvertXmlDocument2XmlElementList(XmlDocument document)
        {
            XmlElement[] listaElm = document.DocumentElement?.ChildNodes.Cast<XmlElement>().ToArray();
            return listaElm;
        }

        /// <summary>
        /// Method to convert a System.Xml.XmlDocument to a specific System.Xml.XmlElement
        /// </summary>
        /// <param name="document"></param>
        /// <param name="indice"></param>
        /// <returns></returns>
        public static XmlElement ConvertXmlDocument2XmlElement(XmlDocument document, int indice)
        {
            XmlElement[] listaElm = document.DocumentElement?.ChildNodes[indice].ChildNodes.Cast<XmlElement>().ToArray();
            XmlElement elm = listaElm?[0];
            return elm;
        }

        #endregion
        //////////////////////////////
        #region READ/REMOVE/UPDATE Multidimansional Arrays
        //////////////////////////////

        /// <summary>
        /// Method to convert a List of Lists to arrays.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>  
        public static T[,] To2DQuadraticArray<T>(this List<List<T>> list)
        {
            int count = 0;
            foreach (List<T> ls in list)
            {
                if (ls.Count > count) count = ls.Count;
            }
            if (list.Count == 0 || count == 0) return null;
            //var result = new T[list.Count, list[0].Count];
            var result = new T[list.Count, count];
            //int maxCount = 0;
            if (result.GetLength(0) > result.GetLength(1))
            {
                result = new T[result.GetLength(0), result.GetLength(0)];
                //maxCount = result.GetLength(0);
            }
            else if (result.GetLength(1) > result.GetLength(0))
            {
                result = new T[result.GetLength(1), result.GetLength(1)];
                //maxCount = result.GetLength(1);
            }
            for (int r = 0; r < list.Count; r++)
            {
                for (int c = 0; c < list[r].Count; c++)
                {
                    try
                    {
                        result[r, c] = list[r][c];
                    }
                    catch (Exception e) { throw new Exception(e.StackTrace); }
                }
            }
            return result;
        }//To2DQuadraticArray<T>


        /// <summary>
        /// Method to convert all null value ona Object[,] to the value of the last not null element on the row
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="originalArray"></param>
        /// <returns></returns>
        public static T[,] ArrayLoadEmptyValue<T>(T[,] originalArray)
        {

            T[,] result = new T[originalArray.GetLength(0), originalArray.GetLength(1)];
            T value = default(T);
            for (int i = 0, j = 0; i < originalArray.GetLength(0); i++)
            {

                for (int k = 0, u = 0; k < originalArray.GetLength(1); k++)
                {
                    if (originalArray[i, k] != null)
                    {
                        value = originalArray[i, k];
                        result[j, u] = originalArray[i, k];
                    }
                    else
                    {
                        result[j, u] = value;
                    }
                    u++;
                }
                j++;
            }
            return result;
        }//ArrayLoadNullValue

        /// <summary>
        /// Method to remove a full null row from a Object[,]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="originalArray"></param>
        /// <returns></returns>
        public static T[,] ArrayRemoveNullRow<T>(T[,] originalArray)
        {
            int rowCount = originalArray.GetUpperBound(0) + 1;
            int columnCount = originalArray.GetUpperBound(1) + 1;
            for (int i = rowCount - 1; i >= 0; i--)
            {
                if (originalArray[i, 0] == null && originalArray[i, columnCount - 1] == null)
                {
                    originalArray = ArrayRemoveRow(i, originalArray);
                }
            }
            return originalArray;
        }

        /// <summary>
        /// Method to remove a full null column from a Object[,]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="originalArray"></param>
        /// <returns></returns>
        public static T[,] ArrayRemoveNullColumn<T>(T[,] originalArray)
        {
            int columnCount = originalArray.GetUpperBound(1) + 1;
            int rowCount = originalArray.GetUpperBound(0) + 1;
            for (int i = columnCount - 1; i >= 0; i--)
            {
                if (originalArray[0, i] == null && originalArray[rowCount - 1, i] == null)
                {
                    originalArray = ArrayRemoveColumn(i, originalArray);
                }
            }
            return originalArray;
        }

        /// <summary>
        /// Method to remove a row from a Object[,]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rowToRemove"></param>
        /// <param name="originalArray"></param>
        /// <returns></returns>
        public static T[,] ArrayRemoveRow<T>(int rowToRemove, T[,] originalArray)
        {
            T[,] result = new T[originalArray.GetLength(0) - 1, originalArray.GetLength(1)];
            bool jump = false;
            for (int i = 0, j = 0; i < originalArray.GetLength(0); i++)
            {
                for (int k = 0, u = 0; k < originalArray.GetLength(1); k++)
                {
                    if (i == rowToRemove)
                    {
                        jump = true;
                        break;
                    }
                    result[j, u] = originalArray[i, k];
                    u++;
                }
                if (jump)
                {
                    jump = false;
                    continue;
                }
                j++;
            }
            return result;
        }

        /// <summary>
        /// Method to remove a column from a Object[,]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="columnToRemove"></param>
        /// <param name="originalArray"></param>
        /// <returns></returns>
        public static T[,] ArrayRemoveColumn<T>(int columnToRemove, T[,] originalArray)
        {
            T[,] result = new T[originalArray.GetLength(0), originalArray.GetLength(1) - 1];
            for (int i = 0, j = 0; i < originalArray.GetLength(0); i++)
            {
                for (int k = 0, u = 0; k < originalArray.GetLength(1); k++)
                {
                    if (k == columnToRemove)
                    {
                        continue;
                    }
                    result[j, u] = originalArray[i, k];
                    u++;
                }
                j++;
            }
            return result;
        }

        /// <summary>
        /// Method to resize a already exists multidimansional array
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original"></param>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        public static T[,] ResizeArray<T>(T[,] original, int rows, int cols)
        {
            var newArray = new T[rows, cols];
            int minRows = Math.Min(rows, original.GetLength(0));
            int minCols = Math.Min(cols, original.GetLength(1));
            for (int i = 0; i < minRows; i++)
                for (int j = 0; j < minCols; j++)
                    newArray[i, j] = original[i, j];
            return newArray;
        }
        #endregion 
        //////////////////////////////
       
    }//end of the class HAXU2Support   
}
