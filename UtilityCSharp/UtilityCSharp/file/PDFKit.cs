﻿using System;
using System.IO;
using System.Windows.Forms;
using Common.Logging;
using UtilityCSharp.logging;

namespace UtilityCSharp.file
{
    public class PdfKit
    {
        private static readonly ILog Log = LogManager.GetLogger<PdfKit>();
        //METHOD FOR DOWNLOAD FILE FROM WEB
        public static void SavePdf(byte[] pdfData, string pathFilePdf)
        {
            //procedura che salva un file pdf da un array di byte a un file fisico pathFilePdf

            if (File.Exists(pathFilePdf)) File.Delete(pathFilePdf);

            FileInfo fiPdf = new FileInfo(pathFilePdf);

            try
            {
                //string _Path = Path.GetDirectoryName(Application.ExecutablePath);
                string path = Path.GetTempPath();

                using (StreamWriter filePdf = new StreamWriter(path + "\\" + fiPdf.Name))
                {
                    filePdf.BaseStream.Write(pdfData, 0, pdfData.Length);
                    filePdf.Close();
                    filePdf.Dispose();

                    if (path + "\\" + fiPdf.Name != pathFilePdf)
                        File.Move(path + "\\" + fiPdf.Name, pathFilePdf);
                }

            }
            catch (Exception err)
            {
                Log.Error("SITE-ERROR: Errore estrazione PDF: " + err.Message,err);
            }

        }


        private void TestOfflineExtractPdf(string fullPath)
        {
            //SaveDataStruct _TempSaveData = new SaveDataStruct();

            byte[] pdfData;
            byte[] readfileoutput;
            string filePdf;
            using (StreamReader filePdfInput = new StreamReader(fullPath))
            {
                pdfData = new byte[(int)filePdfInput.BaseStream.Length];

                filePdfInput.BaseStream.Read(pdfData, 0, (int)filePdfInput.BaseStream.Length);
                filePdfInput.Close();
                filePdfInput.Dispose();
            }
            ExtractPDF(pdfData, out readfileoutput, out filePdf);
            //ExtractInformationFromPdf(_readfileoutput, ref _TempSaveData);
            //ExecuteResult(_TempSaveData, Guid);            
        }//TestOfflineExtractPdf


        private void ExtractPDF(byte[] pdfData, out byte[] fileTxtData, out string fileNamePdf)
        {
            fileNamePdf = null;
            Guid guidPdf = Guid.NewGuid();
            fileTxtData = null;

            try
            {
                string executablePath = Path.GetDirectoryName(Application.ExecutablePath);
                string path = executablePath + @"\temp";
                if (Directory.Exists(path) == false)
                    Directory.CreateDirectory(path);

                using (StreamWriter filePdf = new StreamWriter(path + "\\" + guidPdf + ".pdf"))
                {
                    filePdf.BaseStream.Write(pdfData, 0, pdfData.Length);
                    filePdf.Close();
                    filePdf.Dispose();
                }
                System.Diagnostics.Process pdfExtract = new System.Diagnostics.Process
                {
                    StartInfo =
                    {
                        FileName = executablePath + @"\tools\pdtxt.exe",
                        Arguments =
                            " -u -s -r -o " + "\"" + path + "\\" + guidPdf + ".txt \" \"" + path + "\\" + guidPdf +
                            ".pdf\"",
                        WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
                    }
                };
                //_PdfExtract.StartInfo.UseShellExecute = false;
                //_PdfExtract.StartInfo.RedirectStandardInput = true;
                //_PdfExtract.StartInfo.RedirectStandardOutput = true;            
                pdfExtract.Start();
                pdfExtract.WaitForExit(30000);
                if (File.Exists(path + "\\" + guidPdf + ".txt"))
                {
                    using (StreamReader fileTxt = new StreamReader(path + "\\" + guidPdf + ".txt"))
                    {
                        fileTxtData = System.Text.Encoding.UTF8.GetBytes(fileTxt.ReadToEnd());
                        fileTxt.Close();
                        fileTxt.Dispose();
                    }
                    File.Delete(path + "\\" + guidPdf + ".txt");
                    //if (ConfigFile.GetValue(Prefix_Environment + "/Romis", "SavePdf").ToLower() != "true")
                    //    File.Delete(_Path + "\\" + _FilePDF + ".pdf");
                    //else
                    //fileNamePdf = _FilePDF + ".pdf";
                    fileNamePdf = guidPdf + ".pdf";
                }
            }
            catch (Exception err)
            {
                SystemLog.Err("SITE-ERROR: Errore estrazione PDF: " + err.Message);
            }

        }//ExtractPDF
        #region ExtracInformationFromPdf
        //private void ExtractInformationFromPdf(byte[] _readfileoutput, ref SaveDataStruct _TempSaveData)
        //{
        //    PdTxtExtraction pdTxtFile = new PdTxtExtraction();
        //    bool IntestazioneSezione = false;

        //    bool IntestazioneRilevata = false;

        //    SezioniEnum SezioneAttiva = SezioniEnum.None;
        //    SubSezioniElencoNotaEnum SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.None; //Identifica la sezione attiva in questo momento

        //    SaveDataStruct.DatiImmobiliStruct _CurImmNegoz = null;
        //    SaveDataStruct.DatiImmobiliStruct.ImmobiliStruct _CurImm = null;
        //    SaveDataStruct.DatiImmobiliStruct.ImmobiliStruct.DettImmobiliStruct _CurImmDett = null;
        //    SaveDataStruct.DatiImmobiliStruct.ImmobiliStruct.DettImmobiliFormalitaPrecedente _CurImmFormPrecedenti = null;

        //    SaveDataStruct.AnnotazioniStruct _CurAnnotazione = null;
        //    SaveDataStruct.RettificheStruct _CurRettifiche = null;

        //    string _SogFavoreContro = "";
        //    bool SoggettiDebitoriNonDatori = false;
        //    SaveDataStruct.DatiSoggettiStruct _CurSog = null;
        //    SaveDataStruct.DatiSoggettiStruct.DatiQuoteStruct _CurSogQuote = null;
        //    SaveDataStruct.DatiSoggettiStruct.DatiPFStruct _CurSogPF = null;
        //    SaveDataStruct.DatiSoggettiStruct.DatiPGStruct _CurSogPG = null;


        //    bool ContinueInNextLine = false;  
        //    bool ExcludeNextLine = false;
        //    object ContinueInNextLineOBJ = new object();            

        //    char[] CharsColSplit = { ',' };

        //    int iStart = 0;
        //    int iStop  = 0;
        //    string[] LinesFile = System.Text.Encoding.UTF8.GetString(_readfileoutput).Split('\n');           
        //    for (int iRow = 0; iRow < LinesFile.Length; iRow++)
        //    {
        //        if (LinesFile[iRow].Trim() != "")
        //        {
        //            string _ReadLine = LinesFile[iRow].Replace("\r", string.Empty);                   
        //            pdTxtFile.Add(_ReadLine);
        //        }
        //    }

        //    List<PdTxtExtraction.PdTxtLine> Lines = pdTxtFile.Output();

        //    for (int iRow = 0; iRow < Lines.Count; iRow++)
        //    //foreach (PdTxtExtraction.PdTxtLine RowLine in clsPdTXT.Output())
        //    {                
        //        //Elimino HEADER e FOOTER ma leggo sempre l'intestazione per capire su che pagina mi trovo
        //        if ((Lines[iRow].YCoord < (Single)748.22 && Lines[iRow].YCoord >= (Single)624.338) || _TempSaveData.DataStampaPDF == DateTime.MinValue || SezioneAttiva == SezioniEnum.InfoStampa)
        //        {
        //            string RigaCorrente = Lines[iRow].Text.Replace("\"", string.Empty).Trim() + (char)0;

        //            //Elenco Sezione del PDF
        //            if (RigaCorrente.StartsWith("Ispezione ipotecaria") && _TempSaveData.DataStampaPDF == DateTime.MinValue)
        //            { SezioneAttiva = SezioniEnum.InfoStampa; }
        //            if (RigaCorrente.StartsWith("Ispezione telematica"))
        //            { SezioneAttiva = SezioniEnum.Intestazione; }
        //            if (RigaCorrente.StartsWith("Nota di"))
        //            { SezioneAttiva = SezioniEnum.ElencoNota; }
        //            if (RigaCorrente.StartsWith("Elenco annotazioni, comunicazioni, rettifiche e formalità successive"))
        //            { SezioneAttiva = SezioniEnum.ElencoNota; SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Commenti | SubSezioniElencoNotaEnum.Annotazioni; }             

        //            if (SezioneAttiva == SezioniEnum.InfoStampa)
        //            {
        //                if (RigaCorrente.StartsWith("Ufficio Provinciale di"))
        //                {
        //                    iStart = RigaCorrente.IndexOf("di") + 2;
        //                    iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                    iStop = RigaCorrente.IndexOf("- ", iStart);

        //                    DataRow[] _dtCodUff = Utilities._dictCodiciCons.Select("DescrConsSister = '" + RigaCorrente.Substring(iStart, iStop - iStart).Trim().Replace("'", "''") + "'");
        //                    if (_dtCodUff.Length > 0)
        //                    {
        //                        _TempSaveData.CodConservatoria = _dtCodUff[0]["CodBelfiore"].ToString();                               
        //                    } 

        //                    iStart = RigaCorrente.IndexOf("Data") + 4;
        //                    iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                    iStop = RigaCorrente.IndexOf("Ora", iStart);                            
        //                    string DataOraRegistrazione = "";
        //                    DataOraRegistrazione = RigaCorrente.Substring(iStart, iStop - iStart).Trim() + " ";                            
        //                    iStart = RigaCorrente.IndexOf("Ora") + 3;
        //                    iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                    iStop = RigaCorrente.IndexOf('\0', iStart);
        //                    DataOraRegistrazione += RigaCorrente.Substring(iStart, iStop - iStart).Replace(":",".").Trim();
        //                    DateTime.TryParseExact(DataOraRegistrazione, "dd/MM/yyyy HH.mm.ss", null, System.Globalization.DateTimeStyles.None, out _TempSaveData.DataStampaPDF);
        //                    continue;
        //                }
        //                if (RigaCorrente.StartsWith("Servizio di Pubblicità Immobiliare di"))
        //                {
        //                    iStart = RigaCorrente.IndexOf("Immobiliare di") + 14;
        //                    iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                    iStop = RigaCorrente.IndexOf('\0', iStart);
        //                    DataRow[] _dtCodUff = Utilities._dictCodiciCons.Select("DescrConsSister = '" + RigaCorrente.Substring(iStart, iStop - iStart).Trim().Replace("'", "''") + "'");
        //                    if (_dtCodUff.Length > 0)
        //                    {
        //                        _TempSaveData.CodConservatoria = _dtCodUff[0]["CodBelfiore"].ToString();
        //                    }                                                                 
        //                    continue;
        //                }

        //            }

        //            if (SezioneAttiva == SezioniEnum.Intestazione && IntestazioneSezione == false)
        //            {                       
        //                if (RigaCorrente.StartsWith("n."))
        //                {
        //                    iStart = RigaCorrente.IndexOf("n.") + 2;
        //                    iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                    iStop = RigaCorrente.IndexOf("del", iStart);
        //                    _TempSaveData.NumeroIspezione = RigaCorrente.Substring(iStart, iStop - iStart).Trim();                            
        //                    _TempSaveData.NumeroIspezione = System.Text.RegularExpressions.Regex.Replace(_TempSaveData.NumeroIspezione, "[A-Z ]*", string.Empty);
        //                    continue;
        //                }
        //                if (RigaCorrente.StartsWith("Richiedente"))
        //                {
        //                    iStart = RigaCorrente.IndexOf("€") + 1;
        //                    iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                    iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                    _TempSaveData.AddebitoRicerca = double.Parse(RigaCorrente.Substring(iStart, iStop - iStart).Trim());
        //                    IntestazioneSezione = true; //Lo avvaloro all'ultimo dato recuperato
        //                    continue;
        //                }
        //            }
        //        }

        //        if (SezioneAttiva == SezioniEnum.ElencoNota)
        //        {
        //            //_TempSaveData.DataAtto -> Ultimo dato da leggere che conclude la sezione
        //            if ((Lines[iRow].YCoord < (Single)748.22 && IntestazioneRilevata == false) || (Lines[iRow].YCoord < (Single)624.338 && IntestazioneRilevata != false))
        //            {
        //                string RigaCorrente = Lines[iRow].Text.Replace("\"", string.Empty).Trim() + (char)0;

        //                //Selezione della sezione 
        //                if (RigaCorrente.StartsWith("Nota di"))
        //                { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Intestazione; }
        //                if (RigaCorrente.StartsWith("Dati relativi al titolo"))
        //                { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.DatiTitolo; continue; }
        //                if (RigaCorrente.StartsWith("Dati relativi all'ipoteca o al privilegio"))
        //                { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.DatiIpotecaPrivilegio; continue; }
        //                if (RigaCorrente.StartsWith("Dati relativi alla convenzione"))
        //                { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.DatiConvenzione; continue; }
        //                if (RigaCorrente.StartsWith("Altri dati"))
        //                { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.AltriDati; continue; }
        //                if (RigaCorrente.StartsWith("Dati riepilogativi"))
        //                { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.DatiRiepilogativi; continue; }
        //                if (RigaCorrente.StartsWith("Sezione B - Immobili"))
        //                { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Immobili; continue; }
        //                if (RigaCorrente.StartsWith("Sezione C - Soggetti"))
        //                { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Soggetti; continue; }
        //                if (RigaCorrente.StartsWith("Sezione D - Ulteriori informazioni"))
        //                { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Commenti; continue; }                        

        //                if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.Intestazione) == SubSezioniElencoNotaEnum.Intestazione)
        //                {                            
        //                    if (RigaCorrente.StartsWith("Nota di"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Nota di") + 7;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        switch (RigaCorrente.Substring(iStart, iStop - iStart).ToUpper().Trim())
        //                        {
        //                            case "ANNOTAMENTO":
        //                                _TempSaveData.NaturaAtto = "A";
        //                                break;
        //                            case "TRASCRIZIONE":
        //                                _TempSaveData.NaturaAtto = "T";
        //                                break;
        //                            case "ISCRIZIONE":
        //                                _TempSaveData.NaturaAtto = "I";
        //                                break;
        //                        }

        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Registro generale"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("n.") + 2;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _TempSaveData.NRegGenerale = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Registro particolare"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("n.") + 2;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _TempSaveData.NRegParticolare = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        iStart = RigaCorrente.IndexOf("del") + 3;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        DateTime.TryParseExact(RigaCorrente.Substring(iStart, iStop - iStart).Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out _TempSaveData.DataRegistrazione);
        //                        IntestazioneRilevata = true;
        //                        continue;
        //                    }
        //                }

        //                if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.DatiTitolo) == SubSezioniElencoNotaEnum.DatiTitolo)
        //                {
        //                    if (RigaCorrente.StartsWith("Descrizione"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Descrizione") + 11;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _TempSaveData.DatiTitolo.Descrizione = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Data"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Data") + 4;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        DateTime.TryParseExact(RigaCorrente.Substring(iStart, iStop - iStart).Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out _TempSaveData.DatiTitolo.DataAtto);
        //                        iStart = RigaCorrente.IndexOf("repertorio") + 10;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _TempSaveData.DatiTitolo.NumeroRepertorio = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Notaio"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Notaio") + 6;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf("Codice", iStart);
        //                        _TempSaveData.DatiTitolo.PubblicoUfficialeTipo = 1;
        //                        _TempSaveData.DatiTitolo.PubblicoUfficiale = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        iStart = RigaCorrente.IndexOf("Codice fiscale") + 14;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _TempSaveData.DatiTitolo.CodiceFiscale = RigaCorrente.Substring(iStart, iStop - iStart).ToUpper().Replace(" ", String.Empty);
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Pubblico ufficiale"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("ufficiale") + 9;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf("Codice", iStart);
        //                        _TempSaveData.DatiTitolo.PubblicoUfficialeTipo = 2;
        //                        _TempSaveData.DatiTitolo.PubblicoUfficiale = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        iStart = RigaCorrente.IndexOf("Codice fiscale") + 14;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _TempSaveData.DatiTitolo.CodiceFiscale = RigaCorrente.Substring(iStart, iStop - iStart).ToUpper().Replace(" ", String.Empty);
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Sede"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Sede") + 4;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _TempSaveData.DatiTitolo.Sede = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                }

        //                if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.DatiIpotecaPrivilegio) == SubSezioniElencoNotaEnum.DatiIpotecaPrivilegio)
        //                {
        //                    if (RigaCorrente.StartsWith("Specie dell'ipoteca"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("privilegio") + 10;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _TempSaveData.DatiAtto.Specie = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Derivante da"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("da") + 2;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _TempSaveData.DatiAtto.CodiceAtto = RigaCorrente.Substring(iStart, iStop - iStart).Trim();

        //                        iStart = iStop + 1;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _TempSaveData.DatiAtto.DescrizioneAtto = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Capitale"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Capitale") + 8;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf("Tasso", iStart);
        //                        ConvertToCurrency(RigaCorrente.Substring(iStart, iStop - iStart).Trim(), out _TempSaveData.DatiAtto.Capitale, out _TempSaveData.DatiAtto.CapitaleValuta);                                

        //                        iStart = RigaCorrente.IndexOf("annuo") + 5;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf("Tasso", iStart);
        //                        ConvertToFloat(RigaCorrente.Substring(iStart, iStop - iStart).Trim().Replace("%", string.Empty), out _TempSaveData.DatiAtto.TassoInteresseAnnuo);
        //                        //_TempSaveData.DatiAtto.TassoInteresseAnnuo = RigaCorrente.Substring(iStart, iStop - iStart).Trim();

        //                        iStart = RigaCorrente.IndexOf("semestrale") + 10;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        ConvertToFloat(RigaCorrente.Substring(iStart, iStop - iStart).Trim().Replace("%", string.Empty), out _TempSaveData.DatiAtto.TassoInteresseSemetrale);
        //                        //_TempSaveData.DatiAtto.TassoInteresseSemetrale = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Interessi"))
        //                    {
        //                        //iStart = RigaCorrente.IndexOf("Interessi") + 9;
        //                        //iStart = RigaCorrente.IndexOfAny(CharsStartLine, iStart);
        //                        //iStop = RigaCorrente.IndexOf("Spese", iStart);
        //                        //ConvertToFloat(RigaCorrente.Substring(iStart, iStop - iStart).Trim().Replace("%", string.Empty), out _TempSaveData.DatiAtto.Interessi);
        //                        ////_TempSaveData.DatiAtto.Interessi =  RigaCorrente.Substring(iStart, iStop - iStart).Trim();

        //                        iStart = RigaCorrente.IndexOf("Spese") + 5;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf("Totale", iStart);
        //                        ConvertToCurrency(RigaCorrente.Substring(iStart, iStop - iStart).Trim(), out _TempSaveData.DatiAtto.Spese, out _TempSaveData.DatiAtto.SpeseValuta);                                
        //                        //_TempSaveData.DatiAtto.Spese = RigaCorrente.Substring(iStart, iStop - iStart).ToUpper().Trim();

        //                        iStart = RigaCorrente.IndexOf("Totale") + 6;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        ConvertToCurrency(RigaCorrente.Substring(iStart, iStop - iStart).Trim(), out _TempSaveData.DatiAtto.Totale, out _TempSaveData.DatiAtto.TotaleValuta);                                                                
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Importi variabili"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("variabili") + 9;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _TempSaveData.DatiAtto.ImportiVariabili = RigaCorrente.Substring(iStart, iStop - iStart).Trim();

        //                        //if (RigaCorrente.Contains("Valuta estera"))
        //                        //{
        //                        //    iStart = RigaCorrente.IndexOf("estera", iStart) + 6;
        //                        //    iStart = RigaCorrente.IndexOfAny(CharsStartLine, iStart);
        //                        //    iStop = RigaCorrente.IndexOfAny(CharsEndLine, iStart);
        //                        //    _TempSaveData.DatiAtto.ValutaEstera = RigaCorrente.Substring(iStart, iStop - iStart).Trim();                                    
        //                        //}
        //                        if (RigaCorrente.Contains("Somma iscritta da aumentare automaticamente"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("automaticamente", iStart) + 15;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _TempSaveData.DatiAtto.SommaAumenta = RigaCorrente.Substring(iStart, iStop - iStart).Trim();                                    
        //                        }

        //                        continue;
        //                    }                            
        //                    if (RigaCorrente.StartsWith("Presenza di condizione risolutiva"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Durata") + 6;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);                                

        //                        string Durata = RigaCorrente.Substring(iStart, iStop - iStart).Replace("  ", " ").Replace("-", string.Empty).Trim() + '\0';
        //                        //Durata = Durata.ToUpper().Replace("METRI QUADRI", "MQ"); //Così evito lo spazio tra queste due parole

        //                        iStart = 0;
        //                        iStop = 0;

        //                        while (Durata.Substring(iStart) != "\0")
        //                        {
        //                            iStop = Durata.IndexOfAny(Utilities.CharsEndLine, iStart + 1);
        //                            string Num = Durata.Substring(iStart, iStop - iStart).Trim().Replace(".", System.Globalization.CultureInfo.CreateSpecificCulture("it-IT").NumberFormat.CurrencyDecimalSeparator);
        //                            Num = Num.Replace(",", System.Globalization.CultureInfo.CreateSpecificCulture("it-IT").NumberFormat.CurrencyDecimalSeparator);
        //                            iStart = Durata.IndexOfAny(Utilities.CharsStartLine, iStop);
        //                            iStop = Durata.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            string Desc = Durata.Substring(iStart, iStop - iStart).Trim();
        //                            iStart = Durata.IndexOfAny(Utilities.CharsEndLine, iStart);

        //                            switch (Desc.ToUpper())
        //                            {
        //                                case "ANNI":
        //                                    ConvertToFloat(Num, out _TempSaveData.DatiAtto.DurataAnni);
        //                                    //_TempSaveData.DatiAtto.DurataAnni = int.Parse(Num);
        //                                    break;
        //                                case "MESI":
        //                                    ConvertToFloat(Num, out _TempSaveData.DatiAtto.DurataMesi);
        //                                    //_TempSaveData.DatiAtto.DurataMesi = int.Parse(Num);
        //                                    break;
        //                                case "GIORNI":
        //                                    ConvertToFloat(Num, out _TempSaveData.DatiAtto.DurataGiorni);
        //                                    //_TempSaveData.DatiAtto.DurataGiorni = int.Parse(Num);
        //                                    break;
        //                            }
        //                        }

        //                        continue;
        //                    }
        //                    //if (RigaCorrente.StartsWith("Termine dell'ipoteca"))
        //                    //{
        //                    //    iStart = RigaCorrente.IndexOf("ipoteca") + 7;
        //                    //    iStart = RigaCorrente.IndexOfAny(CharsStartLine, iStart);
        //                    //    iStop = RigaCorrente.IndexOfAny(CharsEndLine, iStart);
        //                    //    _TempSaveData.DatiAtto.TermineIpoteca = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                    //    continue;
        //                    //}                            
        //                }
        //                if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.DatiConvenzione) == SubSezioniElencoNotaEnum.DatiConvenzione)
        //                {
        //                    if (RigaCorrente.StartsWith("Specie"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Specie") + 6;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _TempSaveData.DatiAtto.Specie = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Descrizione"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Descrizione") + 12;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _TempSaveData.DatiAtto.CodiceAtto = RigaCorrente.Substring(iStart, iStop - iStart).Trim();

        //                        iStart = iStop + 1;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _TempSaveData.DatiAtto.DescrizioneAtto = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Atto mortis causa"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("morte") + 5;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        DateTime.TryParseExact(RigaCorrente.Substring(iStart, iStop - iStart).Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out _TempSaveData.DatiAtto.DataMorte);                                
        //                        continue;
        //                    }                            
        //                }



        //                if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.AltriDati) == SubSezioniElencoNotaEnum.AltriDati)
        //                {       
        //                    if (RigaCorrente.StartsWith("Formalità di riferimento"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("riferimento:") + 12;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        switch (RigaCorrente.Substring(iStart, iStop - iStart).ToUpper().Trim())
        //                        {
        //                            case "ANNOTAMENTO":
        //                                _TempSaveData.DatiAtto.FormalitaRiferimento = "A";
        //                                break;
        //                            case "TRASCRIZIONE":
        //                                _TempSaveData.DatiAtto.FormalitaRiferimento = "T";
        //                                break;
        //                            case "ISCRIZIONE":
        //                                _TempSaveData.DatiAtto.FormalitaRiferimento = "I";
        //                                break;
        //                            case "-":
        //                                _TempSaveData.DatiAtto.FormalitaRiferimento = _TempSaveData.NaturaAtto;
        //                                break;
        //                        }         
        //                        iStart = RigaCorrente.IndexOf("particolare") + 11;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf("del", iStart);
        //                        _TempSaveData.DatiAtto.FormalitaRiferimentoRegPart = RigaCorrente.Substring(iStart, iStop - iStart).ToUpper().Trim();
        //                        iStart = RigaCorrente.IndexOf("del") + 3;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        DateTime.TryParseExact(RigaCorrente.Substring(iStart, iStop - iStart).Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out _TempSaveData.DatiAtto.FormalitaRiferimentoData);                                

        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Richiedente"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Richiedente") + 11;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _TempSaveData.DatiAtto.RichiedenteNome = RigaCorrente.Substring(iStart, iStop - iStart).Trim();                                
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Indirizzo"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Indirizzo") + 9;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _TempSaveData.DatiAtto.RichiedenteSede = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Non sono presenti nella sezione"))
        //                    {                                
        //                        //_TempSaveData.DatiAtto.AltriDati += RigaCorrente.Substring(iStart, iStop - iStart).Trim() + Environment.NewLine;
        //                        _TempSaveData.DatiAtto.AltriDatiFlagA = "N";
        //                        _TempSaveData.DatiAtto.AltriDatiFlagB = "N";
        //                        _TempSaveData.DatiAtto.AltriDatiFlagC = "N";
        //                    }
        //                    if (RigaCorrente.StartsWith("Sono presenti nella sezione"))
        //                    {                                
        //                        _TempSaveData.DatiAtto.AltriDatiFlagA = (RigaCorrente.Contains("sezione A") ? "S" : "N");
        //                        _TempSaveData.DatiAtto.AltriDatiFlagB = (RigaCorrente.Contains("sezione B") ? "S" : "N");
        //                        _TempSaveData.DatiAtto.AltriDatiFlagC = (RigaCorrente.Contains("sezione C") ? "S" : "N");                              

        //                        //_TempSaveData.DatiAtto.AltriDati += RigaCorrente.Substring(iStart, iStop - iStart).Trim() + Environment.NewLine;
        //                    }
        //                    continue;
        //                }


        //                if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.DatiRiepilogativi) == SubSezioniElencoNotaEnum.DatiRiepilogativi)
        //                {
        //                    if (RigaCorrente.StartsWith("Unità negoziali"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("negoziali") + 9;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf("Soggetti", iStart);
        //                        _TempSaveData.DatiRiepilogativi.TotUnitàNegoziali = RigaCorrente.Substring(iStart, iStop - iStart).Trim();

        //                        iStart = RigaCorrente.IndexOf("favore") + 6;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf("Soggetti", iStart);
        //                        _TempSaveData.DatiRiepilogativi.TotSoggettiFavore = RigaCorrente.Substring(iStart, iStop - iStart).Trim();

        //                        iStart = RigaCorrente.IndexOf("contro") + 6;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _TempSaveData.DatiRiepilogativi.TotSoggettiContro = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                }

        //                if (((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.Immobili) == SubSezioniElencoNotaEnum.Immobili) || 
        //                    ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.ImmobiliPrecedenti) == SubSezioniElencoNotaEnum.ImmobiliPrecedenti))
        //                {                            
        //                    if (RigaCorrente.StartsWith("Unità negoziale"))
        //                    {
        //                        _CurImmNegoz = new SaveDataStruct.DatiImmobiliStruct(); //Nuova Unità Negoziale
        //                        _TempSaveData.DatiImmobili.Add(_CurImmNegoz);        //Salvo la nuova referenza                    

        //                        iStart = RigaCorrente.IndexOf("n.") + 2;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _CurImmNegoz.NUnitàNegoziale = int.Parse(RigaCorrente.Substring(iStart, iStop - iStart).Trim());
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Immobile n."))
        //                    {
        //                        SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Immobili; //Forzo il riconoscimento di questa sezione, mi serve per disabilitare la sezione degli immobili precedenti                                

        //                        _CurImm = new SaveDataStruct.DatiImmobiliStruct.ImmobiliStruct(); //Nuovo Immobile
        //                        _CurImmNegoz.Immobili.Add(_CurImm);

        //                        iStart = RigaCorrente.IndexOf("n.") + 2;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _CurImm.NImmobile = int.Parse(RigaCorrente.Substring(iStart, iStop - iStart).Trim());

        //                        if (RigaCorrente.Contains("Gruppo graffati"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("graffati") + 8;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurImm.GruppoGraffati = int.Parse(RigaCorrente.Substring(iStart, iStop - iStart).Trim());
        //                            continue;
        //                        }

        //                        continue;
        //                    }

        //                    //Sezione da abilitare solo se non sono nella sezione Immobili precedenti
        //                    if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.ImmobiliPrecedenti) != SubSezioniElencoNotaEnum.ImmobiliPrecedenti)
        //                    {
        //                        if (RigaCorrente.StartsWith("Comune"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("Comune") + 6;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('-', iStart);
        //                            _CurImm.CodCatastaleComune = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            iStart = RigaCorrente.IndexOf("-") + 1;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('\0', iStart);
        //                            _CurImm.Comune = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            continue;
        //                        }
        //                        if (RigaCorrente.StartsWith("Catasto"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("Catasto") + 7;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('\0', iStart);
        //                            if (RigaCorrente.Substring(iStart, iStop - iStart).Trim().ToUpper() == "TERRENI")
        //                                _CurImm.Catasto = "T";
        //                            else if (RigaCorrente.Substring(iStart, iStop - iStart).Trim().ToUpper() == "FABBRICATI")
        //                                _CurImm.Catasto = "U";

        //                            continue;
        //                        }
        //                        if (RigaCorrente.StartsWith("Protocollo") || RigaCorrente.StartsWith("Scheda") || RigaCorrente.StartsWith("Variazione"))
        //                        {
        //                            _CurImmDett = new SaveDataStruct.DatiImmobiliStruct.ImmobiliStruct.DettImmobiliStruct();                                    
        //                            iStart = RigaCorrente.IndexOf("Protocollo") + 10;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurImmDett.Protocollo = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            iStart = RigaCorrente.IndexOf("Anno") + 4;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurImmDett.Anno = RigaCorrente.Substring(iStart, iStop - iStart).Trim();                                    

        //                            _CurImm.DettImmobile.Add(_CurImmDett);        //Salvo la nuova referenza     
        //                            continue;
        //                        }
        //                        if (RigaCorrente.StartsWith("Sezione urbana") || RigaCorrente.StartsWith("Foglio"))
        //                        {
        //                            _CurImmDett = new SaveDataStruct.DatiImmobiliStruct.ImmobiliStruct.DettImmobiliStruct();
        //                            if (RigaCorrente.IndexOf("urbana") > -1)
        //                            {
        //                                iStart = RigaCorrente.IndexOf("urbana") + 6;
        //                                iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                                iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                                _CurImmDett.SezioneUrbana = RigaCorrente.Substring(iStart, iStop - iStart).Replace("-", string.Empty).Trim();
        //                            }
        //                            iStart = RigaCorrente.IndexOf("Foglio") + 6;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurImmDett.Foglio = RigaCorrente.Substring(iStart, iStop - iStart).Replace("-", string.Empty).Trim();
        //                            iStart = RigaCorrente.IndexOf("Particella") + 10;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurImmDett.Particella = RigaCorrente.Substring(iStart, iStop - iStart).Replace("-", string.Empty).Trim();
        //                            iStart = RigaCorrente.IndexOf("Subalterno") + 10;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurImmDett.Subalterno = RigaCorrente.Substring(iStart, iStop - iStart).Replace("-", string.Empty).Trim();

        //                            _CurImm.DettImmobile.Add(_CurImmDett);        //Salvo la nuova referenza     
        //                            continue;
        //                        }
        //                    }

        //                    if (RigaCorrente.StartsWith("Natura") || (RigaCorrente.Contains("Consistenza")))
        //                    {
        //                        if (RigaCorrente.StartsWith("Natura"))
        //                        { 
        //                            iStart = RigaCorrente.IndexOf("Natura") + 6;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('-', iStart);
        //                            _CurImm.CatCatastale = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            //iStart = RigaCorrente.IndexOf('-', iStart) + 1;
        //                            //iStart = RigaCorrente.IndexOfAny(CharsStartLine, iStart);
        //                            //iStop = RigaCorrente.IndexOf("Consistenza", iStart);
        //                            //_CurImm.CatCatastaleDescr = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            //ContinueInNextLine = true;
        //                            //ContinueInNextLineOBJ = new StringBuilder(_CurImm.CatCatastaleDescr);
        //                        }
        //                        if (RigaCorrente.Contains("Consistenza"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("Consistenza") + 11;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            if (iStart > -1)
        //                            {
        //                                iStop = RigaCorrente.IndexOf('\0', iStart);
        //                                string Consistenza = RigaCorrente.Substring(iStart, iStop - iStart).Trim() + '\0';
        //                                Consistenza = Consistenza.ToUpper().Replace("METRI QUADRI", "MQ").Replace("METRI CUBI", "MC").Replace("-", string.Empty);
        //                                while (Consistenza.IndexOf("  ") != -1)                                        
        //                                    Consistenza = Consistenza.Replace("  ", " ").Trim(); //Così evito lo spazio tra queste due parole                                                                            

        //                                iStart = 0;
        //                                iStop = 0;

        //                                while (Consistenza.Substring(iStart) != "\0")
        //                                {
        //                                    iStop = Consistenza.IndexOfAny(Utilities.CharsEndLine, iStart + 1);
        //                                    string Num = Consistenza.Substring(iStart, iStop - iStart).Trim().Replace(".", System.Globalization.CultureInfo.CreateSpecificCulture("it-IT").NumberFormat.CurrencyDecimalSeparator);
        //                                    Num = Num.Replace(",", System.Globalization.CultureInfo.CreateSpecificCulture("it-IT").NumberFormat.CurrencyDecimalSeparator);
        //                                    iStart = Consistenza.IndexOfAny(Utilities.CharsStartLine, iStop);
        //                                    iStop = Consistenza.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                                    string Desc = Consistenza.Substring(iStart, iStop - iStart).Trim();
        //                                    iStart = Consistenza.IndexOfAny(Utilities.CharsEndLine, iStart);

        //                                    switch (Desc.ToUpper())
        //                                    {
        //                                        case "ETTARI":
        //                                            ConvertToFloat(Num, out _CurImm.ConsistHa);
        //                                            //_CurImm.ConsistHa = float.Parse(Num);
        //                                            break;
        //                                        case "ARE":
        //                                            ConvertToFloat(Num, out _CurImm.ConsistAre);
        //                                            //_CurImm.ConsistAre = float.Parse(Num);
        //                                            break;
        //                                        case "CENTIARE":
        //                                            ConvertToFloat(Num, out _CurImm.ConsistCAre);
        //                                            //_CurImm.ConsistCAre = float.Parse(Num);
        //                                            break;
        //                                        case "MQ":
        //                                            ConvertToFloat(Num, out _CurImm.ConsistMQ);
        //                                            //_CurImm.ConsistMQ = float.Parse(Num);
        //                                            break;
        //                                        case "MC":

        //                                            break;
        //                                        case "VANI":
        //                                            ConvertToFloat(Num, out _CurImm.ConsistVani);
        //                                            //_CurImm.ConsistVani = float.Parse(Num);
        //                                            break;
        //                                    }

        //                                }
        //                            }
        //                        }

        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Indirizzo"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Indirizzo") + 9;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf("N. civico", iStart);
        //                        _CurImm.Indirizzo = RigaCorrente.Substring(iStart, iStop - iStart).Replace("-", string.Empty).Trim();
        //                        iStart = RigaCorrente.IndexOf("N. civico") + 9;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        if (iStart > -1)
        //                        {
        //                            iStop = RigaCorrente.IndexOf('\0', iStart);
        //                            _CurImm.NCivico = RigaCorrente.Substring(iStart, iStop - iStart).Replace("-", string.Empty).Trim();
        //                        }
        //                        continue;
        //                    }
        //                    if (RigaCorrente.Contains("Interno") ||
        //                        RigaCorrente.Contains("Piano") ||
        //                        RigaCorrente.Contains("Edificio") ||
        //                        RigaCorrente.Contains("Scala"))
        //                    {
        //                        if (RigaCorrente.Contains("Interno"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("Interno") + 7;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            if (iStart > -1)
        //                            {
        //                                iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                                _CurImm.Interno = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            }
        //                        }
        //                        if (RigaCorrente.Contains("Piano"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("Piano") + 5;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            if (iStart > -1)
        //                            {
        //                                iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                                _CurImm.Piano = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            }
        //                        }
        //                        if (RigaCorrente.Contains("Edificio"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("Edificio") + 8;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            if (iStart > -1)
        //                            {
        //                                iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                                _CurImm.Edificio = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            }
        //                        }
        //                        if (RigaCorrente.Contains("Scala"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("Scala") + 5;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            if (iStart > -1)
        //                            {
        //                                iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                                _CurImm.Scala = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            }
        //                        }
        //                        continue;
        //                    }
        //                    if ((RigaCorrente.StartsWith("Identificativo dell'immobile nella formalità") && RigaCorrente.Contains("precedente"))
        //                        || ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.ImmobiliPrecedenti) == SubSezioniElencoNotaEnum.ImmobiliPrecedenti))
        //                    {
        //                        SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.ImmobiliPrecedenti;

        //                        if (RigaCorrente.StartsWith("Comune"))
        //                        {
        //                            _CurImmFormPrecedenti = new SaveDataStruct.DatiImmobiliStruct.ImmobiliStruct.DettImmobiliFormalitaPrecedente();
        //                            _CurImm.DettImmobiliFormPrec.Add(_CurImmFormPrecedenti);        //Salvo la nuova referenza     

        //                            iStart = RigaCorrente.IndexOf("Comune") + 6;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('-', iStart);
        //                            _CurImmFormPrecedenti.CodCatastaleComune = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            iStart = RigaCorrente.IndexOf("-") + 1;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('\0', iStart);
        //                            _CurImmFormPrecedenti.Comune = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            continue;
        //                        }
        //                        if (RigaCorrente.StartsWith("Catasto"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("Catasto") + 7;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('\0', iStart);
        //                            if (RigaCorrente.Substring(iStart, iStop - iStart).Trim().ToUpper() == "TERRENI")
        //                                _CurImmFormPrecedenti.Catasto = "T";
        //                            else if (RigaCorrente.Substring(iStart, iStop - iStart).Trim().ToUpper() == "FABBRICATI")
        //                                _CurImmFormPrecedenti.Catasto = "U";

        //                            continue;
        //                        }

        //                        if (RigaCorrente.StartsWith("Sezione urbana") || RigaCorrente.StartsWith("Foglio"))
        //                        {                                    
        //                            if (RigaCorrente.IndexOf("urbana") > -1)
        //                            {
        //                                iStart = RigaCorrente.IndexOf("urbana") + 6;
        //                                iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                                iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                                _CurImmFormPrecedenti.SezioneUrbana = RigaCorrente.Substring(iStart, iStop - iStart).Replace("-", string.Empty).Trim();
        //                            }
        //                            iStart = RigaCorrente.IndexOf("Foglio") + 6;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurImmFormPrecedenti.Foglio = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            iStart = RigaCorrente.IndexOf("Particella") + 10;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurImmFormPrecedenti.Particella = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            iStart = RigaCorrente.IndexOf("Subalterno") + 10;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurImmFormPrecedenti.Subalterno = RigaCorrente.Substring(iStart, iStop - iStart).Trim();

        //                            continue;
        //                        }


        //                        continue;
        //                    }
        //                    if (ContinueInNextLine)
        //                    {
        //                        iStart = 0;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        ((StringBuilder)ContinueInNextLineOBJ).Append(" " + RigaCorrente.Substring(iStart, iStop - iStart).Trim());
        //                    }
        //                }

        //                if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.Soggetti) == SubSezioniElencoNotaEnum.Soggetti)
        //                {
        //                    if (RigaCorrente.StartsWith("A favore"))
        //                    {
        //                        _SogFavoreContro = "FAVORE";
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Contro"))
        //                    {
        //                        _SogFavoreContro = "CONTRO";
        //                        continue;
        //                    }

        //                    if (RigaCorrente.StartsWith("Debitori non datori di ipoteca"))
        //                    {
        //                        SoggettiDebitoriNonDatori = true; 
        //                    }

        //                    if (RigaCorrente.StartsWith("Soggetto n."))
        //                    {
        //                        _CurSog = new SaveDataStruct.DatiSoggettiStruct();
        //                        if (SoggettiDebitoriNonDatori == false)
        //                            _TempSaveData.DatiSoggetti.Add(_CurSog);
        //                        else
        //                        {
        //                            _TempSaveData.DatiSoggettiDebitoriNonDatori.Add(_CurSog);
        //                            //Copio tutte le unità negoziali presenti
        //                            foreach (SaveDataStruct.DatiImmobiliStruct _imm  in _TempSaveData.DatiImmobili)
        //                            {
        //                                SaveDataStruct.DatiSoggettiStruct.DatiQuoteStruct _curQuota =new SaveDataStruct.DatiSoggettiStruct.DatiQuoteStruct();
        //                                _curQuota.NUnitàNegoziale  =_imm.NUnitàNegoziale;
        //                                _CurSog.Quote.Add(_curQuota);
        //                            }
        //                        }

        //                        ContinueInNextLine = false; //Azzero eventuali next line

        //                        _CurSog.Effetto = _SogFavoreContro;

        //                        iStart = RigaCorrente.IndexOf("n.") + 2;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _CurSog.NSoggetto = int.Parse(RigaCorrente.Substring(iStart, iStop - iStart).Trim());
        //                        iStart = RigaCorrente.IndexOf("di") + 2;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _CurSog.Qualita = RigaCorrente.Substring(iStart, iStop - iStart).Replace("-", string.Empty).Trim();
        //                        switch (_CurSog.Qualita.ToUpper().Trim())
        //                        {
        //                            case "TERZO DATORE DI IPOTECA":
        //                                _CurSog.TerzoDatore = "S";
        //                                break;
        //                            case "FAVORE":
        //                                break;
        //                            case "CONTRO":
        //                                break;
        //                        }

        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Denominazione"))
        //                    {
        //                        _CurSogPG = new SaveDataStruct.DatiSoggettiStruct.DatiPGStruct();
        //                        _CurSog.SoggettoPG.Add(_CurSogPG);

        //                        iStart = RigaCorrente.IndexOf("sociale") + 7;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _CurSogPG.D = new StringBuilder(RigaCorrente.Substring(iStart, iStop - iStart).Trim());
        //                        ContinueInNextLine = true;
        //                        ContinueInNextLineOBJ = _CurSogPG.D;
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Sede"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Sede") + 4;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        if (iStart > -1)
        //                        {
        //                            iStop = RigaCorrente.IndexOf('\0', iStart);
        //                            _CurSogPG.Sede = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        }
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Codice fiscale"))  //Persona Giuridica
        //                    {
        //                        iStart = RigaCorrente.IndexOf("fiscale") + 7;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        if (RigaCorrente.Contains("Domicilio"))
        //                        {
        //                            iStop = RigaCorrente.IndexOf("Domicilio", iStart);
        //                            if (_CurSogPG != null)
        //                            {
        //                                _CurSogPG.CF = RigaCorrente.Substring(iStart, iStop - iStart).Trim().Replace("-", string.Empty).Replace(" ", string.Empty);
        //                                iStart = RigaCorrente.IndexOf("eletto") + 6;
        //                                iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                                iStop = RigaCorrente.IndexOf('\0', iStart);
        //                                _CurSogPG.DomicilioIpotecario = new StringBuilder(RigaCorrente.Substring(iStart, iStop - iStart).Trim());
        //                                ContinueInNextLine = true;
        //                                ContinueInNextLineOBJ = _CurSogPG.DomicilioIpotecario;
        //                            }
        //                            else
        //                            {
        //                                _CurSogPF.CF = RigaCorrente.Substring(iStart, iStop - iStart).Trim().Replace("-", string.Empty).Replace(" ", string.Empty);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            iStop = RigaCorrente.IndexOf('\0', iStart);
        //                            if (_CurSogPG != null)                                    
        //                                _CurSogPG.CF = RigaCorrente.Substring(iStart, iStop - iStart).Trim().Replace("-", string.Empty).Replace(" ", string.Empty);                                    
        //                            else
        //                                _CurSogPF.CF = RigaCorrente.Substring(iStart, iStop - iStart).Trim().Replace("-", string.Empty).Replace(" ", string.Empty);

        //                        }                              
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Cognome"))
        //                    {
        //                        _CurSogPF = new SaveDataStruct.DatiSoggettiStruct.DatiPFStruct();
        //                        _CurSog.SoggettoPF.Add(_CurSogPF);

        //                        iStart = RigaCorrente.IndexOf("Cognome") + 7;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf("Nome", iStart);
        //                        _CurSogPF.C = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        iStart = RigaCorrente.IndexOf("Nome") + 4;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _CurSogPF.N = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        continue;
        //                    }
        //                    if (RigaCorrente.Contains("Nato il") || RigaCorrente.Contains("Nata il"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("il") + 2;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf("a", iStart);
        //                        DateTime.TryParseExact(RigaCorrente.Substring(iStart, iStop - iStart).Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out _CurSogPF.DN);
        //                        iStart = RigaCorrente.IndexOf("a", iStart) + 1;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _CurSogPF.CN = RigaCorrente.Substring(iStart, iStop - iStart).Trim();                                
        //                    }
        //                    if (RigaCorrente.StartsWith("Sesso"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("Sesso") + 5;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _CurSogPF.S = RigaCorrente.Substring(iStart, iStop - iStart).Trim();                                                     
        //                    }
        //                    if (RigaCorrente.Contains("Codice fiscale"))
        //                    {                                
        //                        iStart = RigaCorrente.IndexOf("fiscale") + 7;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _CurSogPF.CF = RigaCorrente.Substring(iStart, iStop - iStart).Replace(" ", string.Empty);
        //                    }

        //                    if (RigaCorrente.Contains("Relativamente all'unità negoziale n."))
        //                    {
        //                        _CurSogQuote = new SaveDataStruct.DatiSoggettiStruct.DatiQuoteStruct();
        //                        _CurSog.Quote.Add(_CurSogQuote);

        //                        iStart = RigaCorrente.IndexOf("n.") + 2;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                        _CurSogQuote.NUnitàNegoziale = int.Parse(RigaCorrente.Substring(iStart, iStop - iStart).Trim());
        //                        iStart = RigaCorrente.IndexOf("diritto di") + 10;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);

        //                        DataRow[] _dtCodProp = Utilities._dictCodiciProprieta.Select("Description = '" + RigaCorrente.Substring(iStart, iStop - iStart).Trim().Replace("'", "''") + "'");
        //                        if (_dtCodProp.Length > 0)
        //                        {
        //                            _CurSogQuote.Diritto = _dtCodProp[0]["Code"].ToString();
        //                        }                                

        //                        continue;
        //                    }
        //                    if (RigaCorrente.ToLower().Contains("relativamente a tutte le unità negoziali"))
        //                    {
        //                        _CurSogQuote = new SaveDataStruct.DatiSoggettiStruct.DatiQuoteStruct();
        //                        _CurSog.Quote.Add(_CurSogQuote);

        //                        _CurSogQuote.NUnitàNegoziale = 999;
        //                        iStart = RigaCorrente.IndexOf("diritto di") + 10;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);

        //                        DataRow[] _dtCodProp = Utilities._dictCodiciProprieta.Select("Description = '" + RigaCorrente.Substring(iStart, iStop - iStart).Trim().Replace("'", "''") + "'");
        //                        if (_dtCodProp.Length > 0)
        //                        {
        //                            _CurSogQuote.Diritto = _dtCodProp[0]["Code"].ToString();
        //                        }   
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Per la quota di"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("di") + 2;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('/', iStart);
        //                        if (RigaCorrente.Substring(iStart, 1).Trim().Replace("-", string.Empty) != "")
        //                        {
        //                            _CurSogQuote.QuotaNum = RigaCorrente.Substring(iStart, iStop - iStart).Trim();

        //                            iStart = RigaCorrente.IndexOf("/", iStart) + 1;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurSogQuote.QuotaDen = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        }

        //                        if (RigaCorrente.ToLower().Contains("in regime di"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("di", iStart) + 2;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('\0', iStart);
        //                            _CurSogQuote.RegimePatrimoniale = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        }
        //                        continue;
        //                    }
        //                    if (RigaCorrente.StartsWith("Terzo datore"))
        //                    {
        //                        iStart = RigaCorrente.IndexOf("datore") + 6;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        if (RigaCorrente.Substring(iStart, iStop - iStart).Trim().ToUpper() == "SI")
        //                            _CurSog.TerzoDatore = "S";
        //                        else if (RigaCorrente.Substring(iStart, iStop - iStart).Trim().ToUpper() == "NO")
        //                            _CurSog.TerzoDatore = "N";

        //                        continue;
        //                    }
        //                    if (ContinueInNextLine)
        //                    {
        //                        iStart = 0;
        //                        iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);                                
        //                        ((StringBuilder)ContinueInNextLineOBJ).Append(" " + RigaCorrente.Substring(iStart, iStop - iStart).Trim());
        //                    }
        //                }

        //                if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.Commenti) == SubSezioniElencoNotaEnum.Commenti)
        //                {
        //                    if (RigaCorrente == "Annotazioni\0")
        //                    { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Commenti | SubSezioniElencoNotaEnum.Annotazioni; continue; }
        //                    if (RigaCorrente == "Comunicazioni\0")
        //                    { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Commenti | SubSezioniElencoNotaEnum.Comunicazioni; continue; }
        //                    if (RigaCorrente == "Rettifiche\0")
        //                    { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Commenti | SubSezioniElencoNotaEnum.Rettifiche; continue; }
        //                    if (RigaCorrente == "Formalità successive (nelle quali la nota è formalità di riferimento)\0")
        //                    { SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Commenti | SubSezioniElencoNotaEnum.FormalitaSuccessive; continue; }


        //                    if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.Annotazioni) == SubSezioniElencoNotaEnum.Annotazioni)
        //                    {
        //                        SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Commenti | SubSezioniElencoNotaEnum.Annotazioni;

        //                        if (RigaCorrente.StartsWith("ANNOTAZIONE"))
        //                        {
        //                            _CurAnnotazione = new SaveDataStruct.AnnotazioniStruct();
        //                            _TempSaveData.Annotazioni.Add(_CurAnnotazione);

        //                            iStart = RigaCorrente.IndexOf("il") + 2;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            DateTime.TryParseExact(RigaCorrente.Substring(iStart, iStop - iStart).Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out _CurAnnotazione.DataPresentazione);
        //                        }

        //                        if (RigaCorrente.StartsWith("Registro particolare"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("n.") + 2;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurAnnotazione.NRegParticolare = RigaCorrente.Substring(iStart, iStop - iStart).Trim();

        //                            if (RigaCorrente.Contains("Registro generale"))
        //                            {
        //                                iStart = RigaCorrente.IndexOf("generale") + 8;
        //                                iStart = RigaCorrente.IndexOf("n.", iStart) + 2;
        //                                iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                                iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                                _CurAnnotazione.NRegGenerale = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            }
        //                            continue;
        //                        }
        //                        if (RigaCorrente.StartsWith("Tipo di atto"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("atto:") + 5;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('-', iStart);
        //                            _CurAnnotazione.CodiceAtto = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            iStart = RigaCorrente.IndexOf('-') + 1;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('\0', iStart);
        //                            _CurAnnotazione.DescrizioneAtto = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        }

        //                    }
        //                    else if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.Comunicazioni) == SubSezioniElencoNotaEnum.Comunicazioni)
        //                    {
        //                        SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Commenti | SubSezioniElencoNotaEnum.Comunicazioni;
        //                    }
        //                    else if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.Rettifiche) == SubSezioniElencoNotaEnum.Rettifiche)
        //                    {
        //                        SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Commenti | SubSezioniElencoNotaEnum.Rettifiche;

        //                        if (RigaCorrente.Contains("presentata il"))
        //                        {
        //                            _CurRettifiche = new SaveDataStruct.RettificheStruct();
        //                            _TempSaveData.Rettifiche.Add(_CurRettifiche);

        //                            iStart = RigaCorrente.IndexOf("il") + 2;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            DateTime.TryParseExact(RigaCorrente.Substring(iStart, iStop - iStart).Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out _CurRettifiche.DataPresentazione);
        //                        }

        //                        if (RigaCorrente.StartsWith("Registro particolare"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("n.") + 2;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                            _CurRettifiche.NRegParticolare = RigaCorrente.Substring(iStart, iStop - iStart).Trim();

        //                            if (RigaCorrente.Contains("Registro generale"))
        //                            {
        //                                iStart = RigaCorrente.IndexOf("generale") + 8;
        //                                iStart = RigaCorrente.IndexOf("n.", iStart) + 2;
        //                                iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                                iStop = RigaCorrente.IndexOfAny(Utilities.CharsEndLine, iStart);
        //                                _CurRettifiche.NRegGenerale = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            }
        //                            continue;
        //                        }
        //                        if (RigaCorrente.StartsWith("Tipo di atto"))
        //                        {
        //                            iStart = RigaCorrente.IndexOf("atto:") + 5;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('-', iStart);
        //                            _CurRettifiche.CodiceAtto = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                            iStart = RigaCorrente.IndexOf('-') + 1;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('\0', iStart);
        //                            _CurRettifiche.DescrizioneAtto = RigaCorrente.Substring(iStart, iStop - iStart).Trim();
        //                        }
        //                    }
        //                    else if ((SubSezioneElencoNotaAttiva & SubSezioniElencoNotaEnum.FormalitaSuccessive) == SubSezioniElencoNotaEnum.FormalitaSuccessive)
        //                    {
        //                        SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Commenti | SubSezioniElencoNotaEnum.FormalitaSuccessive;

        //                        if (RigaCorrente != "Non sono presenti formalità successive\0")
        //                        {
        //                            iStart = 0;
        //                            iStart = RigaCorrente.IndexOfAny(Utilities.CharsStartLine, iStart);
        //                            iStop = RigaCorrente.IndexOf('\0', iStart);
        //                            _TempSaveData.Commenti += RigaCorrente.Substring(iStart, iStop - iStart).Trim() + " ";
        //                        }
        //                        continue;
        //                    }
        //                    else                            
        //                    {
        //                        SubSezioneElencoNotaAttiva = SubSezioniElencoNotaEnum.Commenti;

        //                        iStart = 0;
        //                        //iStart = RigaCorrente.IndexOfAny(CharsStartLine, iStart);
        //                        iStop = RigaCorrente.IndexOf('\0', iStart);
        //                        _TempSaveData.Commenti += RigaCorrente.Substring(iStart, iStop - iStart).Trim() + " ";
        //                        continue;
        //                    }

        //                }                      
        //            }
        //            //Se arrivo qui disabilito il continuo sulla riga successiva, perchè già estratta
        //            ContinueInNextLine = false;
        //            ContinueInNextLineOBJ = null;
        //        } //Fine Sezione ElencoNota
        //    }            
        //}
        #endregion

        //private void btnDownload_Click(object sender, EventArgs e)
        //{
        //    WebClient webClient = new WebClient();
        //    webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
        //    webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
        //    webClient.DownloadFileAsync(new Uri("http://mysite.com/myfile.txt"), @"c:\myfile.txt");
        //
        //    //OPPURE
        //    webClient.DownloadFile("http://mysite.com/myfile.txt", @"c:\myfile.txt");
        //}

        //private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        //{
        //    progressBar.Value = e.ProgressPercentage;
        //}

        //private void Completed(object sender, AsyncCompletedEventArgs e)
        //{
        //    MessageBox.Show("Download completed!");
        //}
    }
}
