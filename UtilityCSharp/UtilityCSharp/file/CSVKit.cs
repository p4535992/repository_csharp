﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Common.Logging;
using Microsoft.VisualBasic.FileIO;
using UtilityCSharp.OFFICE;

namespace UtilityCSharp.file
{
    /// <summary>
    /// 2015-04-13
    /// </summary>
    class CsvKit
    {                   
        #region EXPORT Csv
        public class CsvExport
        {
            private static readonly ILog Log = LogManager.GetLogger<CsvExport>();
            private static string _strConnDb = "";

            public static void SetConnectionString(string filePath,string fileName)
            {
                ConnectString.Filepath = "";
                ConnectString.Filename = "";
                _strConnDb = ConnectString.GetConnectionString(filePath, fileName, ConnectionType.Csv);
            }

            
            
            public CsvExport(string separator, string encoding)
            {
                strFormat = separator;
                strEncoding = encoding;
            }

            private string strEncoding;
            private string strFormat;
            /*
             * Loads list of user tables from the SQL database, and fills
             * a ListBox control with tatble names.
             */

            private void LoadTables()
            {
                // Conncets to database, and selects the table names.            
                var cn = new SqlConnection(_strConnDb);
                var da = new SqlDataAdapter("select name from dbo.sysobjects where xtype = 'U' and name <> 'dtproperties' order by name", cn);
                var dt = new DataTable();
                // Fills the list to an DataTable.
                da.Fill(dt);
                // Clears the ListBox
                //this.lbxTables.Items.Clear();
                // Fills the table names to the ListBox.
                // Notifies user if there is no user table in the database yet.
                if (dt.Rows.Count == 0)
                {
                    //MessageBox.Show("There is no user table in the specified database. Import a Csv file first.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //this.lbxTables.Items.Add("< no user table in database >");
                    //this.btnExportToCSV.Enabled = false;
                }
                else
                {
                    //this.btnExportToCSV.Enabled = true               
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                        //this.lbxTables.Items.Add(dt.Rows[i][0].ToString());
                    //}
                    //this.lbxTables.SelectedIndex = 0;
                }
            }

            /*
             * Returns the specified Separator character
             */

            private string Separator
            {
                get
                {
                    if (strFormat.Length <= 2) return strFormat.Length == 1 ? strFormat : ";";
                    if (strFormat.ToLower().Contains("semicolon"))
                    {
                        return ";";
                    }
                    return strFormat.ToLower().Contains("tab") ? "\t" : ";";
                }
            }
         
            /*
             * Returnes the appropriate Encoding object.
             */

            private Encoding EncodingCsv
            {
                get
                {
                    if (strEncoding.ToLower().Contains("unicode"))
                    {
                        return Encoding.Unicode;
                    }
                    if (strEncoding.ToLower().Contains("ascii"))
                    {
                        return Encoding.ASCII;
                    }
                    if (strEncoding.ToLower().Contains("utf7"))
                    {
                        return Encoding.UTF7;
                    }
                    if (strEncoding.ToLower().Contains("utf8"))
                    {
                        return Encoding.UTF8;
                    }                    
                    return Encoding.Unicode;                   
                    // You can add other options, for ex.:
                    //return Encoding.GetEncoding("iso-8859-2");
                    //return Encoding.Default;
                }
            }        

            /*
             * Asks a filename and location from the user to export the data, and
             * runs the export operation.
             */

            private void ExportToCsv(string fileOut,string tableName,bool firstRowHeader)
            {

                //Asks the filenam with a SaveFileDialog control.

                var saveFileDialogCsv = new SaveFileDialog
                {
                    InitialDirectory = Application.ExecutablePath,
                    Filter = @"Csv files (*.csv)|*.csv|All files (*.*)|*.*",
                    FilterIndex = 1,
                    RestoreDirectory = true,
                    FileName = fileOut
                };

                if (saveFileDialogCsv.ShowDialog() == DialogResult.OK)
                {
                    // Runs the export operation if the given filename is valid.
                    ExportToCsVfile(saveFileDialogCsv.FileName, tableName, firstRowHeader);
                }
            }



            /*
             * Exports data to the Csv file.
             */
            private void ExportToCsVfile(string fileOut,string tableName,bool firstRowHeader)
            {
                // Connects to the database, and makes the select command.
                var conn = new SqlConnection(_strConnDb);
                var sqlQuery = "select * from " + tableName;
                var command = new SqlCommand(sqlQuery, conn);
                conn.Open();

                // Creates a SqlDataReader instance to read data from the table.
                var dr = command.ExecuteReader();
                // Retrives the schema of the table.
                var dtSchema = dr.GetSchemaTable();
                // Creates the Csv file as a stream, using the given encoding.
                var sw = new StreamWriter(fileOut, false, EncodingCsv);

                // Writes the column headers if the user previously asked that.
                if (firstRowHeader)
                {
                    sw.WriteLine(ColumnNames(dtSchema, Separator));
                }

                // Reads the rows one by one from the SqlDataReader
                // transfers them to a string with the given Separator character and
                // writes it to the file.
                while (dr.Read())
                {
                    var strRow = ""; // represents a full row
                    for (var i = 0; i < dr.FieldCount; i++)
                    {
                        strRow += dr.GetString(i);
                        if (i < dr.FieldCount - 1)
                        {
                            strRow += Separator;
                        }
                    }
                    sw.WriteLine(strRow);
                }


                // Closes the text stream and the database connenction.
                sw.Close();
                conn.Close();

                // Notifies the user.
                //MessageBox.Show("ready");
            }


            /*
             * Retrieves the header row from the schema table.
             */

            private static string ColumnNames(DataTable dtSchemaTable, string delimiter)
            {
                string strOut = "";
                if (delimiter.ToLower() == "tab")
                {
                    delimiter = "\t";
                }

                for (int i = 0; i < dtSchemaTable.Rows.Count; i++)
                {
                    strOut += dtSchemaTable.Rows[i][0].ToString();
                    if (i < dtSchemaTable.Rows.Count - 1)
                    {
                        strOut += delimiter;
                    }

                }
                return strOut;
            }
        }//end class CsvExport
        #endregion

        #region IMPORT Csv
        public class CsvImport
        {
            private static readonly ILog Log = LogManager.GetLogger<CsvImport>();
            private static string _strConnDb = "";

            public static void SetConnectionString(string filePath, string fileName)
            {
                ConnectString.Filepath = "";
                ConnectString.Filename = "";
                _strConnDb = ConnectString.GetConnectionString(filePath, fileName, ConnectionType.Csv);
            }

            public CsvImport(string separator,string encoding,string fullFileName,string directoryFile){
                _strFormat = separator;
                _strEncoding = encoding;
                //this.fileCSV = fullFileName;
                _dirCsv = directoryFile;
                _fileNevCsv = fullFileName;
            }

            public CsvImport()
            {
                // TODO: Complete member initialization
            }

            //private string fileCSV;		//full file name
            private string _dirCsv;		//directory of file to import
            private string _fileNevCsv;	//name (with extension) of file to import - field

            public string FileNevCsv	//name (with extension) of file to import - property
            {
                get { return _fileNevCsv; }
                set { _fileNevCsv = value; }
            }

            private string _strFormat;	//Csv separator character
            private string _strEncoding; //Encoding of Csv file

            private long _rowCount;	//row number of source file

            private DataSet _ds = new DataSet();

            // Browses file with OpenFileDialog control

            public string LoadCsvFile(string txtFileToImport)
            {
                var openFileDialogCsv = new OpenFileDialog
                {
                    InitialDirectory = Application.ExecutablePath,
                    Filter = @"Csv files (*.csv)|*.csv|All files (*.*)|*.*",
                    FilterIndex = 1,
                    RestoreDirectory = true
                };


                if (openFileDialogCsv.ShowDialog() == DialogResult.OK)
                {
                    txtFileToImport = openFileDialogCsv.FileName; //TextBox
                }
                return txtFileToImport;

            }



            // Delimiter character selection
            //semicolon,tab.other
            //ansi,unicode,oem
            private void Format()
            {
                try
                {
                    if (_strFormat.Length > 2)
                    {
                        if (_strFormat.ToLower().Contains("semicolon"))
                        {_strFormat = "Delimited(;)";}
                        else if (_strFormat.ToLower().Contains("tab"))
                        {_strFormat = "TabDelimited";}
                        else if (_strFormat.ToLower().Contains("other"))
                        {_strFormat = "Delimited(" + _strFormat.Trim() + ")";}
                        else
                        {_strFormat = "Delimited(;)"; }
                    }
                    else
                    {_strFormat = "Delimited(" + _strFormat.Trim() + ")";}
                }
                catch (Exception)
                {
                    //MessageBox.Show(ex.Message, "Format");
                }              
            }


            // Encoding selection
            private void Encoding()
            {
                try
                {
                    if (_strEncoding.ToLower().Contains("ansi"))
                    {
                        _strEncoding = "ANSI";
                    }
                    else if (_strEncoding.ToLower().Contains("unicode"))
                    {
                        _strEncoding = "Unicode";
                    }
                    else if (_strEncoding.ToLower().Contains("oem"))
                    {
                        _strEncoding = "OEM";
                    }
                    else
                    {
                        _strEncoding = "ANSI";
                    }
                }
                catch (Exception)
                {
                    //MessageBox.Show(ex.Message, "Encoding");
                }                
            }



            /* Schema.ini File (Text File Driver)

            When the Text driver is used, the format of the text file is determined by using a
            schema information file. The schema information file, which is always named Schema.ini
            and always kept in the same directory as the text data source, provides the IISAM 
            with information about the general format of the file, the column name and data type
            information, and a number of other data characteristics*/

            private void WriteSchema()
            {
                try
                {
                    var fsOutput = new FileStream(_dirCsv + "\\schema.ini", FileMode.Create, FileAccess.Write);
                    var srOutput = new StreamWriter(fsOutput);
                   
                    var s1 = "[" + FileNevCsv + "]";
                    var s2 = "ColNameHeader=" + true.ToString();
                    var s3 = "Format=" + _strFormat;
                    const string s4 = "MaxScanRows=25";
                    var s5 = "CharacterSet=" + _strEncoding;

                    srOutput.WriteLine(s1 + "\r\n" + s2 + "\r\n" + s3 + "\r\n" + s4 + "\r\n" + s5);
                    srOutput.Close();
                    fsOutput.Close();
                }
                catch (Exception)
                {
                    //MessageBox.Show(ex.Message, "WriteSchema");
                }                
            }

            /*
             * Loads the csv file into a DataSet.
             * 
             * If the numberOfRows parameter is -1, it loads oll rows, otherwise it
             * loads the first specified number of rows (for preview)
             */
            public DataSet LoadCsv(int numberOfRows)
            {
                //DataSet _ds = new DataSet();
                try
                {                    
                    // Creates and opens an ODBC connection                    
                    //string strConnString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + this._dirCsv.Trim() + ";Extensions=asc,csv,tab,txt;Persist Security Info=False";
                    string sqlSelect;                    
                    var conn = new OdbcConnection(_strConnDb.Trim());
                    conn.Open();
                    //Creates the select command text
                    if (numberOfRows == -1)
                    {
                        sqlSelect = "select * from [" + FileNevCsv.Trim() + "]";
                    }
                    else
                    {
                        sqlSelect = "select top " + numberOfRows + " * from [" + FileNevCsv.Trim() + "]";
                    }
                    //Creates the data adapter
                    var objOledbDa = new OdbcDataAdapter(sqlSelect, conn);
                    //Fills dataset with the records from Csv file
                    objOledbDa.Fill(_ds, _fileNevCsv);
                    //closes the connection
                    conn.Close();
                }
                catch (Exception) //Error
                {
                    //MessageBox.Show(e.Message, "Error - LoadCsv", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return _ds;
            }

            // Checks if a file was given.
            private bool FileCheck()
            {
                return !string.IsNullOrEmpty(FileNevCsv);
            }

            /* Loads the preview of Csv file in the DataGridView control.
             */

            private DataSet LoadPreview()
            {
                //DataSet _ds = new DataSet();
                try
                {
                    // select format, encoding, an write the schema file
                    Format();
                    Encoding();
                    WriteSchema();

                    // loads the first 500 rows from Csv file, and fills the
                    // DataGridView control.
                    
                    _ds = LoadCsv(500);

                    //this.dataGridView_preView.DataMember = "csv";
                }
                catch (Exception)
                {
                    //MessageBox.Show(e.Message, "Error - LoadPreview", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return _ds;
            }
     
            /*
             * Loads the Csv file to a dataset, and imports data
             * to the database with SqlBulkCopy.
             */

            public void SaveToDatabase_withDataSet(string databaseName,string tableName,bool createTable)
            {
                //DataSet _ds = new DataSet();
                try
                {
                    if (!FileCheck()) return;
                    // select format, encoding, and write the schema file
                    Format();
                    Encoding();
                    WriteSchema();

                    // loads all rows from from csv file
                    var ds = LoadCsv(-1); //a DataSet

                    // gets the number of rows
                    _rowCount = ds.Tables[0].Rows.Count;

                    // Makes a DataTableReader, which reads data from data set.
                    // It is nececery for bulk copy operation.
                    var dtr = ds.Tables[0].CreateDataReader();

                    // Creates schema table. It gives column names for create table command.
                    var dt = dtr.GetSchemaTable();

                    // You can view that schema table if you want:
                    //this.dataGridView_preView.DataSource = dt;

                    // Creates a new and empty table in the sql database

                    if (createTable)
                    {
                        CreateTableInDatabase(dt, databaseName, tableName, _strConnDb);
                    }


                    // Copies all rows to the database from the dataset.
                    using (var bc = new SqlBulkCopy(_strConnDb))
                    {
                        // Destination table with owner - this example doesn't
                        // check the owner and table names!
                        bc.DestinationTableName = "[" + databaseName + "].[" + tableName + "]";

                        // User notification with the SqlRowsCopied event
                        //bc.NotifyAfter = 100;
                        //bc.SqlRowsCopied += new SqlRowsCopiedEventHandler(OnSqlRowsCopied);

                        // Starts the bulk copy.
                        bc.WriteToServer(ds.Tables[0]);

                        // Closes the SqlBulkCopy instance
                        bc.Close();
                    }

                    // Writes the number of imported rows to the form
                    //var lblProgress = "Imported: " + _rowCount + "/" + _rowCount + " row(s)";
                    //this.lblProgress.Refresh();

                    // Notifies user
                    //MessageBox.Show("ready");
                }
                catch (Exception)
                {
                    //MessageBox.Show(e.Message, "Error - SaveToDatabase_withDataSet", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //return _ds;
            }
       
            /*
             * Imports data to the database with SqlBulkCopy.
             * This method doesn't use a temporary dataset, it loads
             * data immediately from the ODBC connection
             */
            public void SaveToDatabaseDirectly(string databaseName,string tableName,bool createTable)
            {             
                try
                {
                    if (!FileCheck()) return;
                    // select format, encoding, and write the schema file
                    Format();
                    Encoding();
                    WriteSchema();
                    CountRowCsv();
                    //int count =(int) CountRowCsv();
                        
                    /*
                        // Creates the ODBC command
                        sql_select = "select * from [" + this.FileNevCsv.Trim() + "]";
                        OdbcCommand commandSourceData = new OdbcCommand(sql_select, conn);

                        
                        // Makes on OdbcDataReader for reading data from Csv
                        OdbcDataReader dataReader = commandSourceData.ExecuteReader();

                        // Creates schema table. It gives column names for create table command.
                        DataTable dt;
                        dt = dataReader.GetSchemaTable();
                        */
                    // You can view that schema table if you want:
                    //this.dataGridView_preView.DataSource = dt;
                    //strConnString = "server=(local);database=Test_CSV_impex;Trusted_Connection=True";
                    // Creates a new and empty table in the sql database                   
                    /*
                        if (createTable)
                        {

                            CreateTableInDatabase(dt, databaseName, tableName, strConnString);
                        }
                            
                        // Copies all rows to the database from the data reader.
                        //string conn = "server=(local);database=Test_CSV_impex;Trusted_Connection=True";
                        //string conn = connectionString;
                        */

                    //METODO INSRIMENTO RECORD SU Server SQL CON SQLBULKCOPY
                    /*
                        using (SqlBulkCopy bc = new SqlBulkCopy(connectionString))
                        {
                            // Destination table with owner - this example doesn't
                            // check the owner and table names!
                            bc.DestinationTableName = "[" + databaseName + "].[" + tableName + "]";

                            // User notification with the SqlRowsCopied event
                            //bc.NotifyAfter = 100;
                            //bc.SqlRowsCopied += new SqlRowsCopiedEventHandler(OnSqlRowsCopied);

                            // Starts the bulk copy.
                            bc.WriteToServer(dataReader);

                            // Closes the SqlBulkCopy instance
                            bc.Close();
                        } 
                            */                     
                                         
                    //METODO INSERIMENTO RECORD SU Server SQL CON SQLCONNECTION
                    var dt = ConvertCsvToDataTable(_fileNevCsv, _strFormat, (int)_rowCount);

                    //Guid g = Guid.NewGuid();
                    var g = Guid.NewGuid();
                    for (var j = 0; j < dt.Rows.Count; j++)
                    {                      
                        //INSERIAMO I BILANCI (NO IMPORTI) JUST ONE TIME FOR FILE
                        if (j == 0)
                        {
                            InsertDataTableBilanciInDatabase(dt, databaseName, tableName, g, j);
                        }

                        //INSERIAMO I BILANCI (CON GLI IMPORTI)

                        InsertDataTableBilanciAndImportInDatabase(dt, databaseName, tableName, g,j);
                    }
                    // Writes the number of imported rows to the form
                    //string lblProgres = "Imported: " + this._rowCount.ToString() + "/" + this._rowCount.ToString() + " row(s)";
                       
                    // Notifies user
                    //MessageBox.Show("ready");
                }
                catch (Exception)
                {
                    //MessageBox.Show(e.Message, "Error - SaveToDatabaseDirectly", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            public long CountRowCsv()
            {
                // Creates and opens an ODBC connection
                var strConnString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + _dirCsv.Trim() + ";Extensions=asc,csv,tab,txt;Persist Security Info=False";
                //string strConnString = connectionString;
                var sqlSelect = "SELECT COUNT(*) FROM [" + FileNevCsv.Trim() + "]" ;
                var conn = new OdbcConnection(strConnString.Trim());
                conn.Open();

                //Counts the row number in csv file - with an sql query
                //OdbcCommand commandRowCount = new OdbcCommand("SELECT COUNT(*) FROM [" + this.FileNevCsv.Trim() + "]", conn);
                var commandRowCount = new OdbcCommand(sqlSelect, conn);
                _rowCount = Convert.ToInt32(commandRowCount.ExecuteScalar());
                return _rowCount;
            }

            /*
             * Generates the create table command using the schema table, and
             * runs it in the sql database.
             */

            private static bool CreateTableInDatabase(DataTable dtSchemaTable, string tableOwner, string tableName, string connectionString)
            {
                try
                {
                    // Generates the create table command.
                    // The first column of schema table contains the column names.
                    // The data type is nvarcher(4000) in all columns.

                    var ctStr = "CREATE TABLE [" + tableOwner + "].[" + tableName + "](\r\n";
                    for (var i = 0; i < dtSchemaTable.Rows.Count; i++)
                    {
                        ctStr += "  [" + dtSchemaTable.Rows[i][0] + "] [nvarchar](4000) NULL";
                        if (i < dtSchemaTable.Rows.Count)
                        {
                            ctStr += ",";
                        }
                        ctStr += "\r\n";
                    }
                    ctStr += ")";

                    // You can check the sql statement if you want:
                    //MessageBox.Show(ctStr);


                    // Runs the sql command to make the destination table.
                    
                    SqlConnection conn = new SqlConnection(connectionString);
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = ctStr;
                    conn.Open();
                    command.ExecuteNonQuery();
                    command.Dispose();
                    conn.Close();
                    return true;
                }
                catch (Exception)
                {
                    //.Show(e.Message, "CreateTableInDatabase", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            private static bool InsertDataTableBilanciInDatabase(DataTable dt, string databaseName, string tableName,Guid g,int j)
            {
                //Sql = string.Concat("update [PA].dbo.[ListaDownload] set stato = 'U' where uid = '", dtRowLista["uid"].ToString(), "'");
                //oCmd = new SqlCommand(Sql, _ConnDB);
                //oCmd.ExecuteNonQuery();
                //oCmd.Dispose();
                var dtB = dt.DefaultView.ToTable(false, "codiceEnte", "periodicita");
                dtB.Columns["codiceEnte"].ColumnName = "codamm";
                dtB.Columns["periodicita"].ColumnName = "databil";
                dtB.Columns.Add("comparto");
                dtB.Columns.Add("uid").SetOrdinal(0);
                try
                {
                    var values = new List<string>();
                    for (var i = 0; i < dtB.Columns.Count; i++) { values.Add("@" + dtB.Columns[i].ColumnName); }
                    
                    using (var connection = new SqlConnection(_strConnDb))
                    {
                        connection.Open();

                        //******************************************************************************************
                    //SET SOME VALUE                                                     
                    var  sanno = Regex.Replace(dtB.Rows[j]["databil"].ToString(),"[^\\d]","").Trim();
                    var anno = Convert.ToInt32(sanno.Trim());
                    const string comparto = "";
                            
                    var datetime = new DateTime(anno,1,1);
                    //datetime.Year = 2012;
                    //******************************************************************************************
                    //BEFORE INSERT DELETE THE RECORD IF ALREADY EXISTS
                    var ctStr = "DELETE FROM [" + databaseName + "].dbo.[" + tableName + "] WHERE " +
                                   "codamm = '" + dtB.Rows[j]["codamm"].ToString().Replace("'", "''") + "'" +
                                   " AND " +
                                   "databil = '" + anno.ToString().Replace("'", "''") + "'" +
                                   " AND " +
                                   "comparto = '" + comparto.Replace("'", "''") + "'";
                    //*******************************************************************************************                                                     
                    //INSERT INTO THE Database THE RECORD
               
                    var command = new SqlCommand(ctStr, connection);
                    command.ExecuteNonQuery();
                    command.Dispose();

                    command = new SqlCommand(null, connection);
                    ctStr = "INSERT INTO [" + databaseName + "].[dbo].[" + tableName + "](\r\n";
                    for (var i = 0; i < dtB.Columns.Count; i++)
                    {
                        ctStr += " " + dtB.Columns[i].ColumnName + " ";
                        if (i < dtB.Columns.Count-1)
                        {
                            ctStr += ",";
                        }
                        ctStr += "\r\n";
                    }
                    ctStr += ")";
                    ctStr += " VALUES (\r\n";                    
                    for (var i = 0; i < dtB.Columns.Count; i++)
                    {                            
                        ctStr += "  @" + dtB.Columns[i].ColumnName;
                        //values.Add("@" + dtB.Columns[i].ColumnName.ToString());
                        if (i < dtB.Columns.Count-1)
                        {
                            ctStr += ",";
                        }
                        ctStr += "\r\n";        
                    }
                    ctStr += "\r\n";                  
                    ctStr += ")";

                    //SqlCommand cmd = new SqlCommand(ctStr);
                    //cmd.CommandType = CommandType.Text;
                    //cmd.Connection = connection;
                    //cmd.Parameters.AddWithValue("@uid", txtUid.text);
                    //cmd.Parameters.AddWithValue("@codamm", txtPhone.Text);
                    //cmd.Parameters.AddWithValue("@databil", txtAddress.Text);
                    //cmd.Parameters.AddWithValue("@comparto", txtAddress.Text);
                    //cmd.ExecuteNonQuery();                      
                    //SqlParameter uidParam = new SqlParameter("@uid", SqlDbType.UniqueIdentifier);
                    //SqlParameter codammParam = new SqlParameter("@codamm", SqlDbType.VarChar,20);
                    //SqlParameter databilParam = new SqlParameter("@databil", SqlDbType.DateTime);
                    //SqlParameter compartoParam = new SqlParameter("@comparto", SqlDbType.VarChar,2);

                    //Set the type of the element you want to insert
                    SqlParameter uidParam = new SqlParameter(values[0], SqlDbType.UniqueIdentifier);
                    SqlParameter codammParam = new SqlParameter(values[1], SqlDbType.VarChar, 20);
                    SqlParameter databilParam = new SqlParameter(values[2], SqlDbType.DateTime);
                    SqlParameter compartoParam = new SqlParameter(values[3], SqlDbType.VarChar, 2);

                    //Add your param to thwe command
                    command.Parameters.Add(uidParam);
                    command.Parameters.Add(codammParam);
                    command.Parameters.Add(databilParam);
                    command.Parameters.Add(compartoParam);

                    // Call Prepare after setting the Commandtext and Parameters.
                    command.CommandText = ctStr;
                    command.Prepare();
                    //command.ExecuteNonQuery();

                    // Change parameter values and call ExecuteNonQuery.
                          
                    //command.Parameters[0].Value = "NEWID()";
                    command.Parameters[0].Value = g;
                    command.Parameters[1].Value = dtB.Rows[j]["codamm"].ToString().Replace("'", "''");
                    //p = Regex.Replace(dt.Rows[j]["databil"].ToString(), "[^\\d]", "").Trim().Replace("'", "''");
                    command.Parameters[2].Value = datetime;
                                
                    command.Parameters[3].Value = comparto;
                        
                    //string test = command.CommandText;
                    command.ExecuteNonQuery();
                    command.Dispose();
                                              
                    connection.Close();
                    }//using  
                    values.Clear();
                    return true;

                }
                catch (Exception)
                {
                    //.Show(e.Message, "CreateTableInDatabase", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            private bool InsertDataTableBilanciAndImportInDatabase(DataTable dt, string databaseName, string tableName, Guid g,int j)
            {
                DataTable dtB = dt.DefaultView.ToTable(false, "codiceGestionale",
                    "descrizioneCodiceGestionale", "importoEnte", "importoCumulatoEnte"); //rename the column
                //SET THE CODE FOR THE PROSPECT
                             
                dtB.Columns["codiceGestionale"].ColumnName = "codice";
                dtB.Columns["descrizioneCodiceGestionale"].ColumnName = "descrizione";
                dtB.Columns["importoEnte"].ColumnName = "importoperiodo";
                dtB.Columns["importoCumulatoEnte"].ColumnName = "importocumulato";
               
                dtB.Columns.Add("uid").SetOrdinal(0);
                dtB.Columns.Add("uidBilanci").SetOrdinal(1);
                dtB.Columns.Add("codprospetto").SetOrdinal(2);
             
                try
                {
                    string ctStr;
                    var values = new List<string>();
                    for (var i = 0; i < dtB.Columns.Count; i++) { values.Add("@" + dtB.Columns[i].ColumnName); }                    
                    using (var connection = new SqlConnection(_strConnDb))
                    {
                        connection.Open();
                        const string codProspetto = "";
                        //Guid uidBilanci = new Guid();   
                        //******************************************************************************************
                        //BEFORE INSERT DELETE THE RECORD IF ALREADY EXISTS
                        ctStr = "DELETE FROM [" + databaseName + "].dbo.[" + tableName + "] WHERE " +
                                            "codice = '" + dtB.Rows[j]["codice"].ToString().Replace("'", "''") + "'" +
                                            " AND " +
                                            "codprospetto = '" + codProspetto.Replace("'", "''") + "'" +
                                            " AND " +
                                            "importoperiodo = '" + dtB.Rows[j]["importoperiodo"].ToString().Replace("'", "''") + "'"+
                                            " AND " +
                                            "importocumulato = '" + dtB.Rows[j]["importocumulato"].ToString().Replace("'", "''") + "'"
                                            ;
                        //*******************************************************************************************                                                     
                        //INSERT INTO THE Database THE RECORD

                        var command = new SqlCommand(ctStr, connection);
                        command.ExecuteNonQuery();
                        command.Dispose();

                        command = new SqlCommand(null, connection);
                        ctStr = "INSERT INTO [" + databaseName + "].[dbo].[" + tableName + "](\r\n";
                        for (var i = 0; i < dtB.Columns.Count; i++)
                        {
                            ctStr += " " + dtB.Columns[i].ColumnName + " ";
                            if (i < dtB.Columns.Count - 1)
                            {
                                ctStr += ",";
                            }
                            ctStr += "\r\n";
                        }
                        ctStr += ")";
                        ctStr += " VALUES (\r\n";
                        for (var i = 0; i < dtB.Columns.Count; i++)
                        {
                            ctStr += "  @" + dtB.Columns[i].ColumnName;
                            values.Add("@" + dtB.Columns[i].ColumnName);
                            if (i < dtB.Columns.Count - 1)
                            {
                                ctStr += ",";
                            }
                            ctStr += "\r\n";
                        }
                        ctStr += "\r\n";
                        ctStr += ")";
                           
                        //Set the type of the element you want to insert
                        var uidParam = new SqlParameter(values[0], SqlDbType.UniqueIdentifier);
                        var uidBilanciParam = new SqlParameter(values[1], SqlDbType.UniqueIdentifier);
                        var codProspParam = new SqlParameter(values[2], SqlDbType.VarChar, 5);
                        var codGestParam = new SqlParameter(values[3], SqlDbType.VarChar, 4);
                        var descrParam = new SqlParameter(values[4], SqlDbType.VarChar, 50);
                            
                        var importoperiodoParam = new SqlParameter(values[5], SqlDbType.Decimal, 18);

                        var importcumulatoParam = new SqlParameter(values[6], SqlDbType.Decimal, 18);
                        
                        //Add your param to thwe command
                        command.Parameters.Add(uidParam);
                        command.Parameters.Add(uidBilanciParam);
                        command.Parameters.Add(codProspParam);
                        command.Parameters.Add(codGestParam);
                        command.Parameters.Add(descrParam);
                        command.Parameters.Add(importoperiodoParam);
                        command.Parameters.Add(importcumulatoParam);

                        // Call Prepare after setting the Commandtext and Parameters.
                        command.CommandText = ctStr;
                        command.Prepare();
                        //command.ExecuteNonQuery();

                        // Change parameter values and call ExecuteNonQuery.

                        //command.Parameters[0].Value = "NEWID()";
                        command.Parameters[0].Value = Guid.NewGuid();
                        command.Parameters[1].Value = g;
                        command.Parameters[2].Value = codProspetto;
                        command.Parameters[3].Value = dtB.Rows[j]["codice"].ToString().Replace("'", "''");
                        command.Parameters[4].Value = dtB.Rows[j]["descrizione"].ToString().Replace("'", "''");
                        command.Parameters[5].Value = dtB.Rows[j]["importoperiodo"].ToString().Replace("'", "''");
                        command.Parameters[5].Precision = 18;
                        command.Parameters[5].Scale = 2;
                        command.Parameters[6].Value = dtB.Rows[j]["importocumulato"].ToString().Replace("'", "''");
                        command.Parameters[6].Precision = 18;
                        command.Parameters[6].Scale = 2;

                        //string test = command.CommandText;
                        command.ExecuteNonQuery();
                        command.Dispose();
                        connection.Close();
                    }//using               
                    return true;
                }
                catch (Exception)
                {
                    //.Show(e.Message, "CreateTableInDatabase", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            private bool InsertDataTableBilanciInDatabase(string[] line, string databaseName, string tableName, Guid g,string[] columns)
            {                
                try
                {
                    var values = columns.Select(t => "@" + t).ToList();
                    //for (int i = 0; i < columns.Length; i++) { values.Add("@" + columns[i]); }
                    using (var connection = new SqlConnection(_strConnDb))
                    {
                        connection.Open();
                        var sanno = Regex.Replace(line[6], "[^\\d]", "").Trim();
                        var anno = Convert.ToInt32(sanno.Trim());
                        const string comparto = "";
                        var datetime = new DateTime(anno, 1, 1);                     
                        var ctStr = "DELETE FROM [" + databaseName + "].dbo.[" + tableName + "] WHERE " +
                                       "codamm = '" + line[0].Replace("'", "''") + "'" +
                                       " AND " +
                                       "databil = '" + anno.ToString().Replace("'", "''") + "'" +
                                       " AND " +
                                       "comparto = '" + comparto.Replace("'", "''") + "'";
                        //*******************************************************************************************                                                     
                        //INSERT INTO THE Database THE RECORD

                        var command = new SqlCommand(ctStr, connection);
                        command.ExecuteNonQuery();
                        command.Dispose();

                        command = new SqlCommand(null, connection);
                        ctStr = "INSERT INTO [" + databaseName + "].[dbo].[" + tableName + "](\r\n";
                        for (var i = 0; i < columns.Length; i++)
                        {
                            ctStr += " " + columns[i] + " ";
                            if (i < columns.Length - 1)
                            {
                                ctStr += ",";
                            }
                            ctStr += "\r\n";
                        }
                        ctStr += ")";
                        ctStr += " VALUES (\r\n";
                        for (var i = 0; i < columns.Length; i++)
                        {
                            ctStr += "  @" + columns[i];
                            //values.Add("@" + dtB.Columns[i].ColumnName.ToString());
                            if (i < columns.Length - 1)
                            {
                                ctStr += ",";
                            }
                            ctStr += "\r\n";
                        }
                        ctStr += "\r\n";
                        ctStr += ")";

                        //Set the type of the element you want to insert
                        var uidParam = new SqlParameter(values[0], SqlDbType.UniqueIdentifier);
                        var codammParam = new SqlParameter(values[1], SqlDbType.VarChar, 20);
                        var databilParam = new SqlParameter(values[2], SqlDbType.DateTime);
                        var compartoParam = new SqlParameter(values[3], SqlDbType.VarChar, 2);

                        //Add your param to thwe command
                        command.Parameters.Add(uidParam);
                        command.Parameters.Add(codammParam);
                        command.Parameters.Add(databilParam);
                        command.Parameters.Add(compartoParam);

                        // Call Prepare after setting the Commandtext and Parameters.
                        command.CommandText = ctStr;
                        command.Prepare();
                        //command.ExecuteNonQuery();

                        // Change parameter values and call ExecuteNonQuery.

                        //command.Parameters[0].Value = "NEWID()";
                        command.Parameters[0].Value = g;
                        command.Parameters[1].Value = line[0].Replace("'", "''");
                        //p = Regex.Replace(dt.Rows[j]["databil"].ToString(), "[^\\d]", "").Trim().Replace("'", "''");
                        command.Parameters[2].Value = datetime;

                        command.Parameters[3].Value = comparto;

                        //string test = command.CommandText;
                        command.ExecuteNonQuery();
                        command.Dispose();

                        //}//foreach rows
                        connection.Close();
                    }//using  
                    values.Clear();
                    return true;
                }
                catch (Exception)
                {
                    //.Show(e.Message, "CreateTableInDatabase", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            private bool InsertDataTableBilanciAndImportInDatabase(string[] line, string databaseName, string tableName, Guid g, string[] columns)
            {            
                try
                {
                    var values = columns.Select(t => "@" + t).ToList();
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //TABELLA 2 - db0.SIOPE_ListaAmministrazioni_Bilanci
                    //uid	uniqueidentifier	Unchecked
                    //uidBilanci	uniqueidentifier	Unchecked
                    //codprospetto	varchar(5)	Unchecked
                    //codice	varchar(4)	Unchecked
                    //descrizione	varchar(50)	Unchecked
                    //importoperiodo	numeric(18, 2)	Unchecked
                    //importocumulato	numeric(18, 2)	Unchecked                    
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    using (var connection = new SqlConnection(_strConnDb))
                    {
                        connection.Open();
                        //******************************************************************************************
                        //SET SOME VALUE       
                        const string codProspetto = "";              
                        //******************************************************************************************
                        //BEFORE INSERT DELETE THE RECORD IF ALREADY EXISTS
                        var ctStr = "DELETE FROM [" + databaseName + "].dbo.[" + tableName + "] WHERE " +
                                       "codice = '" + line[3].Replace("'", "''") + "'" +
                                       " AND " +
                                       "codprospetto = '" + codProspetto.Replace("'", "''") + "'" +
                                       " AND " +
                                       "importoperiodo = '" + line[5].Replace("'", "''") + "'" +
                                       " AND " +
                                       "importocumulato = '" + line[6].Replace("'", "''") + "'";
                        //*******************************************************************************************                                                     
                        //INSERT INTO THE Database THE RECORD

                        var command = new SqlCommand(ctStr, connection);
                        command.ExecuteNonQuery();
                        command.Dispose();
                     
                        command = new SqlCommand(null, connection);
                        ctStr = "INSERT INTO [" + databaseName + "].[dbo].[" + tableName + "](\r\n";
                        for (var i = 0; i < columns.Length; i++)
                        {
                            ctStr += " " + columns[i] + " ";
                            if (i < columns.Length - 1)
                            {
                                ctStr += ",";
                            }
                            ctStr += "\r\n";
                        }
                        ctStr += ")";
                        ctStr += " VALUES (\r\n";
                        for (var i = 0; i < columns.Length; i++)
                        {
                            ctStr += "  @" + columns[i];                        
                            if (i < columns.Length - 1)
                            {
                                ctStr += ",";
                            }
                            ctStr += "\r\n";
                        }
                        ctStr += "\r\n";
                        ctStr += ")";

                        //Set the type of the element you want to insert
                        var uidParam = new SqlParameter(values[0], SqlDbType.UniqueIdentifier);
                        var uidBilanciParam = new SqlParameter(values[1], SqlDbType.UniqueIdentifier);
                        var codProspParam = new SqlParameter(values[2], SqlDbType.VarChar, 5);
                        var codGestParam = new SqlParameter(values[3], SqlDbType.VarChar, 4);
                        var descrParam = new SqlParameter(values[4], SqlDbType.VarChar, 50);

                        var importoperiodoParam = new SqlParameter(values[5], SqlDbType.Decimal, 18);

                        var importcumulatoParam = new SqlParameter(values[6], SqlDbType.Decimal, 18);

                        //Add your param to thwe command
                        command.Parameters.Add(uidParam);
                        command.Parameters.Add(uidBilanciParam);
                        command.Parameters.Add(codProspParam);
                        command.Parameters.Add(codGestParam);
                        command.Parameters.Add(descrParam);
                        command.Parameters.Add(importoperiodoParam);
                        command.Parameters.Add(importcumulatoParam);

                        // Call Prepare after setting the Commandtext and Parameters.
                        command.CommandText = ctStr;
                        command.Prepare();
                        //command.ExecuteNonQuery();

                        // Change parameter values and call ExecuteNonQuery.

                        //command.Parameters[0].Value = "NEWID()";
                        command.Parameters[0].Value = Guid.NewGuid();
                        command.Parameters[1].Value = g;
                        command.Parameters[2].Value = codProspetto;
                        command.Parameters[3].Value = line[3].Replace("'", "''");
                        command.Parameters[4].Value = line[4].Replace("'", "''");
                        command.Parameters[5].Value = line[5].Replace("'", "''");
                        command.Parameters[5].Precision = 18;
                        command.Parameters[5].Scale = 2;
                        command.Parameters[6].Value = line[6].Replace("'", "''");
                        command.Parameters[6].Precision = 18;
                        command.Parameters[6].Scale = 2;

                        //string test = command.CommandText;
                        command.ExecuteNonQuery();
                        command.Dispose();
                        //}//foreach rows
                        connection.Close();
                    }//using               
                    return true;

                }
                catch (Exception)
                {
                    //.Show(e.Message, "CreateTableInDatabase", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            /*
             * Makes file and directory information, and suggests a table name
             */

            private string SuggestTableName()
            {
                // full file name
                _fileNevCsv = "";
                // creates a System.IO.FileInfo object to retrive information from selected file.
                var fi = new FileInfo(_fileNevCsv);
                // retrives directory
                _dirCsv = fi.DirectoryName;
                // retrives file name with extension
                FileNevCsv = fi.Name;
                // database table name
                return fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length).Replace(" ", "_");
                //return TableName;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="pathFileName"></param>
            /// <param name="separator"></param>
            /// <param name="batchSize"></param>
            /// <returns></returns>
            public DataTable ConvertCsvToDataTable(string pathFileName, string separator, int batchSize)
            {
                var createdCount = 0;
                var dataTable = new DataTable();
                //string path = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;
                //var fileName = @"C:\Path\To\File.csv";                              
                using (var textFieldParser = new TextFieldParser(pathFileName))
                {
                    textFieldParser.TextFieldType = FieldType.Delimited;
                    if (separator.Length > 1) {
                        separator = separator.Replace("Delimited(", "").Replace(")", "");
                    }
                    //textFieldParser.Delimiters = new[] { "," };
                    textFieldParser.Delimiters = new[] { separator };
                    textFieldParser.HasFieldsEnclosedInQuotes = true;
                    // Loop through the Csv and load each set of 100,000 records into a DataTable
                    // Then send it to the LiveTable                   
                    while (!textFieldParser.EndOfData)
                    {
                        
                        if (textFieldParser.LineNumber ==1)
                        {
                            var columns = textFieldParser.ReadFields();
                            if (columns != null)
                            {
                                foreach (var c in columns)
                                {
                                    dataTable.Columns.Add(c);
                                }
                            }
                        }
                        //string[] test = textFieldParser.ReadFields();
                        string[] fields = textFieldParser.ReadFields();
                        if (fields != null)
                        {
                            dataTable.Rows.Add(new object[] {fields});
                            createdCount++;
                        }

                        if (createdCount % _rowCount == 0)
                        {
                            break; //Just if you want one test
                        }
                    }
                }//using textFieldParser
                return dataTable;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="pathFileName"></param>
            /// <param name="separator"></param>
            public void ReadCsvAndLoadToSqlServer(string pathFileName, string separator) 
            {
                //var counter = 0;
                //List<string> values = new List<string>();
                // Read the file and display it line by line.
                using (var connection = new SqlConnection(_strConnDb))
                {
                    connection.Open();                     
                        using (var file = new StreamReader(pathFileName))
                        {
                            var g = new Guid();
                            var firstRow = true;
                            var setRecordBilanci = true;
                            string row;
                            while ((row = file.ReadLine()) != null)
                            {
                                //JUMP TH FIRST LINE
                                if (firstRow)
                                {
                                    //row = file.ReadLine();
                                    firstRow = false;
                                }
                                else
                                {                                  
                                    string[] line = row.Split(separator.ToCharArray()[0]);
                                    //INSERIAMO I BILANCI (NO IMPORTI) JUST ONE TIME FOR FILE
                                    string[] columns;
                                    SqlCommand command;
                                    string ctStr;
                                    if (setRecordBilanci)
                                    {
                                        g = Guid.NewGuid();
                                        //InsertDataTableBilanciInDatabase(values, "PA", "SIOPE_ListaAmministrazioni_Bilanci", g, columns);
                                        columns = new[] { "uid", "codamm", "databil", "comparto" };                                      
                                        setRecordBilanci = false;                                                                         
                                        //for (int i = 0; i < columns.Length; i++) { values.Add("@" + columns[i]); }
                                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        //LINE: codiceEnte,descEnte,categoria,sottoCategoria,prospetto,tipoReport,periodicita,codiceGestionale,descrizioneCodiceGestionale,importoEnte,importoCumulatoEnte
                                        //TABELLA 1 - db0.SIOPE_ListaAmministrazioni_Bilanci
                                        //uid	uniqueidentifier	Unchecked
                                        //codamm	varchar(20)	Unchecked
                                        //databil	datetime	Unchecked
                                        //comparto	varchar(2)	Unchecked                                     
                                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////                                                                                                          
                                        //******************************************************************************************
                                        //SET SOME VALUE                                                     
                                        var sanno = Regex.Replace(line[6], "[^\\d]", "").Trim();
                                        var anno = Convert.ToInt32(sanno.Trim());
                                        var comparto = "";
                                        var datetime = new DateTime(anno, 1, 1);
                                        //******************************************************************************************
                                        //BEFORE INSERT DELETE THE RECORD IF ALREADY EXISTS
                                        ctStr = "DELETE FROM [PA].dbo.[SIOPE_ListaAmministrazioni_Bilanci] WHERE " +
                                                            "codamm = '" + line[0].Replace("'", "''") + "'" +
                                                            " AND " +
                                                            "databil = '" + anno.ToString().Replace("'", "''") + "'" +
                                                            " AND " +
                                                            "comparto = '" + comparto.Replace("'", "''") + "'"
                                                            ;
                                        //*******************************************************************************************                                                     
                                        //INSERT INTO THE Database THE RECORD

                                        command = new SqlCommand(ctStr, connection);
                                        command.ExecuteNonQuery();
                                        command.Dispose();
                                      
                                        command = new SqlCommand(null, connection);
                                        ctStr = "INSERT INTO [PA].[dbo].[SIOPE_ListaAmministrazioni_Bilanci](\r\n";
                                        for (var i = 0; i < columns.Length; i++)
                                        {
                                            ctStr += " " + columns[i] + " ";
                                            if (i < columns.Length - 1)
                                            {
                                                ctStr += ",";
                                            }
                                            ctStr += "\r\n";
                                        }
                                        ctStr += ")";
                                        ctStr += " VALUES (\r\n";
                                        for (var i = 0; i < columns.Length; i++)
                                        {
                                            ctStr += "  @" + columns[i];
                                            //values.Add("@" + dtB.Columns[i].ColumnName.ToString());
                                            if (i < columns.Length - 1)
                                            {
                                                ctStr += ",";
                                            }
                                            ctStr += "\r\n";
                                        }
                                        ctStr += "\r\n";
                                        ctStr += ")";

                                        //Set the type of the element you want to insert
                                        SqlParameter uidParam = new SqlParameter("@" + columns[0], SqlDbType.UniqueIdentifier);
                                        SqlParameter codammParam = new SqlParameter("@" + columns[1], SqlDbType.VarChar, 20);
                                        SqlParameter databilParam = new SqlParameter("@" + columns[2], SqlDbType.DateTime);
                                        SqlParameter compartoParam = new SqlParameter("@" + columns[3], SqlDbType.VarChar, 2);

                                        //Add your param to thwe command
                                        command.Parameters.Add(uidParam);
                                        command.Parameters.Add(codammParam);
                                        command.Parameters.Add(databilParam);
                                        command.Parameters.Add(compartoParam);

                                        // Call Prepare after setting the Commandtext and Parameters.
                                        command.CommandText = ctStr;
                                        command.Prepare();

                                        // Change parameter values and call ExecuteNonQuery.
                                        command.Parameters[0].Value = g;
                                        command.Parameters[1].Value = line[0].Replace("'", "''");                                      
                                        command.Parameters[2].Value = datetime;
                                        command.Parameters[3].Value = comparto;
                                        command.ExecuteNonQuery();
                                        command.Dispose();                                                                                              
                                        //values.Clear();
                                    }//if -> fine record bilanci

                                    //INSERIAMO I BILANCI (CON GLI IMPORTI)
                                    //InsertDataTableBilanciAndImportInDatabase(dt, "PA", "SIOPE_ListaAmministrazioni_Bilanci_Importi", g, j);  
                                    columns = new[] { "uid", "uidBilanci", "codprospetto", "codice", "descrizione", "importoperiodo", "importocumulato" };
                                   
                                    //for (int i = 0; i < columns.Length; i++) { values.Add("@" + columns[i]); }
                                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    //TABELLA 2 - db0.SIOPE_ListaAmministrazioni_Bilanci
                                    //uid	uniqueidentifier	Unchecked
                                    //uidBilanci	uniqueidentifier	Unchecked
                                    //codprospetto	varchar(5)	Unchecked
                                    //codice	varchar(4)	Unchecked
                                    //descrizione	varchar(50)	Unchecked
                                    //importoperiodo	numeric(18, 2)	Unchecked
                                    //importocumulato	numeric(18, 2)	Unchecked                    
                                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    //******************************************************************************************
                                    //SET SOME VALUE       
                                    string codProspetto = "";
                                    //******************************************************************************************
                                    //BEFORE INSERT DELETE THE RECORD IF ALREADY EXISTS
                                    ctStr = "DELETE FROM [PA].dbo.[SIOPE_ListaAmministrazioni_Bilanci_Importi] WHERE " +
                                                     "codice = '" + line[7].Replace("'", "''") + "'" +
                                                     " AND " +
                                                     "codprospetto = '" + codProspetto.Replace("'", "''") + "'" +
                                                     " AND " +
                                                     "importoperiodo = '" + line[9].Replace("'", "''") + "'" +
                                                     " AND " +
                                                     "importocumulato = '" + line[10].Replace("'", "''") + "'"
                                                     ;
                                    //*******************************************************************************************                                                     
                                    //INSERT INTO THE Database THE RECORD

                                    command = new SqlCommand(ctStr, connection);
                                    command.ExecuteNonQuery();
                                    command.Dispose();

                                    command = new SqlCommand(null, connection);
                                    ctStr = "INSERT INTO [PA].[dbo].[SIOPE_ListaAmministrazioni_Bilanci_Importi](\r\n";
                                    for (var i = 0; i < columns.Length; i++)
                                    {
                                        ctStr += " " + columns[i] + " ";
                                        if (i < columns.Length - 1)
                                        {
                                            ctStr += ",";
                                        }
                                        ctStr += "\r\n";
                                    }
                                    ctStr += ")";
                                    ctStr += " VALUES (\r\n";
                                    for (var i = 0; i < columns.Length; i++)
                                    {
                                        ctStr += "  @" + columns[i];
                                        if (i < columns.Length - 1)
                                        {
                                            ctStr += ",";
                                        }
                                        ctStr += "\r\n";
                                    }
                                    ctStr += "\r\n";
                                    ctStr += ")";

                                    //Set the type of the element you want to insert
                                    var uidParam2 = new SqlParameter("@" + columns[0], SqlDbType.UniqueIdentifier);
                                    var uidBilanciParam = new SqlParameter("@" + columns[1], SqlDbType.UniqueIdentifier);
                                    var codProspParam = new SqlParameter("@" + columns[2], SqlDbType.VarChar, 5);
                                    var codGestParam = new SqlParameter("@" + columns[3], SqlDbType.VarChar, 4);
                                    var descrParam = new SqlParameter("@" + columns[4], SqlDbType.VarChar, 50);
                                    var importoperiodoParam = new SqlParameter("@" + columns[5], SqlDbType.Decimal, 18);
                                    var importcumulatoParam = new SqlParameter("@" + columns[6], SqlDbType.Decimal, 18);

                                    //Add your param to thwe command
                                    //Change parameter values and call ExecuteNonQuery.
                                    command.Parameters.Add(uidParam2);
                                    command.Parameters[0].Value = Guid.NewGuid();
                                    command.Parameters.Add(uidBilanciParam);
                                    command.Parameters[1].Value = g;
                                    command.Parameters.Add(codProspParam);
                                    command.Parameters[2].Value = codProspetto;
                                    command.Parameters.Add(codGestParam);
                                    command.Parameters[3].Value = line[7];
                                    command.Parameters.Add(descrParam);
                                    command.Parameters[4].Value = line[8];

                                    importoperiodoParam.Precision = 18;
                                    importoperiodoParam.Scale = 2;
                                    decimal d1;
                                    //CultureInfo.InvariantCulture
                                    Decimal.TryParse(line[9], NumberStyles.Any, new CultureInfo("en-US"), out d1);
                                    importoperiodoParam.Value =  d1;
                                    //importoperiodoParam.Direction = ParameterDirection.Output;
                                    command.Parameters.Add(importoperiodoParam);


                                    importcumulatoParam.Precision = 18;
                                    importcumulatoParam.Scale = 2;
                                    decimal d2;
                                    Decimal.TryParse(line[10], NumberStyles.Any, new CultureInfo("en-US"), out d2);
                                    importcumulatoParam.Value = d2;
                                    //importcumulatoParam.Direction = ParameterDirection.Output;
                                    //command.Parameters.AddWithValue(values[5], d1);
                                    command.Parameters.Add(importcumulatoParam);
                                    // Call Prepare after setting the Commandtext and Parameters.
                                    command.CommandText = ctStr;
                                    command.Prepare();
                                     
                                    command.ExecuteNonQuery();
                                    command.Dispose();
                                    //values.Clear();
                                }//else

                                //counter++;
                            }//while                   
                            file.Close();
                        }//using streamreader 
                        connection.Close();
                }//using sql connection
                // Writes the number of imported rows to the form
                //string lblProgres = "Imported: " + counter.ToString() + "/" + counter.ToString() + " row(s)";                          
                // Suspend the screen.                
            }
           
        }//end class CsvImport


        
        #endregion

        //OTHER METHOD

        
        /// <summary>
        /// 
        /// </summary>
        public class CsvBulkCopyDataIntoSqlServer
        {
            private static readonly ILog Log = LogManager.GetLogger<CsvBulkCopyDataIntoSqlServer>();
            protected const string TruncateLiveTableCommandText = @"TRUNCATE TABLE YourTableName";
            protected const int BatchSize = 100000;

            public void LoadCsvDataIntoSqlServer(string tableName,string filePathName)
            {
                // This should be the full path
                //var fileName = @"C:\Path\To\File.csv";

                var createdCount = 0;

                using (var textFieldParser = new TextFieldParser(filePathName))
                {
                    textFieldParser.TextFieldType = FieldType.Delimited;
                    textFieldParser.Delimiters = new[] { "," };
                    textFieldParser.HasFieldsEnclosedInQuotes = true;
                    
                    //var connectionString = ConfigurationManager.ConnectionStrings["CMSConnectionString"].ConnectionString;
                    var connectionString = "";
                    var dataTable = new DataTable(tableName);

                    // Add the columns in the temp table
                    //dataTable.Columns.Add("FirstName");
                    //dataTable.Columns.Add("LastName");

                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        sqlConnection.Open();

                        // Truncate the live table
                        using (var sqlCommand = new SqlCommand(TruncateLiveTableCommandText, sqlConnection))
                        {
                            sqlCommand.ExecuteNonQuery();
                        }

                        // Create the bulk copy object
                        var sqlBulkCopy = new SqlBulkCopy(sqlConnection)
                        {
                            DestinationTableName = tableName
                        };

                        // Setup the column mappings, anything ommitted is skipped
                        //sqlBulkCopy.ColumnMappings.Add("FirstName", "FirstName");
                        //sqlBulkCopy.ColumnMappings.Add("LastName", "LastName");

                        // Loop through the Csv and load each set of 100,000 records into a DataTable
                        // Then send it to the LiveTable
                        while (!textFieldParser.EndOfData)
                        {
                            string[] fields = textFieldParser.ReadFields();
                            if (fields != null)
                            {
                                dataTable.Rows.Add(new object[] { fields });
                                createdCount++;
                            }
                            //dataTable.Rows.Add(textFieldParser.ReadFields());
                            //createdCount++;
                            if (createdCount % BatchSize == 0)
                            {
                                InsertDataTable(sqlBulkCopy, sqlConnection, dataTable);

                                //break; //Just if you want one test
                            }
                        }

                        // Don't forget to send the last batch under 100,000
                        InsertDataTable(sqlBulkCopy, sqlConnection, dataTable);

                        sqlConnection.Close();
                    }
                }
            }

            protected void InsertDataTable(SqlBulkCopy sqlBulkCopy, SqlConnection sqlConnection, DataTable dataTable)
            {
                sqlBulkCopy.WriteToServer(dataTable);

                dataTable.Rows.Clear();
            }

            public List<string> GetAllFileNameOnSpecificDirectory(string directory, string extension) 
            {
                return FileUtilities.GetFilesFromDirectory(directory,extension);
            }

            
        }

       

    }

    public class SupportCsv 
    {
        private static readonly ILog Log = LogManager.GetLogger<SupportCsv>();
        /// <summary>
        /// Metodo che legge un file csv di bilancio di SIOPE e ne carica le informazioni su SQL Server
        /// </summary>
        /// <param name="pathFilesNames"></param>
        /// <param name="separator"></param>
        public void ReadCsvAndLoadToSqlServer(List<string> pathFilesNames, string separator)
        {
            //GUID DI RIFERIMENTO DEI BILANCI
            Guid g = Guid.NewGuid();
            //int counter = 0;
            string strConnDB = "";
            //CI SEVRE UN SOLO RECORD DI BILANCIO PER OGNI ISIEME DI BILANCI CON LO STESSO CODICE
            bool setRecordBilanci = true;
            foreach (string pathFileName in pathFilesNames)
            {
                // Read the file and display it line by line.

                using (SqlConnection connection = new SqlConnection(strConnDB))
                {
                    connection.Open();

                    using (StreamReader file = new StreamReader(pathFileName))
                    {

                        bool firstRow = true;
                        string row;
                        while ((row = file.ReadLine()) != null)
                        {
                            //JUMP THE FIRST LINE
                            if (firstRow)
                            {
                                //SALTIAMO LA PRIMA RIGA COONTENETE I NOMI DELLE COLONNE
                                //DA QUEL CHE SI é VISTO SONO SMEPRE LE STESSE
                                //row = file.ReadLine();
                                firstRow = false;
                            }
                            else
                            {
                                string[] line = row.Split(separator.ToCharArray()[0]);
                                //INSERIAMO I BILANCI (NO IMPORTI) JUST ONE TIME FOR FILE
                                SqlCommand command;
                                string ctStr;
                                if (setRecordBilanci)
                                {
                                    setRecordBilanci = false;
                                    //******************************************************************************************
                                    //TABELLA 1 - db0.SIOPE_ListaAmministrazioni_Bilanci
                                    //uid	uniqueidentifier	Unchecked
                                    //codamm	varchar(20)	Unchecked
                                    //databil	datetime	Unchecked
                                    //comparto	varchar(2)	Unchecked                                                                                                                                             
                                    //******************************************************************************************
                                    //SET SOME VALUE                                      
                                    // sanno = getBilancio().PERIODICITA.ANNO;
                                    string sanno = Regex.Replace(line[6], "[a-zA-Z]", "").Trim();
                                    int anno;//1990
                                    int mese = 1;
                                    int day = 1;
                                    if (sanno.Contains("/"))
                                    {
                                        //GIORNALIERA 16/01/2014
                                        string[] days = sanno.Split('/');
                                        int.TryParse(days[0], out day);
                                        int.TryParse(days[1], out mese);
                                        int.TryParse(days[2], out anno);
                                    }
                                    else
                                    {
                                        //anno = System.Convert.ToInt32(sanno.Trim());
                                        int.TryParse(sanno, out anno);
                                    }
                                    //string comparto = Siope.getBilancio().PERIODICITA.CODICE_PERIODO;
                                    //string comparto = Regex.Replace(line[6], "[\\d]|[/]", "").Trim();
                                    string comparto = "";

                                    DateTime datetime = new DateTime(anno, mese, day);
                                    //******************************************************************************************
                                    //BEFORE INSERT DELETE THE RECORD BILANCI AND ALL IMPORT RELATED TO THAST IF ALREADY EXISTS                                   
                                    ctStr = "SELECT uid FROM [PA].dbo.[SIOPE_ListaAmministrazioni_Bilanci] WHERE " +
                                                        "codamm = '" + line[0].Replace("'", "''") + "'" +
                                                        " AND " +
                                                        "databil = '" + anno.ToString().Replace("'", "''") + "'" +
                                                        " AND " +
                                                        "comparto = '" + comparto.Replace("'", "''") + "'";
                                    command = new SqlCommand(ctStr, connection);
                                    SqlDataReader rdr = command.ExecuteReader();
                                    Guid rdrUid = new Guid();
                                    while (rdr.Read())
                                    {
                                        rdrUid = (Guid)rdr["uid"];
                                    }                                  
                                    rdr.Close();                                    
                                    command.Dispose();
                                    if (rdrUid.ToString() != "00000000-0000-0000-0000-000000000000")
                                    {
                                        ctStr = "DELETE FROM [PA].dbo.[SIOPE_ListaAmministrazioni_Bilanci] WHERE " +
                                                            "uid = '" + rdrUid + "'"
                                            //"codamm = '" + line[0].ToString().Replace("'", "''") + "'" +
                                            //" AND " +
                                            //"databil = '" + anno.ToString().Replace("'", "''") + "'" +
                                            //" AND " +
                                            //"comparto = '" + comparto.Replace("'", "''") + "'"
                                                            ;
                                        command = new SqlCommand(ctStr, connection);
                                        command.ExecuteNonQuery();
                                        command.Dispose();

                                        //BEFORE INSERT DELETE THE RECORD IF ALREADY EXISTS
                                        ctStr = "DELETE FROM [PA].dbo.[SIOPE_ListaAmministrazioni_Bilanci_Importi] WHERE " +
                                                            "uidBilanci = '" + rdrUid + "'"
                                            //" AND " +
                                            //"codice = '" + line[7].Replace("'", "''") + "'" +
                                            //" AND " +
                                            //"codprospetto = '" + codProspetto.Replace("'", "''") + "'"
                                            //" AND " +
                                            //"importoperiodo = '" + line[9].Replace("'", "''") + "'" +
                                            //" AND " +
                                            // "importocumulato = '" + line[10].Replace("'", "''") + "'"
                                                            ;
                                        command = new SqlCommand(ctStr, connection);
                                        command.ExecuteNonQuery();
                                        command.Dispose();
                                    }


                                    //*******************************************************************************************                                                     
                                    //INSERT INTO THE Database THE RECORD                                 
                                    command = new SqlCommand(null, connection);
                                    ctStr = "INSERT INTO [PA].[dbo].[SIOPE_ListaAmministrazioni_Bilanci](uid ,codamm ,databil ,comparto ) VALUES (@uid,@codamm,@databil,@comparto)";
                                    #region OLD CODE
                                    /*
                                    //Set the type of the element you want to insert
                                    SqlParameter uidParam = new SqlParameter("@uid", SqlDbType.UniqueIdentifier);
                                    SqlParameter codammParam = new SqlParameter("@codamm", SqlDbType.VarChar, 20);
                                    SqlParameter databilParam = new SqlParameter("@databil", SqlDbType.DateTime);
                                    SqlParameter compartoParam = new SqlParameter("@comparto", SqlDbType.VarChar, 2);

                                    //Add your param to thwe command
                                    command.Parameters.Add(uidParam);
                                    command.Parameters.Add(codammParam);
                                    command.Parameters.Add(databilParam);
                                    command.Parameters.Add(compartoParam);

                                    // Call Prepare after setting the Commandtext and Parameters.
                                    command.CommandText = ctStr;
                                    command.Prepare();

                                    // Change parameter values and call ExecuteNonQuery.
                                    command.Parameters[0].Value = g;
                                    command.Parameters[1].Value = line[0].Replace("'", "''");
                                    command.Parameters[2].Value = datetime;
                                    command.Parameters[3].Value = comparto;
                                    command.ExecuteNonQuery();
                                    command.Dispose();
                                    */
                                    #endregion
                                    //command.CommandType = CommandType.StoredProcedure;

                                    command.Parameters.Add(new SqlParameter("@uid", SqlDbType.UniqueIdentifier)).Value = g;
                                    command.Parameters.Add(new SqlParameter("@codamm", SqlDbType.VarChar, 20)).Value = line[0].Replace("'", "''");
                                    command.Parameters.Add(new SqlParameter("@databil", SqlDbType.DateTime)).Value = datetime;
                                    command.Parameters.Add(new SqlParameter("@comparto", SqlDbType.VarChar, 2)).Value = comparto;

                                    // Call Prepare after setting the Commandtext and Parameters.
                                    command.CommandText = ctStr;
                                    command.Prepare();
                                    // Change parameter values and call ExecuteNonQuery.                                 
                                    command.ExecuteNonQuery();
                                    command.Dispose();
                                }//if -> fine record bilanci

                                //INSERIAMO I BILANCI (CON GLI IMPORTI)
                                //******************************************************************************************
                                //TABELLA 2 - db0.SIOPE_ListaAmministrazioni_Bilanci
                                //uid	uniqueidentifier	Unchecked
                                //uidBilanci	uniqueidentifier	Unchecked
                                //codprospetto	varchar(5)	Unchecked
                                //codice	varchar(4)	Unchecked
                                //descrizione	varchar(50)	Unchecked
                                //importoperiodo	numeric(18, 2)	Unchecked
                                //importocumulato	numeric(18, 2)	Unchecked                                                 
                                //******************************************************************************************
                                //SET SOME VALUE       
                                //string codProspetto = Siope.getBilancio().PROSPETTO.CODICE_PROSPETTO;
                                string codProspetto = "";
                                //Guid g2 = Guid.NewGuid();
                                //******************************************************************************************

                                //INSERT INTO THE Database THE RECORD                          
                                command = new SqlCommand(null, connection);

                                ctStr = "INSERT INTO [PA].[dbo].[SIOPE_ListaAmministrazioni_Bilanci_Importi](uid ,uidBilanci ,codprospetto ,codice ,descrizione ,importoperiodo ,importocumulato )" +
                                    "VALUES (@uid,@uidBilanci,@codprospetto,@codice,@descrizione,@importoperiodo,@importocumulato)";
                                #region OLD CODE
                                /*
                                //Set the type of the element you want to insert
                                SqlParameter uidParam2 = new SqlParameter("@uid", SqlDbType.UniqueIdentifier);
                                SqlParameter uidBilanciParam = new SqlParameter("@uidBilanci", SqlDbType.UniqueIdentifier);
                                SqlParameter codProspParam = new SqlParameter("@codprospetto", SqlDbType.VarChar, 5);
                                SqlParameter codGestParam = new SqlParameter("@codice", SqlDbType.VarChar, 4);
                                SqlParameter descrParam = new SqlParameter("@descrizione", SqlDbType.VarChar, 200);
                                SqlParameter importoperiodoParam = new SqlParameter("@importoperiodo", SqlDbType.Decimal, 18);
                                SqlParameter importcumulatoParam = new SqlParameter("@importocumulato", SqlDbType.Decimal, 18);

                                //Add your param to thwe command
                                //Change parameter values and call ExecuteNonQuery.
                                command.Parameters.Add(uidParam2);
                                command.Parameters[0].Value = Guid.NewGuid(); ;
                                command.Parameters.Add(uidBilanciParam);
                                command.Parameters[1].Value = g;
                                command.Parameters.Add(codProspParam);
                                command.Parameters[2].Value = codProspetto;//codProspParam
                                command.Parameters.Add(codGestParam);
                                command.Parameters[3].Value = line[7].ToString();//codGestParam
                                command.Parameters.Add(descrParam);
                                command.Parameters[4].Value = line[8].ToString().Replace("t{", "à");//descrParam
                                    //.Replace("Indennit{", "Indennità").Replace("calamit{","calamità";

                                importoperiodoParam.Precision = 18;
                                importoperiodoParam.Scale = 2;
                                decimal d1;
                                //CultureInfo.InvariantCulture
                                Decimal.TryParse(line[9], NumberStyles.Any, new CultureInfo("en-US"), out d1);
                                importoperiodoParam.Value = d1;
                                //importoperiodoParam.Direction = ParameterDirection.Output;
                                command.Parameters.Add(importoperiodoParam);


                                importcumulatoParam.Precision = 18;
                                importcumulatoParam.Scale = 2;
                                decimal d2;
                                Decimal.TryParse(line[10], NumberStyles.Any, new CultureInfo("en-US"), out d2);
                                importcumulatoParam.Value = d2;
                                //importcumulatoParam.Direction = ParameterDirection.Output;
                                //command.Parameters.AddWithValue(values[5], d1);
                                command.Parameters.Add(importcumulatoParam);
                                // Call Prepare after setting the Commandtext and Parameters.
                                command.CommandText = ctStr;
                                command.Prepare();

                                command.ExecuteNonQuery();
                                command.Dispose();
                                */
                                #endregion

                                //Set the type of the element you want to insert
                                //Change parameter values and call ExecuteNonQuery.                              
                                decimal d1;
                                Decimal.TryParse(line[9], NumberStyles.Any, new CultureInfo("en-US"), out d1); //CultureInfo.InvariantCulture                            
                                decimal d2;
                                Decimal.TryParse(line[10], NumberStyles.Any, new CultureInfo("en-US"), out d2);

                                //Add your param to the command
                                command.Parameters.Add(new SqlParameter("@uid", SqlDbType.UniqueIdentifier)).Value = Guid.NewGuid();
                                command.Parameters.Add(new SqlParameter("@uidBilanci", SqlDbType.UniqueIdentifier)).Value = g;
                                command.Parameters.Add(new SqlParameter("@codprospetto", SqlDbType.VarChar, 5)).Value = codProspetto;
                                command.Parameters.Add(new SqlParameter("@codice", SqlDbType.VarChar, 4)).Value = line[7];
                                command.Parameters.Add(new SqlParameter("@descrizione", SqlDbType.VarChar, 200)).Value = line[8].Replace("t{", "à");
                                command.Parameters.Add(new SqlParameter("@importoperiodo", SqlDbType.Decimal, 18)).Value = d1;
                                command.Parameters.Add(new SqlParameter("@importocumulato", SqlDbType.Decimal, 18)).Value = d2;

                                command.Parameters[5].Precision = 18;//importoperiodoParam
                                command.Parameters[5].Scale = 2;//importoperiodoParam

                                command.Parameters[6].Precision = 18;
                                command.Parameters[6].Scale = 2;
                                // Call Prepare after setting the Commandtext and Parameters.
                                command.CommandText = ctStr;
                                command.Prepare();


                                command.ExecuteNonQuery();
                                command.Dispose();
                            }//else

                            //counter++;
                        }//while                   
                        file.Close();
                    }//using streamreader 
                    connection.Close();
                }//using sql connection
            }//for each prospetto con lo stesso codice ente e periodicità
            // Writes the number of imported rows to the form
            //string lblProgres = "Imported: " + counter.ToString() + "/" + counter.ToString() + " row(s)";


            // Suspend the screen.

        }//end of method set read list of csv with same code


        /// <summary>
        /// Metodo di supporto che aiuta la rinomina dei file con il download manuale non viene utilizzato nel flusso del programma
        /// ATTENZIONE!!!: Questo metodo non fa parte del flusso del programma
        /// </summary>
        public void RenameAllCsvFile(string newFileName)
        {
            string newfileName = "";
            List<string> listCsv = new List<string>();
            string dirPath = ""; //path to the directory
            DirectoryInfo d = new DirectoryInfo(dirPath);//Assuming Test is your Folder
            FileInfo[] files = d.GetFiles("*.csv"); //Getting Text files
            foreach (FileInfo file in files)
            {
                //string str = str + ", " + file.Name;
                listCsv.Add(file.Name);
            }
            foreach (string row in listCsv)
            {
                try
                {
                    string fileName = row;
                    newFileName = newFileName + "_" + fileName;

                    //Rename the file
                    if (File.Exists(newfileName))
                    {
                        //File.Delete(newfileName);
                        //do nothing for now
                        //System.IO.File.Move(fileName, newfileName);
                    }
                    else
                    {
                        File.Move(fileName, newfileName);
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }//for each code

        }

        /// <summary>
        /// Metodo di supporot non facente parte del flusso principale del programma
        /// ATTENZIONE!!!: Questo metodo non fa parte del flusso del programma
        /// </summary>
        /// <returns></returns>
        public void GroupByCsvFileForCodiceEnte()
        {
            //string newfileName = "";          
            List<string> listCsv = new List<string>();
            string dirPath = "C:\\Users\\esd91martent\\Desktop\\CRIF 2015-03-18\\SIOPE 2015-03-18\\Crif.CrifLab.Web.PA\\Crif.CrifLab.Web.PA.Siope\\bin\\x86\\Debug\\";
            DirectoryInfo d = new DirectoryInfo(dirPath);//Assuming Test is your Folder
            FileInfo[] files = d.GetFiles("*.csv"); //Getting Text files
            foreach (FileInfo file in files)
            {
                //string str = str + ", " + file.Name;
                listCsv.Add(file.Name);
            }
            List<string> l = new List<string>();
            List<List<string>> ll = new List<List<string>>();
            List<string> codecs = new List<string>();

            foreach (string f in listCsv)
            {
                string code = "";
                Regex.Replace(f, @"[\d]{5,20}", delegate(Match match)
                {
                    code = match.ToString();
                    return code;
                });
                if (codecs.Contains(code) == false) { codecs.Add(code); }
            }
            ll.Clear();
            l.Clear();
            foreach (string code in codecs)
            {
                l.Clear();
                foreach (string f in listCsv)
                {
                    if (f.Contains(code))
                    {
                        l.Add(f);
                    }
                }//for each file with specific code
                List<string> copy = new List<string>(l);
                ll.Add(copy);

            }//foreach code                 

            foreach (List<string> list in ll)
            {
                ReadCsvAndLoadToSqlServer(list, ";");
            }
        }       
   
    }
}
