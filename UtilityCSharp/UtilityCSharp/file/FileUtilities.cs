using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Common.Logging;

namespace UtilityCSharp.file
{

    public class FileUtilities
    {
        private static readonly ILog Log = LogManager.GetLogger<FileUtilities>();
        public static List<string> GetFilesFromDirectory(string directoryPath)
        {
            return Directory.GetFiles(directoryPath).ToList();
            //foreach (string s in filePaths) { files.Add(s); }
            // returns:
            // "c:\MyDir\my-car.BMP"
            // "c:\MyDir\my-house.jpg"          
        }

        public static List<string> GetFilesFromDirectoryRecursively(string directoryRootPath)
        {
            return GetFilesFromDirectoryRecursively(directoryRootPath, false);
        }

        public static List<string> GetFilesFromDirectoryRecursively(string directoryRootPath,bool enumerated)
        {
            return enumerated ? 
                Directory.EnumerateFiles(directoryRootPath, "*.*", SearchOption.AllDirectories).ToList() : 
                Directory.GetFiles(directoryRootPath, "*.*", SearchOption.AllDirectories).ToList();
        }

        public static List<string> GetFilesFromDirectoryRecursively(string directoryRootPath, string extension)
        {
            return Directory.GetFiles(directoryRootPath, "*." + extension.ToLower() + "", SearchOption.AllDirectories).ToList();
            // returns:
            // "c:\MyDir\my-car.BMP"
        }

        public static List<string> GetFilesFromDirectory(string directoryRootPath, string extension)
        {
            return Directory.GetFiles(directoryRootPath, "*." + extension.ToLower() + "").ToList();    
        }

        /// <summary>
        /// Metodo che legge il contenuto di un file di default se la filepath � null lo legge dalla cartella di debug
        /// OLD_NAME: ReadFileWithStreamReader.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static List<string> Read(string fileName, string filePath)
        {
            var listOfRecord = new List<string>();
            //"input_masterdetail.csv"
            if (!File.Exists(fileName)) return listOfRecord;
            using (var sr = new StreamReader(filePath + fileName, true))
            {
                while (sr.EndOfStream == false)
                {
                    listOfRecord.Add(sr.ReadLine());
                    if (sr.EndOfStream) break;
                }
            }
            return listOfRecord;
        }

        /// <summary>
        /// Metodo che scrive il contenuto di una lista di stringhe in un file di default se la filepath � null lo legge dalla cartella di debug
        /// OLD_NAME: WriteFileWithStreamWrite
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="filePath"></param>
        /// <param name="listOfRecord"></param>
        public static void Write(string fileName, string filePath, List<string> listOfRecord)
        {
            if (filePath == null) { filePath = ""; }
            foreach (var s in listOfRecord)
            {
                using (var sw = new StreamWriter(filePath + fileName, true))
                {
                    sw.WriteLine(s);
                    sw.Flush();
                }
            }
        }

        public static void RenameAllFile(string dirPath,string newFileName,string extension)
        {            
            DirectoryInfo d = new DirectoryInfo(dirPath);//Assuming Test is your Folder
            FileInfo[] files = d.GetFiles("*."+ extension); //Getting Text files
            List<string> listCsv = files.Select(file => file.Name).ToList();
            foreach (var row in listCsv)
            {
                try
                {
                    string fileName = row;
                    newFileName = newFileName + "_" + fileName;
                    //Rename the file
                    if (File.Exists(newFileName))
                    {
                        //File.Delete(newfileName);
                        //do nothing for now
                        //System.IO.File.Move(fileName, newfileName);
                    }
                    else
                    {
                        File.Move(fileName,newFileName);
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }//for each code

        }
    }
}

