﻿
using System;
using System.Collections.Generic;
using System.IO;
using VDS.RDF.Parsing;
using VDS.RDF.Query;
using VDS.RDF.Writing;
using VDS.RDF.Writing.Formatting;

namespace UtilityCSharp.repositoryRDF
{
    namespace ConsoleAppSPARQL3
    {
       
        public struct ObjectNamespace
        {
            //VARIABLE   
            string _prefix;

            //CONSTRUCT
            public ObjectNamespace(string prefix, string stringNamespace)
            {
                StringNamespace = stringNamespace;
                _prefix = prefix;

            }
            //GETTER AND SETTER
            public string Prefix
            {
                get { return _prefix; }
                set { _prefix = value; }
            }

            public string StringNamespace { get; set; }
        }

        /// <summary>
        /// 2015-04-13
        /// </summary>
        public class ConsoleAppSparql3
        {
                          
            //Query che estare 20900 Amministrazioni con le informazioni di base
            
           
            //Query che estrae le 21235 Amministrazioni (compresi i blank node di riferimento per le amministrazioni
            static string sparqlGetAllAmministration = @"
                                                        select distinct ?cod_amm ?categoriaAmm where  {    
                                                                ?cod_amm a spcdata:Amministrazione.
                                                                OPTIONAL{?cod_amm org:classification ?categoriaAmm}
                
                                         }";

            readonly List<ObjectNamespace> _namespaceList = new List<ObjectNamespace>();
                            
            /// <summary>
            /// 
            /// </summary>
            public ConsoleAppSparql3(string idCategoriaAmministrazione, int limit, int offset, string endPointUri, string defaultGraphUris, string querySparql)
            {
                IdCategoriaAmministrazione = idCategoriaAmministrazione;
                Limit = limit;
                Offset = offset;
                EndPointUri = endPointUri;
                DefaultGraphUris = defaultGraphUris;               
                QuerySparql = querySparql;
                FilePath5 =new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;

                //DI DEFAULT IMPOSTIAMO I SEGUENTI PREFISSI DEI NAMESPACE NELLE NOSTRE SPARQL
                //prefix dc:      <http://purl.org/dc/terms/> 
                //prefix prov:    <http://www.w3.org/ns/prov#> 
                //prefix foaf:    <http://xmlns.com/foaf/0.1/> 
                //prefix spcdata: <http://spcdata.digitpa.gov.it/> 
                //prefix org:     <http://www.w3.org/ns/org#> 
                //prefix adms:    <http://www.w3.org/ns/adms#> 
                //prefix xkos:    <http://purl.org/linked-data/xkos#> 
                //prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> 
                //prefix dcat:    <http://www.w3.org/ns/dcat#> 
                //prefix owl:     <http://www.w3.org/2002/07/owl#> 
                //prefix xsd:     <http://www.w3.org/2001/XMLSchema#> 
                //prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                //prefix skos:    <http://www.w3.org/2004/02/skos/core#> 
                //prefix cc:      <http://creativecommons.org/ns#> 
                //prefix geonames:<http://www.geonames.org/ontology#> 
                //prefix locn:    <http://www.w3.org/ns/locn#>
                //prefix owl:     <http://www.w3.org/2002/07/owl#>
                _namespaceList.Add(new ObjectNamespace("dc", "http://purl.org/dc/terms/"));
                _namespaceList.Add(new ObjectNamespace("prov", "http://www.w3.org/ns/prov#"));
                _namespaceList.Add(new ObjectNamespace("foaf", "http://xmlns.com/foaf/0.1/"));
                _namespaceList.Add(new ObjectNamespace("spcdata", "http://spcdata.digitpa.gov.it/"));
                _namespaceList.Add(new ObjectNamespace("org", "http://www.w3.org/ns/org#"));
                _namespaceList.Add(new ObjectNamespace("adms", "http://www.w3.org/ns/adms#"));
                _namespaceList.Add(new ObjectNamespace("xkos", "http://purl.org/linked-data/xkos#"));
                _namespaceList.Add(new ObjectNamespace("rdfs", "http://www.w3.org/2000/01/rdf-schema#"));
                _namespaceList.Add(new ObjectNamespace("dcat", "http://www.w3.org/ns/dcat#"));
                _namespaceList.Add(new ObjectNamespace("owl", "http://www.w3.org/2002/07/owl#"));
                _namespaceList.Add(new ObjectNamespace("xsd", "http://www.w3.org/2001/XMLSchema#"));
                _namespaceList.Add(new ObjectNamespace("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#"));
                _namespaceList.Add(new ObjectNamespace("skos", "http://www.w3.org/2004/02/skos/core#"));
                _namespaceList.Add(new ObjectNamespace("cc", "http://creativecommons.org/ns#"));
                _namespaceList.Add(new ObjectNamespace("geonames", "http://www.geonames.org/ontology#"));
                _namespaceList.Add(new ObjectNamespace("locn", "http://www.w3.org/ns/locn#"));
                _namespaceList.Add(new ObjectNamespace("owl", "http://www.w3.org/2002/07/owl#"));

                _namespaceList.Add(new ObjectNamespace("", "http://spcdata.digitpa.gov.it/"));
            }

            //public static SparqlParameterizedString queryString = new SparqlParameterizedString();


            //Variabile che identifica la Path della cartella debug all'interno del programma
            //string PathDDL = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;  
            //string filePath = Path.GetDirectoryName(ConfigurationManager.AppSettings[PathDLL]);
            //string filePath2 = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            //string filePath3 = Environment.CurrentDirectory; ;
            //string filepath4 = System.IO.Directory.GetCurrentDirectory();
            //Metti un parent per ogni volta che vuoi salire con la cartella
            //string _filePath5 = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName; 
            //string _filePath5 = "";
               
                                             
            //Identificativo delle amministrazioni che vogliamo filtrare, se in futuro vogliamo filtrare 
            //per altri identificativi abbiamo il costrutto già pronto

            //_limit E _offset DELLA QUERY SPARQL
            //Stringa che identifica la path al file Csv/XML
            //NOTA:il file ACCESS deve essere presente nella stessa cartella 
           
            //Endpoint URI di IPA
            //Base URI di IPA

            //La stringa contenente la prima query SPARQL

            public static int IndiceJ { get; } = 0;

            public static int IndiceI { get; } = 0;

            public string IdCategoriaAmministrazione { get; set; } //= "http://spcdata.digitpa.gov.it/CategoriaAmministrazione/81";

            public int Limit { get; set; } //= 10000;

            public int Offset { get; set; } //= 0;

            public string FilePath5 { get; set; } //= new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;

            public string DefaultGraphUris { get; set; } //= "http://spcdata.digitpa.gov.it";

            public string EndPointUri { get; set; } //= "http://spcdata.digitpa.gov.it:8899/sparql";

            public string QuerySparql { get; set; } //= sparqlGetAllAmministration;

            /// <summary>
            /// Metodo che verifica la corretta sintassi della query SPARQL
            /// </summary>
            /// <param name="stringSparql"></param>
            /// <returns></returns>
            private SparqlQuery ParametizedSparqlString(string stringSparql)
            {
                SparqlQuery querySparql =null;
                try
                {
                    //Create a Parameterized String                
                    SparqlParameterizedString queryString = new SparqlParameterizedString();
                    //Add a namespaces declaration
                    //queryString.Namespaces.AddNamespace("ex", new Uri("http://example.org/ns#"));
                    foreach (ObjectNamespace on in _namespaceList)
                    {
                        queryString.Namespaces.AddNamespace(on.Prefix, new Uri(on.StringNamespace));
                    }
                    //Set the SPARQL command
                    //For more complex queries we can do this in multiple lines by using += on the
                    //CommandText property
                    //Note we can use @name style parameters here
                    //e.g. queryString.CommandText = "SELECT * WHERE { ?s ex:property @value }";
                    queryString.CommandText = stringSparql;
                    //Inject a Value for the parameter
                    //queryString.SetUri("value", new Uri("http://example.org/value"));
                    //When we call ToString() we get the full command text with namespaces appended as PREFIX
                    //declarations and any parameters replaced with their declared values
                    //Console.WriteLine(queryString.ToString());
                    //We can turn this into a query by parsing it as in our previous example
                    SparqlQueryParser parser = new SparqlQueryParser();
                    querySparql = parser.ParseFromString(queryString);
                }
                catch (Exception)
                {
                    // ignored
                }
                return querySparql;                                                                                 
            }//ParametizedSparqlString

                      
            /// <summary>
            /// Metodo che invoca il server SPARQL per una risposta in formato XML/Csv/ecc.
            /// </summary>
            /// <param name="endPointUri"></param>
            /// <param name="defaultGraphUris"></param>
            /// <param name="querySparql"></param>
            /// <returns></returns>
            private SparqlResultSet RemoteStandardEndpointSparql(string endPointUri, string defaultGraphUris, SparqlQuery querySparql)
            {
                //Define a remote endpoint
                //Use the DBPedia SPARQL endpoint with the default Graph set to DBPedia
                SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri(endPointUri), defaultGraphUris);

                //Make a SELECT query against the Endpoint
                //e.g. SparqlResultSet results = endpoint.QueryWithResultSet("SELECT DISTINCT ?Concept WHERE {[] a ?Concept}");
                SparqlResultSet results = endpoint.QueryWithResultSet(querySparql.ToString());
                //System.Console.WriteLine("RESULT SET OUTPUT");
                //System.Console.WriteLine(@"*******************************************");    
                //int fakeLIMIT = _limit;
                //int fakeOFFSET = _offset;
                foreach (SparqlResult result in results)
                {
                    int lengthResult = result.Count;
                    Console.WriteLine(lengthResult);
                    //Assegniamo il numero delel variabili dello SPARQl al numero di colonne della tabella.                   
                    //Vediamo ogni singolo record ella query SPARQL

                    //System.Console.WriteLine("[{0}][{1}]:{2}", fakeOFFSET, fakeLIMIT, result.ToString());
                    //fakeOFFSET++;
                    //*****************************************************************************************************************************
                    //*****************************************************************************************************************************
                    //[CODICE OPZIONALE] LE SEGUENTI LINNE DI CODICE SONOMOLTO UTILE PER VERIFCARE CHE CIO CHE VINE ESTRATTO CON LA SPARQL E ESATTAMENTE CIO CHE STIAMO CERCANDO
                    //Per ogni record estratto dalla SPARQL identifichiamo il nome della variabile e il relativo valore (e.g. cod_amm = Comune di Bisenzio.)
                    /*
                    for (int k = 0; k < result.Count; k++)
                    {
                        //Stampa il nome della variabile SPARQL relativa in pratica il nome della colonna della tabella
                        System.Console.WriteLine("CODICE_COLUMN:{0}", result.Variables.ElementAt(k));
                        string codColumn = result.Variables.ElementAt(k);
                        INode n;
                       
                        string valueColumn;
                        try
                        {
                            if (result.TryGetValue(codColumn, out n))
                            {
                                 VDS.RDF.Nodes.StringNode sn;
                                //Stampa il valore della variabile relativa al valore della variabile codColumn
                                valueColumn = n.ToString();
                                System.Console.WriteLine("VALUE_COLUMN:{0}", valueColumn);                         
                            }
                        }
                        catch (NullReferenceException nre) { valueColumn = null; continue; }
                    }//for result.Count
                    */

                    //***********************************************************************************************************************************
                    //*****************************************************************************************************************************      
                    //Make a DESCRIBE query against the Endpoint
                    //INode m;                 
                    //IGraph g = endpoint.QueryWithResultGraph(_querySparql.ToString());
                    //IGraph g = endpoint.QueryWithResultGraph("DESCRIBE ");
                    //Triple t = g.GetListAsTriples(m); 
                    //foreach (Triple t in g.Triples)
                    //{
                    //    System.Console.WriteLine(t.ToString());
                    //}
                    //************************************************************************************************************************************

                } //for result of results 
                //System.Console.WriteLine("*******************************************");               
                return results;
            }//RemoteStandardEndpointSparql                   

            /// <summary>
            /// Metodo che salva la risposta del server SPARQL in formato XML
            /// </summary>
            /// <param name="results"></param>
            /// <param name="filename"></param>
            private void SaveResultQueryToXmlFileOrString(SparqlResultSet results, string filename)
            {             
                SparqlXmlWriter soc = new SparqlXmlWriter();
                //Assume that the Graph to be saved has already been loaded into a variable g
                //RdfXmlWriter rdfxmlwriter = new RdfXmlWriter();                        
                soc.Save(results, filename);             
                Console.WriteLine(soc.ToString());          
            }
           
            /// <summary>
            /// Metodo per la rimozione dei namespace 
            /// </summary>
            /// <param name="xmlDocument"></param>
            /// <returns></returns>
            private static string RemoveAllNamespacesBruteForce(string xmlDocument)
            {
                //StreamReader sr = new StreamReader(xmlDocument);
                return xmlDocument.Replace("xmlns=\"http://www.w3.org/2005/sparql-results#\"", "");               
            }
                                                         
           
            /// <summary>
            /// CODICE OPZIONALE:FORMATTA IL CONTENUTO DI UNA STRINGA IN FORMATO Csv
            /// </summary>
            /// <param name="outputFormat"></param>
            /// <returns></returns>
            private CsvFormatter SetTheFormatterForTheOutputResultOfSparqlinCsv(string outputFormat)
            {
                if (outputFormat.Equals("csv"))
                {
                    //VDS.RDF.Writing.Formatting.TurtleFormatter formatter = new VDS.RDF.Writing.Formatting.TurtleFormatter();
                    CsvFormatter csvFormatter = new CsvFormatter();
                    return csvFormatter;
                }
                return null;
            }

            /// <summary>
            /// CODICE OPZIONALE: SALVA IL RISULTATO DELLA QUERY SPARQL IN UN FILE Csv CON SEPARATORI ","
            /// </summary>
            /// <param name="results"></param>
            /// <param name="filename"></param>
            private void SaveResultQueryToCsvFileOrString(SparqlResultSet results, string filename)
            {
                SparqlCsvWriter soc = new SparqlCsvWriter();
                //Assume that the Graph to be saved has already been loaded into a variable g
                //RdfXmlWriter rdfxmlwriter = new RdfXmlWriter();
                //System.IO.StringWriter sw = new System.IO.StringWriter();
                soc.Save(results, filename);
                //System.Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++");
                //System.Console.WriteLine(soc.ToString());
                //System.Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++");
            }

        }//class ConsoleAppSparql3


        
        #region SE AVANZA TEMPO SAREBBE UTILE INTRODURRE UNA CLASSE AGGIUNTIVA PER LA GESTIONE DEL RISULTATO SPARQL (CIOE' LE TRIPLE)
        //A LIVELLO DI PROGRAMMA NEL CASO IN CUI SI VOGLIA FILTRARE A LIVELLO DI CODICE QUALCOSA CHE CI SIAMO  DIMENTICATI DI 
        //FILTRARE A LIVELLO SPARQL SENZA STARE A RE-INTERROGARE L'ENDPOINT URI
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //public class GestioneTriple
        //{     
        //    private string BaseUri;
        //    private IEnumerable<Triple> tsAll;
        //    private IGraph g;

        //    public GestioneTriple(string BaseUri)
        //    {
                
        //        this.BaseUri = BaseUri;
        //    }

        //    public IEnumerable<Triple> EstraiTripleConSPARQL(SparqlRemoteEndpoint endpoint, SparqlQuery querySPARQL, string BaseUri, List<ObjectNamespace> listNamespace)
        //    {

        //        //Creaimo il nostro grafico basandoci sugli URI o un file
        //        //SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri(endPointURI), defaultGraphURIS);
        //        //Creiamo il nostro grafico basandoci sul risultato di una risposta SPARQL
        //        //IGraph g = endpoint.QueryWithResultGraph(querySPARQL.ToString());
        //        g = endpoint.QueryWithResultGraph(querySPARQL.ToString());
        //        foreach (ObjectNamespace n in listNamespace)
        //        {
        //            g.NamespaceMap.AddNamespace(n.Prefix, UriFactory.Create(n.StringNamespace));
        //        }
        //        //Settiamo l'URI di base (il graph di default)
        //        g.BaseUri = UriFactory.Create(BaseUri);

        //        //Prendiamo tutte le triple senza alcun criterio           
        //        //IEnumerable<Triple> ts = g.Triples;
        //        tsAll = g.Triples;
        //        System.Console.WriteLine("?????????????????????????????????????????????????");
        //        foreach (Triple t in tsAll)
        //        {
        //            System.Console.WriteLine(t.ToString());                  
        //        }
        //        System.Console.WriteLine("?????????????????????????????????????????????????");
        //        //APPUNTI UTILI

        //        //Get all Triples involving a given Node
        //        //IUriNode select = g.CreateUriNode(new Uri("http://example.org/select"));
        //        //IEnumerable<Triple> ts = g.GetTriples(select);

        //        //Create a URI Node that refers to the Base URI of the Graph
        //        //Only valid when the Graph has a non-null Base URI
        //        //IUriNode thisGraph = g.CreateUriNode();

        //        //Create a URI Node that refers to some specific URI
        //        //IUriNode dotNetRDF = g.CreateUriNode(UriFactory.Create("http://www.dotnetrdf.org"));

        //        //Create a URI Node using a Prefixed Name
        //        //Need to define a Namespace first
        //        //g.NamespaceMap.AddNamespace("ex", UriFactory.Create("http://example.org/namespace/"));
        //        //IUriNode pname = g.CreateUriNode("ex:demo");
        //        //Resulting URI is http://example.org/namespace/demo

        //        //IUriNode nSubject = g.CreateUriNode("rdf:type");
        //        //IUriNode nPredicate = g.CreateUriNode("spcdata:Amministrazione");
        //        //IUriNode nObject = g.CreateUriNode("spcdata:Amministrazione");
        //        //Esempio filtra le triple con un certo subject e un certo predicate 
        //        // g.GetTriplesWithSubjectPredicate(g.CreateUriNode(UriFactory.Create("http://spcdata.digitpa.gov.it/Amministrazione")),g.CreateUriNode((UriFactory.Create("http://www.w3.org/2000/01/rdf-schema#type"))));
        //        return tsAll;

        //    }

        //    private IEnumerable<Triple> ConUnoSpecificoSubject(IUriNode nSubject)
        //    {
        //        IEnumerable<Triple> ts;
        //        ts = g.GetTriplesWithSubject(nSubject);
        //        return ts;
        //    }

        //    private void printTriple(IEnumerable<Triple> triples)
        //    {
        //        foreach(Triple t in triples)
        //        {
        //            System.Console.WriteLine(t.Subject+"|"+t.Predicate+"|"+t.Object);
        //        }
        //    }
        //}

        //Cpia sicurezza 21112014
        //*********************************************************************************************************                           
        //for (int LIMIT2 = LIMIT; LIMIT2 < 30000; LIMIT2 = LIMIT2 + LIMIT)
        //{
        //    //Aggiunta degli elementi LIMIT e OFFSET alla Query SPARQL
        //    string sLIMIT = LIMIT2.ToString();            
        //    string sOFFSET = OFFSET.ToString();
        //    string stringSPARQL = sparqlGetAllAmministrationWithOptional + " LIMIT " + sLIMIT + " OFFSET " + sOFFSET;
        //    //QUESTO METODO VERIFICA CHE LA QUERY SPARQL SIA COSTRUITA CORRETTAMENTE
        //    SparqlQuery querySparql = wpf1.parametizedSPARQLString(stringSPARQL);
        //    //INVOCA L'ENDPOINT SPARQL                         
        //    SparqlResultSet results = wpf1.remoteStandardEndpointSPARQL(endPointURI, defaultGraphURIS, querySparql);
        //    //SALVA IL RISULTATO DELLA QUERY INVOCATA NELL'ENDPOINT SPARQL SU UN FILE Csv o XML
        //    //wpf1.saveResultQueryToCSVFileOrString(results, "C:/Users/esd91martent/Desktop/[Visual Studio Project]ConsoleAppSparql3/csvfile_test.csv");                                      
        //    wpf1.saveResultQueryToXMLFileOrString(results, pathDirectoryAndFileXML);
        //    //string strFilePath = "C:/Users/esd91martent/Desktop/[Visual Studio Project]ConsoleAppSparql3/csvfile_test.xml";
        //    //CONVERTE IL FILE Csv IN UN DATATABLE
        //    //DataTable dtCSV = ConvertCSVtoDataTable(pathDirectoryAndFileXML);
        //    //CONVERTE IL FILE XML IN UN DATATABLE
        //    DataTable dtXML = ConvertXMLtoDataTable(pathDirectoryAndFileXML);
        //    //SALVIAMO IL NOSTRO OGGETTO DATATABLE SU UNA TABELLA ACCESS IL Database E' DI TIPO Mdb
        //    DataTable dt2 = wpf1.TransferTableToAccess(filePath5,fileNameAccess, dtXML, nome_tabella);
        //    OFFSET = OFFSET + LIMIT2;
        //}
        //*************************************************************************************************************
        #endregion
    }//namespace ConsoleAppSparql3



}
