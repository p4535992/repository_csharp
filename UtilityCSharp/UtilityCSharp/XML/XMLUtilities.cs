﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using Common.Logging;

namespace UtilityCSharp.XML
{
    public class XmlUtilities
    {
        private static readonly ILog Log = LogManager.GetLogger<XmlUtilities>();
        /// <summary>
        /// Metodo per la rimozione dei namespace 
        /// </summary>
        /// <param name="xmlDocument"></param>
        /// <param name="uriXmlns"></param>
        /// <returns></returns>
        public static string RemoveAllNamespacesBruteForce(string xmlDocument,string uriXmlns)
        {
            //StreamReader sr = new StreamReader(xmlDocument);
            var xmlDocumentWithoutNs = xmlDocument.Replace(uriXmlns, "");
            return xmlDocumentWithoutNs;
        }
    
        //Implemented based on interface, not part of algorithm
        public static string RemoveAllNamespaces(string xmlDocument)
        {
            var xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

            return xmlDocumentWithoutNs.ToString();
        }//RemoveAllNamespaces

        //Core recursion function
        public static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (xmlDocument.HasElements)
            {
                //return new XElement(xmlDocument.Name.LocalName,
                //    xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
                return new XElement(xmlDocument.Name.LocalName,
                    xmlDocument.Elements().Select(RemoveAllNamespaces));
            }
            var xElement = new XElement(xmlDocument.Name.LocalName) { Value = xmlDocument.Value };

            foreach (var attribute in xmlDocument.Attributes())
                xElement.Add(attribute);

            return xElement;
        }//RemoveAllNamespaces

         public bool ValidateSchemaXml(XmlDocument xmlToValidate, string fileNameXsd, out string error)
        {
            //Validate XML
            bool bValidate;
            error = ""; 
            var eventHandler = new ValidationEventHandler(ValidationEventHandler);
            try
            {
                // the following call to Validate succeeds.
                xmlToValidate.Schemas.Add(null, Application.StartupPath + "\\" + fileNameXsd);   //path xsd                     
                xmlToValidate.Validate(eventHandler);
                bValidate = true;
            }
            catch (Exception ex)
            {
                bValidate = false;
                error = ex.Message;
            }

            return bValidate;
        }//ValidateSchemaXml

        private static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    //Console.WriteLine("error: {0}", e.Message);  
                    throw new XmlException("error Validation: " + e.Message);
                case XmlSeverityType.Warning:
                    //Console.WriteLine("Warning {0}", e.Message);
                    //throw new XmlException("Warning Validation: " + e.Message);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        } //ValidationEventHandler
    }
}
