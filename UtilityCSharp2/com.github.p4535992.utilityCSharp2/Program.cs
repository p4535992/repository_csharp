﻿using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityCSharp2.db;

namespace com.github.p4535992.utilityCSharp2
{
   
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger<Program>();

        static void Main(string[] args)
        {
            //LogTraceMessage();
            //LogDebugMessage();
            //LogInfoMessage();
            //LogWarningMessage();
            //LogErrorMessage();
            //LogFatalErrorMessage();
            //Console.ReadLine();
            Log.Info("Hello Common Logging");
            string file = "C:\\Users\\Marco\\Desktop\\geodocument_2015_09_18.sql";
            SqlUtilities.CleanSqlScript(file);
        }

        private static void LogTraceMessage()
        {
            Log.Trace("This is a trace...");
        }

        private static void LogDebugMessage()
        {
            Log.Debug("This is a debug message...");
        }

        private static void LogInfoMessage()
        {
            Log.Info("This is an info message...");
        }

        private static void LogWarningMessage()
        {
            Log.Warn("This is a warning...");
        }

        private static void LogErrorMessage()
        {
            Log.Error("This is an error...");
        }

        private static void LogFatalErrorMessage()
        {
            Log.Fatal("This is a fatal error...");
        }
    }
}
