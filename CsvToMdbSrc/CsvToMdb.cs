﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Runtime.InteropServices;
using ADODB;
using ADOX;
using JRO;
using LumenWorks.Framework.IO.Csv;

namespace CsvToMdb
{
  class CsvToMdb
  {
    private static string SqlEscape(string sql)
    {
        return sql?.Replace("'", "''");
    }

    class ColumnInfo
    {
      public readonly ADOX.DataTypeEnum ColumnType;
      public readonly int ColumnSize;
        private string ColumnName;

        public ColumnInfo(ADOX.DataTypeEnum columnType, int columnSize)
      {
        ColumnType = columnType;
        ColumnSize = columnSize;
      }

      public ColumnInfo(ADOX.DataTypeEnum columnType,string columnName)
      {
          ColumnType = columnType;
          ColumnName = columnName;
      }

      
    }

    private static Dictionary<string, ColumnInfo> ChooseColumnTypes(string csvFile)
    {
        Dictionary<string, ColumnInfo> columnTypes = new Dictionary<string, ColumnInfo>();

        // first populate in order of CSV columns
        using (StreamReader reader = new StreamReader(csvFile))
        {
            // populate
            //create column
            using (CsvReader csvReader = new CsvReader(reader, true))
            {
                csvReader.ReadNextRecord();
                int column = 0;
                while (column < csvReader.FieldCount)
                {
                    foreach (string field in csvReader.FieldHeaders.Keys)
                    {
                        if (field.Length > 10)
                        {
                            string sfield = field.Substring(0, 10).Replace(" ", "_");
                            if ((int)csvReader.FieldHeaders[field] == column)
                            {
                                // assume everything is an integer initially
                                columnTypes[sfield] = new ColumnInfo(ADOX.DataTypeEnum.adInteger, 0);
                                column++;
                            }

                        }
                        else {
                            if ((int)csvReader.FieldHeaders[field] == column)
                            {
                                // assume everything is an integer initially
                                columnTypes[field] = new ColumnInfo(ADOX.DataTypeEnum.adInteger, 0);
                                column++;
                            }
                        }
                       
                    }
                }
            }
        }

        using (StreamReader reader = new StreamReader(csvFile))
        {
            // populate
            //try to assign a type to the column
            using (CsvReader csvReader = new CsvReader(reader, true))
            {
                while (csvReader.ReadNextRecord())
                {
                    foreach (string field in csvReader.FieldHeaders.Keys)
                    {

                        if (field.Length > 10)
                        {
                            string sfield = field.Substring(0, 10).Replace(" ", "_");

                            if (!string.IsNullOrEmpty(csvReader[field]))
                            {
                                int number;
                                if (!int.TryParse(csvReader[field], out number))
                                {
                                    double floatingNumber;
                                    if (double.TryParse(csvReader[field], out floatingNumber))
                                    {
                                        if (columnTypes[sfield].ColumnType == ADOX.DataTypeEnum.adInteger)
                                            columnTypes[sfield] = new ColumnInfo(ADOX.DataTypeEnum.adDouble, 0);
                                    }
                                    else
                                    {
                                        DateTime date;
                                        if (DateTime.TryParse(csvReader[field], out date))
                                        {
                                            if (columnTypes[sfield].ColumnType == ADOX.DataTypeEnum.adInteger)
                                                columnTypes[sfield] = new ColumnInfo(ADOX.DataTypeEnum.adDate, 0);
                                        }
                                        else
                                            columnTypes[sfield] = new ColumnInfo(ADOX.DataTypeEnum.adVarWChar,
                                              Math.Max(columnTypes[sfield].ColumnSize, 10));
                                    }
                                }
                            }//if
                        }
                        else {
                            if (!string.IsNullOrEmpty(csvReader[field]))
                            {
                                int number;
                                if (!int.TryParse(csvReader[field], out number))
                                {
                                    double floatingNumber;
                                    if (double.TryParse(csvReader[field], out floatingNumber))
                                    {
                                        if (columnTypes[field].ColumnType == ADOX.DataTypeEnum.adInteger)
                                            columnTypes[field] = new ColumnInfo(ADOX.DataTypeEnum.adDouble, 0);
                                    }
                                    else
                                    {
                                        DateTime date;
                                        if (DateTime.TryParse(csvReader[field], out date))
                                        {
                                            if (columnTypes[field].ColumnType == ADOX.DataTypeEnum.adInteger)
                                                columnTypes[field] = new ColumnInfo(ADOX.DataTypeEnum.adDate, 0);
                                        }
                                        else
                                            columnTypes[field] = new ColumnInfo(ADOX.DataTypeEnum.adVarWChar,
                                              Math.Max(columnTypes[field].ColumnSize, csvReader[field].Length));
                                    }
                                }
                            }//if

                        }//else                                            
                    }
                }
            }
        }

        return columnTypes;
    }


    private static Dictionary<string, ColumnInfo> ChooseDefaultColumnTypes(string csvFile)
    {
        Dictionary<string, ColumnInfo> columnTypes = new Dictionary<string, ColumnInfo>();
        try
        {          
            var fields = new List<string>();
            // first populate in order of CSV columns
            using (StreamReader reader = new StreamReader(csvFile))
            {
                // populate
                //create column
                using (CsvReader csvReader = new CsvReader(reader, true))
                {
                    //conta il numero di colonne
                    csvReader.ReadNextRecord();
                    int columns = csvReader.FieldCount;
                    int column = 0;

                    while (column < columns)
                    {
                        //foreach (string field in csvReader.FieldHeaders.Keys)
                        //{
                        string field = "field" + column;
                        fields.Add(field);
                        // assume everything is an integer initially
                        //columnTypes[field] = new ColumnInfo(ADOX.DataTypeEnum.adInteger, 0);
                        columnTypes[field] = new ColumnInfo(ADOX.DataTypeEnum.adVarWChar, field);
                        column++;
                        //}    
                    }
                }
            }

            using (StreamReader reader = new StreamReader(csvFile))
            {
                // populate
                //try to assign a type to the column
                using (CsvReader csvReader = new CsvReader(reader, true))
                {
                    while (csvReader.ReadNextRecord())
                    {
                        int i =0;
                        foreach (string field in csvReader.FieldHeaders.Keys)
                        {
                            string f = "field" + i;                 
                            if (!string.IsNullOrEmpty(csvReader[field]))
                            //if (!string.IsNullOrEmpty(csvReader[field]))
                            {
                                int number;
                                if (!int.TryParse(csvReader[field], out number))
                                //if (int.TryParse(csvReader[field], out number))
                                {
                                    double floatingNumber;
                                    if (double.TryParse(csvReader[field], out floatingNumber))
                                    {
                                        //if (columnTypes[field].ColumnType == ADOX.DataTypeEnum.adVarWChar)
                                        columnTypes[f] = new ColumnInfo(ADOX.DataTypeEnum.adDouble, f);
                                    }
                                    else
                                    {
                                        DateTime date;
                                        if (DateTime.TryParse(csvReader[field], out date))
                                        {
                                            //if (columnTypes[field].ColumnType == ADOX.DataTypeEnum.adVarWChar)
                                            columnTypes[f] = new ColumnInfo(ADOX.DataTypeEnum.adDate, f);
                                        }
                                        else
                                        {

                                            //int key;
                                            //int.TryParse(columnTypes["field0"].ColumnName.Replace("field", ""), out key);
                                           
                                            //columnTypes[field] = new ColumnInfo(
                                            //    ADOX.DataTypeEnum.adVarWChar,
                                            //    Math.Max(key,
                                            //    csvReader[field].Length));
                                            columnTypes[f] = new ColumnInfo(ADOX.DataTypeEnum.adVarWChar,f);
                                        }
                                    }
                                }//if is a integer
                                else
                                {
                                    columnTypes[f] = new ColumnInfo(ADOX.DataTypeEnum.adInteger,f); 
                                }

                            }
                           
                            i++;
                        }
                    }
                }
            }
        }
        catch (Exception)
        {
            // ignored
        }

        return columnTypes;
    }

    static string GetConnectionSring(string filename)
    {
      return $"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={filename};Jet OLEDB:Engine Type=5";
    }

    static void Main(string[] args)
    {
        //args = new[] { "ANAG_CODGEST_USCITE.csv", "true" };
        bool firstRow;
        Boolean.TryParse(args[1],out firstRow);
        if (args[2] == null) args[2] = "";
        Execute(args[0], firstRow, args[2]);
    }

    public static void Execute(string csvFile, bool firstRow,string nameKeyColumn)
    {
      try
      {
          //TEST
          //args = new string[] { "ANAG_CODGEST_USCITE.csv","true"};     
        //if (args.Length < 1)
        //{
        //  // show usage
        //  Console.WriteLine("CSV to MDB application");
        //  Console.WriteLine("Usage - CsvToMdb.exe CSVfile [primary key]");
        //  Console.WriteLine("");
        //  Console.WriteLine("Press any key");
        //  Console.ReadKey();
        //}
        
          // read command line
          //string csvFile = args[0];
          //bool firstRow;
          //Boolean.TryParse(hasHeader,out firstRow);

          string fileNameWithPath = Path.ChangeExtension(csvFile, "mdb");

          if (File.Exists(fileNameWithPath))
            File.Delete(fileNameWithPath);

          // create the database
          Console.WriteLine("Creating the database");
          Catalog cat = new Catalog();
          string connstr = GetConnectionSring(fileNameWithPath);
          cat.Create(connstr);

          Console.WriteLine("Choosing column types");

          Dictionary<string, ColumnInfo> columnTypes;

          Table table = new Table();
          string tableName = Path.GetFileNameWithoutExtension(csvFile);
            //if the frist row is column
          if (firstRow)
          {
              columnTypes = ChooseColumnTypes(csvFile);
              // create the table
              Console.WriteLine("Creating the table");

              table.Name = tableName;
              foreach (string field in columnTypes.Keys)
              {
                  string sField;
                  int number;
                  DateTime dateTime;              
                  if (int.TryParse(field, out number))
                  {
                      table.Columns.Append(field, columnTypes[field].ColumnType, columnTypes[field].ColumnSize);
                  }
                  else if(DateTime.TryParse(field,out dateTime))
                  {
                        table.Columns.Append(field, columnTypes[field].ColumnType, columnTypes[field].ColumnSize);
                  }
                  else
                  {
                      if (field.Length > 10)
                      {
                          sField = field.Substring(0, 10).Replace(" ", "_");
                          table.Columns.Append(sField, columnTypes[field].ColumnType, 10);
                      }
                      else
                      {
                          table.Columns.Append(field, columnTypes[field].ColumnType, columnTypes[field].ColumnSize);
                      }
                    
                  }   

                  //table.Columns.Append(field, columnTypes[field].ColumnType, columnTypes[field].ColumnSize);
                  //i++;                                     
              }
          }
              //...if the first row is not a column
          else {
              columnTypes = ChooseDefaultColumnTypes(csvFile);
              // create the table
              Console.WriteLine("Creating the table");

              table.Name = tableName;
              //int i = 0;
              foreach (string field in columnTypes.Keys)
              {
                  table.Columns.Append(field, columnTypes[field].ColumnType, columnTypes[field].ColumnSize);
                  //table.Columns.Append("field"+i, columnTypes[field].ColumnType, columnTypes[field].ColumnSize);
                  //i++;
              }
          }

          // ...make every column nullable
          foreach (Column column in table.Columns)
          {
            column.Attributes = ColumnAttributesEnum.adColNullable;
          }
    
          // ...set the primary key
          if (nameKeyColumn.Length > 1)
          {
            table.Keys.Append("primaryKey", KeyTypeEnum.adKeyPrimary, nameKeyColumn);
          }

          cat.Tables.Append(table);
          Connection con = cat.ActiveConnection as Connection;
          con?.Close();
          Marshal.ReleaseComObject(cat);

          Console.WriteLine("Populating the table");
          int cursor = Console.CursorTop;
          int processed = 1;
          //using (StreamReader reader = new StreamReader(csvFile))
          //{
          //    string line = reader.ReadLine();
          //  // populate
          //  using (CsvReader csvReader = new CsvReader(reader, true))
          //  {
          //
          bool flag = true;
          using (CsvReader csvReader =  new CsvReader(new StreamReader(csvFile), true)){      
              OleDbConnection conn = new OleDbConnection(connstr);
              conn.Open();
    
              while (csvReader.ReadNextRecord())
              {
                using (OleDbCommand command = conn.CreateCommand())
                {
                  string commandText = "INSERT INTO [" + tableName + "] (";

                  // add field names
                  int count = 0;
                  //add column
                  if (firstRow)
                  {
                      foreach (string field in csvReader.FieldHeaders.Keys)
                      {
                          commandText += "[" + field + "]";                         
                          count++;                       
                          if (count < csvReader.FieldCount)
                              commandText += ", ";
                      }
                     
                  }
                  else
                  {                    
                      for(int j=0; j < table.Columns.Count; j++)
                      {
                          //string record = csvReader[field];
                          commandText += "[field" + j + "]";
                          count++;
                          if (count < csvReader.FieldCount)
                              commandText += ", ";
                      }                
                  }
                  commandText += ") SELECT ";
                  // add 
                  count = 0;
                  if (firstRow)
                  {
                      foreach (string field in csvReader.FieldHeaders.Keys)
                      {
                          if (string.IsNullOrEmpty(SqlEscape(csvReader[field])))
                              commandText += "NULL";
                          else
                              commandText += "'" + SqlEscape(csvReader[field]) + "'";
                          count++;
                          if (count < csvReader.FieldCount)
                              commandText += ", ";
                      }
                  }
                  else {
                     
                          if (flag)
                          {
                              foreach (string field in csvReader.FieldHeaders.Keys)
                              {
                                  if (string.IsNullOrEmpty(field))
                                      commandText += "NULL";
                                  else
                                      commandText += "'" + field + "'";
                                  count++;
                                  if (count < csvReader.FieldCount)
                                      commandText += ", ";
                               }
                                command.CommandText = commandText;
                                Console.WriteLine("Processing record " + processed);
                                Console.SetCursorPosition(0, cursor);
                                command.ExecuteNonQuery();
                                //command.Dispose();
                                //...fucking second line field
                                count = 0;
                                commandText = "INSERT INTO [" + tableName + "] (";
                                for (int j = 0; j < table.Columns.Count; j++)
                                {
                                    //string record = csvReader[field];
                                    commandText += "[field" + j + "]";
                                    count++;
                                    if (count < csvReader.FieldCount)
                                        commandText += ", ";
                                }
                                count = 0;
                                commandText += ") SELECT ";
                                foreach (string field in csvReader.FieldHeaders.Keys)
                                {
                                    if (string.IsNullOrEmpty(SqlEscape(csvReader[field])))
                                        commandText += "NULL";
                                    else
                                        commandText += "'" + SqlEscape(csvReader[field]) + "'";
                                    count++;
                                    if (count < csvReader.FieldCount)
                                        commandText += ", ";
                                      
                                }
                          }
                          else
                          {
                              foreach (string field in csvReader.FieldHeaders.Keys)
                              {
                                  if (string.IsNullOrEmpty(SqlEscape(csvReader[field])))
                                      commandText += "NULL";
                                  else
                                      commandText += "'" + SqlEscape(csvReader[field]) + "'";
                                  count++;
                                  if (count < csvReader.FieldCount)
                                      commandText += ", ";
                              }
                          }
                      
                      flag = false;
                  }
                  command.CommandText = commandText;

                  Console.WriteLine("Processing record " + processed);
                  Console.SetCursorPosition(0, cursor);
                  command.ExecuteNonQuery();
                }

                processed++;

                // compress the database since it can get large very quickly
                if (processed % 100000 == 0)
                {
                  conn.Close();
                  conn.Dispose();

                  JetEngine jro = new JetEngine();
                  string backupFileName = Path.Combine(Path.GetDirectoryName(fileNameWithPath), "backup.mdb");
                  string backupFile = GetConnectionSring(backupFileName);
                  jro.CompactDatabase(connstr, backupFile);
                  File.Delete(fileNameWithPath);
                  File.Move(backupFileName, fileNameWithPath);

                  conn = new OleDbConnection(connstr);
                  conn.Open();
                }
              }
            }//csv reader
      }//main try
      //  }
      //}
      catch (Exception ex)
      {
        Console.WriteLine("An error occured - " + ex.Message);
        Console.WriteLine("Press any key");
        Console.ReadKey();
      }
    }
  }
}
